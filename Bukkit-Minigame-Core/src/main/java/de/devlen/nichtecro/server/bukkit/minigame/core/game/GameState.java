package de.devlen.nichtecro.server.bukkit.minigame.core.game;

/**
 * Created by Tarik on 28.05.2016.
 */
public enum GameState {

    WAITING,
    RUNNING,
    ENDING,
    RESTARTING,
    MAINTENANCE;

}
