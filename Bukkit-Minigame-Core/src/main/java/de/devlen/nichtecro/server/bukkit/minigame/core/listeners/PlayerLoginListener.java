package de.devlen.nichtecro.server.bukkit.minigame.core.listeners;

import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.Game;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerLoginEvent;

import static de.devlen.nichtecro.server.bukkitcore.language.Language.tl;

/**
 * Created by Tarik on 29.05.2016.
 */
public class PlayerLoginListener extends ListenerHelper {

    @EventHandler(ignoreCancelled = true)
    public void onPlayerLogin(PlayerLoginEvent e) {
        Player player = e.getPlayer();
        switch (Game.getInstance().getGameState()) {
            case ENDING:
                e.disallow(PlayerLoginEvent.Result.KICK_OTHER, Chat.replaceFormatVariables("%u" + tl("bukkit.minigame.core.login.ending", player)));
                break;
            case MAINTENANCE:
                e.disallow(PlayerLoginEvent.Result.KICK_OTHER, Chat.replaceFormatVariables("%u" + tl("bukkit.minigame.core.login.maintenance", player)));
                break;
            case RESTARTING:
                e.disallow(PlayerLoginEvent.Result.KICK_OTHER, Chat.replaceFormatVariables("%u" + tl("bukkit.minigame.core.login.restarting", player)));
                break;
            case RUNNING:
                e.disallow(PlayerLoginEvent.Result.KICK_OTHER, Chat.replaceFormatVariables("%u" + tl("bukkit.minigame.core.login.running", player)));
                break;
            case WAITING:

                e.allow();
                break;
        }
    }

}
