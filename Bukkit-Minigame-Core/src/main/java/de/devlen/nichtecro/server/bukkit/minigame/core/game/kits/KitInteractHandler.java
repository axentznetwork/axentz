package de.devlen.nichtecro.server.bukkit.minigame.core.game.kits;

import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Created by Tarik on 29.05.2016.
 */
public interface KitInteractHandler {

    void handle(PlayerInteractEvent e);

}
