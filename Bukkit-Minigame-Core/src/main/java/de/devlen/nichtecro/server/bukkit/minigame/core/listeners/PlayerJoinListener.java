package de.devlen.nichtecro.server.bukkit.minigame.core.listeners;

import de.devlen.nichtecro.server.bukkit.minigame.core.game.Game;
import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager;
import de.devlen.nichtecro.server.bukkitcore.manager.Rank;
import de.devlen.nichtecro.server.bukkitcore.message.CScoreboard;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;

import static de.devlen.nichtecro.server.bukkitcore.language.Language.tl;

/**
 * Created by Tarik on 28.05.2016.
 */
public class PlayerJoinListener extends ListenerHelper {

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        Game.getInstance().getGameTaskManager().startLobby();
        for (Player target : Bukkit.getOnlinePlayers()) {
            Chat.send(target, tl(Game.getInstance().getGameNameNode(), target), tl("bukkit.minigame.core.join.message", target, Rank.getHighestRank(MPlayerSettingsManager.getRanks(player)).getChatPrefixTranslatable(target), player.getName()));
        }
        player.teleport(Game.getInstance().getLobbySpawnLocation());

        if (Bukkit.getOnlinePlayers().size() > 1 && (Bukkit.getOnlinePlayers().size() / Game.getInstance().getMaximumPlayerAmount() >= 0.5)) {
            Game.getInstance().getGameTaskManager().speedUpLobbyTime();
        }

    }

}
