package de.devlen.nichtecro.server.bukkit.minigame.core.listeners;

import de.devlen.nichtecro.server.bukkit.minigame.core.game.Game;
import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerPickupItemEvent;

/**
 * Created by Tarik on 31.05.2016.
 */
public class PlayerPickupItemListener extends ListenerHelper {

    @EventHandler
    public void onPlayerPickupItem(PlayerPickupItemEvent e) {
        if(Game.getInstance().isItemPickupAllowed()){
            if(Game.getInstance().getDisallowedItemPickups().contains(e.getItem().getItemStack())){
                e.setCancelled(true);
            }
        }else{
            if(!Game.getInstance().getAllowedItemDrops().contains(e.getItem().getItemStack())){
                e.setCancelled(true);
            }
        }
    }

}
