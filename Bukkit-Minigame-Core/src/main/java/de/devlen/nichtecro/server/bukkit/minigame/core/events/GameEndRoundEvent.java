package de.devlen.nichtecro.server.bukkit.minigame.core.events;

import de.devlen.nichtecro.server.bukkitcore.events.EventHelper;

/**
 * Created by Tarik on 29.05.2016.
 */
public class GameEndRoundEvent extends EventHelper {

    private int roundNumber = 0;

    public GameEndRoundEvent(int roundNumber) {
        this.roundNumber = roundNumber;
    }

    public int getRoundNumber() {
        return roundNumber;
    }
}
