package de.devlen.nichtecro.server.bukkit.minigame.core.game.teams;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Tarik on 01.06.2016.
 */
public class GameTeam {

    private String teamName = "";
    private String prefixNode = "";
    private ChatColor chatColor = ChatColor.WHITE;
    private ItemStack itemStack = new ItemStack(Material.WOOL);
    private Location spawnLocation = null;

}
