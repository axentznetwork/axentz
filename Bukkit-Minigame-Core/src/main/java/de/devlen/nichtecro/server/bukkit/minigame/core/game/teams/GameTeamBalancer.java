package de.devlen.nichtecro.server.bukkit.minigame.core.game.teams;

import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by Tarik on 01.06.2016.
 */
public class GameTeamBalancer {

    private GameTeamBalancer instance = null;
    private List<GameTeam> registeredGameTeams = new ExtendedList<>();

    public GameTeamBalancer getInstance() {
        if (instance == null) {
            instance = new GameTeamBalancer();
        }
        return instance;
    }

    public void registerTeam(GameTeam gameTeam) {

    }

    public void addPlayerToRandomTeam(Player player) {

    }
}
