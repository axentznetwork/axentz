package de.devlen.nichtecro.server.bukkit.minigame.core.game.kits;

import de.devlen.nichtecro.server.bukkitcore.items.StackFactory;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Tarik on 29.05.2016.
 */
public class KitItem {

    private int id = 0;
    private ItemStack itemStack;
    private KitInteractHandler kitInteractHandler;
    private KitInventoryClickHandler kitInventoryClickHandler;

    private KitItem id(int id) {
        this.id = id;
        return this;
    }

    private KitItem item(StackFactory stackFactory) {
        return item(stackFactory.build());
    }

    private KitItem item(ItemStack itemStack) {
        this.itemStack = itemStack;
        return this;
    }

    private KitItem interact(KitInteractHandler kitInteractHandler) {
        this.kitInteractHandler = kitInteractHandler;
        return this;
    }

    private KitItem click(KitInventoryClickHandler kitInventoryClickHandler) {
        this.kitInventoryClickHandler = kitInventoryClickHandler;
        return this;
    }

    public int getId() {
        return id;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public KitInteractHandler getKitInteractHandler() {
        return kitInteractHandler;
    }

    public KitInventoryClickHandler getKitInventoryClickHandler() {
        return kitInventoryClickHandler;
    }
}
