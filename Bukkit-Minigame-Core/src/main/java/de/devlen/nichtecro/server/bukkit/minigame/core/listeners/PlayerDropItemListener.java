package de.devlen.nichtecro.server.bukkit.minigame.core.listeners;

import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.Game;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerDropItemEvent;

/**
 * Created by Tarik on 31.05.2016.
 */
public class PlayerDropItemListener extends ListenerHelper {

    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent e) {
        if(Game.getInstance().isItemDroppingAllowed()){
            if(Game.getInstance().getDisallowedItemDrops().contains(e.getItemDrop().getItemStack())){
                e.setCancelled(true);
            }
        }else{
            if(!Game.getInstance().getAllowedItemDrops().contains(e.getItemDrop().getItemStack())){
                e.setCancelled(true);
            }
        }
    }

}
