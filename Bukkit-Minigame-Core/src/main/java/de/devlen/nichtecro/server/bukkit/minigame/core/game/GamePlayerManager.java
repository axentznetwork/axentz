package de.devlen.nichtecro.server.bukkit.minigame.core.game;

import com.mongodb.BasicDBObject;
import de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager;
import de.devlen.nichtecro.server.bukkitcore.player.UUIDControl;
import org.bson.Document;
import org.bukkit.entity.Player;

import java.util.UUID;

import static com.mongodb.client.model.Filters.eq;

/**
 * Created by Tarik on 28.05.2016.
 */
public class GamePlayerManager extends MPlayerSettingsManager {

    private static String collectionNamePlayers = getCollectionPrefix() + "players";

    public static void setGameValue(Player player, String gameName, String key, Object value) {
        setGameValue(UUIDControl.getUUID(player), gameName, key, value);
    }

    public static void setGameValue(String name, String gameName, String key, Object value) {
        setGameValue(UUIDControl.getUUID(name), gameName, key, value);
    }

    public static void setGameValue(UUID uuid, String gameName, String key, Object value) {
        BasicDBObject update = new BasicDBObject("$set", new BasicDBObject("games." + gameName + "." + key, value));
        update(collectionNamePlayers, eq("uuid", uuid.toString()), update);
    }

    public static int getGameValueInteger(Player player, String gameName, String key) {
        return getGameValueInteger(UUIDControl.getUUID(player), gameName, key);
    }

    public static int getGameValueInteger(String name, String gameName, String key) {
        return getGameValueInteger(UUIDControl.getUUID(name), gameName, key);
    }

    public static int getGameValueInteger(UUID uuid, String gameName, String key) {
        Document document = getFirstDocument(collectionNamePlayers, eq("uuid", uuid.toString()));
        try {
            Document gameDocument = (Document) document.get("games");
            Document gameNameDocument = (Document) gameDocument.get(gameName);
            Integer value = gameNameDocument.getInteger(key);
            if (value != null) {
                return value;
            }
            return 0;
        } catch (Exception ex) {
            return 0;
        }
    }

    public static String getGameValueString(UUID uuid, String gameName, String key) {
        Document document = getFirstDocument(collectionNamePlayers, eq("uuid", uuid.toString()));
        try {
            Document gameDocument = (Document) document.get("games");
            Document gameNameDocument = (Document) gameDocument.get(gameName);
            String value = gameNameDocument.getString(key);
            if (value != null) {
                return value;
            }
            return "";
        } catch (Exception ex) {
            return "";
        }
    }

}
