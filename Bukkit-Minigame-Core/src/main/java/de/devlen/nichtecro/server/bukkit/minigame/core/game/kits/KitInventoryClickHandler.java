package de.devlen.nichtecro.server.bukkit.minigame.core.game.kits;

import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * Created by Tarik on 29.05.2016.
 */
public interface KitInventoryClickHandler {

    void handle(InventoryClickEvent e);

}
