package de.devlen.nichtecro.server.bukkit.minigame.core.events;

import de.devlen.nichtecro.server.bukkitcore.events.EventHelper;

/**
 * Created by Tarik on 29.05.2016.
 */
public class GameNewRoundEvent extends EventHelper {

    private int rounds = 0;

    public GameNewRoundEvent(int rounds) {
        this.rounds = rounds;
    }

    public int getRounds() {
        return rounds;
    }
}
