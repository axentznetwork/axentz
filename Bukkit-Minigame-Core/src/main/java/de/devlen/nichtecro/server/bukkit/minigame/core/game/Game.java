package de.devlen.nichtecro.server.bukkit.minigame.core.game;

import de.devlen.nichtecro.server.bukkit.minigame.core.events.GameRoundAmountChangedEvent;
import de.devlen.nichtecro.server.bukkit.minigame.core.events.GameStateChangeEvent;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.maps.BlockDescriptor;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.taskmanager.GameRoundTaskManager;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.taskmanager.GameTaskManager;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.taskmanager.IGameTaskManager;
import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.UUID;

import static de.devlen.nichtecro.server.bukkitcore.language.Language.tl;

/**
 * Created by Tarik on 28.05.2016.
 */
public class Game {

    private String gameNameNode = "";
    private static Game instance = null;
    private GameState gameState = GameState.WAITING;
    private IGameTaskManager gameTaskManager = null;
    private int minimumPlayerAmount = 2;
    private int maximumPlayerAmount = 20;
    private int fullLobbyTime = 0;
    private int shrinkedLobbyTime = 0;
    private int fullGameTime = 0;
    private int shrinkedGameTime = 0;
    private int rounds = 0;
    private int respawnTime = 0;
    private boolean teamGame = false;
    private boolean roundBased = false;
    private boolean blockBreakingAllowed = false;
    private boolean blockPlacingAllowed = false;
    private boolean itemDroppingAllowed = false;
    private boolean itemPickupAllowed = false;
    private boolean tntExplosionAllowed = false;
    private boolean creeperExplosionAllowed = false;
    private boolean pvpAllowed = false;
    private boolean playerRespawnAllowed = false;
    private boolean playerDamageAllowed = false;
    private boolean entityDamageAllowed = false;
    private boolean entitySpawnAllowed = false;
    private boolean hungerAllowed = false;
    private boolean multipleGameSpawnsEnabled = false;
    private List<BlockDescriptor> allowedBreakingBlocks = new ExtendedList<>();
    private List<BlockDescriptor> disallowedBreakingBlocks = new ExtendedList<>();
    private List<BlockDescriptor> allowedPlacingBlocks = new ExtendedList<>();
    private List<BlockDescriptor> disallowedPlacingBlocks = new ExtendedList<>();
    private List<ItemStack> allowedItemDrops = new ExtendedList<>();
    private List<ItemStack> disallowedItemDrops = new ExtendedList<>();
    private List<ItemStack> allowedItemPickups = new ExtendedList<>();
    private List<ItemStack> disallowedItemPickups = new ExtendedList<>();
    private List<EntityType> whitelistedEntitySpawnTypes = new ExtendedList<>();
    private List<EntityType> blacklistedEntitySpawnTypes = new ExtendedList<>();
    private List<UUID> blockedRespawnPlayers = new ExtendedList<>();
    private List<UUID> spectators = new ExtendedList<>();
    private List<Location> gameSpawns = new ExtendedList<>();
    private Location lobbySpawnLocation = null;
    private Location evaluationSpawnLocation = null;

    public Game() {
        instance = this;
    }

    public String getGameNameNode() {
        return gameNameNode;
    }

    public void setGameNameNode(String gameNameNode) {
        this.gameNameNode = gameNameNode;
    }

    public static Game getInstance() {
        if (instance == null) {
            instance = new Game();
        }
        return instance;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        new GameStateChangeEvent(this.gameState, gameState).call();
        this.gameState = gameState;
    }

    public int getMinimumPlayerAmount() {
        return minimumPlayerAmount;
    }

    public void setMinimumPlayerAmount(int minimumPlayerAmount) {
        this.minimumPlayerAmount = minimumPlayerAmount;
    }

    public int getMaximumPlayerAmount() {
        return maximumPlayerAmount;
    }

    public void setMaximumPlayerAmount(int maximumPlayerAmount) {
        this.maximumPlayerAmount = maximumPlayerAmount;
    }

    public int getFullLobbyTime() {
        return fullLobbyTime;
    }

    public void setFullLobbyTime(int fullLobbyTime) {
        this.fullLobbyTime = fullLobbyTime;
    }

    public int getFullGameTime() {
        return fullGameTime;
    }

    public void setFullGameTime(int fullGameTime) {
        this.fullGameTime = fullGameTime;
    }

    public int getRounds() {
        return rounds;
    }

    public void setRounds(int rounds) {
        new GameRoundAmountChangedEvent(this.rounds, rounds).call();
        this.rounds = rounds;
    }

    public boolean isTeamGame() {
        return teamGame;
    }

    public void setTeamGame(boolean teamGame) {
        this.teamGame = teamGame;
    }

    public boolean isRoundBased() {
        return roundBased;
    }

    public void setRoundBased(boolean roundBased) {
        this.roundBased = roundBased;
        if (roundBased) {
            gameTaskManager = new GameRoundTaskManager();
        } else {
            gameTaskManager = new GameTaskManager();
        }
    }

    public boolean isBlockBreakingAllowed() {
        return blockBreakingAllowed;
    }

    public void setBlockBreakingAllowed(boolean blockBreakingAllowed) {
        this.blockBreakingAllowed = blockBreakingAllowed;
    }

    public boolean isItemDroppingAllowed() {
        return itemDroppingAllowed;
    }

    public void setItemDroppingAllowed(boolean itemDroppingAllowed) {
        this.itemDroppingAllowed = itemDroppingAllowed;
    }

    public boolean isItemPickupAllowed() {
        return itemPickupAllowed;
    }

    public void setItemPickupAllowed(boolean itemPickupAllowed) {
        this.itemPickupAllowed = itemPickupAllowed;
    }

    public boolean isTntExplosionAllowed() {
        return tntExplosionAllowed;
    }

    public void setTntExplosionAllowed(boolean tntExplosionAllowed) {
        this.tntExplosionAllowed = tntExplosionAllowed;
    }

    public boolean isCreeperExplosionAllowed() {
        return creeperExplosionAllowed;
    }

    public void setCreeperExplosionAllowed(boolean creeperExplosionAllowed) {
        this.creeperExplosionAllowed = creeperExplosionAllowed;
    }

    public List<BlockDescriptor> getAllowedBreakingBlocks() {
        return allowedBreakingBlocks;
    }

    public void setAllowedBreakingBlocks(List<BlockDescriptor> allowedBreakingBlocks) {
        this.allowedBreakingBlocks = allowedBreakingBlocks;
    }

    public List<BlockDescriptor> getDisallowedBreakingBlocks() {
        return disallowedBreakingBlocks;
    }

    public void setDisallowedBreakingBlocks(List<BlockDescriptor> disallowedBreakingBlocks) {
        this.disallowedBreakingBlocks = disallowedBreakingBlocks;
    }

    public List<ItemStack> getAllowedItemDrops() {
        return allowedItemDrops;
    }

    public void setAllowedItemDrops(List<ItemStack> allowedItemDrops) {
        this.allowedItemDrops = allowedItemDrops;
    }

    public List<ItemStack> getDisallowedItemDrops() {
        return disallowedItemDrops;
    }

    public void setDisallowedItemDrops(List<ItemStack> disallowedItemDrops) {
        this.disallowedItemDrops = disallowedItemDrops;
    }

    public List<ItemStack> getAllowedItemPickups() {
        return allowedItemPickups;
    }

    public void setAllowedItemPickups(List<ItemStack> allowedItemPickups) {
        this.allowedItemPickups = allowedItemPickups;
    }

    public List<ItemStack> getDisallowedItemPickups() {
        return disallowedItemPickups;
    }

    public void setDisallowedItemPickups(List<ItemStack> disallowedItemPickups) {
        this.disallowedItemPickups = disallowedItemPickups;
    }

    public int getShrinkedLobbyTime() {
        return shrinkedLobbyTime;
    }

    public void setShrinkedLobbyTime(int shrinkedLobbyTime) {
        this.shrinkedLobbyTime = shrinkedLobbyTime;
    }

    public int getShrinkedGameTime() {
        return shrinkedGameTime;
    }

    public void setShrinkedGameTime(int shrinkedGameTime) {
        this.shrinkedGameTime = shrinkedGameTime;
    }

    public boolean isPvpAllowed() {
        return pvpAllowed;
    }

    public void setPvpAllowed(boolean pvpAllowed) {
        this.pvpAllowed = pvpAllowed;
    }

    public boolean isHungerAllowed() {
        return hungerAllowed;
    }

    public void setHungerAllowed(boolean hungerAllowed) {
        this.hungerAllowed = hungerAllowed;
    }

    public boolean isMultipleGameSpawnsEnabled() {
        return multipleGameSpawnsEnabled;
    }

    public void setMultipleGameSpawnsEnabled(boolean multipleGameSpawnsEnabled) {
        this.multipleGameSpawnsEnabled = multipleGameSpawnsEnabled;
    }

    public List<Location> getGameSpawns() {
        return gameSpawns;
    }

    public void setGameSpawns(List<Location> gameSpawns) {
        this.gameSpawns = gameSpawns;
    }

    public Location getLobbySpawnLocation() {
        return lobbySpawnLocation;
    }

    public void setLobbySpawnLocation(Location lobbySpawnLocation) {
        this.lobbySpawnLocation = lobbySpawnLocation;
    }

    public Location getEvaluationSpawnLocation() {
        return evaluationSpawnLocation;
    }

    public void setEvaluationSpawnLocation(Location evaluationSpawnLocation) {
        this.evaluationSpawnLocation = evaluationSpawnLocation;
    }

    public IGameTaskManager getGameTaskManager() {
        return gameTaskManager;
    }

    public void setGameTaskManager(IGameTaskManager gameTaskManager) {
        this.gameTaskManager = gameTaskManager;
    }

    public boolean isEntityDamageAllowed() {
        return entityDamageAllowed;
    }

    public void setEntityDamageAllowed(boolean entityDamageAllowed) {
        this.entityDamageAllowed = entityDamageAllowed;
    }

    public boolean isEntitySpawnAllowed() {
        return entitySpawnAllowed;
    }

    public void setEntitySpawnAllowed(boolean entitySpawnAllowed) {
        this.entitySpawnAllowed = entitySpawnAllowed;
    }

    public List<EntityType> getWhitelistedEntitySpawnTypes() {
        return whitelistedEntitySpawnTypes;
    }

    public void setWhitelistedEntitySpawnTypes(List<EntityType> whitelistedEntitySpawnTypes) {
        this.whitelistedEntitySpawnTypes = whitelistedEntitySpawnTypes;
    }

    public List<EntityType> getBlacklistedEntitySpawnTypes() {
        return blacklistedEntitySpawnTypes;
    }

    public void setBlacklistedEntitySpawnTypes(List<EntityType> blacklistedEntitySpawnTypes) {
        this.blacklistedEntitySpawnTypes = blacklistedEntitySpawnTypes;
    }

    public boolean isPlayerDamageAllowed() {
        return playerDamageAllowed;
    }

    public void setPlayerDamageAllowed(boolean playerDamageAllowed) {
        this.playerDamageAllowed = playerDamageAllowed;
    }

    public int getRespawnTime() {
        return respawnTime;
    }

    public void setRespawnTime(int respawnTime) {
        this.respawnTime = respawnTime;
    }

    public boolean isPlayerRespawnAllowed() {
        return playerRespawnAllowed;
    }

    public void setPlayerRespawnAllowed(boolean playerRespawnAllowed) {
        this.playerRespawnAllowed = playerRespawnAllowed;
    }

    public List<UUID> getBlockedRespawnPlayers() {
        return blockedRespawnPlayers;
    }

    public List<UUID> getSpectators() {
        return spectators;
    }

    public void addBlockedRespawnPlayers(Player... players) {
        List<UUID> adding = new ExtendedList<>();
        for (Player player : players) {
            adding.add(player.getUniqueId());
        }
        blockedRespawnPlayers.addAll(adding);
    }

    public void addSpectators(Player... players) {
        List<UUID> adding = new ExtendedList<>();
        for (Player player : players) {
            adding.add(player.getUniqueId());
        }
        spectators.addAll(adding);
    }

    public boolean isBlockPlacingAllowed() {
        return blockPlacingAllowed;
    }

    public void setBlockPlacingAllowed(boolean blockPlacingAllowed) {
        this.blockPlacingAllowed = blockPlacingAllowed;
    }

    public List<BlockDescriptor> getAllowedPlacingBlocks() {
        return allowedPlacingBlocks;
    }

    public void setAllowedPlacingBlocks(List<BlockDescriptor> allowedPlacingBlocks) {
        this.allowedPlacingBlocks = allowedPlacingBlocks;
    }

    public List<BlockDescriptor> getDisallowedPlacingBlocks() {
        return disallowedPlacingBlocks;
    }

    public void setDisallowedPlacingBlocks(List<BlockDescriptor> disallowedPlacingBlocks) {
        this.disallowedPlacingBlocks = disallowedPlacingBlocks;
    }

    public void announce(String messageNode, String... variables) {
        announce(null, messageNode, variables);
    }

    public void announce(Player player, String messageNode, String... variables) {
        for (Player target : Bukkit.getOnlinePlayers()) {
            if (player == null || (player != null && !player.getName().equalsIgnoreCase(target.getName()))) {
                Chat.send(target, tl(getGameNameNode(), target), tl(messageNode, target, variables));
            }
        }
    }

    public boolean checkStart() {
        switch (getGameState()) {
            case ENDING:
            case RESTARTING:
            case RUNNING:
            case MAINTENANCE:
                return false;
            case WAITING:
                return true;
            default:
                return false;
        }
    }


}
