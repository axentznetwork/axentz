package de.devlen.nichtecro.server.bukkit.minigame.core.game.taskmanager;

import de.devlen.nichtecro.server.bukkit.minigame.core.BukkitMinigameCore;
import de.devlen.nichtecro.server.bukkit.minigame.core.events.GameSpeedUpGameEvent;
import de.devlen.nichtecro.server.bukkit.minigame.core.events.GameSpeedUpLobbyEvent;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.Game;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.GameState;
import de.devlen.nichtecro.server.bukkitcore.bridge.BungeeCordBridge;
import de.devlen.nichtecro.server.bukkitcore.message.Title;
import de.devlen.nichtecro.server.bukkitcore.time.Delay;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by Tarik on 29.05.2016.
 */
public class GameTaskManager implements IGameTaskManager {

    private int lobbyTaskID = 0;
    private int roundTaskID = 0;
    private int countdownLobbyTime = Game.getInstance().getFullLobbyTime();
    private int countdownGameTime = Game.getInstance().getFullGameTime();
    private int playedRounds = 0;
    private boolean lobbyTaskRunning = false;
    private boolean gameTaskRunning = false;

    private void resetLobbyTime() {
        countdownLobbyTime = Game.getInstance().getFullLobbyTime();
    }

    public void resetGameTime() {
        countdownGameTime = Game.getInstance().getFullGameTime();
    }

    public void speedUpLobbyTime() {
        if (Game.getInstance().getGameState() == GameState.WAITING && countdownLobbyTime > Game.getInstance().getShrinkedLobbyTime()) {
            Game.getInstance().announce("bukkit.minigame.core.speedup.time.lobby", String.valueOf(Game.getInstance().getShrinkedLobbyTime()));
            countdownLobbyTime = Game.getInstance().getShrinkedLobbyTime();
            new GameSpeedUpLobbyEvent().call();
        }
    }

    public void speedUpGameTime() {
        if (Game.getInstance().getGameState() == GameState.RUNNING && countdownGameTime > Game.getInstance().getShrinkedGameTime()) {
            Game.getInstance().announce("bukkit.minigame.core.speedup.time.game", String.valueOf(Game.getInstance().getShrinkedGameTime()));
            countdownGameTime = Game.getInstance().getShrinkedGameTime();
            new GameSpeedUpGameEvent().call();
        }
    }

    public void startLobby() {
        if (Game.getInstance().getGameState() == GameState.WAITING && !lobbyTaskRunning && !gameTaskRunning) {
            lobbyTaskRunning = true;
            lobbyTaskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(BukkitMinigameCore.getInstance(), new Runnable() {
                @Override
                public void run() {
                    countdownLobby();
                }
            }, 0, 20L);
        }
    }

    private void countdownLobby() {
        if (countdownLobbyTime <= 0) {
            startGame();
        } else {
            countdownLobbyTime--;
            for (Player target : Bukkit.getOnlinePlayers()) {
                target.setLevel(countdownLobbyTime);
            }
            switch (countdownLobbyTime) {
                case 60:
                case 30:
                case 10:
                case 5:
                case 4:
                    Game.getInstance().announce("bukkit.minigame.core.countdown.lobby.plural", String.valueOf(countdownLobbyTime));
                    break;
                case 3:
                    Game.getInstance().announce("bukkit.minigame.core.countdown.lobby.plural", String.valueOf(countdownLobbyTime));
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        Title.send(target, "�a" + countdownLobbyTime, "", 4, 12, 4);
                    }
                    break;
                case 2:
                    Game.getInstance().announce("bukkit.minigame.core.countdown.lobby.plural", String.valueOf(countdownLobbyTime));
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        Title.send(target, "�e" + countdownLobbyTime, "", 4, 12, 4);
                    }
                    break;
                case 1:
                    Game.getInstance().announce("bukkit.minigame.core.countdown.lobby.singular");
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        Title.send(target, "�c" + countdownLobbyTime, "", 4, 12, 4);
                    }
                    break;
            }
        }
    }

    public void startGame() {
        startGame(false);
    }

    public void startGame(boolean force) {
        Bukkit.getScheduler().cancelTask(lobbyTaskID);
        resetLobbyTime();
        if (Game.getInstance().getGameState() == GameState.WAITING && !gameTaskRunning && lobbyTaskRunning) {
            lobbyTaskRunning = false;
            for (Player target : Bukkit.getOnlinePlayers()) {
                target.setLevel(0);
            }
            if (force || Bukkit.getOnlinePlayers().size() >= Game.getInstance().getMinimumPlayerAmount()) {
                gameTaskRunning = true;
                Game.getInstance().setGameState(GameState.RUNNING);
                countdownGame();
            } else {
                Game.getInstance().announce("bukkit.minigame.core.game.start.canceled.minplayers", String.valueOf(Game.getInstance().getMinimumPlayerAmount() - Bukkit.getOnlinePlayers().size()));
                startLobby();
            }
        }
    }

    private void countdownGame() {
        if (countdownGameTime <= 0) {
            Bukkit.getScheduler().cancelTask(roundTaskID);
            startEvaluation();
            resetGameTime();
        } else {
            countdownGameTime--;
            for (Player target : Bukkit.getOnlinePlayers()) {
                target.setLevel(countdownGameTime);
            }
            switch (countdownGameTime) {
                case 30:
                case 10:
                case 9:
                case 8:
                case 7:
                case 6:
                case 5:
                case 4:
                case 3:
                case 2:
                    Game.getInstance().announce("bukkit.minigame.core.countdown.game.plural", String.valueOf(countdownGameTime));
                    break;
                case 1:
                    Game.getInstance().announce("bukkit.minigame.core.countdown.game.singular");
                    break;
            }
        }
    }

    public void startEvaluation() {
        if (Game.getInstance().getGameState() == GameState.RUNNING && gameTaskRunning && !lobbyTaskRunning) {
            gameTaskRunning = false;
            Game.getInstance().setGameState(GameState.ENDING);
            Game.getInstance().announce("bukkit.minigame.core.ending.teleport.soon", String.valueOf(10));
            for (Player target : Bukkit.getOnlinePlayers()) {
                target.teleport(Game.getInstance().getEvaluationSpawnLocation());
            }
            Delay.make(10 * 20L, new Runnable() {
                @Override
                public void run() {
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        BungeeCordBridge.sendPlayerToServer(target, "lobby01");
                    }
                    lobbyTaskID = 0;
                    roundTaskID = 0;
                    countdownLobbyTime = Game.getInstance().getFullLobbyTime();
                    countdownGameTime = Game.getInstance().getFullGameTime();
                    playedRounds = 0;
                    Game.getInstance().setGameState(GameState.WAITING);
                }
            });
            //TODO complete evaluation
        }
    }
}
