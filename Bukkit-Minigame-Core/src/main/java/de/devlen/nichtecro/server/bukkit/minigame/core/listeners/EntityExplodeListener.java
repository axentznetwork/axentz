package de.devlen.nichtecro.server.bukkit.minigame.core.listeners;

import de.devlen.nichtecro.server.bukkit.minigame.core.game.Game;
import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityExplodeEvent;

/**
 * Created by Tarik on 31.05.2016.
 */
public class EntityExplodeListener extends ListenerHelper {

    @EventHandler
    public void onEntityExplode(EntityExplodeEvent e) {
        if (!Game.getInstance().isTntExplosionAllowed()) {
            if (e.getEntityType().equals(EntityType.PRIMED_TNT)) {
                e.setCancelled(true);
            }
        }

        if (!Game.getInstance().isCreeperExplosionAllowed()) {
            if (e.getEntityType().equals(EntityType.CREEPER)) {
                e.setCancelled(true);
            }
        }
    }

}
