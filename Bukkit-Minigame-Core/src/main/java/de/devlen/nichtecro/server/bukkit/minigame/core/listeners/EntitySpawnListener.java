package de.devlen.nichtecro.server.bukkit.minigame.core.listeners;

import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.Game;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntitySpawnEvent;

/**
 * Created by Tarik on 31.05.2016.
 */
public class EntitySpawnListener extends ListenerHelper {

    @EventHandler
    public void onEntitySpawn(EntitySpawnEvent e) {
        if (Game.getInstance().isEntitySpawnAllowed()) {
            if (Game.getInstance().getBlacklistedEntitySpawnTypes().contains(e.getEntityType())) {
                e.setCancelled(true);
            }
        } else {
            if (!Game.getInstance().getWhitelistedEntitySpawnTypes().contains(e.getEntityType())) {
                e.setCancelled(true);
            }
        }
    }

}
