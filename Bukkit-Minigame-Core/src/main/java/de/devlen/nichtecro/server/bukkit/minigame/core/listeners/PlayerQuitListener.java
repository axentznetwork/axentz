package de.devlen.nichtecro.server.bukkit.minigame.core.listeners;

import de.devlen.nichtecro.server.bukkit.minigame.core.game.Game;
import de.devlen.nichtecro.server.bukkitcore.language.Language;
import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager;
import de.devlen.nichtecro.server.bukkitcore.manager.Rank;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerQuitEvent;

import static de.devlen.nichtecro.server.bukkitcore.language.Language.tl;

/**
 * Created by Tarik on 28.05.2016.
 */
public class PlayerQuitListener extends ListenerHelper {

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        for (Player target : Bukkit.getOnlinePlayers()) {
            if (!target.getName().equalsIgnoreCase(player.getName()))
                Chat.send(target, Language.tl(Game.getInstance().getGameNameNode(), target), tl("bukkit.minigame.core.quit.message", target, Rank.getHighestRank(MPlayerSettingsManager.getRanks(player)).getChatPrefixTranslatable(target), player.getName()));
        }


    }

}
