package de.devlen.nichtecro.server.bukkit.minigame.core.game.taskmanager;

/**
 * Created by Tarik on 31.05.2016.
 */
public interface IGameTaskManager {

    void speedUpLobbyTime();
    void speedUpGameTime();
    void startLobby();
    void startGame();

}
