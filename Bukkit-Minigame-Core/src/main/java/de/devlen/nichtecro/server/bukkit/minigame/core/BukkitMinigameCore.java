package de.devlen.nichtecro.server.bukkit.minigame.core;

import de.devlen.nichtecro.server.bukkit.minigame.core.listeners.*;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Tarik on 28.05.2016.
 */
public class BukkitMinigameCore extends JavaPlugin {

    private static Plugin instance;

    public void onEnable() {
        instance = this;
        registerListeners();
    }

    public void onDisable() {

    }

    public static void registerCommands() {

    }

    public static void registerListeners() {
        new EntityDamageListener().register();
        new EntityExplodeListener().register();
        new EntitySpawnListener().register();
        new FoodLevelChangeListener().register();
        new PlayerDropItemListener().register();
        new PlayerJoinListener().register();
        new PlayerLoginListener().register();
        new PlayerPickupItemListener().register();
        new PlayerQuitListener().register();
    }

    public static Plugin getInstance() {
        return instance;
    }
}
