package de.devlen.nichtecro.server.bukkit.minigame.core.listeners;

import de.devlen.nichtecro.server.bukkit.minigame.core.game.Game;
import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * Created by Tarik on 31.05.2016.
 */
public class EntityDamageListener extends ListenerHelper {

    @EventHandler
    public void onEntityDamage(EntityDamageEvent e) {
        if (!Game.getInstance().isPvpAllowed()) {
            if (e.getEntityType().equals(EntityType.PLAYER)) {
                e.setCancelled(true);
            }
        }
    }

}
