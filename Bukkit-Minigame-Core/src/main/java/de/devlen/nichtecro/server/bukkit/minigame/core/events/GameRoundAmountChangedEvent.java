package de.devlen.nichtecro.server.bukkit.minigame.core.events;

import de.devlen.nichtecro.server.bukkitcore.events.EventHelper;

/**
 * Created by Tarik on 02.06.2016.
 */
public class GameRoundAmountChangedEvent extends EventHelper {

    private int oldAmount = 0;
    private int newAmount = 0;

    public GameRoundAmountChangedEvent(int oldAmount, int newAmount) {
        this.oldAmount = oldAmount;
        this.newAmount = newAmount;
    }

    public int getOldAmount() {
        return oldAmount;
    }

    public int getNewAmount() {
        return newAmount;
    }
}
