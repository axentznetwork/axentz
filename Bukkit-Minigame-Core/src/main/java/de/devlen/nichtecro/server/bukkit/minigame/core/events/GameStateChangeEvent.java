package de.devlen.nichtecro.server.bukkit.minigame.core.events;

import de.devlen.nichtecro.server.bukkitcore.events.EventHelper;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.GameState;

/**
 * Created by Tarik on 01.06.2016.
 */
public class GameStateChangeEvent extends EventHelper {

    private GameState fromState = GameState.WAITING;
    private GameState toState = GameState.RUNNING;

    public GameStateChangeEvent(GameState fromState, GameState toState) {
        this.fromState = fromState;
        this.toState = toState;
    }

    public GameState getFromState() {
        return fromState;
    }

    public GameState getToState() {
        return toState;
    }
}
