package de.devlen.nichtecro.server.bukkit.minigame.core.game.maps;

import org.bukkit.Material;

/**
 * Created by Tarik on 29.05.2016.
 */
public class BlockDescriptor {

    private Material material = null;
    private int[] damages = null;

    public BlockDescriptor(Material material, int... damages) {
        this.material = material;
        this.damages = damages;
    }

    public static BlockDescriptor newBD(Material material, int... damages) {
        return new BlockDescriptor(material, damages);
    }

    public Material getMaterial() {
        return material;
    }

    public int[] getDamages() {
        return damages;
    }
}
