package de.devlen.axentz.server.bukkit.minigame.wingrace.map.rings.types;

/**
 * Created by Tarik on 21.06.2016.
 */
public enum RingType {

    USUAL(1),
    CHECKPOINT(3),
    BOOST(1),
    TELEPORT(1);

    private int points = 0;

    RingType(int points) {
        this.points = points;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
