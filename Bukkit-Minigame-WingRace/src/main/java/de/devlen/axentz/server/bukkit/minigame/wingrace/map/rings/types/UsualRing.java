package de.devlen.axentz.server.bukkit.minigame.wingrace.map.rings.types;

import de.devlen.axentz.server.bukkit.minigame.wingrace.map.rings.Ring;

/**
 * Created by Tarik on 21.06.2016.
 */
public class UsualRing extends Ring {

    protected UsualRing(int order) {
        super(order);
    }

    @Override
    public RingType getRingType() {
        return RingType.USUAL;
    }

    @Override
    public boolean isUsualRing() {
        return true;
    }

    @Override
    public boolean isCheckpointRing() {
        return false;
    }

    @Override
    public boolean isBoostRing() {
        return false;
    }

    @Override
    public boolean isTeleportRing() {
        return false;
    }
}
