package de.devlen.axentz.server.bukkit.minigame.wingrace.map.rings.types;

import de.devlen.axentz.server.bukkit.minigame.wingrace.map.rings.Ring;

/**
 * Created by Tarik on 21.06.2016.
 */
public class BoostRing extends Ring {

    private BoostRing(int order) {
        super(order);
    }

    @Override
    public RingType getRingType() {
        return RingType.BOOST;
    }

    @Override
    public boolean isUsualRing() {
        return false;
    }

    @Override
    public boolean isCheckpointRing() {
        return false;
    }

    @Override
    public boolean isBoostRing() {
        return true;
    }

    @Override
    public boolean isTeleportRing() {
        return false;
    }
}
