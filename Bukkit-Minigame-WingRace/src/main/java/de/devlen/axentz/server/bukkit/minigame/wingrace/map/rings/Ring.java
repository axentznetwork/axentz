package de.devlen.axentz.server.bukkit.minigame.wingrace.map.rings;

import de.devlen.axentz.server.bukkit.minigame.wingrace.map.rings.types.RingType;

/**
 * Created by Tarik on 21.06.2016.
 */
public abstract class Ring {

    private int order = 0;

    protected Ring(int order) {
        this.order = order;
    }

    public abstract RingType getRingType();

    public abstract boolean isUsualRing();

    public abstract boolean isCheckpointRing();

    public abstract boolean isBoostRing();

    public abstract boolean isTeleportRing();

    public int getPoints() {
        return (getRingType() != null ? getRingType().getPoints() : 0);
    }

    public int getOrder() {
        return order;
    }
}
