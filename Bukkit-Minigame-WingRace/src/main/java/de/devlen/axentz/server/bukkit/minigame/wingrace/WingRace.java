package de.devlen.axentz.server.bukkit.minigame.wingrace;

import de.devlen.nichtecro.server.bukkit.minigame.core.game.Game;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.GameState;
import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Tarik on 19.06.2016.
 */
public class WingRace extends JavaPlugin {

    private static Plugin instance;

    public void onEnable() {
        instance = this;

        for (World world : Bukkit.getWorlds()) {
            world.setGameRuleValue("doFireTick", "false");
        }

        Game game = Game.getInstance();
        game.setGameState(GameState.WAITING);
        game.setBlockBreakingAllowed(false);
        game.setFullLobbyTime(120);
        game.setRoundBased(false);
        game.setFullGameTime(360); //itz one-groud-game
        game.setShrinkedGameTime(30);
        game.setHungerAllowed(false);
        game.setGameNameNode("bukkit.minigame.wingrace.tag");
        game.setMinimumPlayerAmount(2);
        game.setMaximumPlayerAmount(16);
        game.setMultipleGameSpawnsEnabled(false);
        game.setLobbySpawnLocation(new Location(Bukkit.getWorld("lobby_2_0"), 251.5, 38, -490.5));
        game.setEvaluationSpawnLocation(new Location(Bukkit.getWorld("lobby_2_0"), 238.5, 41, -488.5));
        game.setGameSpawns(new ExtendedList<>(new Location(Bukkit.getWorld("lobby_2_0"), 237.5, 46, -521.5), new Location(Bukkit.getWorld("lobby_2_0"), 272.5, 47, -522.5)));
        game.setShrinkedLobbyTime(30);
        game.setEntitySpawnAllowed(false);
        game.setRespawnTime(0);
        game.setPlayerDamageAllowed(false);
        game.setPlayerRespawnAllowed(true);
    }

    public static Plugin getInstance() {
        return instance;
    }
}
