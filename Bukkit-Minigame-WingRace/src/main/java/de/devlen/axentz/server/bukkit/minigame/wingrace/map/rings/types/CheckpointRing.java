package de.devlen.axentz.server.bukkit.minigame.wingrace.map.rings.types;

import de.devlen.axentz.server.bukkit.minigame.wingrace.map.rings.Ring;

/**
 * Created by Tarik on 21.06.2016.
 */
public class CheckpointRing extends Ring {

    protected CheckpointRing(int order) {
        super(order);
    }

    @Override
    public RingType getRingType() {
        return RingType.CHECKPOINT;
    }

    @Override
    public boolean isUsualRing() {
        return false;
    }

    @Override
    public boolean isCheckpointRing() {
        return true;
    }

    @Override
    public boolean isBoostRing() {
        return false;
    }

    @Override
    public boolean isTeleportRing() {
        return false;
    }
}
