import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tarik on 26.05.2016.
 */
public class FileIterator {

    private static String veryBase = "C:/Users/Tarik/IdeaProjects/NichtEcroServer/Bukkit-Core-API/build/classes/work";

    public static void main(String[] args) {
        for (String s : getQualifiedNamesInFolder(new File(veryBase))) {
            System.out.println(s);
        }
    }

    private static List<File> javaFiles = new ArrayList<>();

    public static void iteratorOverFilesAndSubdirs(String basicPath) {
        File basicFile = new File(basicPath);
        if (basicFile.isDirectory()) {
            File[] subFolders = basicFile.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    return (pathname.isDirectory());
                }
            });
            File[] classFiles = basicFile.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return (name.endsWith(".class"));
                }
            });
        } else {
            throw new RuntimeException("Sorry, this is not a Directory!");
        }
    }

    public static ArrayList<String> getQualifiedNamesInFolder(final File folder) {
        ArrayList<String> list = new ArrayList<>();
        for (final File fileEntry : folder.listFiles()) {

            if (fileEntry.isDirectory()) {
                ArrayList<String> pls = getQualifiedNamesInFolder(fileEntry);
                for (int i = 0; i < pls.size(); i++) {
                    list.add(pls.get(i));
                }
            } else {
                if (fileEntry.getName().endsWith(".class")) {
                    String relativePath = fileEntry.getAbsolutePath().substring(veryBase.length() + 1);
                    list.add(relativePath.substring(0, relativePath.length() - 6).replace("\\", "."));
                }
            }
        }
        return list;
    }

    public static ArrayList<String> getFoldersInFolder(final File folder) {
        File base = folder;
        ArrayList<String> list = new ArrayList<>();
        for (final File fileEntry : folder.listFiles()) {

            if (fileEntry.isDirectory()) {
                list.add(base.toURI().relativize(fileEntry.toURI()).getPath());
            }
        }
        return list;
    }

}
