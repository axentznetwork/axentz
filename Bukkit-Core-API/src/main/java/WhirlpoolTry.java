import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Tarik on 19.06.2016.
 */
public class WhirlpoolTry {

    public static void main(String[] args){
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("whirlpool"); //md5, sha-512
            byte[] digested = messageDigest.digest("hagen240163".getBytes("UTF-8"));
            BigInteger bigInteger = new BigInteger(1, digested);
            String hashed = bigInteger.toString(16);
            while(hashed.length() < 32){
                hashed= "0" + hashed;
            }
            System.out.println(hashed);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

}
