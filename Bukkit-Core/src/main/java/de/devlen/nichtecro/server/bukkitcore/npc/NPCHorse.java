package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.HorseMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCHorse extends NPCAnimal {

    public NPCHorse(int entityId, Location location) {
        super(entityId, EntityType.HORSE, location);
    }

    @Override
    public HorseMetadata getEntityMetadata() {
        return (HorseMetadata) super.getEntityMetadata();
    }
}
