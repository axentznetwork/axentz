package de.devlen.nichtecro.server.bukkitcore.npc;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 01.04.2016.
 */
public class NPCPlayer extends NPCLiving {

    public NPCPlayer(int entityId, Location location) {
        super(entityId, EntityType.PLAYER, location);
    }
}
