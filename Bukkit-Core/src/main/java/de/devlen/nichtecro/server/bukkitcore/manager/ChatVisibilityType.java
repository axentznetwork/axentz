package de.devlen.nichtecro.server.bukkitcore.manager;

/**
 * Created by Tarik on 06.06.2016.
 */
public enum ChatVisibilityType {

    ALL,
    PREMIUM,
    NONE;

}
