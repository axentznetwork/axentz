package de.devlen.nichtecro.server.bukkitcore.manager.logging;

import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import de.devlen.nichtecro.server.bukkitcore.database.types.ColumnDescriptor;
import de.devlen.nichtecro.server.bukkitcore.database.types.DataType;
import de.devlen.nichtecro.server.bukkitcore.database.types.SpecialField;
import de.devlen.nichtecro.server.bukkitcore.manager.Manager;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Tarik on 25.03.2016.
 */
public class ChatLogManager extends Manager {

    private static String tableName = getTablePrefix() + "logged_chats";
    private static List<ChatLogEntry> chatLogEntryList = new ExtendedList<>();

    public static void createTable() {
        ColumnDescriptor[] columnDescriptors = {
                new ColumnDescriptor("id", DataType.INT, null, SpecialField.PRIMARY_KEY, SpecialField.AUTO_INC),
                new ColumnDescriptor("uuid", DataType.VARCHAR, 36),
                new ColumnDescriptor("time", DataType.DATETIME, null),
                new ColumnDescriptor("message", DataType.TEXT, 1000)};
        createTable(tableName, columnDescriptors);
    }

    public static void insertMessage(Player player, String message) {
//        Map<String, Object> stringObjectMap = Maps.newHashMap();
//        stringObjectMap.put("uuid", UUIDControl.getUUID(player));
//        stringObjectMap.put("time", "NOW()");
//        stringObjectMap.put("message", message);
//        ResultSet resultSet = insertData(tableName, stringObjectMap);
//        try {
//            while (resultSet.next()) {
//                int id = resultSet.getInt(1);
//                UUID uuid = UUIDControl.getUUID(player);
//                Date date = new Date();
//                ChatLogEntry chatLogEntry = new ChatLogEntry(id, uuid, date, message);
//                chatLogEntryList.add(chatLogEntry);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
    }

    public static void cacheCommandLogEntries() {
        ResultSet resultSet = selectData(tableName, "*", "");
        try {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                UUID uuid = UUID.fromString(resultSet.getString("uuid"));
                Date date = resultSet.getTimestamp("time");
                String message = resultSet.getString("message");
                ChatLogEntry chatLogEntry = new ChatLogEntry(id, uuid, date, message);
                chatLogEntryList.add(chatLogEntry);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<ChatLogEntry> getChatLogEntries(UUID uuid) {
        List<ChatLogEntry> chatLogEntries = new ExtendedList<>();
        try {
            for (ChatLogEntry commandLogEntry : chatLogEntryList) {
                if (commandLogEntry.getUuid().toString().equalsIgnoreCase(uuid.toString())) {
                    chatLogEntries.add(commandLogEntry);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Collections.reverse(chatLogEntries);
        return chatLogEntries;
    }

}
