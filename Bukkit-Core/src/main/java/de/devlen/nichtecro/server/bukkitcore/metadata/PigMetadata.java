package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class PigMetadata extends AnimalMetadata {

    public void setSaddle(boolean saddle){
        setMeta(12, saddle);
    }

    public boolean hasSaddle(){
        return (boolean) getMeta(12);
    }

}
