package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class BlazeMetadata extends MonsterMetadata {

    public void setOnFire(boolean onFire){
        setMeta(11, (onFire ? 1 : 0));
    }

    public boolean isOnFire(){
        return ((byte) getMeta(11) == 1 ? true : false);
    }

}
