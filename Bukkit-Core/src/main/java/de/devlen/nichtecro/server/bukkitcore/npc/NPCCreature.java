package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.CreatureMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCCreature extends NPCInsentient {

    public NPCCreature(int entityId, EntityType entityType, Location location) {
        super(entityId, entityType, location);
    }

    @Override
    public CreatureMetadata getEntityMetadata() {
        return (CreatureMetadata) super.getEntityMetadata();
    }
}
