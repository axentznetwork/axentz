package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.GolemMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCShulker extends NPCGolem {

    public NPCShulker(int entityId, Location location) {
        super(entityId, EntityType.SHULKER, location);
    }

    @Override
    public GolemMetadata getEntityMetadata() {
        return super.getEntityMetadata();
    }
}
