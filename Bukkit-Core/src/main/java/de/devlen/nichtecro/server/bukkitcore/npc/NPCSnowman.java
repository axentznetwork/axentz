package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.GolemMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCSnowman extends NPCGolem {

    public NPCSnowman(int entityId, Location location) {
        super(entityId, EntityType.SNOWMAN, location);
    }

    @Override
    public GolemMetadata getEntityMetadata() {
        return super.getEntityMetadata();
    }
}
