package de.devlen.nichtecro.server.bukkitcore.particle;

import com.comphenix.protocol.wrappers.EnumWrappers;
import de.devlen.nichtecro.server.bukkitcore.packetwrapper.WrapperPlayServerWorldParticles;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

/**
 * Created by Tarik on 01.05.2016.
 */
public class ParticleSender {

    public static void send(Player player, EnumWrappers.Particle particle, Location location, Vector offset, float speed, int amount) {
        WrapperPlayServerWorldParticles wrapperPlayServerWorldParticles = new WrapperPlayServerWorldParticles();
        wrapperPlayServerWorldParticles.setParticleType(particle);
        wrapperPlayServerWorldParticles.setLongDistance(true);
        wrapperPlayServerWorldParticles.setX((float) location.getX());
        wrapperPlayServerWorldParticles.setY((float) location.getY());
        wrapperPlayServerWorldParticles.setZ((float) location.getZ());
        wrapperPlayServerWorldParticles.setOffsetX((float) offset.getX());
        wrapperPlayServerWorldParticles.setOffsetY((float) offset.getY());
        wrapperPlayServerWorldParticles.setOffsetZ((float) offset.getZ());
        wrapperPlayServerWorldParticles.setParticleData(speed);
        wrapperPlayServerWorldParticles.setNumberOfParticles(amount);
        wrapperPlayServerWorldParticles.setData(new int[0]);
        wrapperPlayServerWorldParticles.sendPacket(player);
    }

}
