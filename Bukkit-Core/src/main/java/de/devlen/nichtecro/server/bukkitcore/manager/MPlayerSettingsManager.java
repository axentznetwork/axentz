package de.devlen.nichtecro.server.bukkitcore.manager;

import com.mongodb.BasicDBObject;
import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import de.devlen.nichtecro.server.bukkitcore.database.MongoDBManager;
import de.devlen.nichtecro.server.bukkitcore.events.DisguiseRankChangedEvent;
import de.devlen.nichtecro.server.bukkitcore.player.UUIDControl;
import org.bson.Document;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static com.mongodb.client.model.Filters.eq;

/**
 * Created by Tarik on 22.05.2016.
 */
public class MPlayerSettingsManager extends MongoDBManager {

    private static String collectionName = getCollectionPrefix() + "players";

    /**
     * Checks if a player is existing in the database. If not you should deny any action which is document related.
     *
     * @param player The player you want to check.
     * @return isExisting
     * @deprecated never needed but created for formal reason.
     */
    @Deprecated
    public static boolean isPlayerExisting(Player player) {
        UUID uuid = UUIDControl.getUUID(player);
        return isPlayerExisting(uuid);
    }

    /**
     * Checks if a player is existing in the database. If not you should deny any action which is document related.
     *
     * @param clientName The client you want to check.
     * @return isExisting
     */
    public static boolean isPlayerExisting(String clientName) {
        return isPlayerExisting(UUIDControl.getUUID(clientName));
    }

    /**
     * Checks if a player is existing in the database. If not you should deny any action which is document related.
     *
     * @param uuid The fetched Mojang-UUID.
     * @return isExisting
     */
    public static boolean isPlayerExisting(UUID uuid) {
        if (getCursor(collectionName, new BasicDBObject("uuid", uuid.toString())).iterator().hasNext()) {
            return true;
        }
        return false;
    }

    /**
     * Please call this method only in Bukkit-Core.
     *
     * @param uuid The fetched Mojang-UUID
     * @param name The clientName you want to associate.
     */
    public static void insertNewPlayer(UUID uuid, String name) {
        if (!isPlayerExisting(uuid)) {
            Document document = new Document("uuid", uuid.toString())
                    .append("player_name", name)
                    .append("rank", Arrays.asList(Rank.GUEST.name()))
                    .append("language", "de_DE")
                    .append("dyes", 0)
                    .append("disguised_rank", null);
            insert(collectionName, document);
        }
    }

    public static void insertNewPlayer(Player player) {
        UUID uuid = UUIDControl.getUUID(player);
        String name = player.getName();
        insertNewPlayer(uuid, name);
    }

    public static List<Rank> getRanks(Player player) {
        return getRanks(UUIDControl.getUUID(player));

    }

    public static List<Rank> getRanks(String name) {
        return getRanks(UUIDControl.getUUID(name));
    }

    public static List<Rank> getRanks(UUID uuid) {
        if (isPlayerExisting(uuid)) {
            Document document = getFirstDocument(collectionName, eq("uuid", uuid.toString()));
            List<String> rankNames = (List<String>) document.get("rank");
            List<Rank> ranks = new ExtendedList<>();
            if (rankNames != null && rankNames.size() > 0) {
                for (String rankName : rankNames) {
                    try {
                        Rank rank = Rank.valueOf(rankName.toUpperCase());
                        ranks.add(rank);
                    } catch (Exception ex) {
                    }
                }
                Collections.sort(ranks);
                Collections.reverse(ranks);
                return ranks;
            } else {
                return new ExtendedList<>(Rank.GUEST);
            }
        } else {
            return new ExtendedList<>(Rank.GUEST);
        }
    }

    public static Rank getDisguisedRank(Player player) {
        return getDisguisedRank(UUIDControl.getUUID(player));
    }

    public static Rank getDisguisedRank(String name) {
        return getDisguisedRank(UUIDControl.getUUID(name));
    }

    public static Rank getDisguisedRank(UUID uuid) {
        if (isPlayerExisting(uuid)) {
            Document document = getFirstDocument(collectionName, eq("uuid", uuid.toString()));
            String disguiseRankName = document.getString("disguise_rank");
            if (disguiseRankName == null) {
                return Rank.getHighestRank(getRanks(uuid));
            }
            return Rank.valueOf(disguiseRankName.toUpperCase());
        }
        return Rank.GUEST;
    }

    public static void setDisguisedRank(Player player, Rank rank) {
        setDisguisedRank(UUIDControl.getUUID(player), rank);
        new DisguiseRankChangedEvent(player, rank).call();
    }

    public static void setDisguisedRank(String name, Rank rank) {
        setDisguisedRank(UUIDControl.getUUID(name), rank);
    }

    public static void setDisguisedRank(UUID uuid, Rank rank) {
        if (isPlayerExisting(uuid)) {
            update(collectionName, eq("uuid", uuid.toString()), new BasicDBObject("$set", new BasicDBObject("disguise_rank", (rank != null ? rank.name() : null))));
        }
    }

    @Deprecated
    public static Rank getRank(Player player) {
        return getRank(UUIDControl.getUUID(player));
    }

    @Deprecated
    public static Rank getRank(String name) {
        return getRank(UUIDControl.getUUID(name));
    }

    @Deprecated
    public static Rank getRank(UUID uuid) {
        if (isPlayerExisting(uuid)) {
            Document document = getFirstDocument(collectionName, eq("uuid", uuid.toString()));
            List<String> rankName = (List<String>) document.get("rank");
            if (rankName != null && rankName.get(0) != null) {
                return Rank.valueOf(rankName.get(0).toUpperCase());
            }
            return Rank.GUEST;
        } else {
            return Rank.GUEST;
        }
    }

    public static void addRank(Player player, Rank rank) {
        addRank(UUIDControl.getUUID(player), rank);
    }

    public static void addRank(String name, Rank rank) {
        addRank(UUIDControl.getUUID(name), rank);
    }

    public static void addRank(UUID uuid, Rank rank) {
        if (isPlayerExisting(uuid)) {
            if (!getRanks(uuid).contains(rank)) {
                update(collectionName, eq("uuid", uuid.toString()), new BasicDBObject("$push", new BasicDBObject("rank", rank.name())));
            }
        }
    }

    public static boolean removeRank(Player player, Rank rank) {
        return removeRank(UUIDControl.getUUID(player), rank);
    }

    public static boolean removeRank(String name, Rank rank) {
        return removeRank(UUIDControl.getUUID(name), rank);
    }

    public static boolean removeRank(UUID uuid, Rank rank) {
        if (isPlayerExisting(uuid)) {
            List<Rank> ranks = getRanks(uuid);
            if (ranks.contains(rank) && ranks.size() > 1 && rank != Rank.GUEST) {
                update(collectionName, eq("uuid", uuid.toString()), new BasicDBObject("$pull", new BasicDBObject("rank", rank.name())));
                return true;
            }
            return false;
        }
        return false;
    }

    public static void setRank(Player player, Rank rank) {
        setRank(UUIDControl.getUUID(player), rank);
    }

    public static void setRank(String name, Rank rank) {
        setRank(UUIDControl.getUUID(name), rank);
    }

    public static void setRank(UUID uuid, Rank rank) {
        if (isPlayerExisting(uuid)) {
            update(collectionName, eq("uuid", uuid.toString()), new BasicDBObject("$set", new BasicDBObject("rank", Arrays.asList(rank.name()))));
        }
    }

    public static String getLanguage(String name) {
        return getLanguage(UUIDControl.getUUID(name));
    }

    public static String getLanguage(Player player) {
        return getLanguage(UUIDControl.getUUID(player));
    }

    /**
     * Language-locales are e.g. en_US, de_DE, fr_CA.
     *
     * @param uuid - The Mojang-fetched UUID. See {@link #getLanguage(String)} or {@link #getLanguage(Player) for further usage.}
     * @return A language locale.
     */
    public static String getLanguage(UUID uuid) {
        if (isPlayerExisting(uuid)) {
            Document document = getFirstDocument(collectionName, eq("uuid", uuid.toString()));
            String language = document.getString("language");
            return language;
        }
        return "en_US";
    }

    /**
     * Used by the incoming packet of minecraft to set the language known for the server.
     * Language-locales are e.g. en_US, de_DE, fr_CA.
     *
     * @param player   - A Bukkit-player.
     * @param language - A language locale.
     */
    public static void setLanguage(Player player, String language) {
        setLanguage(UUIDControl.getUUID(player), language);
    }

    /**
     * Used by the incoming packet of minecraft to set the language known for the server.
     * Language-locales are e.g. en_US, de_DE, fr_CA.
     *
     * @param name     - A valid Minecraft-username.
     * @param language - A language locale.
     */
    public static void setLanguage(String name, String language) {
        setLanguage(UUIDControl.getUUID(name), language);
    }

    /**
     * Used by the incoming packet of minecraft to set the language known for the server.
     * Language-locales are e.g. en_US, de_DE, fr_CA.
     *
     * @param uuid     - A Mojang-fetched UUID. See {@link #setLanguage(Player, String) or {@link #setLanguage} for further usage.}
     * @param language - A language locale.
     */
    public static void setLanguage(UUID uuid, String language) {
        if (isPlayerExisting(uuid)) {
            update(collectionName, eq("uuid", uuid.toString()), new BasicDBObject("$set", new BasicDBObject("language", language)));
        }
    }

    public static void transferDyes(Player player, long dyes) {
        transferDyes(UUIDControl.getUUID(player), dyes);
    }

    public static void transferDyes(String name, long dyes) {
        transferDyes(UUIDControl.getUUID(name), dyes);
    }

    public static void transferDyes(UUID uuid, long dyes) {
        update(collectionName, eq("uuid", uuid.toString()), new BasicDBObject("$inc", new BasicDBObject("dyes", dyes)));
    }

    public static boolean debitDyes(Player player, long dyes) {
        return debitDyes(UUIDControl.getUUID(player), dyes);
    }

    /**
     * Dyes are the currency. Be careful what you do!
     *
     * @param name Name of the client
     * @param dyes Amount of dyes.
     */
    public static boolean debitDyes(String name, long dyes) {
        return debitDyes(UUIDControl.getUUID(name), dyes);
    }

    /**
     * Dyes are the currency. Be careful what you do!
     *
     * @param uuid - The fetched Mojang-UUID.
     * @param dyes - How much dyes should be debited.
     * @return Transaction was sucessfully.
     */
    public static boolean debitDyes(UUID uuid, long dyes) {
        if (hasEnoughDyes(uuid, dyes)) {
            update(collectionName, eq("uuid", uuid.toString()), new BasicDBObject("$inc", new BasicDBObject("dyes", -dyes)));
            return true;
        }
        return false;
    }

    public static boolean hasEnoughDyes(UUID uuid, long dyes) {
        if (isPlayerExisting(uuid)) {
            return getDyes(uuid) > dyes;
        }
        return false;
    }

    private static void setDyes(UUID uuid, long dyes) {
        if (isPlayerExisting(uuid)) {
            update(collectionName, eq("uuid", uuid.toString()), new BasicDBObject("$set", new BasicDBObject("dyes", dyes)));
        }
    }

    public static Long getDyes(Player player) {
        return getDyes(UUIDControl.getUUID(player));
    }

    public static Long getDyes(UUID uuid) {
        if (isPlayerExisting(uuid)) {
            Document document = getFirstDocument(collectionName, eq("uuid", uuid.toString()));
            Long dyes = document.getLong("dyes");
            if (dyes != null) {
                return dyes;
            }
            return 0l;
        }
        return 0l;
    }

    public static void decreaseAmount(Player player, String subCategory, int amount) {
        decreaseAmount(UUIDControl.getUUID(player), subCategory, amount);
    }

    public static void decreaseAmount(String name, String subCategory, int amount) {
        decreaseAmount(UUIDControl.getUUID(name), subCategory, amount);
    }

    public static void decreaseAmount(UUID uuid, String subCategory, int amount) {
        if (isPlayerExisting(uuid)) {
            update(collectionName, eq("uuid", uuid.toString()), new BasicDBObject("$inc", new BasicDBObject("amounts." + subCategory, -amount)));
        }
    }

    public static void increaseAmount(Player player, String subCategory, int amount) {
        increaseAmount(UUIDControl.getUUID(player), subCategory, amount);
    }

    public static void increaseAmount(String name, String subCategory, int amount) {
        increaseAmount(UUIDControl.getUUID(name), subCategory, amount);
    }

    public static void increaseAmount(UUID uuid, String subCategory, int amount) {
        if (isPlayerExisting(uuid)) {
            update(collectionName, eq("uuid", uuid.toString()), new BasicDBObject("$inc", new BasicDBObject("amounts." + subCategory, amount)));
        }
    }

    public static void setAmount(Player player, String subCategory, int amount) {
        setAmount(UUIDControl.getUUID(player), subCategory, amount);
    }

    public static void setAmount(String name, String subCategory, int amount) {
        setAmount(UUIDControl.getUUID(name), subCategory, amount);
    }

    public static void setAmount(UUID uuid, String subCategory, int amount) {
        if (isPlayerExisting(uuid)) {
            update(collectionName, eq("uuid", uuid.toString()), new BasicDBObject("$set", new BasicDBObject("amounts." + subCategory, amount)));
        }
    }

    public static int getAmount(Player player, String subCategory) {
        return getAmount(UUIDControl.getUUID(player), subCategory);
    }

    public static int getAmount(String name, String subCategory) {
        return getAmount(UUIDControl.getUUID(name), subCategory);
    }

    public static int getAmount(UUID uuid, String subCategory) {
        if (isPlayerExisting(uuid)) {
            Document document = getFirstDocument(collectionName, eq("uuid", uuid.toString()));
            Document subCategoryDocument = (Document) document.get("amounts");
            if (subCategoryDocument != null) {
                Integer amount = subCategoryDocument.getInteger(subCategory);
                if (amount != null) {
                    return amount;
                }
                return 0;
            }
            return 0;
        }
        return 0;
    }

    public static void setChatVisibilityType(Player player, ChatVisibilityType value) {
        setChatVisibilityType(UUIDControl.getUUID(player), value);
    }

    public static void setChatVisibilityType(String name, ChatVisibilityType value) {
        setChatVisibilityType(UUIDControl.getUUID(name), value);
    }

    public static void setChatVisibilityType(UUID uuid, ChatVisibilityType value) {
        if (isPlayerExisting(uuid)) {
            update(collectionName, eq("uuid", uuid.toString()), new BasicDBObject("$set", new BasicDBObject("settings.chat.visibility", value.name())));
        }
    }

    public static ChatVisibilityType getChatVisibilityType(Player player) {
        return getChatVisibilityType(UUIDControl.getUUID(player));
    }

    public static ChatVisibilityType getChatVisibilityType(String name) {
        return getChatVisibilityType(UUIDControl.getUUID(name));
    }

    public static ChatVisibilityType getChatVisibilityType(UUID uuid) {
        if (isPlayerExisting(uuid)) {
            Document document = getFirstDocument(collectionName, eq("uuid", uuid.toString()));
            Document settingsDocument = (Document) document.get("settings");
            if (settingsDocument != null) {
                Document chatSettingsDocument = (Document) settingsDocument.get("chat");
                if (chatSettingsDocument != null) {
                    String chatVisibilityString = chatSettingsDocument.getString("visibility");
                    if (chatVisibilityString != null) {
                        ChatVisibilityType chatVisibilityType = ChatVisibilityType.valueOf(chatVisibilityString);
                        return chatVisibilityType;
                    }
                    return ChatVisibilityType.ALL;
                }
            }
            return ChatVisibilityType.ALL;
        }
        return ChatVisibilityType.ALL;
    }
}
