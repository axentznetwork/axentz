package de.devlen.nichtecro.server.bukkitcore.validation;

import de.devlen.nichtecro.server.bukkitcore.manager.Rank;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

/**
 * Created by Tarik on 30.04.2016.
 */
public class Validate {

    public static boolean isRank(String rankName) {
        try {
            Rank.valueOf(rankName.toUpperCase());
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public static boolean isSimilarList(List<String> list1, List<String> list2) {
        return (list1.containsAll(list2)) && (list2.containsAll(list1));
    }

    public static boolean isSimilarItem(ItemStack is1, ItemStack is2) {
        if ((is1.getType() == is2.getType()) &&
                (is1.getDurability() == is2.getDurability()) &&
                (is1.hasItemMeta() == is2.hasItemMeta())) {
            if (is1.hasItemMeta()) {
                ItemMeta im1 = is1.getItemMeta();
                ItemMeta im2 = is2.getItemMeta();
                if (im1.getDisplayName().equals(im2.getDisplayName())) {
                    if (im1.getLore() != null && im2.getLore() != null) {
                        return true;
                    }
                    return true;
                }
            }
            return true;
        }
        return false;
    }

}
