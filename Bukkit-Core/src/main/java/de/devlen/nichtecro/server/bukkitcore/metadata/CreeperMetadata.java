package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class CreeperMetadata extends MonsterMetadata {

    /**
     * -1 - idle
     * 1 - fuse
     *
     * @param state
     */
    public void setState(int state) {
        setMeta(11, state);
    }

    public int getState() {
        return (int) getMeta(11);
    }

    public void setCharged(boolean charged) {
        setMeta(12, charged);
    }

    public boolean isCharged() {
        return (boolean) getMeta(12);
    }

    public void setIgnited(boolean ignited) {
        setMeta(13, ignited);
    }

    public boolean isIgnited(){
        return (boolean) getMeta(13);
    }

}
