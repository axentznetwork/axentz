package de.devlen.nichtecro.server.bukkitcore.database;

import de.devlen.nichtecro.server.bukkitcore.Constants;

/**
 * Created by Tarik on 21.03.2016.
 */
public class DatabaseManager {

    private static Database mySQL;

    public static void initDatabase() {
        mySQL = new MySQL(Constants.getMysqlAddress(), Constants.getMysqlPort(), Constants.getMysqlDatabase(), Constants.getMysqlUser(), Constants.getMysqlPassword());
    }

    public static Database database() {
        if (mySQL == null) {
            initDatabase();
        }
        return mySQL;
    }

}
