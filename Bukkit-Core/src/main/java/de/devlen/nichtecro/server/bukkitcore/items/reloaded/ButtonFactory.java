package de.devlen.nichtecro.server.bukkitcore.items.reloaded;

import com.google.common.collect.Maps;
import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import de.devlen.nichtecro.server.bukkitcore.data.KeyPair;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.List;
import java.util.Map;

import static de.devlen.nichtecro.server.bukkitcore.language.Language.tl;

/**
 * Created by Tarik on 04.06.2016.
 */
public class ButtonFactory {

    private String translatableName = "bukkit.core.buttonfactory.name";
    private String[] nameVariables = new String[0];
    private List<KeyPair<String, String[]>> translatableLores = new ExtendedList<>();
    private Material material = Material.STONE;
    private int amount = 1;
    private int damage = 0;
    private Map<Enchantment, Integer> enchantments = Maps.newHashMap();
    private List<ItemFlag> itemFlags = new ExtendedList<>();
    private String skin = null;
    private Color color = null;

    public ButtonFactory nameNode(String nameNode, String... variables) {
        translatableName = nameNode;
        this.nameVariables = variables;
        return this;
    }

    public ButtonFactory loreNode(String loreNode, String... variables) {
        translatableLores.add(new KeyPair<>(loreNode, variables));
        return this;
    }

    public ButtonFactory type(Material material) {
        this.material = material;
        return this;
    }

    public ButtonFactory amount(int amount) {
        this.amount = amount;
        return this;
    }

    public ButtonFactory damage(int damage) {
        this.damage = damage;
        return this;
    }

    public ButtonFactory enchant(Enchantment enchantment, int level) {
        enchantments.put(enchantment, level);
        return this;
    }

    public ButtonFactory flag(ItemFlag... itemFlags) {
        for (ItemFlag itemFlag : itemFlags) {
            this.itemFlags.add(itemFlag);
        }
        return this;
    }

    public ButtonFactory skin(String skin) {
        this.skin = skin;
        return this;
    }

    public ButtonFactory leatherColor(Color color) {
        this.color = color;
        return this;
    }

    public String getTranslatableName() {
        return translatableName;
    }

    public String[] getNameVariables() {
        return nameVariables;
    }

    public List<KeyPair<String, String[]>> getTranslatableLores() {
        return translatableLores;
    }

    public Material getMaterial() {
        return material;
    }

    public int getAmount() {
        return amount;
    }

    public int getDamage() {
        return damage;
    }

    public Map<Enchantment, Integer> getEnchantments() {
        return enchantments;
    }

    public List<ItemFlag> getItemFlags() {
        return itemFlags;
    }

    public String getSkin() {
        return skin;
    }

    public Color getColor() {
        return color;
    }

    public ItemStack build(Player player) {
        ItemStack itemStack = new ItemStack(material, amount);
        itemStack.setDurability((short) damage);

        ItemMeta itemMeta = itemStack.getItemMeta();

        String translatedName = Chat.replaceFormatVariables(tl(translatableName, player, nameVariables));
        List<String> translatedLores = new ExtendedList<>();

        for (KeyPair<String, String[]> translatableLore : translatableLores) {
            translatedLores.add(Chat.replaceFormatVariables(tl(translatableLore.getKey(), player, translatableLore.getValue())));
        }

        itemMeta.setDisplayName(translatedName);
        itemMeta.setLore(translatedLores);

        for (Map.Entry<Enchantment, Integer> enchantment : enchantments.entrySet()) {
            itemMeta.addEnchant(enchantment.getKey(), enchantment.getValue(), true);
        }

        itemMeta.addItemFlags(itemFlags.toArray(new ItemFlag[0]));

        if (skin != null && itemMeta instanceof SkullMeta) {
            SkullMeta skullMeta = (SkullMeta) itemMeta;
            skullMeta.setOwner(skin);
        }

        if (color != null && itemMeta instanceof LeatherArmorMeta) {
            LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) itemMeta;
            leatherArmorMeta.setColor(color);
        }

        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }
}
