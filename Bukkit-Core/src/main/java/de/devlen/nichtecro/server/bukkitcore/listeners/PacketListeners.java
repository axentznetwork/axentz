package de.devlen.nichtecro.server.bukkitcore.listeners;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import de.devlen.nichtecro.server.bukkitcore.BukkitCore;
import de.devlen.nichtecro.server.bukkitcore.events.ClientSettingsChangeEvent;
import de.devlen.nichtecro.server.bukkitcore.packetwrapper.WrapperPlayClientSettings;

/**
 * Created by Tarik on 26.05.2016.
 */
public class PacketListeners {

    public static void registerClientSettingsPacketListener() {
        ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(BukkitCore.getInstance(), WrapperPlayClientSettings.TYPE) {
            @Override
            public void onPacketReceiving(PacketEvent event) {
                WrapperPlayClientSettings wrapperPlayClientSettings = new WrapperPlayClientSettings(event.getPacket());
                new ClientSettingsChangeEvent(event.getPlayer(), wrapperPlayClientSettings.getLocale()).call();
            }
        });
    }

}
