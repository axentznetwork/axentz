package de.devlen.nichtecro.server.bukkitcore.disguise;

import org.bukkit.entity.EntityType;

public enum DisguiseType
{
    NONE(-1, "Nichts", EntityType.UNKNOWN),
    BOAT(1, "Boot", EntityType.BOAT),
    ITEM_STACK(2, "Item", EntityType.DROPPED_ITEM),
    MINECART(10, "Lore", EntityType.MINECART),
    ACTIVATED_TNT(50, "Gezündetes TNT", EntityType.PRIMED_TNT),
    ENDER_CRYSTAL(51, "Enderkristall", EntityType.ENDER_CRYSTAL),
    ARROW(60, "Pfeil", EntityType.ARROW),
    SNOWBALL(61, "Schneeball", EntityType.SNOWBALL),
    EGG(62, "Ei", EntityType.EGG),
    FIRE_BALL(63, "Feuerball", EntityType.FIREBALL),
    FIRE_CHARGE(64, "Feuerball", EntityType.SMALL_FIREBALL),
    ENDER_PEARL(65, "Enderperle", EntityType.ENDER_PEARL),
    WITHER_SKULL(66, "Witherschädel", EntityType.WITHER_SKULL),
    FALLING_OBJECT(70, "Fallendes Objekt", EntityType.FALLING_BLOCK),
    ITEM_FRAME(71, "Itemrahmen", EntityType.ITEM_FRAME),
    EYE_OF_ENDER(72, "Enderauge", EntityType.UNKNOWN),
    POTION(73, "Wurftrank", EntityType.SPLASH_POTION),
    FALLING_DRAGON_EGG(74, "fallendes Drachenei", EntityType.UNKNOWN),
    EXP_BOTTLE(75, "XP-Flasche", EntityType.THROWN_EXP_BOTTLE),
    FIREWORK_ROCKET(76, "Feuerwerksrakete", EntityType.FIREWORK),
    LEASH_KNOT(77, "Leinenknoten", EntityType.LEASH_HITCH),
    ARMOR_STAND(78, "Rüstungsständer", EntityType.ARMOR_STAND),
    FISHING_FLOAT(90, "Fischerhaken", EntityType.FISHING_HOOK),
    MOB(48, "Mob", EntityType.UNKNOWN),
    MONSTER(49, "Monster", EntityType.UNKNOWN),
    CREEPER(50, "Creeper", EntityType.CREEPER),
    SKELETON(51, "Skellett", EntityType.SKELETON),
    SPIDER(52, "Spinne", EntityType.SPIDER),
    GIANT_ZOMBIE(53, "Gigant", EntityType.GIANT),
    ZOMBIE(54, "Zombie", EntityType.ZOMBIE),
    SLIME(55, "Schleim", EntityType.SLIME),
    GHAST(56, "Ghast", EntityType.GHAST),
    ZOMBIE_PIGMAN(57, "Zombie Pigman", EntityType.PIG_ZOMBIE),
    ENDERMAN(58, "Enderman", EntityType.ENDERMAN),
    CAVE_SPIDER(59, "Höhlenspinne", EntityType.CAVE_SPIDER),
    SILVERFISH(60, "Silberfisch", EntityType.SILVERFISH),
    BLAZE(61, "Lohe", EntityType.BLAZE),
    MAGMA_CUBE(62, "Magmaschleim", EntityType.MAGMA_CUBE),
    ENDER_DRAGON(63, "Enderdrache", EntityType.ENDER_DRAGON),
    WITHER(64, "Wither", EntityType.WITHER),
    BAT(65, "Fledermaus", EntityType.BAT),
    WITCH(66, "Hexe", EntityType.WITCH),
    ENDERMITE(67, "Endermilbe", EntityType.ENDERMITE),
    GUARDIAN(68, "Wächter", EntityType.GUARDIAN),
    PIG(90, "Schwein", EntityType.PIG),
    SHEEP(91, "Schaaf", EntityType.SHEEP),
    COW(92, "Kuh", EntityType.COW),
    CHICKEN(93, "Huhn", EntityType.CHICKEN),
    SQUID(94, "Tintenfisch", EntityType.SQUID),
    WOLF(95, "Wolf", EntityType.WOLF),
    MUSHROOM(96, "Pilz", EntityType.MUSHROOM_COW),
    SNOWMAN(97, "Schneemann", EntityType.SNOWMAN),
    OCELOT(98, "Katze", EntityType.OCELOT),
    IRON_GOLEM(99, "Eisengolem", EntityType.IRON_GOLEM),
    HORSE(100, "Pferd", EntityType.HORSE),
    RABBIT(101, "Hase", EntityType.RABBIT),
    VILLAGER(120, "Dorfbewohner", EntityType.VILLAGER);
    
    private int id;
    private String name;
    private EntityType entityType;
    
    private DisguiseType(final int id, final String name, final EntityType entityType) {
        this.id = id;
        this.name = name;
        this.entityType = entityType;
    }
    
    public int getId() {
        return this.id;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public EntityType getEntityType() {
        return this.entityType;
    }
    
    public void setEntityType(final EntityType entityType) {
        this.entityType = entityType;
    }
}
