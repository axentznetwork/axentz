package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class GuardianMetadata extends MonsterMetadata {

    /**
     * 0x02 - is retracting spikes
     * 0x04 - is elderly
     * @param guardianFlags
     */
    public void setGuardianFlags(byte guardianFlags){
        setMeta(11, guardianFlags);
    }

    public byte getGuardianFlags(){
        return (byte) getMeta(11);
    }

    public void setTargetEntityId(int targetEntityId){
        setMeta(12, targetEntityId);
    }

    public int getTargetEntityId(){
        return (int) getMeta(12);
    }

}
