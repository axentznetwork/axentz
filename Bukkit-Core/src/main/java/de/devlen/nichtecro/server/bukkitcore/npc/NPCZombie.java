package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.ZombieMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCZombie extends NPCMonster {

    public NPCZombie(int entityId, Location location) {
        super(entityId, EntityType.ZOMBIE, location);
    }

    @Override
    public ZombieMetadata getEntityMetadata() {
        return (ZombieMetadata) super.getEntityMetadata();
    }
}
