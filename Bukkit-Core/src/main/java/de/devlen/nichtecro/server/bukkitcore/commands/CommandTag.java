package de.devlen.nichtecro.server.bukkitcore.commands;

import de.devlen.nichtecro.server.bukkitcore.comfy.CommandListener;
import de.devlen.nichtecro.server.bukkitcore.comfy.annotation.Arg;
import de.devlen.nichtecro.server.bukkitcore.comfy.annotation.CommandHandler;
import de.devlen.nichtecro.server.bukkitcore.comfy.bukkit.BukkitCommandContext;
import de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager;
import de.devlen.nichtecro.server.bukkitcore.manager.Rank;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import de.devlen.nichtecro.server.bukkitcore.validation.Validate;
import org.bukkit.entity.Player;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Tarik on 03.06.2016.
 */
public class CommandTag implements CommandListener {

    @CommandHandler("bukkit.core.command.tag.rank")
    public void onCommandTagRank(BukkitCommandContext c, @Arg("rank") String rankName) {
        if (c.getSender().isPlayer()) {
            Player player = (Player) c.getSender().getSender();
            List<Rank> ranks = MPlayerSettingsManager.getRanks(player);
            if (Validate.isRank(rankName)) {
                Rank rank = Rank.valueOf(rankName.toUpperCase());
                if (ranks.contains(rank)) {
                    MPlayerSettingsManager.setDisguisedRank(player, rank);
                    Rank.setTagPrefix(player, rank);
                    Chat.sendTl(player, "bukkit.core.tag.tag", "bukkit.core.tag.changed.successfully", Chat.normalizeEnumValueName(rank));
                } else {
                    String possibleRanks = "";
                    Iterator<Rank> rankIterator = ranks.iterator();
                    while (rankIterator.hasNext()) {
                        Rank rankTarget = rankIterator.next();
                        possibleRanks += "%l" + Chat.normalizeEnumValueName(rankTarget);
                        if (rankIterator.hasNext())
                            possibleRanks += "%u, ";
                    }

                    Chat.sendTl(player, "bukkit.core.tag.tag", "bukkit.core.tag.changed.failed", Chat.normalizeEnumValueName(rank));
                    Chat.sendTl(player, "bukkit.core.tag.tag", "bukkit.core.tag.possibilities", possibleRanks);
                }
            } else if (rankName.equalsIgnoreCase("remove")) {
                MPlayerSettingsManager.setDisguisedRank(player, null);
                Rank.setTagPrefix(player, Rank.getHighestRank(MPlayerSettingsManager.getRanks(player)));
                Chat.sendTl(player, "bukkit.core.tag.tag", "bukkit.core.tag.changed.removed");
            } else {
                String possibleRanks = "";
                Iterator<Rank> rankIterator = ranks.iterator();
                while (rankIterator.hasNext()) {
                    Rank rankTarget = rankIterator.next();
                    possibleRanks += "%l" + Chat.normalizeEnumValueName(rankTarget);
                    if (rankIterator.hasNext())
                        possibleRanks += "%u, ";
                }

                possibleRanks += ", %lremove";

                Chat.sendTl(player, "bukkit.core.tag.tag", "bukkit.core.tag.not.rank", rankName);
                Chat.sendTl(player, "bukkit.core.tag.tag", "bukkit.core.tag.possibilities", possibleRanks);
            }
        } else {
            c.getSender().warning("Sorry, this command is only available to player, ingame!");
        }
    }

}
