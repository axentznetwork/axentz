package de.devlen.nichtecro.server.bukkitcore.commands;

import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.PlayerInfoData;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import de.devlen.nichtecro.server.bukkitcore.comfy.CommandListener;
import de.devlen.nichtecro.server.bukkitcore.comfy.annotation.CommandHandler;
import de.devlen.nichtecro.server.bukkitcore.comfy.bukkit.BukkitCommandContext;
import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import de.devlen.nichtecro.server.bukkitcore.packetwrapper.WrapperPlayServerPlayerInfo;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by Tarik on 27.05.2016.
 */
public class CommandTest implements CommandListener {

    public static List<Location> speedingLocationsCentres = new ExtendedList<>();

    @CommandHandler("command.test")
    public void onCommandTest(BukkitCommandContext c) {

        if (c.getSender().isPlayer()) {
            Player player = (Player) c.getSender().getSender();
            if (player.getName().equalsIgnoreCase("DevLen_")) {
//                makeCylinderNorthSouth(player.getLocation(), Material.WOOL, 6, 6, 1, false);
//                speedingLocationsCentres.add(player.getLocation());
//                Inventory inventory = Bukkit.createInventory(null, InventoryType.HOPPER, "test");
//                inventory.setItem(2, new ItemStack(Material.STONE, 10));
//                player.openInventory(inventory);
//                Chat.send(player, "test", "chat mute value: " + MPlayerSettingsManager.getChatVisibilityType(player));
                WrapperPlayServerPlayerInfo wrapperPlayServerPlayerInfo = new WrapperPlayServerPlayerInfo();
                wrapperPlayServerPlayerInfo.setAction(EnumWrappers.PlayerInfoAction.ADD_PLAYER);
                wrapperPlayServerPlayerInfo.setData(new ExtendedList<>(new PlayerInfoData(new WrappedGameProfile(Bukkit.getOfflinePlayer("jeb_").getUniqueId(), "jeb_"), 1, EnumWrappers.NativeGameMode.ADVENTURE, WrappedChatComponent.fromText("jeb_"))));
                wrapperPlayServerPlayerInfo.sendPacket(player);
            }
        }
    }

    public int makeCylinderNorthSouth(Location pos, Material block, double radiusX, double radiusZ, int height, boolean filled) {
        int affected = 0;

        radiusX += 0.5;
        radiusZ += 0.5;

        if (height == 0) {
            return 0;
        } else if (height < 0) {
            height = -height;
            pos = pos.subtract(0, height, 0);
        }

        if (pos.getBlockY() < 0) {
            pos.setY(0);
        } else if (pos.getBlockY() + height - 1 > 256) {
            height = 256 - pos.getBlockY() + 1;
        }

        final double invRadiusX = 1 / radiusX;
        final double invRadiusZ = 1 / radiusZ;

        final int ceilRadiusX = (int) Math.ceil(radiusX);
        final int ceilRadiusZ = (int) Math.ceil(radiusZ);

        double nextXn = 0;
        forX:
        for (int x = 0; x <= ceilRadiusX; ++x) {
            final double xn = nextXn;
            nextXn = (x + 1) * invRadiusX;
            double nextZn = 0;
            forZ:
            for (int z = 0; z <= ceilRadiusZ; ++z) {
                final double zn = nextZn;
                nextZn = (z + 1) * invRadiusZ;

                double distanceSq = lengthSq(xn, zn);
                if (distanceSq > 1) {
                    if (z == 0) {
                        break forX;
                    }
                    break forZ;
                }

                if (!filled) {
                    if (lengthSq(nextXn, zn) <= 1 && lengthSq(xn, nextZn) <= 1) {
                        continue;
                    }
                }

                for (int y = 0; y < height; ++y) {
                    pos.add(x, z, y).getBlock().setType(block);
                    pos.add(-x, -z, -y);

                    pos.add(-x, z, y).getBlock().setType(block);
                    pos.add(x, -z, -y);

                    pos.add(x, z, -y).getBlock().setType(block);
                    pos.add(-x, -z, y);

                    pos.add(-x, z, -y).getBlock().setType(block);
                    pos.add(x, -z, y);

                    pos.add(x, -z, y).getBlock().setType(block);
                    pos.add(-x, z, -y);

                    pos.add(-x, -z, y).getBlock().setType(block);
                    pos.add(x, z, -y);

                    pos.add(x, -z, -y).getBlock().setType(block);
                    pos.add(-x, z, y);

                    pos.add(-x, -z, -y).getBlock().setType(block);
                    pos.add(x, z, y);
                }
            }
        }

        return affected;
    }

    public int makeCylinderEastWest(Location pos, Material block, double radiusX, double radiusZ, int height, boolean filled) {
        int affected = 0;

        radiusX += 0.5;
        radiusZ += 0.5;

        if (height == 0) {
            return 0;
        } else if (height < 0) {
            height = -height;
            pos = pos.subtract(0, height, 0);
        }

        if (pos.getBlockY() < 0) {
            pos.setY(0);
        } else if (pos.getBlockY() + height - 1 > 256) {
            height = 256 - pos.getBlockY() + 1;
        }

        final double invRadiusX = 1 / radiusX;
        final double invRadiusZ = 1 / radiusZ;

        final int ceilRadiusX = (int) Math.ceil(radiusX);
        final int ceilRadiusZ = (int) Math.ceil(radiusZ);

        double nextXn = 0;
        forX:
        for (int x = 0; x <= ceilRadiusX; ++x) {
            final double xn = nextXn;
            nextXn = (x + 1) * invRadiusX;
            double nextZn = 0;
            forZ:
            for (int z = 0; z <= ceilRadiusZ; ++z) {
                final double zn = nextZn;
                nextZn = (z + 1) * invRadiusZ;

                double distanceSq = lengthSq(xn, zn);
                if (distanceSq > 1) {
                    if (z == 0) {
                        break forX;
                    }
                    break forZ;
                }

                if (!filled) {
                    if (lengthSq(nextXn, zn) <= 1 && lengthSq(xn, nextZn) <= 1) {
                        continue;
                    }
                }

                for (int y = 0; y < height; ++y) {
                    pos.add(y, x, z).getBlock().setType(block);
                    pos.add(-y, -x, -z);

                    pos.add(-y, x, z).getBlock().setType(block);
                    pos.add(y, -x, -z);

                    pos.add(y, x, -z).getBlock().setType(block);
                    pos.add(-y, -x, z);

                    pos.add(-y, x, -z).getBlock().setType(block);
                    pos.add(y, -x, z);

                    pos.add(y, -x, z).getBlock().setType(block);
                    pos.add(-y, x, -z);

                    pos.add(-y, -x, z).getBlock().setType(block);
                    pos.add(y, x, -z);

                    pos.add(y, -x, -z).getBlock().setType(block);
                    pos.add(-y, x, z);

                    pos.add(-y, -x, -z).getBlock().setType(block);
                    pos.add(y, x, z);
                }
            }
        }

        return affected;
    }

    public int makeCylinderUpDown(Location pos, Material block, double radiusX, double radiusZ, int height, boolean filled) {
        int affected = 0;

        radiusX += 0.5;
        radiusZ += 0.5;

        if (height == 0) {
            return 0;
        } else if (height < 0) {
            height = -height;
            pos = pos.subtract(0, height, 0);
        }

        if (pos.getBlockY() < 0) {
            pos.setY(0);
        } else if (pos.getBlockY() + height - 1 > 256) {
            height = 256 - pos.getBlockY() + 1;
        }

        final double invRadiusX = 1 / radiusX;
        final double invRadiusZ = 1 / radiusZ;

        final int ceilRadiusX = (int) Math.ceil(radiusX);
        final int ceilRadiusZ = (int) Math.ceil(radiusZ);

        double nextXn = 0;
        forX:
        for (int x = 0; x <= ceilRadiusX; ++x) {
            final double xn = nextXn;
            nextXn = (x + 1) * invRadiusX;
            double nextZn = 0;
            forZ:
            for (int z = 0; z <= ceilRadiusZ; ++z) {
                final double zn = nextZn;
                nextZn = (z + 1) * invRadiusZ;

                double distanceSq = lengthSq(xn, zn);
                if (distanceSq > 1) {
                    if (z == 0) {
                        break forX;
                    }
                    break forZ;
                }

                if (!filled) {
                    if (lengthSq(nextXn, zn) <= 1 && lengthSq(xn, nextZn) <= 1) {
                        continue;
                    }
                }

                for (int y = 0; y < height; ++y) {
                    pos.add(x, y, z).getBlock().setType(block);
                    pos.add(-x, -y, -z);

                    pos.add(-x, y, z).getBlock().setType(block);
                    pos.add(x, -y, -z);

                    pos.add(x, y, -z).getBlock().setType(block);
                    pos.add(-x, -y, z);

                    pos.add(-x, y, -z).getBlock().setType(block);
                    pos.add(x, -y, z);
                }
            }
        }

        return affected;
    }


    private static double lengthSq(double x, double y, double z) {
        return (x * x) + (y * y) + (z * z);
    }

    private static double lengthSq(double x, double z) {
        return (x * x) + (z * z);
    }
}
