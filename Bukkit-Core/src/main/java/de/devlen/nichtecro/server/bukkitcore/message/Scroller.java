package de.devlen.nichtecro.server.bukkitcore.message;

import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;

import java.util.List;

/**
 * Created by Tarik on 30.05.2016.
 */
public class Scroller {

    private String text = "";
    private int width = 0;
    private int spaceBetween = 0;
    private int step = 0;
    private List<String> scrolling = null;

    public Scroller(String text, int width, int spaceBetween) {
        this.text = text;
        this.width = width;
        this.spaceBetween = spaceBetween;
        scrolling = scroll(text, width, spaceBetween);
    }

    public String next() {
        return scrolling.get(step++ % scrolling.size());
    }

    private List<String> scroll(String string, int width, int spaceBetween) {
        List<String> list = new ExtendedList<>();
        if (string.length() < width) {
            StringBuilder stringBuilder = new StringBuilder(string);
            while (stringBuilder.length() < width) {
                stringBuilder.append(" ");
            }
            string = stringBuilder.toString();
        }

        if (width < 1) {
            width = 1;
        }

        if (spaceBetween < 0) {
            spaceBetween = 0;
        }

        for (int i = 0; i < string.length() - width; i++) {
            list.add(string.substring(i, i + width));
        }

        StringBuilder space = new StringBuilder();
        for (int i = 0; i < spaceBetween; i++) {
            list.add(string.substring(string.length() - width + (i > width ? width : i), string.length()) + space);
            if (space.length() < width) {
                space.append(" ");
            }
        }

        for (int i = 0; i < width - spaceBetween; i++) {
            list.add(string.substring(string.length() - width + spaceBetween + i, string.length()) + space + string.substring(0, i));
        }

        for (int i = 0; i < spaceBetween; i++) {
            if (i > space.length())
                break;
            list.add(space.substring(0, space.length() - i) + string.substring(0, width - (spaceBetween > width ? width : spaceBetween) + i));
        }

        return list;
    }

}
