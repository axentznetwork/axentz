package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class TippedArrowMetadata extends ArrowMetadata {

    public void setColor(int color){
        setMeta(6, color);
    }

    public int getColor(){
        return (int) getMeta(6);
    }

}
