package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.GolemMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCGolem extends NPCCreature {

    public NPCGolem(int entityId, EntityType entityType, Location location) {
        super(entityId, entityType, location);
    }

    @Override
    public GolemMetadata getEntityMetadata() {
        return (GolemMetadata) super.getEntityMetadata();
    }
}
