package de.devlen.nichtecro.server.bukkitcore.metadata;

import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import com.google.common.base.Optional;
import de.devlen.nichtecro.server.bukkitcore.items.NBTReflectionUtil;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

/**
 * Created by Tarik on 31.03.2016.
 */
public class EntityMetadata {

    private WrappedDataWatcher wrappedDataWatcher;

    public EntityMetadata newEntityMetadata() {
        return new EntityMetadata();
    }

    public void setAir(int air) {
        setMeta(1, air);
    }

    public int getAir() {
        return (int) getMeta(1);
    }

    public void setCustomName(String customName) {
        setMeta(2, customName);
    }


    public String getCustomName() {
        return (String) getMeta(2);
    }

    public void setCustomNameVisible(boolean customNameVisible) {
        setMeta(3, customNameVisible);
    }

    public boolean isCustomNameVisible() {
        return (boolean) getMeta(3);
    }

    public void setSilent(boolean silent) {
        setMeta(4, silent);
    }

    public boolean isSilent() {
        return (boolean) getMeta(4);
    }

    protected void setMeta(int index, Object o) {
        WrappedDataWatcher.Serializer serializer = null;
        if (o instanceof Integer) {
            serializer = WrappedDataWatcher.Registry.get(Integer.class);
        } else if (o instanceof Byte) {
            serializer = WrappedDataWatcher.Registry.get(Byte.class);
        } else if (o instanceof String) {
            serializer = WrappedDataWatcher.Registry.get(String.class);
        } else if (o instanceof Boolean) {
            serializer = WrappedDataWatcher.Registry.get(Boolean.class);
        } else if (o instanceof Float) {
            serializer = WrappedDataWatcher.Registry.get(Float.class);
        } else if (o instanceof UUID) {
            serializer = WrappedDataWatcher.Registry.get(UUID.class);
        } else if (o instanceof ItemStack) {
            serializer = WrappedDataWatcher.Registry.get(NBTReflectionUtil.nmsItemStackClass());
            o = Optional.of(NBTReflectionUtil.getNMSItemStack((ItemStack) o));
        }
        this.wrappedDataWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(index, serializer), o);
    }

    protected Object getMeta(int index) {
        return this.wrappedDataWatcher.getObject(index);
    }

    public boolean getFlag(int index, int mask) {
        return ((int) this.getMeta(index) & mask) != 0x0;
    }

    public void setFlag(int index, byte maskSum) {
        this.setMeta(index, maskSum);
    }

    public WrappedDataWatcher getWrappedDataWatcher() {
        return wrappedDataWatcher;
    }

    public void setWrappedDataWatcher(WrappedDataWatcher wrappedDataWatcher) {
        this.wrappedDataWatcher = wrappedDataWatcher;
    }

    @Deprecated
    public void setMetadata(WrappedDataWatcher wrappedDataWatcher) {
        setWrappedDataWatcher(wrappedDataWatcher);
    }

    @Deprecated
    public WrappedDataWatcher getMetadata() {
        return getWrappedDataWatcher();
    }
}
