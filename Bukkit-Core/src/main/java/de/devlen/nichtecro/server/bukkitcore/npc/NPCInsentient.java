package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.InsentientMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCInsentient extends NPCLiving {

    public NPCInsentient(int entityId, EntityType entityType, Location location) {
        super(entityId, entityType, location);
    }

    @Override
    public InsentientMetadata getEntityMetadata() {
        return (InsentientMetadata) super.getEntityMetadata();
    }
}
