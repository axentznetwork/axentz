package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.SkeletonMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCSkeleton extends NPCMonster {

    public NPCSkeleton(int entityId, Location location) {
        super(entityId, EntityType.SKELETON, location);
    }

    @Override
    public SkeletonMetadata getEntityMetadata() {
        return (SkeletonMetadata) super.getEntityMetadata();
    }
}
