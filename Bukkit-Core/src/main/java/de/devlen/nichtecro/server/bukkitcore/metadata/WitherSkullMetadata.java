package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class WitherSkullMetadata extends FireballMetadata {

    public void setInvulnerable(boolean invulnerable){
        setMeta(5, invulnerable);
    }

    public boolean getInvulnerable(){
        return (boolean) getMeta(5);
    }

}
