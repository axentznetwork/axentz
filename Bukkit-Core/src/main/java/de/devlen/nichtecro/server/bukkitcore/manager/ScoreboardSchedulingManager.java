package de.devlen.nichtecro.server.bukkitcore.manager;

import com.google.common.collect.Maps;
import de.devlen.nichtecro.server.bukkitcore.BukkitCore;
import de.devlen.nichtecro.server.bukkitcore.message.Scroller;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.Map;
import java.util.UUID;

import static de.devlen.nichtecro.server.bukkitcore.language.Language.tl;

/**
 * Created by Tarik on 14.06.2016.
 */
public class ScoreboardSchedulingManager {

    private static Map<UUID, Integer> scoreboardTasks = Maps.newHashMap();

    public static void registerFurtherPlayers(Scoreboard scoreboard) {
        for(Player player : Bukkit.getOnlinePlayers()){
            Rank rank = MPlayerSettingsManager.getDisguisedRank(player);
            Team team = scoreboard.getTeam(rank.getTeamName());
            if (team == null) {
                team = scoreboard.registerNewTeam(rank.getTeamName());
            }
            team.addEntry(player.getName());
        }
    }

    public static void runScoreboardScrollTask(Player player) {
        if (!scoreboardTasks.containsKey(player.getUniqueId())) {
            final Scoreboard scoreboard = player.getScoreboard();
            final Map<Rank, Scroller> scrollerMap = Maps.newHashMap();

            for (final Rank rank : Rank.values()) {
                String tl = tl(rank.getTagPrefixNode(), player);
                if (tl.length() > 0) {
                    scrollerMap.put(rank, new Scroller(tl, 7, 3));
                } else {
                    Team team = scoreboard.getTeam(rank.getTeamName());
                    if (team == null) {
                        team = scoreboard.registerNewTeam(rank.getTeamName());
                    }
                    team.setPrefix("�7");
                }
            }

            scoreboardTasks.put(player.getUniqueId(), Bukkit.getScheduler().scheduleSyncRepeatingTask(BukkitCore.getInstance(), new Runnable() {
                @Override
                public void run() {
                    for (Map.Entry<Rank, Scroller> scrollerEntry : scrollerMap.entrySet()) {
                        Team team = scoreboard.getTeam(scrollerEntry.getKey().getTeamName());
                        if (team == null) {
                            team = scoreboard.registerNewTeam(scrollerEntry.getKey().getTeamName());
                        }
                        team.setPrefix(scrollerEntry.getKey().getFormatParamters() + scrollerEntry.getValue().next() + " �7");
                    }
                }
            }, 0, 4));
        } else {
            removeScoreboardScrollTask(player);
            runScoreboardScrollTask(player);
        }
    }

    public static void removeScoreboardScrollTask(Player player) {
        if (scoreboardTasks.containsKey(player.getUniqueId())) {
            Bukkit.getScheduler().cancelTask(scoreboardTasks.get(player.getUniqueId()));
            scoreboardTasks.remove(player.getUniqueId());
        }
    }

}
