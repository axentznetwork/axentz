package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class TameableAnimalMetadata extends AnimalMetadata {

    /**
     * 0x01 - is sitting
     * 0x04 - is tamed
     * @param tableableAnimalFlags
     */
    public void setTableableAnimalFlags(byte tableableAnimalFlags) {
        setMeta(12, tableableAnimalFlags);
    }

    public byte getTameableAnimalFlags(){
        return (byte) getMeta(12);
    }

}
