package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.RabbitMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCRabbit extends NPCAnimal {

    public NPCRabbit(int entityId, Location location) {
        super(entityId, EntityType.RABBIT, location);
    }

    @Override
    public RabbitMetadata getEntityMetadata() {
        return (RabbitMetadata) super.getEntityMetadata();
    }
}
