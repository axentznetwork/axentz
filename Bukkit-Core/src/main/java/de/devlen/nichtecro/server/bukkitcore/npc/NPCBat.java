package de.devlen.nichtecro.server.bukkitcore.npc;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCBat extends NPCInsentient {

    public NPCBat(int entityId, Location location) {
        super(entityId, EntityType.BAT, location);
    }
}
