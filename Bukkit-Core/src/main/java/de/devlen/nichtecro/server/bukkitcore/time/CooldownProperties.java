package de.devlen.nichtecro.server.bukkitcore.time;

/**
 * Created by Tarik on 31.05.2016.
 */
public class CooldownProperties {

    private String tag = "";
    private long startedMillis = 0;
    private long lengthMillis = 0;

    public CooldownProperties(String tag, long startedMillis, long lengthMillis) {
        this.tag = tag;
        this.startedMillis = startedMillis;
        this.lengthMillis = lengthMillis;
    }

    public String getTag() {
        return tag;
    }

    public long getStartedMillis() {
        return startedMillis;
    }

    public long getLengthMillis() {
        return lengthMillis;
    }
}
