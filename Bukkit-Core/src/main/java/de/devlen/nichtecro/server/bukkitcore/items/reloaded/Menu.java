package de.devlen.nichtecro.server.bukkitcore.items.reloaded;

import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import de.devlen.nichtecro.server.bukkitcore.items.NBTItem;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;

import static de.devlen.nichtecro.server.bukkitcore.language.Language.tl;

/**
 * Created by Tarik on 04.06.2016.
 */
public class Menu {

    private String translatableTitle = "";
    private int lines = 1;
    private InventoryType inventoryType = InventoryType.CHEST;
    private List<Button> buttonList = new ExtendedList<>();

    public Menu titleNode(String titleNode) {
        this.translatableTitle = titleNode;
        return this;
    }

    public Menu lines(int lines) {
        this.lines = lines;
        return this;
    }

    public Menu type(InventoryType inventoryType) {
        this.inventoryType = inventoryType;
        return this;
    }

    public Menu buttons(Button... buttons) {
        for (Button button : buttons) {
            buttonList.add(button);
        }
        return this;
    }

    public void openMenu(Player player) {
        Inventory inventory;
        if (inventoryType != null && inventoryType != InventoryType.CHEST) {
            inventory = Bukkit.createInventory(null, inventoryType, Chat.replaceFormatVariables(tl(translatableTitle, player)));
        } else {
            inventory = Bukkit.createInventory(null, lines * 9, Chat.replaceFormatVariables(tl(translatableTitle, player)));
        }

        for (Button button : buttonList) {
            ItemStack itemStack = button.getButtonFactory().build(player);
            NBTItem nbtItem = new NBTItem(itemStack);
            nbtItem.setString("server_name_id", button.getNameId());
            inventory.setItem(button.getSlot(), nbtItem.getItem());
            MenuManager.putClickHandler(button.getNameId(), button.getClickHandler());
        }
        player.openInventory(inventory);
    }

}
