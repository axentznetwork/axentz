package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class FishingHookMetadata extends EntityMetadata {

    public void setHookedEntityId(int id){
        setMeta(5, id);
    }

    public int getHookedEntityId(){
        return (int) getMeta(5);
    }

}
