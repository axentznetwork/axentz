package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class PlayerMetadata extends LivingMetadata {

    public void setAdditionalHearts(float additionalHearts) {
        setMeta(10, additionalHearts);
    }

    public int getAdditionalHearts() {
        return (int) getMeta(10);
    }

    public void setScore(int score) {
        setMeta(11, score);
    }

    public int getScore() {
        return (int) getMeta(11);
    }

    public void setSkinFlags(byte skinFlags) {
        setMeta(12, skinFlags);
    }

    public byte getSkinFlags(){
        return (byte) getMeta(12);
    }

    /**
     * Left - 0
     * Right - 1
     */
    public void setMainHand(byte mainHand){
        setMeta(13, mainHand);
    }

    public byte getMainHand(){
        return (byte) getMeta(13);
    }

}
