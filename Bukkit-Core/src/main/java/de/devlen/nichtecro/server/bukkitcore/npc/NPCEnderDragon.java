package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.EnderDragonMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCEnderDragon extends NPCInsentient {

    public NPCEnderDragon(int entityId, Location location) {
        super(entityId, EntityType.ENDER_DRAGON, location);
    }

    @Override
    public EnderDragonMetadata getEntityMetadata() {
        return (EnderDragonMetadata) super.getEntityMetadata();
    }
}
