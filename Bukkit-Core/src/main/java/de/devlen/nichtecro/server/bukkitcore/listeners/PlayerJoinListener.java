package de.devlen.nichtecro.server.bukkitcore.listeners;

import de.devlen.nichtecro.server.bukkitcore.BukkitCore;
import de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager;
import de.devlen.nichtecro.server.bukkitcore.manager.PermissionManager;
import de.devlen.nichtecro.server.bukkitcore.manager.Rank;
import de.devlen.nichtecro.server.bukkitcore.manager.ScoreboardSchedulingManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.ArrayList;

/**
 * Created by Tarik on 14.05.2016.
 */
public class PlayerJoinListener extends ListenerHelper {

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        MPlayerSettingsManager.insertNewPlayer(player);
        player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
        ScoreboardSchedulingManager.registerFurtherPlayers(player.getScoreboard());
        Rank.setTagPrefix(player, MPlayerSettingsManager.getDisguisedRank(player));
        ScoreboardSchedulingManager.runScoreboardScrollTask(player);
        PermissionManager.calculatePermissions(player);
        e.setJoinMessage(null);
        BukkitCore.setDisguiseAreas(player, new ArrayList());
    }

}
