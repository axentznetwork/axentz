package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.ItemMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 07.06.2016.
 */
public class NPCItem extends NPCEntity {

    public NPCItem(int entityId, Location location) {
        super(entityId, EntityType.DROPPED_ITEM, location);
    }

    @Override
    public ItemMetadata getEntityMetadata() {
        return (ItemMetadata) super.getEntityMetadata();
    }
}
