package de.devlen.nichtecro.server.bukkitcore.listeners;

import de.devlen.nichtecro.server.bukkitcore.manager.PermissionManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerKickEvent;

/**
 * Created by Tarik on 24.05.2016.
 */
public class PlayerKickListener extends ListenerHelper {

    @EventHandler
    public void onPlayerKick(PlayerKickEvent e) {
        Player player = e.getPlayer();
        PermissionManager.invalidatePermissions(player);
        e.setLeaveMessage(null);
    }

}
