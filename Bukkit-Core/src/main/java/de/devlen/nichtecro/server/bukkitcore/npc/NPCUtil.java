package de.devlen.nichtecro.server.bukkitcore.npc;

import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

public class NPCUtil {
    public static WrappedDataWatcher getDefaultWatcher(World world, EntityType type) {
        Entity entity;
        switch (type) {
            case DROPPED_ITEM:
                entity = world.dropItem(new Location(world, 0.0, 256.0, 0.0), new ItemStack(Material.STONE));
                break;
            case BOAT:

            default:
                entity = world.spawnEntity(new Location(world, 0.0, 256.0, 0.0), type);
                break;
        }
        WrappedDataWatcher watcher = WrappedDataWatcher.getEntityWatcher(entity).deepClone();
        entity.remove();
        return watcher;
    }
}
