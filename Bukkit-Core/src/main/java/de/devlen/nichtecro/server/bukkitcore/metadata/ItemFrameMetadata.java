package de.devlen.nichtecro.server.bukkitcore.metadata;

import org.bukkit.inventory.ItemStack;

/**
 * Created by Tarik on 31.03.2016.
 */
public class ItemFrameMetadata extends HangingMetadata {

    public void setItem(ItemStack itemStack){
        setMeta(5, itemStack);
    }

    public ItemStack getItem(){
        return (ItemStack) getMeta(5);
    }

    public void setRotation(int rotation){
        setMeta(6, rotation);
    }

    public int getRotation(){
        return (int) getMeta(6);
    }

}
