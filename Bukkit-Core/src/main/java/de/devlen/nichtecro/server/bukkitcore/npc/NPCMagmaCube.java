package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.SlimeMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCMagmaCube extends NPCInsentient {

    public NPCMagmaCube(int entityId, Location location) {
        super(entityId, EntityType.MAGMA_CUBE, location);
    }

    @Override
    public SlimeMetadata getEntityMetadata() {
        return (SlimeMetadata) super.getEntityMetadata();
    }

}
