package de.devlen.nichtecro.server.bukkitcore.comfy.exception;

public class HandlerException extends RuntimeException {
    public HandlerException(String msg) {
        super(msg);
    }
}
