package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class HorseMetadata extends AnimalMetadata {

    /**
     * 0x02 - is Tame
     * 0x04 - Unknown
     * 0x08 - has Chest
     * 0X10 - Unknown
     * 0x20 - eating
     * 0x04 - put on hind legs
     * 0x80 - mouth open
     *
     * @param horseFlags
     */
    public void setHorseFlags(byte horseFlags) {
        setMeta(12, horseFlags);
    }

    public byte getHorseFlags() {
        return (byte) getMeta(12);
    }

    public void setVariant(int variant) {
        setMeta(13, variant);
    }

    public int getVariant() {
        return (int) getMeta(13);
    }

    public void setColorStyle(int colorStyle) {
        setMeta(14, colorStyle);
    }

    public int getColorStyle() {
        return (int) getMeta(14);
    }

    public void setArmor(int armor) {
        setMeta(16, armor);
    }

    public int getArmor() {
        return (int) getMeta(16);
    }

}
