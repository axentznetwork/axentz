package de.devlen.nichtecro.server.bukkitcore.message;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.AbstractMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Tarik on 06.06.2016.
 */
public class CScoreboard {

    private Scoreboard scoreboard;
    private String objectiveName;
    private String title;
    private Map<String, Integer> scores;
    private List<Team> teams;

    public CScoreboard(Player player, String title, String objectiveName) {
        this.scoreboard = player.getScoreboard();
        this.title = title;
        this.scores = Maps.newLinkedHashMap();
        this.teams = Lists.newArrayList();
        this.objectiveName = objectiveName;
    }

    public void blankLine() {
        add("�r");
    }

    public void add(String text) {
        add(Chat.replaceFormatVariables(text), null);
    }

    public void add(String text, Integer score) {
        if (text.length() > 48) {
            text = text.substring(0, 47);
        }
        text = fixDuplicates(text);
        this.scores.put(Chat.replaceFormatVariables(text), score);
    }

    private String fixDuplicates(String text) {
        while (this.scores.containsKey(text)) {
            text = text + "�r";
        }
        if (text.length() > 48) {
            text = text.substring(0, 47);
        }
        return text;
    }

    private Map.Entry<Team, String> createTeam(String text) {
        String result;
        if (text.length() <= 16) {
            return new AbstractMap.SimpleEntry(null, text);
        }
        Team team = this.scoreboard.registerNewTeam("text-" + this.scoreboard.getTeams().size());
        Iterator<String> iterator = Splitter.fixedLength(16).split(text).iterator();
        team.setPrefix(iterator.next());
        result = iterator.next();
        if (text.length() > 32) {
            team.setSuffix(iterator.next());
        }
        this.teams.add(team);
        return new AbstractMap.SimpleEntry(team, result);
    }

    public void build() {
        String obj1 = this.objectiveName + "1";
        String obj2 = this.objectiveName + "2";

        Objective obj;
        Objective sbObj1 = this.scoreboard.getObjective(obj1);
        if (sbObj1 == null) {
            sbObj1 = this.scoreboard.registerNewObjective(obj1, "dummy");
        }

        Objective sbObj2 = this.scoreboard.getObjective(obj2);
        if (sbObj2 == null) {
            sbObj2 = this.scoreboard.registerNewObjective(obj2, "dummy");
        }

        if (this.scoreboard.getObjective(DisplaySlot.SIDEBAR) != null && this.scoreboard.getObjective(DisplaySlot.SIDEBAR).getName().equalsIgnoreCase(obj1)) {
            obj = sbObj2;
        } else {
            obj = sbObj1;
        }

        String line;
        for (String s : obj.getScoreboard().getEntries()) {
            line = s;
            obj.getScoreboard().resetScores(line);
        }
        this.title = Chat.replaceFormatVariables(this.title);
        if (this.title.length() > 32) {
            this.title = this.title.substring(0, 31);
        }
        obj.setDisplayName(this.title);
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);

        int index = this.scores.size();
        for (Map.Entry<String, Integer> text : this.scores.entrySet()) {
            Map.Entry<Team, String> team = createTeam(text.getKey());
            Integer score = text.getValue() != null ? text.getValue() : index;
            if (team.getKey() != null) {
                team.getKey().addEntry(team.getValue());
            }
            obj.getScore(team.getValue()).setScore(score);
            index--;
        }


    }

    public String getObjectiveName() {
        return this.objectiveName;
    }

    public void setObjectiveName(String objectiveName) {
        this.objectiveName = objectiveName;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void resetScores() {
        this.scores.clear();
    }

    public Scoreboard getScoreboard() {
        return this.scoreboard;
    }

    public void send(Player player) {
        player.setScoreboard(getScoreboard());
    }

}
