package de.devlen.nichtecro.server.bukkitcore.metadata;

import org.bukkit.Material;

/**
 * Created by Tarik on 31.03.2016.
 */
public class EndermanMetadata extends MonsterMetadata {

    public void setCarriedBlock(Material material, int dmg) {
        setMeta(11, material.getId() << 4 | dmg);
    }

    public int getCaririedBlock(){
        return (int) getMeta(11);
    }

    public void setScreaming(boolean screaming){
        setMeta(12, screaming);
    }

    public boolean isScreaming(){
        return (boolean) getMeta(12);
    }

}
