package de.devlen.nichtecro.server.bukkitcore.comfy.exception;

public class PathException extends Exception {
    public PathException(String msg) {
        super(msg);
    }
}
