package de.devlen.nichtecro.server.bukkitcore.manager;

import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Tarik on 21.05.2016.
 */
public class CPermission {

    private String node = "";
    private List<Rank> ranks = new ExtendedList<>(Rank.ADMINISTRATOR);
    private List<String> servers = Arrays.asList("");

    public CPermission(String node, List<Rank> ranks, List<String> servers) {
        this.node = node;
        this.ranks = ranks;
        this.servers = servers;
    }

    public String getNode() {
        return node;
    }

    public List<Rank> getRanks() {
        return ranks;
    }

    public List<String> getServers() {
        return servers;
    }
}
