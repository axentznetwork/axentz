package de.devlen.nichtecro.server.bukkitcore.message;

import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import de.devlen.nichtecro.server.bukkitcore.packetwrapper.WrapperPlayServerTitle;
import org.bukkit.entity.Player;

/**
 * Created by Tarik on 14.05.2016.
 */
public class Title {

    public static void send(Player player, String title, String subtitle, int in, int stay, int out) {
        WrapperPlayServerTitle wrapperPlayServerTitle = new WrapperPlayServerTitle();
        wrapperPlayServerTitle.setAction(EnumWrappers.TitleAction.TIMES);
        wrapperPlayServerTitle.setFadeIn(in);
        wrapperPlayServerTitle.setStay(stay);
        wrapperPlayServerTitle.setFadeOut(out);
        wrapperPlayServerTitle.sendPacket(player);

//        wrapperPlayServerTitle = new WrapperPlayServerTitle();
//        wrapperPlayServerTitle.setAction(EnumWrappers.TitleAction.SUBTITLE);
//        wrapperPlayServerTitle.setSubtitle(WrappedChatComponent.fromText(subtitle));
//        wrapperPlayServerTitle.sendPacket(player);

        wrapperPlayServerTitle = new WrapperPlayServerTitle();
        wrapperPlayServerTitle.setAction(EnumWrappers.TitleAction.TITLE);
        wrapperPlayServerTitle.setTitle(WrappedChatComponent.fromText(title));
        wrapperPlayServerTitle.sendPacket(player);
    }

}
