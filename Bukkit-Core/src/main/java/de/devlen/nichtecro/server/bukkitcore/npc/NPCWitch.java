package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.WitchMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCWitch extends NPCMonster {

    public NPCWitch(int entityId, Location location) {
        super(entityId, EntityType.WITCH, location);
    }

    @Override
    public WitchMetadata getEntityMetadata() {
        return (WitchMetadata) super.getEntityMetadata();
    }
}
