package de.devlen.nichtecro.server.bukkitcore.message;

import com.comphenix.protocol.wrappers.WrappedChatComponent;
import de.devlen.nichtecro.server.bukkitcore.packetwrapper.WrapperPlayServerPlayerListHeaderFooter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Iterator;

public class TabList {

    public static void send(Player player, ArrayList<String> header, ArrayList<String> footer) {
        String headerString = "";
        String footerString = "";
        Iterator<String> headerIterator = header.iterator();
        Iterator<String> footerIterator = footer.iterator();
        while (headerIterator.hasNext()) {
            String headerLine = headerIterator.next();
            headerString += headerLine;
            if (headerIterator.hasNext()) headerString += "\n";
        }
        while (footerIterator.hasNext()) {
            String footerLine = footerIterator.next();
            footerString += footerLine;
            if (footerIterator.hasNext()) footerString += "\n";
        }
        WrappedChatComponent wrappedChatComponentHeader = WrappedChatComponent.fromText(headerString);
        WrappedChatComponent wrappedChatComponentFooter = WrappedChatComponent.fromText(footerString);

        WrapperPlayServerPlayerListHeaderFooter playerListHeaderFooterPacket = new WrapperPlayServerPlayerListHeaderFooter();
        playerListHeaderFooterPacket.setHeader(wrappedChatComponentHeader);
        playerListHeaderFooterPacket.setFooter(wrappedChatComponentFooter);
        playerListHeaderFooterPacket.sendPacket(player);
    }
}
