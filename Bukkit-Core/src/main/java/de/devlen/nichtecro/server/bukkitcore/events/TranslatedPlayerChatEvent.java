package de.devlen.nichtecro.server.bukkitcore.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;

import java.util.Set;

/**
 * Created by Tarik on 03.06.2016.
 */
public class TranslatedPlayerChatEvent extends EventHelper implements Cancellable {

    private Player player = null;
    private String message = "";
    private boolean cancelled = false;
    private Set<Player> participants = null;

    public TranslatedPlayerChatEvent(Player player, String message, Set<Player> participants) {
        this.player = player;
        this.message = message;
        this.participants = participants;
    }

    public Player getPlayer() {
        return player;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }

    public Set<Player> getParticipants() {
        return participants;
    }
}
