package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class AgeableMetadata extends CreatureMetadata {

    public void setBaby(boolean baby){
        setMeta(11, baby);
    }

    public boolean isBaby(){
        return (boolean) getMeta(11);
    }

}
