package de.devlen.nichtecro.server.bukkitcore.npc;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCIronGolem extends NPCGolem {

    public NPCIronGolem(int entityId, Location location) {
        super(entityId, EntityType.IRON_GOLEM, location);
    }
}
