package de.devlen.nichtecro.server.bukkitcore.disguise;


import de.devlen.nichtecro.server.bukkitcore.npc.INPC;
import org.bukkit.entity.Player;

public class Disguise<T extends INPC> {
    private Player player;
    private T npc;

    public Disguise(Player player, T npc) {
        this.player = player;
        this.npc = npc;
    }

    public Player getPlayer() {
        return this.player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public T getNpc() {
        return this.npc;
    }

    public void setNpc(T npc) {
        this.npc = npc;
    }

    public static void send(Player player, Player packetReceiver) {
        Disguise disguise = DisguiseManager.getDisguise(player);
        if (!player.equals(packetReceiver) && disguise != null) {
            disguise.getNpc().spawn(packetReceiver);
        }
    }

    @Override
    public String toString() {
        return "Disguise{player=" + this.player + ", npc=" + this.npc + ", npcIsNull=" + (this.npc == null) + '}';
    }
}
