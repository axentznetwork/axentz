package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.VillagerMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCVillager extends NPCAgeable {

    public NPCVillager(int entityId, Location location) {
        super(entityId, EntityType.VILLAGER, location);
    }

    @Override
    public VillagerMetadata getEntityMetadata() {
        return (VillagerMetadata) super.getEntityMetadata();
    }
}
