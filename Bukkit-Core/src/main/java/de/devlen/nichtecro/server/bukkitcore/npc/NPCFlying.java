package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.FlyingMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCFlying extends NPCInsentient {

    public NPCFlying(int entityId, EntityType entityType, Location location) {
        super(entityId, entityType, location);
    }

    @Override
    public FlyingMetadata getEntityMetadata() {
        return (FlyingMetadata) super.getEntityMetadata();
    }
}
