package de.devlen.nichtecro.server.bukkitcore.npc;

import com.comphenix.protocol.wrappers.*;
import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import de.devlen.nichtecro.server.bukkitcore.metadata.*;
import de.devlen.nichtecro.server.bukkitcore.packetwrapper.*;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCEntity implements INPC {

    protected int entityId = 0;
    protected EntityType entityType = EntityType.UNKNOWN;
    protected Location location = null;
    protected EntityMetadata entityMetadata = new EntityMetadata();
    private OfflinePlayer skinPlayer = null;
    private Player metdataPlayer = null;

    public NPCEntity(int entityId, EntityType entityType, Location location) {
        this.entityId = entityId;
        this.entityType = entityType;
        this.location = location;
        switch (entityType) {
            case DROPPED_ITEM:
                this.entityMetadata = new ItemMetadata();
                break;
            case ARMOR_STAND:
                this.entityMetadata = new ArmorStandMetadata();
                break;
            case CREEPER:
                this.entityMetadata = new CreeperMetadata();
                break;
            case SKELETON:
                this.entityMetadata = new SkeletonMetadata();
                break;
            case SPIDER:
                this.entityMetadata = new SpiderMetadata();
                break;
            case GIANT:
                this.entityMetadata = new MonsterMetadata();
                break;
            case ZOMBIE:
                this.entityMetadata = new ZombieMetadata();
                break;
            case SLIME:
                this.entityMetadata = new SlimeMetadata();
                break;
            case GHAST:
                this.entityMetadata = new GhastMetadata();
                break;
            case PIG_ZOMBIE:
                this.entityMetadata = new MonsterMetadata();
                break;
            case ENDERMAN:
                this.entityMetadata = new EndermanMetadata();
                break;
            case CAVE_SPIDER:
                this.entityMetadata = new SpiderMetadata();
                break;
            case SILVERFISH:
                this.entityMetadata = new MonsterMetadata();
                break;
            case BLAZE:
                this.entityMetadata = new BlazeMetadata();
                break;
            case MAGMA_CUBE:
                this.entityMetadata = new SlimeMetadata();
                break;
            case ENDER_DRAGON:
                this.entityMetadata = new EnderDragonMetadata();
                break;
            case WITHER:
                this.entityMetadata = new MonsterMetadata();
                break;
            case BAT:
                this.entityMetadata = new BatMetadata();
                break;
            case WITCH:
                this.entityMetadata = new WitchMetadata();
                break;
            case ENDERMITE:
                this.entityMetadata = new MonsterMetadata();
                break;
            case GUARDIAN:
                this.entityMetadata = new GuardianMetadata();
                break;
            case SHULKER:
                this.entityMetadata = new GolemMetadata();
                break;
            case PIG:
                this.entityMetadata = new PigMetadata();
                break;
            case SHEEP:
                this.entityMetadata = new SheepMetadata();
                break;
            case COW:
                this.entityMetadata = new AnimalMetadata();
                break;
            case CHICKEN:
                this.entityMetadata = new AnimalMetadata();
                break;
            case SQUID:
                this.entityMetadata = new AnimalMetadata();
                break;
            case WOLF:
                this.entityMetadata = new WolfMetadata();
                break;
            case MUSHROOM_COW:
                this.entityMetadata = new AnimalMetadata();
                break;
            case SNOWMAN:
                this.entityMetadata = new GolemMetadata();
                break;
            case OCELOT:
                this.entityMetadata = new OcelotMetadata();
                break;
            case IRON_GOLEM:
                this.entityMetadata = new GolemMetadata();
                break;
            case HORSE:
                this.entityMetadata = new HorseMetadata();
                break;
            case RABBIT:
                this.entityMetadata = new RabbitMetadata();
                break;
            case VILLAGER:
                this.entityMetadata = new VillagerMetadata();
                break;
            case PLAYER:
                this.entityMetadata = new PlayerMetadata();
                break;
            default:
                this.entityMetadata = new EntityMetadata();
                break;
        }
        if (!entityType.equals(EntityType.PLAYER)) {
            this.entityMetadata.setWrappedDataWatcher(NPCUtil.getDefaultWatcher(Bukkit.getWorlds().get(0), entityType));
        } else if (entityType.equals(EntityType.PLAYER)) {
            this.entityMetadata.setWrappedDataWatcher(WrappedDataWatcher.getEntityWatcher(metdataPlayer));
        }
    }

    public OfflinePlayer getSkinPlayer() {
        return skinPlayer;
    }

    public void setSkinPlayer(OfflinePlayer skinPlayer) {
        this.skinPlayer = skinPlayer;
    }

    public void setMetdataPlayer(Player player) {
        this.metdataPlayer = player;
    }

    public Player getMetdataPlayer() {
        return metdataPlayer;
    }

    public EntityType getType() {
        return entityType;
    }

    public EntityMetadata getEntityMetadata() {
        return entityMetadata;
    }

    public void spawn(Player player) {
        switch (getType()) {
            case LIGHTNING:
                WrapperPlayServerSpawnEntityWeather wrapperPlayServerSpawnEntityWeather = new WrapperPlayServerSpawnEntityWeather();
                wrapperPlayServerSpawnEntityWeather.setEntityID(entityId);
                wrapperPlayServerSpawnEntityWeather.setType(1);
                wrapperPlayServerSpawnEntityWeather.setX(location.getX());
                wrapperPlayServerSpawnEntityWeather.setY(location.getY());
                wrapperPlayServerSpawnEntityWeather.setZ(location.getZ());
                wrapperPlayServerSpawnEntityWeather.sendPacket(player);
                break;
            case DROPPED_ITEM:
                WrapperPlayServerSpawnEntity wrapperPlayServerSpawnEntity = new WrapperPlayServerSpawnEntity();
                wrapperPlayServerSpawnEntity.setX(location.getX());
                wrapperPlayServerSpawnEntity.setY(location.getY());
                wrapperPlayServerSpawnEntity.setZ(location.getZ());
                wrapperPlayServerSpawnEntity.setPitch(location.getPitch());
                wrapperPlayServerSpawnEntity.setYaw(location.getYaw());
                wrapperPlayServerSpawnEntity.setType(2);
                wrapperPlayServerSpawnEntity.setEntityID(entityId);
                wrapperPlayServerSpawnEntity.setUniqueId(UUID.randomUUID());
                wrapperPlayServerSpawnEntity.sendPacket(player);
                update(player);
                break;
            case ARMOR_STAND:
            case CREEPER:
            case SKELETON:
            case SPIDER:
            case GIANT:
            case ZOMBIE:
            case SLIME:
            case GHAST:
            case PIG_ZOMBIE:
            case ENDERMAN:
            case CAVE_SPIDER:
            case SILVERFISH:
            case BLAZE:
            case MAGMA_CUBE:
            case ENDER_DRAGON:
            case WITHER:
            case BAT:
            case WITCH:
            case ENDERMITE:
            case GUARDIAN:
            case SHULKER:
            case PIG:
            case SHEEP:
            case COW:
            case CHICKEN:
            case SQUID:
            case WOLF:
            case MUSHROOM_COW:
            case SNOWMAN:
            case OCELOT:
            case IRON_GOLEM:
            case HORSE:
            case RABBIT:
            case VILLAGER:
                WrapperPlayServerSpawnEntityLiving wrapperPlayServerSpawnEntityLiving = new WrapperPlayServerSpawnEntityLiving();
                wrapperPlayServerSpawnEntityLiving.setEntityID(entityId);
                wrapperPlayServerSpawnEntityLiving.setType(getType());
                wrapperPlayServerSpawnEntityLiving.setX(location.getX());
                wrapperPlayServerSpawnEntityLiving.setY(location.getY());
                wrapperPlayServerSpawnEntityLiving.setZ(location.getZ());
                wrapperPlayServerSpawnEntityLiving.setYaw(location.getYaw());//Körper Rotation usually
                wrapperPlayServerSpawnEntityLiving.setPitch(location.getPitch());//Nicken vom Kopf
                wrapperPlayServerSpawnEntityLiving.setHeadPitch(location.getYaw());//Actually the yaw!
                wrapperPlayServerSpawnEntityLiving.setMetadata(getEntityMetadata().getWrappedDataWatcher());
                wrapperPlayServerSpawnEntityLiving.sendPacket(player.getPlayer());
                break;
            case PLAYER:
                WrapperPlayServerPlayerInfo wrapperPlayServerPlayerInfo = new WrapperPlayServerPlayerInfo();
                wrapperPlayServerPlayerInfo.setAction(EnumWrappers.PlayerInfoAction.ADD_PLAYER);
                List<PlayerInfoData> playerInfoDataList = new ExtendedList<>(new PlayerInfoData(WrappedGameProfile.fromOfflinePlayer(skinPlayer), 0, EnumWrappers.NativeGameMode.fromBukkit(GameMode.CREATIVE), WrappedChatComponent.fromText(skinPlayer.getName())));
                wrapperPlayServerPlayerInfo.setData(playerInfoDataList);
                wrapperPlayServerPlayerInfo.sendPacket(player);

                WrapperPlayServerNamedEntitySpawn wrapperPlayServerNamedEntitySpawn = new WrapperPlayServerNamedEntitySpawn();
                wrapperPlayServerNamedEntitySpawn.setEntityID(entityId);
                wrapperPlayServerNamedEntitySpawn.setMetadata(WrappedDataWatcher.getEntityWatcher(metdataPlayer));
                wrapperPlayServerNamedEntitySpawn.setX(location.getX());
                wrapperPlayServerNamedEntitySpawn.setY(location.getY());
                wrapperPlayServerNamedEntitySpawn.setZ(location.getZ());
                wrapperPlayServerNamedEntitySpawn.setYaw(location.getYaw());
                wrapperPlayServerNamedEntitySpawn.setPitch(location.getPitch());
                wrapperPlayServerNamedEntitySpawn.setPlayerUUID(WrappedGameProfile.fromOfflinePlayer(skinPlayer).getUUID());
                wrapperPlayServerNamedEntitySpawn.sendPacket(player);
            default:
        }

    }

    public void update(Player player) {
        WrapperPlayServerEntityMetadata wrapperPlayServerEntityMetadata = new WrapperPlayServerEntityMetadata();
        wrapperPlayServerEntityMetadata.setEntityID(entityId);
        wrapperPlayServerEntityMetadata.setMetadata(getEntityMetadata().getWrappedDataWatcher().getWatchableObjects());
        wrapperPlayServerEntityMetadata.sendPacket(player);

    }

    public void teleport(Player player, Location location) {
        WrapperPlayServerEntityTeleport wrapperPlayServerEntityTeleport = new WrapperPlayServerEntityTeleport();
        wrapperPlayServerEntityTeleport.setEntityID(entityId);
        wrapperPlayServerEntityTeleport.setX(location.getX());
        wrapperPlayServerEntityTeleport.setY(location.getY());
        wrapperPlayServerEntityTeleport.setZ(location.getZ());
        wrapperPlayServerEntityTeleport.setPitch(location.getPitch());
        wrapperPlayServerEntityTeleport.setYaw(location.getYaw());
        wrapperPlayServerEntityTeleport.setOnGround(false);
        wrapperPlayServerEntityTeleport.sendPacket(player);
    }
}
