package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.EntityMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

public interface INPC {
    EntityType getType();

    EntityMetadata getEntityMetadata();

    void spawn(Player player);

    void update(Player player);

    void teleport(Player player, Location location);
}
