package de.devlen.nichtecro.server.bukkitcore.events;

import de.devlen.nichtecro.server.bukkitcore.BukkitCore;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.PluginManager;

/**
 * Created by Tarik on 30.03.2016.
 */
public class EventHelper extends Event {

    private static final HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public void call() {
        PluginManager pluginManager = BukkitCore.getInstance().getServer().getPluginManager();
        pluginManager.callEvent(this);
    }

}
