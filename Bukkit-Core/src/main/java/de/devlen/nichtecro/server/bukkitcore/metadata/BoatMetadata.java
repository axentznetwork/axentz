package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class BoatMetadata extends EntityMetadata {

    public void setLastHitTime(int lastHitTime) {
        setMeta(5, lastHitTime);
    }

    public int getLastHitTime() {
        return (int) getMeta(5);
    }

    public void setForwardDirection(int forwardDirection) {
        setMeta(6, forwardDirection);
    }

    public int getForwardDirection() {
        return (int) getMeta(6);
    }

    public void setDamageTaken(float damageTaken) {
        setMeta(7, damageTaken);
    }

    public float getDamageTaken() {
        return (float) getMeta(7);
    }

    public void setType(int type) {
        setMeta(8, type);
    }

    public int getType() {
        return (int) getMeta(8);
    }
}
