package de.devlen.nichtecro.server.bukkitcore.manager.logging;

import java.util.Date;
import java.util.UUID;

/**
 * Created by Tarik on 30.03.2016.
 */
public class ChatLogEntry {

    private int id = 0;
    private UUID uuid = null;
    private Date date = null;
    private String message = "";

    public ChatLogEntry(int id, UUID uuid, Date date, String message) {
        this.id = id;
        this.uuid = uuid;
        this.date = date;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ChatLogEntry{" +
                "id=" + id +
                ", uuid=" + uuid +
                ", date=" + date +
                ", message='" + message + '\'' +
                '}';
    }
}
