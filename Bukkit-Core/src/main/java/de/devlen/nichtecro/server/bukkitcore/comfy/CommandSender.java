package de.devlen.nichtecro.server.bukkitcore.comfy;

import de.devlen.nichtecro.server.bukkitcore.comfy.tree.CommandPath;

public interface CommandSender {
    void warning(String message);

    void info(String message);

    void pathHelp(CommandPath path);

    boolean hasPermission(String permission);
}
