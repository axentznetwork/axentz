package de.devlen.nichtecro.server.bukkitcore.metadata;

import org.bukkit.Location;

/**
 * Created by Tarik on 31.03.2016.
 */
public class FallingBlockMetadata extends EntityMetadata {

    public void setPosition(Location location) {
        setMeta(5, ProtocolUtil.asBlockPosition(location));
    }

}
