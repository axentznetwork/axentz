package de.devlen.nichtecro.server.bukkitcore.message;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.Iterator;

import static de.devlen.nichtecro.server.bukkitcore.language.Language.tl;

/**
 * Created by Tarik on 14.05.2016.
 */
public class Chat {

    public static void sendTl(CommandSender commandSender, String prefixTranslatableNode, String messageTranslatableNode, String... messageVariables) {
        send(commandSender, tl(prefixTranslatableNode, commandSender), tl(messageTranslatableNode, commandSender, messageVariables));
    }

    public static void send(CommandSender commandSender, String prefix, String message) {
        commandSender.sendMessage(replaceFormatVariables("%d" + prefix + " %s� %u" + message));
    }

    public static String replaceFormatVariables(String message) {
        message = "%u" + message;
        message = message.replace("%s", ChatColor.DARK_GRAY.toString());
        message = message.replace("%u", ChatColor.GRAY.toString());
        message = message.replace("%l", ChatColor.AQUA.toString());
        message = message.replace("%d", ChatColor.DARK_AQUA.toString());
        return message;
    }

    public static String normalizeEnumValueName(Enum enumValue) {
        String name = enumValue.name();
        String[] words = name.split("_");
        String normalized = "";
        Iterator<String> iterator = Arrays.asList(words).iterator();
        while(iterator.hasNext()){
            String word = iterator.next();
            normalized += word.toLowerCase();
            if(iterator.hasNext()){
                normalized += " ";
            }
        }
        normalized = normalized.substring(0, 1).toUpperCase() + normalized.substring(1);
        return normalized;
    }

}
