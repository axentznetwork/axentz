package de.devlen.nichtecro.server.bukkitcore.items.reloaded;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Created by Tarik on 04.06.2016.
 */
public class Button {

    private ButtonFactory buttonFactory = new ButtonFactory();
    private String nameId = "";
    private ClickHandler clickHandler = new ClickHandler() {
        @Override
        public void handle(InventoryClickEvent e) {
        }
    };
    private InteractHandler interactHandler = new InteractHandler() {
        @Override
        public void handle(PlayerInteractEvent e) {
        }
    };

    private int slot = 0;

    public Button factory(ButtonFactory factory) {
        this.buttonFactory = factory;
        return this;
    }

    public Button id(String nameId) {
        this.nameId = nameId;
        return this;
    }

    public Button click(ClickHandler handler) {
        this.clickHandler = handler;
        return this;
    }

    public Button interact(InteractHandler handler) {
        this.interactHandler = handler;
        return this;
    }

    public Button registerHandlers() {
        MenuManager.putClickHandler(nameId, clickHandler);
        MenuManager.putInteractHandler(nameId, interactHandler);
        System.out.println("Registering Handlers");
        System.out.println("Registered Name ID: " + nameId);
        System.out.println("Interact Handler: " + interactHandler);
        return this;
    }

    public Button slot(int slot) {
        this.slot = slot;
        return this;
    }

    public ButtonFactory getButtonFactory() {
        return buttonFactory;
    }

    public String getNameId() {
        return nameId;
    }

    public ClickHandler getClickHandler() {
        return clickHandler;
    }

    public InteractHandler getInteractHandler() {
        return interactHandler;
    }

    public int getSlot() {
        return slot;
    }
}
