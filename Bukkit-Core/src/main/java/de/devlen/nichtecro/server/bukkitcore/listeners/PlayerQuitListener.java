package de.devlen.nichtecro.server.bukkitcore.listeners;

import de.devlen.nichtecro.server.bukkitcore.manager.PermissionManager;
import de.devlen.nichtecro.server.bukkitcore.manager.ScoreboardSchedulingManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Tarik on 14.05.2016.
 */
public class PlayerQuitListener extends ListenerHelper {

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        PermissionManager.invalidatePermissions(player);
        player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
        ScoreboardSchedulingManager.removeScoreboardScrollTask(player);
        e.setQuitMessage(null);
    }

}
