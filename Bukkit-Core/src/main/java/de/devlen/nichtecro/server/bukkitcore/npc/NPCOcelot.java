package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.OcelotMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCOcelot extends NPCTameableAnimal {

    public NPCOcelot(int entityId, Location location) {
        super(entityId, EntityType.OCELOT, location);
    }

    @Override
    public OcelotMetadata getEntityMetadata() {
        return (OcelotMetadata) super.getEntityMetadata();
    }
}
