package de.devlen.nichtecro.server.bukkitcore.manager;

import java.util.UUID;

/**
 * Created by Tarik on 20.05.2016.
 */
public class PlayerData {

    private UUID uniqueId;
    private Rank rank;
    private int coins;

    public PlayerData(UUID uniqueId, Rank rank, int coins) {
        this.uniqueId = uniqueId;
        this.rank = rank;
        this.coins = coins;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public int getCoins() {
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }
}
