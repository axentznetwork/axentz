package de.devlen.nichtecro.server.bukkitcore.metadata;

import org.bukkit.entity.Skeleton;

/**
 * Created by Tarik on 31.03.2016.
 */
public class SkeletonMetadata extends MonsterMetadata {

    public void setType(Skeleton.SkeletonType skeletonType) {
        setMeta(11, skeletonType.getId());
    }

    public int getSkeletonType(){
        return (int) getMeta(11);
    }

}
