package de.devlen.nichtecro.server.bukkitcore.events;

import de.devlen.nichtecro.server.bukkitcore.manager.Rank;
import org.bukkit.entity.Player;

/**
 * Created by Tarik on 15.06.2016.
 */
public class DisguiseRankChangedEvent extends EventHelper {

    private Player player;
    private Rank newRank;

    public DisguiseRankChangedEvent(Player player, Rank newRank) {
        this.player = player;
        this.newRank = newRank;
    }

    public Player getPlayer() {
        return player;
    }

    public Rank getNewRank() {
        return newRank;
    }
}
