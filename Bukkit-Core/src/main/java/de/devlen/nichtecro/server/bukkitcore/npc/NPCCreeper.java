package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.CreeperMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCCreeper extends NPCMonster {

    public NPCCreeper(int entityId, Location location) {
        super(entityId, EntityType.CREEPER, location);
    }

    @Override
    public CreeperMetadata getEntityMetadata() {
        return (CreeperMetadata) super.getEntityMetadata();
    }
}
