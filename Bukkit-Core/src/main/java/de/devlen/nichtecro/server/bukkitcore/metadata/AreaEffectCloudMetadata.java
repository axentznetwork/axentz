package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class AreaEffectCloudMetadata extends EntityMetadata {

    public void setRadius(float radius) {
        setMeta(5, radius);
    }

    public float getRadius() {
        return (float) getMeta(5);
    }

    public void setColor(int color) {
        setMeta(6, color);
    }

    public int getColor() {
        return (int) getMeta(6);
    }

    public void setIgnoreRadius(boolean ignoreRadius) {
        setMeta(7, ignoreRadius);
    }

    public boolean isIgnoreRadius() {
        return (boolean) getMeta(7);
    }

    public void setParticleId(int particleId) {
        setMeta(8, particleId);
    }

    public int getParticleId(){
        return (int) getMeta(8);
    }
}
