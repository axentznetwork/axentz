package de.devlen.nichtecro.server.bukkitcore.comfy.exception;

public class CommandTreeException extends RuntimeException {
    public CommandTreeException(String msg) {
        super(msg);
    }
}
