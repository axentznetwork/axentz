package de.devlen.nichtecro.server.bukkitcore.time;

import de.devlen.nichtecro.server.bukkitcore.BukkitCore;
import org.bukkit.Bukkit;

/**
 * Created by Tarik on 01.06.2016.
 */
public class Delay {

    public static void make(long ticks, Runnable runnable) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(BukkitCore.getInstance(), runnable, ticks);
    }

}
