package de.devlen.nichtecro.server.bukkitcore.listeners;

import de.devlen.nichtecro.server.bukkitcore.commands.CommandTest;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * Created by Tarik on 03.06.2016.
 */
public class PlayerMoveListener extends ListenerHelper {

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {

        for (Location center : CommandTest.speedingLocationsCentres) {
            Location point = e.getPlayer().getLocation();
            double distSquared = point.distanceSquared(center);
            if (distSquared < 6 * 6) {
                e.getPlayer().setVelocity(e.getPlayer().getLocation().getDirection().multiply(2.5));
            }
        }
    }

}
