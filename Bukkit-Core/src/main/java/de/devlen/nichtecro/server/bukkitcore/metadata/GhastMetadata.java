package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class GhastMetadata extends FlyingMetadata {

    public void setAttacking(boolean attacking) {
        setMeta(11, attacking);
    }

    public boolean isAttacking(){
        return (boolean) getMeta(11);
    }

}
