package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.EndermanMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCEnderman extends NPCMonster {

    public NPCEnderman(int entityId, Location location) {
        super(entityId, EntityType.ENDERMAN, location);
    }

    @Override
    public EndermanMetadata getEntityMetadata() {
        return (EndermanMetadata) super.getEntityMetadata();
    }
}
