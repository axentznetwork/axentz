package de.devlen.nichtecro.server.bukkitcore.message;

import com.comphenix.protocol.wrappers.WrappedChatComponent;
import de.devlen.nichtecro.server.bukkitcore.packetwrapper.WrapperPlayServerChat;
import org.bukkit.entity.Player;

/**
 * Created by Tarik on 14.05.2016.
 */
public class Hotbar {

    public static void send(Player player, String message) {
        WrapperPlayServerChat wrapperPlayServerChat = new WrapperPlayServerChat();
        wrapperPlayServerChat.setPosition((byte) 2);
        wrapperPlayServerChat.setMessage(WrappedChatComponent.fromText(Chat.replaceFormatVariables(message)));
        wrapperPlayServerChat.sendPacket(player);
    }

}
