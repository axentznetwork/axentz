package de.devlen.nichtecro.server.bukkitcore.metadata;

import org.bukkit.util.Vector;

/**
 * Created by Tarik on 31.03.2016.
 */
public class ArmorStandMetadata extends LivingMetadata {

    /**
     * 0x01 - isSmall
     * 0x02 - hasGravity
     * 0x04 - hasArms
     * 0x08 - noBasePlate
     * 0x10 - setMarker
     * e.g.: 0x01 | 0x04 --> isSmall and hasArms.
     *
     * @param armorStandFlags
     */
    public void setArmorStandFlags(byte armorStandFlags) {
        setMeta(10, armorStandFlags);
    }

    public byte getArmorStandFlags() {
        return (byte) getMeta(10);
    }

    public void setHeadPosition(final Vector vector) {
        this.setMeta(11, vector);
    }

    public void setBodyPosition(final Vector vector) {
        this.setMeta(12, vector);
    }

    public void setLeftArmPosition(final Vector vector) {
        this.setMeta(13, vector);
    }

    public void setRightArmPosition(final Vector vector) {
        this.setMeta(14, vector);
    }

    public void setLeftLegPosition(final Vector vector) {
        this.setMeta(15, vector);
    }

    public void setRightLegPosition(final Vector vector) {
        this.setMeta(16, vector);
    }

}
