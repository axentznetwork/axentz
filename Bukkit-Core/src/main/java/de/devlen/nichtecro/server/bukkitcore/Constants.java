package de.devlen.nichtecro.server.bukkitcore;

/**
 * Created by Tarik on 07.02.2016.
 */
public class Constants {

    private static String MYSQL_ADDRESS = "localhost";
    private static String MYSQL_USER = "root";
    private static String MYSQL_PASSWORD = "freakapple";
    private static String MYSQL_PORT = "3306";
    private static String MYSQL_DATABASE = "minecraft";
    private static String MONGO_DB_ADDRESS = "localhost";
    private static String MONGO_DB_USER = "root";
    private static String MONGO_DB_PASSWORD = "763255f9b2bcc952df4a47d8f02a3e82be4349a41549a62858edd4f3c87bf9fa8617a3044afdb4b3b3c6aca0f30d4ff327ad13c206d2d98ecc4de55a392acfd0459fbd2d2cb97ecece047d35f05ab16216fed795cadde576c000fe7a896bed30d4816136a0fb31d31719d9ff5a0f9263ea8cb54153a133ddca241ce004265775673797abee08da6544b9013ab8da120f";
    private static String MONGO_DB_DATABASE = "minecraft";

    public static String getMysqlAddress() {
        return MYSQL_ADDRESS;
    }

    public static String getMysqlDatabase() {
        return MYSQL_DATABASE;
    }

    public static String getMysqlPassword() {
        return MYSQL_PASSWORD;
    }

    public static String getMysqlPort() {
        return MYSQL_PORT;
    }

    public static String getMysqlUser() {
        return MYSQL_USER;
    }

    public static String getMongoDbAddress() {
        return MONGO_DB_ADDRESS;
    }

    public static String getMongoDbUser() {
        return MONGO_DB_USER;
    }

    public static String getMongoDbPassword() {
        return MONGO_DB_PASSWORD;
    }

    public static String getMongoDbDatabase() {
        return MONGO_DB_DATABASE;
    }
}
