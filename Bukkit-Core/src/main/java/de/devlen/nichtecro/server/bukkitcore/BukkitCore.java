package de.devlen.nichtecro.server.bukkitcore;

import com.comphenix.protocol.ProtocolLibrary;
import com.google.common.collect.Maps;
import de.devlen.nichtecro.server.bukkitcore.comfy.argument.EnumType;
import de.devlen.nichtecro.server.bukkitcore.comfy.argument.StringType;
import de.devlen.nichtecro.server.bukkitcore.comfy.bukkit.BukkitCommandManager;
import de.devlen.nichtecro.server.bukkitcore.comfy.tree.Argument;
import de.devlen.nichtecro.server.bukkitcore.comfy.tree.Literal;
import de.devlen.nichtecro.server.bukkitcore.commands.CommandMorph;
import de.devlen.nichtecro.server.bukkitcore.commands.CommandRank;
import de.devlen.nichtecro.server.bukkitcore.commands.CommandTag;
import de.devlen.nichtecro.server.bukkitcore.commands.CommandTest;
import de.devlen.nichtecro.server.bukkitcore.database.MongoDBManager;
import de.devlen.nichtecro.server.bukkitcore.disguise.Disguise;
import de.devlen.nichtecro.server.bukkitcore.disguise.DisguiseManager;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.MenuManager;
import de.devlen.nichtecro.server.bukkitcore.listeners.*;
import de.devlen.nichtecro.server.bukkitcore.manager.PermissionManager;
import de.devlen.nichtecro.server.bukkitcore.manager.Rank;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Tarik on 14.05.2016.
 */
public class BukkitCore extends JavaPlugin {

    private static Plugin instance;
    private static boolean updateTaskRunning = false;
    private static HashMap<Player, ArrayList<Player>> disguiseAreas = Maps.newHashMap();
    private static HashMap<String, Scoreboard> scoreboardHashMap = Maps.newHashMap();

    @Override
    public void onEnable() {
        instance = this;
        prepareDatabase();
        registerListeners();
        registerCommands();
        runUpdateTask();
        scoreboardHashMap.put("en_US", Bukkit.getScoreboardManager().getNewScoreboard());
        scoreboardHashMap.put("de_DE", Bukkit.getScoreboardManager().getNewScoreboard());
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        this.getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", new PluginMessageListenerBungee());
    }

    public static Scoreboard getLanguageScoreboard(String locale) {
        if (scoreboardHashMap.keySet().contains(locale)) {
            return scoreboardHashMap.get(locale);
        } else {
            return scoreboardHashMap.get("en_US");
        }
    }

    public static Plugin getInstance() {
        return instance;
    }

    public static void registerListeners() {
        PacketListeners.registerClientSettingsPacketListener();
        new AsyncPlayerChatListener().register();
        new ClientSettingsChangeListener().register();
        new PlayerCommandPreprocessListener().register();
        new PlayerJoinListener().register();
        new PlayerKickListener().register();
        new PlayerMoveListener().register();
        new PlayerQuitListener().register();

        new MenuManager().register();
    }

    public static void registerCommands() {
        HashMap<String, Rank> stringRankHashMap = Maps.newHashMap();
        stringRankHashMap.put("administrator", Rank.ADMINISTRATOR);
        stringRankHashMap.put("developer", Rank.DEVELOPER);
        stringRankHashMap.put("translator", Rank.TRANSLATOR);
        stringRankHashMap.put("mapper", Rank.MAPPER);
        stringRankHashMap.put("builder", Rank.MAPPER);
        stringRankHashMap.put("moderator", Rank.MODERATOR);
        stringRankHashMap.put("supporter", Rank.SUPPORTER);
        stringRankHashMap.put("youtuber", Rank.YOUTUBER);
        stringRankHashMap.put("friend", Rank.FRIEND);
        stringRankHashMap.put("veteran", Rank.VETERAN);
        stringRankHashMap.put("wicked", Rank.WICKED);
        stringRankHashMap.put("ancient", Rank.ANCIENT);
        stringRankHashMap.put("user", Rank.GUEST);
        stringRankHashMap.put("guest", Rank.GUEST);

        BukkitCommandManager bukkitCommandManager = new BukkitCommandManager(getInstance());
        bukkitCommandManager.addCommand(new Literal("morph", "disguise")
                .then(new Argument("morphType", new StringType())
                        .executes("command.morph.morphtype")
                        .permission("bukkit.core.command.morph")));
        bukkitCommandManager.addCommand(new Literal("rank")
                .then(new Literal("add")
                        .then(new Argument("clientName", new StringType())
                                .then(new Argument("rank", new EnumType(stringRankHashMap))
                                        .executes("command.rank.add.clientname.rank")
                                        .permission("bukkit.core.command.rank.add"))))
                .then(new Literal("remove")
                        .then(new Argument("clientName", new StringType())
                                .then(new Argument("rank", new EnumType(stringRankHashMap))
                                        .executes("command.rank.remove.clientname.rank")
                                        .permission("bukkit.core.command.rank.remove")))));
        bukkitCommandManager.addCommand(new Literal("tag", "disguiserank", "rankdisguise")
                .then(new Argument("rank", new StringType())
                        .executes("bukkit.core.command.tag.rank")
                        .permission("bukkit.core.command.tag")
                        .description("Change your rank-tag to a rank you chose!")));
        bukkitCommandManager.addCommand(new Literal("test")
                .executes("command.test")
                .permission("bukkit.core.command.test"));
        bukkitCommandManager.addListener(new CommandMorph());
        bukkitCommandManager.addListener(new CommandRank());
        bukkitCommandManager.addListener(new CommandTag());
        bukkitCommandManager.addListener(new CommandTest());
        bukkitCommandManager.registerCommands();
    }

    public static void prepareDatabase() {
        MongoDBManager.initClient();
        PermissionManager.cache();
    }

    public static boolean isUpdateTaskRunning() {
        return updateTaskRunning;
    }

    public static void runUpdateTask() {
        if (!isUpdateTaskRunning()) {
            Bukkit.getScheduler().scheduleSyncRepeatingTask(getInstance(), new Runnable() {
                @Override
                public void run() {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        if (DisguiseManager.getDisguise(player) != null) {
                            List<Player> trackingPlayers = ProtocolLibrary.getProtocolManager().getEntityTrackers(player.getPlayer());
                            for (Player target : trackingPlayers) {
                                if (!player.getName().equalsIgnoreCase(target.getName()) && !disguiseAreas.get(player).contains(target)) {
                                    ArrayList<Player> registeredPlayers = disguiseAreas.get(player);
                                    registeredPlayers.add(target);
                                    disguiseAreas.put(player, registeredPlayers);
                                    Disguise.send(player, target);
                                }
                            }
                            try {
                                for (Player tracking : disguiseAreas.get(player)) {
                                    if (!trackingPlayers.contains(tracking)) {
                                        ArrayList<Player> registeredPlayers = disguiseAreas.get(player);
                                        registeredPlayers.remove(tracking);
                                        disguiseAreas.put(player, registeredPlayers);
                                    }
                                }
                            } catch (Exception ex) {
                            }
                        }
                    }
                }
            }, 0, 1);
        }
    }

    public static void setDisguiseAreas(Player player, ArrayList arrayList) {
        disguiseAreas.put(player, arrayList);
    }


}
