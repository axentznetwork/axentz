package de.devlen.nichtecro.server.bukkitcore.comfy.bukkit;

import de.devlen.nichtecro.server.bukkitcore.comfy.Arguments;
import de.devlen.nichtecro.server.bukkitcore.comfy.CommandContext;
import de.devlen.nichtecro.server.bukkitcore.comfy.tree.CommandPath;

public class BukkitCommandContext extends CommandContext<BukkitCommandSender> {
    protected BukkitCommandContext(BukkitCommandSender sender, CommandPath path, Arguments arguments) {
        super(sender, path, arguments);
    }
}
