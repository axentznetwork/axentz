package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.AgeableMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCAgeable extends NPCCreature {

    public NPCAgeable(int entityId, EntityType entityType, Location location) {
        super(entityId, entityType, location);
    }

    @Override
    public AgeableMetadata getEntityMetadata() {
        return (AgeableMetadata) super.getEntityMetadata();
    }
}
