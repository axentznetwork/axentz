package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class WitchMetadata extends MonsterMetadata {

    public void setAggresive(boolean aggresive){
        setMeta(11, aggresive);
    }

    public boolean isAggresive(){
        return (boolean) getMeta(11);
    }

}
