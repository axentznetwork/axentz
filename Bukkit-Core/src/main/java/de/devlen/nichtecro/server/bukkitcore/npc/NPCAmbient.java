package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.AmbientMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCAmbient extends NPCInsentient {

    public NPCAmbient(int entityId, EntityType entityType, Location location) {
        super(entityId, entityType, location);
    }

    @Override
    public AmbientMetadata getEntityMetadata() {
        return (AmbientMetadata) super.getEntityMetadata();
    }
}
