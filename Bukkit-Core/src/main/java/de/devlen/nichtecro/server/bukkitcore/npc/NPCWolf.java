package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.WolfMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCWolf extends NPCTameableAnimal {

    public NPCWolf(int entityId, Location location) {
        super(entityId, EntityType.WOLF, location);
    }

    @Override
    public WolfMetadata getEntityMetadata() {
        return (WolfMetadata) super.getEntityMetadata();
    }
}
