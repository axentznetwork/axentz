package de.devlen.nichtecro.server.bukkitcore.listeners;

import de.devlen.nichtecro.server.bukkitcore.BukkitCore;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;

/**
 * Created by Tarik on 14.05.2016.
 */
public class ListenerHelper implements Listener {

    public void register() {
        PluginManager pm = BukkitCore.getInstance().getServer().getPluginManager();
        pm.registerEvents(this, BukkitCore.getInstance());
    }

}
