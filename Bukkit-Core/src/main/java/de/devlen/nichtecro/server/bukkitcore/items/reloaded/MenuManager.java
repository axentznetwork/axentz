package de.devlen.nichtecro.server.bukkitcore.items.reloaded;

import com.google.common.collect.Maps;
import de.devlen.nichtecro.server.bukkitcore.items.NBTItem;
import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Map;

/**
 * Created by Tarik on 04.06.2016.
 */
public class MenuManager extends ListenerHelper {

    private static Map<String, ClickHandler> stringClickHandlerMap = Maps.newHashMap();
    private static Map<String, InteractHandler> stringInteractHandlerMap = Maps.newHashMap();

    public static void putClickHandler(String nameId, ClickHandler clickHandler) {
        stringClickHandlerMap.put(nameId, clickHandler);
    }

    public static void putInteractHandler(String nameId, InteractHandler interactHandler) {
        stringInteractHandlerMap.put(nameId, interactHandler);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryClick(InventoryClickEvent e) {
        if (e.getCurrentItem() != null) {
            NBTItem nbtItem = new NBTItem(e.getCurrentItem());
            if (nbtItem != null) {
                try {
                    if (nbtItem.hasKey("server_name_id")) {
                        String nameId = nbtItem.getString("server_name_id");
                        if (stringClickHandlerMap.containsKey(nameId)) {
                            ClickHandler clickHandler = stringClickHandlerMap.get(nameId);
                            e.setCancelled(true);
                            clickHandler.handle(e);
                        }
                    }
                } catch (NullPointerException ex) {
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (e.getItem() != null) {
            NBTItem nbtItem = new NBTItem(e.getItem());
            if (nbtItem != null) {
                try {
                    if (nbtItem.hasKey("server_name_id")) {
                        String nameId = nbtItem.getString("server_name_id");
                        if (stringInteractHandlerMap.containsKey(nameId)) {
                            InteractHandler interactHandler = stringInteractHandlerMap.get(nameId);
                            e.setCancelled(true);
                            interactHandler.handle(e);
                            System.out.println("Name ID: " + nameId);
                            System.out.println("Event cancelled: " + e.isCancelled());
                            System.out.println("Event: " + e.toString());
                        }
                    }
                } catch (NullPointerException ex) {
                }
            }
        }
//        }
    }
}
