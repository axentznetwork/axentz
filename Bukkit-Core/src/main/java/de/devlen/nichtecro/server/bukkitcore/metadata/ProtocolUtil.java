package de.devlen.nichtecro.server.bukkitcore.metadata;

import com.comphenix.protocol.wrappers.BlockPosition;
import org.bukkit.Location;

/**
 * Created by Tarik on 31.03.2016.
 */
public class ProtocolUtil {

    public static BlockPosition asBlockPosition(Location location) {
        return new BlockPosition(location.toVector());
    }

    public static byte getCompressedAngle(float value) {
        return (byte) (int) (value * 256.0F / 360.0F);
    }

}
