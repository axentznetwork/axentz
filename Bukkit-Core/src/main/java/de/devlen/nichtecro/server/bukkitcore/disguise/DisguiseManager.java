package de.devlen.nichtecro.server.bukkitcore.disguise;

import com.google.common.collect.Maps;
import org.bukkit.entity.Player;

import java.util.HashMap;

/**
 * Created by Tarik on 16.05.2016.
 */
public class DisguiseManager {

    private static HashMap<Player, Disguise<?>> playerDisguiseHashMap = Maps.newHashMap();

    public static void setDisguise(Player player, Disguise disguise) {
        playerDisguiseHashMap.put(player, disguise);
    }

    public static Disguise getDisguise(Player player){
        return playerDisguiseHashMap.get(player);
    }

}
