package de.devlen.nichtecro.server.bukkitcore.listeners;

import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.help.HelpTopic;

import static de.devlen.nichtecro.server.bukkitcore.language.Language.tl;

/**
 * Created by Tarik on 16.05.2016.
 */
public class PlayerCommandPreprocessListener extends ListenerHelper {

    @EventHandler
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent e) {
        Player player = e.getPlayer();
        String enteredMessage = e.getMessage();

        String command = enteredMessage.split(" ")[0];
        HelpTopic helpTopic = Bukkit.getServer().getHelpMap().getHelpTopic(command);
        if (helpTopic == null) {
            Chat.send(player, tl("server.tag", player), tl("server.unknowncommand", player));
            e.setCancelled(true);
        } else {
            if (command.startsWith("bukkit")) {
                if (!player.hasPermission("bukkit.core.listener.playercommandpreprocess.bukkit.bypass")) {
                    Chat.send(player, tl("server.tag", player), tl("server.unknowncommand", player));
                    e.setCancelled(true);
                }
            } else if (command.startsWith("minecraft")) {
                if (!player.hasPermission("bukkit.core.listener.playercommandpreprocess.minecraft.bypass")) {
                    Chat.send(player, tl("server.tag", player), tl("server.unknowncommand", player));
                    e.setCancelled(true);
                }
            }
        }
    }

}
