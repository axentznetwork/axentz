package de.devlen.nichtecro.server.bukkitcore.metadata;

import org.bukkit.entity.Villager;

/**
 * Created by Tarik on 31.03.2016.
 */
public class VillagerMetadata extends AgeableMetadata {

    public void setProfession(Villager.Profession profession) {
        setMeta(12, profession.getId());
    }

    public int getProfession() {
        return (int) getMeta(12);
    }

}
