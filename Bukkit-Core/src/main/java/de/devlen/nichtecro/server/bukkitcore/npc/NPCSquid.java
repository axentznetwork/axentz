package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.AnimalMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCSquid extends NPCAnimal {

    public NPCSquid(int entityId, Location location) {
        super(entityId, EntityType.SQUID, location);
    }

    @Override
    public AnimalMetadata getEntityMetadata() {
        return super.getEntityMetadata();
    }
}
