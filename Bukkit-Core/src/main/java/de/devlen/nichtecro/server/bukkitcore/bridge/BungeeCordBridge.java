package de.devlen.nichtecro.server.bukkitcore.bridge;

import com.google.common.collect.Maps;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import de.devlen.nichtecro.server.bukkitcore.BukkitCore;
import org.bukkit.entity.Player;

import java.util.HashMap;

/**
 * Created by Tarik on 15.05.2016.
 */
public class BungeeCordBridge {

    private static HashMap<String, Integer> serverPlayerCount = Maps.newHashMap();

    public static void sendPlayerToServer(Player player, String serverName) {
        ByteArrayDataOutput output = ByteStreams.newDataOutput();
        output.writeUTF("ConnectOther");
        output.writeUTF(player.getName());
        output.writeUTF(serverName);
        player.sendPluginMessage(BukkitCore.getInstance(), "BungeeCord", output.toByteArray());
    }

    public static int getServerPlayerCount(String serverName) {
        if (serverPlayerCount.containsKey(serverName)) {
            return serverPlayerCount.get(serverName);
        } else {
            return 0;
        }
    }

    public static void setServerPlayerCount(String serverName, int serverPlayerCountInt) {
        serverPlayerCount.put(serverName, serverPlayerCountInt);
    }
}
