package de.devlen.nichtecro.server.bukkitcore.player;


import com.google.common.collect.Maps;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Tarik on 23.04.2016.
 */
public class UUIDControl {

    private static HashMap<String, UUID> uuidCache = Maps.newHashMap();

    public static UUID getUUID(String name) {
        if (uuidCache.containsKey(name)) {
            return uuidCache.get(name);
        }
        UUID uuid = null;
        try {
            uuid = UUIDFetcher.getUUIDOf(name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        uuidCache.put(name, uuid);
        return uuid;
    }

    public static UUID getUUID(Player p) {
        return getUUID(p.getName());
    }

}
