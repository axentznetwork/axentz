package de.devlen.nichtecro.server.bukkitcore.manager.logging;

import java.util.Date;
import java.util.UUID;

/**
 * Created by Tarik on 30.03.2016.
 */
public class CommandLogEntry {

    private int id = 0;
    private UUID uuid = null;
    private Date date = null;
    private String command = "";

    public CommandLogEntry(int id, UUID uuid, Date date, String command) {
        this.id = id;
        this.uuid = uuid;
        this.date = date;
        this.command = command;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    @Override
    public String toString() {
        return "CommandLogEntry{" +
                "id=" + id +
                ", uuid=" + uuid +
                ", date=" + date +
                ", command='" + command + '\'' +
                '}';
    }
}
