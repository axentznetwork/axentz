package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class SpiderMetadata extends MonsterMetadata {

    public void setClimbing(boolean climbing) {
        setMeta(11, (climbing ? 1 : 0));
    }

    public boolean isClimbing() {
        return ((byte) getMeta(11) == 1 ? true : false);
    }

}
