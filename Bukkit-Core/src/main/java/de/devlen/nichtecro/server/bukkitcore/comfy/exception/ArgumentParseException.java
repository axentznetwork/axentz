package de.devlen.nichtecro.server.bukkitcore.comfy.exception;

public class ArgumentParseException extends Exception {
    public ArgumentParseException(String msg) {
        super(msg);
    }
}
