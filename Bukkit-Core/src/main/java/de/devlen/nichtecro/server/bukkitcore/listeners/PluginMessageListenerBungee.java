package de.devlen.nichtecro.server.bukkitcore.listeners;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import de.devlen.nichtecro.server.bukkitcore.bridge.BungeeCordBridge;
import org.bukkit.entity.Player;

/**
 * Created by Tarik on 02.05.2016.
 */
public class PluginMessageListenerBungee implements org.bukkit.plugin.messaging.PluginMessageListener {

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (channel.equalsIgnoreCase("BungeeCord")) {
            ByteArrayDataInput input = ByteStreams.newDataInput(message);
            String subchannel = input.readUTF();
            if (subchannel.equalsIgnoreCase("PlayerCount")) {
                String server = input.readUTF();
                int playerCount = input.readInt();
                BungeeCordBridge.setServerPlayerCount(server, playerCount);
            }
        }
    }
}
