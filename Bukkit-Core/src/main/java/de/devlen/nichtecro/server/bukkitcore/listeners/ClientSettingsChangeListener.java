package de.devlen.nichtecro.server.bukkitcore.listeners;

import de.devlen.nichtecro.server.bukkitcore.events.ClientSettingsChangeEvent;
import de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager;
import de.devlen.nichtecro.server.bukkitcore.manager.ScoreboardSchedulingManager;
import org.bukkit.event.EventHandler;

/**
 * Created by Tarik on 26.05.2016.
 */
public class ClientSettingsChangeListener extends ListenerHelper {

    @EventHandler
    public void onClientSettingsChange(ClientSettingsChangeEvent e) {
        MPlayerSettingsManager.setLanguage(e.getPlayer(), e.getLocale());
        ScoreboardSchedulingManager.runScoreboardScrollTask(e.getPlayer());
    }

}
