package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class RabbitMetadata extends AnimalMetadata {

    public void setType(int type){
        setMeta(12, type);
    }

    public int getType(){
        return (int) getMeta(12);
    }

}
