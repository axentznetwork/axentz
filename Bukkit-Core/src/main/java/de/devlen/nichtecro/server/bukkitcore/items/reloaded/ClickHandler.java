package de.devlen.nichtecro.server.bukkitcore.items.reloaded;

import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * Created by Tarik on 03.04.2016.
 */
public interface ClickHandler {

    void handle(InventoryClickEvent e);

}
