package de.devlen.nichtecro.server.bukkitcore.commands;

import de.devlen.nichtecro.server.bukkitcore.comfy.CommandListener;
import de.devlen.nichtecro.server.bukkitcore.comfy.annotation.Arg;
import de.devlen.nichtecro.server.bukkitcore.comfy.annotation.CommandHandler;
import de.devlen.nichtecro.server.bukkitcore.comfy.bukkit.BukkitCommandContext;
import de.devlen.nichtecro.server.bukkitcore.disguise.Disguise;
import de.devlen.nichtecro.server.bukkitcore.disguise.DisguiseManager;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import de.devlen.nichtecro.server.bukkitcore.npc.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

/**
 * Created by Tarik on 16.05.2016.
 */
public class CommandMorph implements CommandListener {

    @CommandHandler("command.morph.morphtype")
    public void onCommandMorph(BukkitCommandContext c, @Arg("morphType") String morphTypeString) {
        if (c.getSender().isPlayer()) {
            Player player = (Player) c.getSender().getSender();
            EntityType morphType = null;
            try {
                morphType = EntityType.valueOf(morphTypeString.toUpperCase());
            } catch (Exception ex) {
                Chat.send(player, "Morph", "Dieser Morphtyp exsitiert nicht!");
                String morphTypes = "";
                for (EntityType entityType : EntityType.values()) {
                    morphTypes = morphTypes + ((entityType.isAlive()) && (entityType.isSpawnable()) ? "%h" + entityType.name() + "%u, " : "");
                }
                morphTypes = morphTypes.substring(0, morphTypes.length() - 2);
                Chat.send(player, "Morph", "M�gliche Morphtypen: " + morphTypes);
            }

            switch (morphType) {
                case BAT:
                    NPCBat npcBat = new NPCBat(player.getEntityId(), player.getLocation());
                    Disguise<NPCBat> batDisguise = new Disguise(player, npcBat);
                    DisguiseManager.setDisguise(player, batDisguise);
                    break;
                case ENDER_DRAGON:
                    NPCEnderDragon npcEnderDragon = new NPCEnderDragon(player.getEntityId(), player.getLocation());
                    Disguise<NPCEnderDragon> enderDragonDisguise = new Disguise(player, npcEnderDragon);
                    DisguiseManager.setDisguise(player, enderDragonDisguise);
                    break;
                case BLAZE:
                    NPCBlaze npcBlaze = new NPCBlaze(player.getEntityId(), player.getLocation());
                    Disguise<NPCBlaze> blazeDisguise = new Disguise(player, npcBlaze);
                    DisguiseManager.setDisguise(player, blazeDisguise);
                    break;
                case CAVE_SPIDER:
                    NPCCaveSpider npcCaveSpider = new NPCCaveSpider(player.getEntityId(), player.getLocation());
                    Disguise<NPCCaveSpider> caveSpiderDisguise = new Disguise(player, npcCaveSpider);
                    DisguiseManager.setDisguise(player, caveSpiderDisguise);
                    break;
                case CREEPER:
                    NPCCreeper npcCreeper = new NPCCreeper(player.getEntityId(), player.getLocation());
                    Disguise<NPCCreeper> creeperDisguise = new Disguise(player, npcCreeper);
                    DisguiseManager.setDisguise(player, creeperDisguise);
                    break;
                case IRON_GOLEM:
                    NPCIronGolem npcIronGolem = new NPCIronGolem(player.getEntityId(), player.getLocation());
                    Disguise<NPCIronGolem> ironGolemDisguise = new Disguise(player, npcIronGolem);
                    DisguiseManager.setDisguise(player, ironGolemDisguise);
                    break;
                case PIG:
                    NPCPig npcPig = new NPCPig(player.getEntityId(), player.getLocation());
                    Disguise<NPCPig> pigDisguise = new Disguise(player, npcPig);
                    DisguiseManager.setDisguise(player, pigDisguise);
                    break;
                case ZOMBIE:
                    NPCZombie npcZombie = new NPCZombie(player.getEntityId(), player.getLocation());
                    Disguise<NPCZombie> zombieDisguise = new Disguise(player, npcZombie);
                    DisguiseManager.setDisguise(player, zombieDisguise);
                    break;
                case ARMOR_STAND:
                    NPCArmorStand npcArmorStand = new NPCArmorStand(player.getEntityId(), player.getLocation());
                    Disguise<NPCArmorStand> armorStandDisguise = new Disguise(player, npcArmorStand);
                    DisguiseManager.setDisguise(player, armorStandDisguise);
                    break;
                case GHAST:
                    NPCGhast npcGhast = new NPCGhast(player.getEntityId(), player.getLocation());
                    Disguise<NPCGhast> ghastDisguise = new Disguise(player, npcGhast);
                    DisguiseManager.setDisguise(player, ghastDisguise);
                    break;
                case GIANT:
                    NPCGiantZombie npcGiantZombie = new NPCGiantZombie(player.getEntityId(), player.getLocation());
                    Disguise<NPCGiantZombie> giantZombieDisguise = new Disguise(player, npcGiantZombie);
                    DisguiseManager.setDisguise(player, giantZombieDisguise);
                    break;
                case SKELETON:
                    NPCSkeleton npcSkeleton = new NPCSkeleton(player.getEntityId(), player.getLocation());
                    Disguise<NPCSkeleton> skeletonDisguise = new Disguise(player, npcSkeleton);
                    DisguiseManager.setDisguise(player, skeletonDisguise);
                    break;
                case SPIDER:
                    NPCSpider npcSpider = new NPCSpider(player.getEntityId(), player.getLocation());
                    Disguise<NPCSpider> spiderDisguise = new Disguise(player, npcSpider);
                    DisguiseManager.setDisguise(player, spiderDisguise);
                    break;
                case SLIME:
                    NPCSlime npcSlime = new NPCSlime(player.getEntityId(), player.getLocation());
                    Disguise<NPCSlime> slimeDisguise = new Disguise(player, npcSlime);
                    DisguiseManager.setDisguise(player, slimeDisguise);
                    break;
                case PIG_ZOMBIE:
                    NPCZombiePigman npcZombiePigman = new NPCZombiePigman(player.getEntityId(), player.getLocation());
                    Disguise<NPCZombiePigman> zombiePigmanDisguise = new Disguise(player, npcZombiePigman);
                    DisguiseManager.setDisguise(player, zombiePigmanDisguise);
                    break;
                case ENDERMAN:
                    NPCEnderman npcEnderman = new NPCEnderman(player.getEntityId(), player.getLocation());
                    Disguise<NPCEnderman> npcEndermanDisguise = new Disguise(player, npcEnderman);
                    DisguiseManager.setDisguise(player, npcEndermanDisguise);
                    break;
                case SILVERFISH:
                    NPCSilverfish npcSilverfish = new NPCSilverfish(player.getEntityId(), player.getLocation());
                    Disguise<NPCSilverfish> silverfishDisguise = new Disguise(player, npcSilverfish);
                    DisguiseManager.setDisguise(player, silverfishDisguise);
                    break;
                case MAGMA_CUBE:
                    NPCMagmaCube npcMagmaCube = new NPCMagmaCube(player.getEntityId(), player.getLocation());
                    Disguise<NPCMagmaCube> magmaCubeDisguise = new Disguise(player, npcMagmaCube);
                    DisguiseManager.setDisguise(player, magmaCubeDisguise);
                    break;
                case WITHER:
                    NPCWither npcWither = new NPCWither(player.getEntityId(), player.getLocation());
                    Disguise<NPCWither> witherDisguise = new Disguise(player, npcWither);
                    DisguiseManager.setDisguise(player, witherDisguise);
                    break;
                case WITCH:
                    NPCWitch npcWitch = new NPCWitch(player.getEntityId(), player.getLocation());
                    Disguise<NPCWitch> witchDisguise = new Disguise(player, npcWitch);
                    DisguiseManager.setDisguise(player, witchDisguise);
                    break;
                case ENDERMITE:
                    NPCEndermite npcEndermite = new NPCEndermite(player.getEntityId(), player.getLocation());
                    Disguise<NPCEndermite> endermiteDisguise = new Disguise(player, npcEndermite);
                    DisguiseManager.setDisguise(player, endermiteDisguise);
                    break;
                case GUARDIAN:
                    NPCGuardian npcGuardian = new NPCGuardian(player.getEntityId(), player.getLocation());
                    Disguise<NPCGuardian> guardianDisguise = new Disguise(player, npcGuardian);
                    DisguiseManager.setDisguise(player, guardianDisguise);
                    break;
                case SHEEP:
                    NPCSheep npcSheep = new NPCSheep(player.getEntityId(), player.getLocation());
                    Disguise<NPCSheep> sheepDisguise = new Disguise(player, npcSheep);
                    DisguiseManager.setDisguise(player, sheepDisguise);
                    break;
                case COW:
                    NPCCow npcCow = new NPCCow(player.getEntityId(), player.getLocation());
                    Disguise<NPCCow> cowDisguise = new Disguise(player, npcCow);
                    DisguiseManager.setDisguise(player, cowDisguise);
                    break;
                case CHICKEN:
                    NPCChicken npcChicken = new NPCChicken(player.getEntityId(), player.getLocation());
                    Disguise<NPCChicken> chickenDisguise = new Disguise(player, npcChicken);
                    DisguiseManager.setDisguise(player, chickenDisguise);
                    break;
                case SQUID:
                    NPCSquid npcSquid = new NPCSquid(player.getEntityId(), player.getLocation());
                    Disguise<NPCSquid> squidDisguise = new Disguise(player, npcSquid);
                    DisguiseManager.setDisguise(player, squidDisguise);
                    break;
                case WOLF:
                    NPCWolf npcWolf = new NPCWolf(player.getEntityId(), player.getLocation());
                    Disguise<NPCWolf> wolfDisguise = new Disguise(player, npcWolf);
                    DisguiseManager.setDisguise(player, wolfDisguise);
                    break;
                case MUSHROOM_COW:
                    NPCMushroomCow npcMushroomCow = new NPCMushroomCow(player.getEntityId(), player.getLocation());
                    Disguise<NPCMushroomCow> mushroomCowDisguise = new Disguise(player, npcMushroomCow);
                    DisguiseManager.setDisguise(player, mushroomCowDisguise);
                    break;
                case SNOWMAN:
                    NPCSnowman npcSnowman = new NPCSnowman(player.getEntityId(), player.getLocation());
                    Disguise<NPCSnowman> snowmanDisguise = new Disguise(player, npcSnowman);
                    DisguiseManager.setDisguise(player, snowmanDisguise);
                    break;
                case OCELOT:
                    NPCOcelot npcOcelot = new NPCOcelot(player.getEntityId(), player.getLocation());
                    Disguise<NPCOcelot> ocelotDisguise = new Disguise(player, npcOcelot);
                    DisguiseManager.setDisguise(player, ocelotDisguise);
                    break;
                case HORSE:
                    NPCHorse npcHorse = new NPCHorse(player.getEntityId(), player.getLocation());
                    Disguise<NPCHorse> horseDisguise = new Disguise(player, npcHorse);
                    DisguiseManager.setDisguise(player, horseDisguise);
                    break;
                case RABBIT:
                    NPCRabbit npcRabbit = new NPCRabbit(player.getEntityId(), player.getLocation());
                    Disguise<NPCRabbit> rabbitDisguise = new Disguise(player, npcRabbit);
                    DisguiseManager.setDisguise(player, rabbitDisguise);
                    break;
                case VILLAGER:
                    NPCVillager npcVillager = new NPCVillager(player.getEntityId(), player.getLocation());
                    Disguise<NPCVillager> villagerDisguise = new Disguise(player, npcVillager);
                    DisguiseManager.setDisguise(player, villagerDisguise);
                    break;
                case SHULKER:
                    NPCShulker npcShulker = new NPCShulker(player.getEntityId(), player.getLocation());
                    Disguise<NPCShulker> shulkerDisguise = new Disguise<>(player, npcShulker);
                    DisguiseManager.setDisguise(player, shulkerDisguise);
                    break;
                case PLAYER:
                    NPCPlayer npcPlayer = new NPCPlayer(player.getEntityId(), player.getLocation());
                    npcPlayer.setMetdataPlayer(player);
                    npcPlayer.setSkinPlayer(Bukkit.getOfflinePlayer("Notch"));
                    Disguise<NPCPlayer> playerDisguise = new Disguise<>(player, npcPlayer);
                    DisguiseManager.setDisguise(player, playerDisguise);
                    break;

            }
            Disguise disguise = DisguiseManager.getDisguise(player);
            disguise.getNpc().getEntityMetadata().setCustomName(player.getName());
            disguise.getNpc().getEntityMetadata().setCustomNameVisible(true);
            for (Player temporaryExtendedPlayer : Bukkit.getOnlinePlayers()) {
                Disguise.send(player, temporaryExtendedPlayer);
            }
            Chat.send(player, "Morph", "Du hast dich gemorpht zu %l" + DisguiseManager.getDisguise(player).getNpc().getType() + "%u!");
        }
    }

}
