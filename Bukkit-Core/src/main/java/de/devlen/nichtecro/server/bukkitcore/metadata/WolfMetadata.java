package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class WolfMetadata extends TameableAnimalMetadata {

    public void setDamageTaken(float damageTaken) {
        setMeta(14, damageTaken);
    }

    public float getDamageTaken() {
        return (float) getMeta(14);
    }

    public void setBegging(boolean begging) {
        setMeta(15, begging);
    }

    public boolean isBegging() {
        return (boolean) getMeta(15);
    }

    public void setCollarColor(int collarColor) {
        setMeta(16, collarColor);
    }

    public int getCollarColor() {
        return (int) getMeta(16);
    }
}
