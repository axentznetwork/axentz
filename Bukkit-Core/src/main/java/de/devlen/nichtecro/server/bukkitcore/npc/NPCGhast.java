package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.GhastMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCGhast extends NPCFlying {

    public NPCGhast(int entityId, Location location) {
        super(entityId, EntityType.GHAST, location);
    }

    @Override
    public GhastMetadata getEntityMetadata() {
        return (GhastMetadata) super.getEntityMetadata();
    }
}
