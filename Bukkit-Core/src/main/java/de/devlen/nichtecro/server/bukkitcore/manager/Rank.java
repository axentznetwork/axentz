package de.devlen.nichtecro.server.bukkitcore.manager;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.List;

import static de.devlen.nichtecro.server.bukkitcore.language.Language.tl;

/**
 * Created by Tarik on 19.05.2016.
 */
public enum Rank {

    ADMINISTRATOR("�4�l", "bukkit.core.ranks.tag.administrator", "001", 14),
    MODERATOR("�6�l", "bukkit.core.ranks.tag.moderator", "002", 1),
    DEVELOPER("�3�l", "bukkit.core.ranks.tag.developer", "003", 9),
    TRANSLATOR("�2�l", "bukkit.core.ranks.tag.translator", "004", 5),
    MAPPER("�c�l", "bukkit.core.ranks.tag.builder", "005", 14),
    SUPPORTER("�e�l", "bukkit.core.ranks.tag.support", "006", 4),
    YOUTUBER("�f�l", "bukkit.core.ranks.tag.youtube", "007", 0),
    FRIEND("�b�l", "bukkit.core.ranks.tag.friend", "008", 3),
    VETERAN("�1�l", "bukkit.core.ranks.tag.veteran", "009", 11),
    WICKED("�a�l", "bukkit.core.ranks.tag.wicked", "010", 5),
    ANCIENT("�5�l", "bukkit.core.ranks.tag.ancient", "011", 10),
    GUEST("�7", "bukkit.core.ranks.tag.guest", "012", 8);

    private String formatParamters = "";
    private String tagPrefixNode = "";
    private String teamName = "";
    private int damage = 0;

    Rank(String formatParamters, String tagPrefixNode, String teamName, int damage) {
        this.formatParamters = formatParamters;
        this.tagPrefixNode = tagPrefixNode;
        this.teamName = teamName;
        this.damage = damage;
    }

    public String getFormatParamters() {
        return formatParamters;
    }

    public String getChatPrefixTranslatable(Player player) {
        String translated = tl(tagPrefixNode, player);
        return getFormatParamters() + translated + (translated.length() > 0 ? " " : "");
    }

    public String getTagPrefixNode() {
        return tagPrefixNode;
    }

    public String getTeamName() {
        return teamName;
    }

    public int getDamage() {
        return damage;
    }

    public static Rank getHighestRank(List<Rank> ranks) {
        Rank rank = Rank.GUEST;
        for (Rank checkingRank : ranks) {
            if (checkingRank.ordinal() < rank.ordinal()) {
                rank = checkingRank;
            }
        }
        return rank;
    }

    public static void setTagPrefix(Player player, Rank rank) {
        setTagPrefix(player.getName(), rank);
    }

    public static void setTagPrefix(String playerName, Rank rank) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            Scoreboard scoreboard = player.getScoreboard();
            Team team = scoreboard.getTeam(rank.getTeamName());
            if (team == null) {
                team = scoreboard.registerNewTeam(rank.getTeamName());
            }
            team.addEntry(playerName);
        }
    }


    @Override
    public String toString() {
        return "Rank{" +
                "formatParamters='" + formatParamters + '\'' +
                ", tagPrefixNode='" + tagPrefixNode + '\'' +
                ", teamName='" + teamName + '\'' +
                '}';
    }
}
