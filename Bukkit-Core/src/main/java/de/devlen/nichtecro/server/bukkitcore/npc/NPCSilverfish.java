package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.MonsterMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCSilverfish extends NPCMonster {

    public NPCSilverfish(int entityId, Location location) {
        super(entityId, EntityType.SILVERFISH, location);
    }

    @Override
    public MonsterMetadata getEntityMetadata() {
        return super.getEntityMetadata();
    }
}
