package de.devlen.nichtecro.server.bukkitcore.data;

/**
 * Created by Tarik on 05.06.2016.
 */
public class KeyPair<K, V> {

    private K key;
    private V value;

    public KeyPair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }
}
