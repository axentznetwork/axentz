package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class ArrowMetadata extends EntityMetadata {

    public void setCritical(boolean critical){
        setMeta(5, (critical ? 1 : 0));
    }

    public boolean isCritical(){
        return ((byte) getMeta(5) == 1 ? true : false);
    }

}
