package de.devlen.nichtecro.server.bukkitcore.listeners;

import de.devlen.nichtecro.server.bukkitcore.events.TranslatedPlayerChatEvent;
import de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager;
import de.devlen.nichtecro.server.bukkitcore.manager.Rank;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Created by Tarik on 16.05.2016.
 */
public class AsyncPlayerChatListener extends ListenerHelper {

    @EventHandler
    public void onAsyncPlayerChat(AsyncPlayerChatEvent e) {
        Player player = e.getPlayer();
        Rank rank = MPlayerSettingsManager.getDisguisedRank(player);
        TranslatedPlayerChatEvent translatedPlayerChatEvent = new TranslatedPlayerChatEvent(player, e.getMessage(), e.getRecipients());
        translatedPlayerChatEvent.call();
        if (!translatedPlayerChatEvent.isCancelled()) {
            for (Player target : translatedPlayerChatEvent.getParticipants()) {
                target.sendMessage(Chat.replaceFormatVariables(rank.getChatPrefixTranslatable(target) + "%u" + player.getName() + " %s� %u" + e.getMessage()));
            }
        }
        e.setCancelled(true);
    }
}
