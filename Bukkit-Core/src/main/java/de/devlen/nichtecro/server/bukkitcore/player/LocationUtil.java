package de.devlen.nichtecro.server.bukkitcore.player;

import org.bukkit.entity.Player;

/**
 * Created by Tarik on 20.06.2016.
 */
public class LocationUtil {

    public static String getCardinalDirection(Player player) {
        double rotation = (player.getLocation().getYaw() - 90) % 360;
        if (rotation < 0) {
            rotation += 360.0;
        }
        if (0 <= rotation && rotation < 45) {
            return "N";
        } else if (45 <= rotation && rotation < 135) {
            return "E";
        } else if (135 <= rotation && rotation < 225) {
            return "S";
        } else if (225 <= rotation && rotation < 315) {
            return "W";
        } else if (315 <= rotation && rotation < 360.0) {
            return "N";
        } else {
            return null;
        }
    }

}
