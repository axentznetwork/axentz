package de.devlen.nichtecro.server.bukkitcore.metadata;

import org.bukkit.inventory.ItemStack;

/**
 * Created by Tarik on 31.03.2016.
 */
public class ItemMetadata extends EntityMetadata {

    public void setItem(ItemStack item) {
        setMeta(5, item);
    }

    public ItemStack getItem(){
        return (ItemStack) getMeta(5);
    }

}