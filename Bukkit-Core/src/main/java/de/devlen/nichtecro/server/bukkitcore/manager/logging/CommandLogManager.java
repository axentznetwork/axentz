package de.devlen.nichtecro.server.bukkitcore.manager.logging;

import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import de.devlen.nichtecro.server.bukkitcore.database.types.ColumnDescriptor;
import de.devlen.nichtecro.server.bukkitcore.database.types.DataType;
import de.devlen.nichtecro.server.bukkitcore.database.types.SpecialField;
import de.devlen.nichtecro.server.bukkitcore.manager.Manager;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Tarik on 30.03.2016.
 */
public class CommandLogManager extends Manager {

    private static String tableName = getTablePrefix() + "logged_commands";
    private static List<CommandLogEntry> commandLogEntryList = new ExtendedList<>();

    public static void createTable() {
        ColumnDescriptor[] columnDescriptors = {
                new ColumnDescriptor("id", DataType.INT, null, SpecialField.PRIMARY_KEY, SpecialField.AUTO_INC),
                new ColumnDescriptor("uuid", DataType.VARCHAR, 36),
                new ColumnDescriptor("time", DataType.DATETIME, null),
                new ColumnDescriptor("command", DataType.TEXT, 1000)};
        createTable(tableName, columnDescriptors);
    }

    public static void insertCommandExecution(Player player, String command) {
//        Map<String, Object> stringObjectMap = Maps.newHashMap();
//        stringObjectMap.put("uuid", UUIDControl.getUUID(player));
//        stringObjectMap.put("time", "NOW()");
//        stringObjectMap.put("command", command);
//        ResultSet resultSet = insertData(tableName, stringObjectMap);
//        try {
//            while (resultSet.next()) {
//                int id = resultSet.getInt(1);
//                UUID uuid = UUIDControl.getUUID(player);
//                Date date = new Date();
//                CommandLogEntry commandLogEntry = new CommandLogEntry(id, uuid, date, command);
//                commandLogEntryList.add(commandLogEntry);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
    }

    public static void cacheCommandLogEntries() {
        ResultSet resultSet = selectData(tableName, "*", "");
        try {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                UUID uuid = UUID.fromString(resultSet.getString("uuid"));
                Date date = resultSet.getTimestamp("time");
                String command = resultSet.getString("command");
                CommandLogEntry commandLogEntry = new CommandLogEntry(id, uuid, date, command);
                commandLogEntryList.add(commandLogEntry);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<CommandLogEntry> getCommandLogEntries(UUID uuid) {
        List<CommandLogEntry> commandLogEntries = new ExtendedList<>();
        try {
            for (CommandLogEntry commandLogEntry : commandLogEntryList) {
                if (commandLogEntry.getUuid().toString().equalsIgnoreCase(uuid.toString())) {
                     commandLogEntries.add(commandLogEntry);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Collections.reverse(commandLogEntries);
        return commandLogEntries;
    }

}
