package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.TameableAnimalMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCTameableAnimal extends NPCAnimal {

    public NPCTameableAnimal(int entityId, EntityType entityType, Location location) {
        super(entityId, entityType, location);
    }

    @Override
    public TameableAnimalMetadata getEntityMetadata() {
        return (TameableAnimalMetadata) super.getEntityMetadata();
    }
}
