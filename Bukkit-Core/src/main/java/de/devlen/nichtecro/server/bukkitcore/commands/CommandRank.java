package de.devlen.nichtecro.server.bukkitcore.commands;

import de.devlen.nichtecro.server.bukkitcore.comfy.CommandListener;
import de.devlen.nichtecro.server.bukkitcore.comfy.annotation.Arg;
import de.devlen.nichtecro.server.bukkitcore.comfy.annotation.CommandHandler;
import de.devlen.nichtecro.server.bukkitcore.comfy.bukkit.BukkitCommandContext;
import de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager;
import de.devlen.nichtecro.server.bukkitcore.manager.PermissionManager;
import de.devlen.nichtecro.server.bukkitcore.manager.Rank;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import de.devlen.nichtecro.server.bukkitcore.player.UUIDControl;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by Tarik on 20.05.2016.
 */
public class CommandRank implements CommandListener {

    @CommandHandler("command.rank.add.clientname.rank")
    public void onCommandRankAddClientnameRankname(BukkitCommandContext c, @Arg("clientName") String clientName) {
        if (c.getSender().isPlayer()) {
            Rank rank = (Rank) c.getArguments().get("rank");
            Player player = (Player) c.getSender().getSender();
            UUID uuid = UUIDControl.getUUID(clientName);
            if (uuid != null) {
                if (MPlayerSettingsManager.isPlayerExisting(clientName)) {
                    MPlayerSettingsManager.addRank(clientName, rank);
                    Chat.sendTl(player, "bukkit.core.rank.tag", "rank.set.given", Chat.normalizeEnumValueName(rank), clientName);
                    Player target = Bukkit.getPlayer(clientName);
                    if (target != null) {
                        PermissionManager.calculatePermissions(target);
                    }
                } else {
                    Chat.sendTl(player, "bukkit.core.rank.tag", "rank.nodbentry", clientName);
                }
            } else {
                Chat.sendTl(player, "bukkit.core.rank.tag", "rank.nouuidresolved", clientName);
            }
        }
    }


    @CommandHandler("command.rank.remove.clientname.rank")
    public void onCommandRankRemoveClientnameRank(BukkitCommandContext c, @Arg("clientName") String clientName) {
        CommandSender commandSender = c.getSender().getSender();
        Rank rank = (Rank) c.getArguments().get("rank");
        UUID uuid = UUIDControl.getUUID(clientName);
        if (uuid != null) {
            if (MPlayerSettingsManager.isPlayerExisting(clientName)) {
                if (MPlayerSettingsManager.removeRank(clientName, rank)) {
                    if (MPlayerSettingsManager.getDisguisedRank(clientName) == rank) {
                        MPlayerSettingsManager.setDisguisedRank(clientName, null);
                        Rank.setTagPrefix(clientName, Rank.getHighestRank(MPlayerSettingsManager.getRanks(clientName)));
                    }
                    Chat.sendTl(commandSender, "bukkit.core.rank.tag", "rank.set.taken", Chat.normalizeEnumValueName(rank), clientName);
                    Player target = Bukkit.getPlayer(clientName);
                    if (target != null) {
                        PermissionManager.calculatePermissions(target);
                    }
                }
                Chat.sendTl(commandSender, "bukkit.core.rank.tag", "bukkit.core.rank.remove.failed", clientName);
            } else {
                Chat.sendTl(commandSender, "bukkit.core.rank.tag", "rank.nodbentry", clientName);
            }
        } else {
            Chat.sendTl(commandSender, "bukkit.core.rank.tag", "rank.nouuidresolved", clientName);
        }
    }
}
