package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.BlazeMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCBlaze extends NPCMonster {

    public NPCBlaze(int entityId, Location location) {
        super(entityId, EntityType.BLAZE, location);
    }

    @Override
    public BlazeMetadata getEntityMetadata() {
        return (BlazeMetadata) super.getEntityMetadata();
    }
}
