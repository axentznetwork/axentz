package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class ZombieMetadata extends MonsterMetadata {

    public void setBaby(boolean baby) {
        setMeta(11, baby);
    }

    public boolean isBaby() {
        return (boolean) getMeta(11);
    }

    /**
     * Is villager (0 for zombie, profession+1 for villagers)
     *
     * @param type
     */
    public void setType(int type) {
        setMeta(12, type);
    }

    public int getType() {
        return (int) getMeta(12);
    }

    public void setConverting(boolean converting) {
        setMeta(13, converting);
    }

    public boolean isConverting() {
        return (boolean) getMeta(13);
    }

    public void setHandsUp(boolean handsUp){
        setMeta(14, handsUp);
    }

    public boolean areHandsUp(){
        return (boolean) getMeta(14);
    }

}
