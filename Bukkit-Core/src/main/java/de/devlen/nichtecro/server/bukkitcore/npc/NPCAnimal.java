package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.AnimalMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCAnimal extends NPCAgeable {

    public NPCAnimal(int entityId, EntityType entityType, Location location) {
        super(entityId, entityType, location);
    }

    @Override
    public AnimalMetadata getEntityMetadata() {
        return (AnimalMetadata) super.getEntityMetadata();
    }
}
