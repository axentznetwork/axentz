package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class InsentientMetadata extends LivingMetadata {

    /**
     * 0x01 - NoAI
     * 0x02 - Left Handed
     * @param insentientFlags
     */
    public void setInsentientFlags(byte insentientFlags){
        setMeta(10, insentientFlags);
    }

    public byte getInsentientFlags(){
        return (byte) getMeta(10);
    }

}
