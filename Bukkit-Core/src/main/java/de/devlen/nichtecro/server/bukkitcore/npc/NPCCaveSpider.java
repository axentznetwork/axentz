package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.SpiderMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCCaveSpider extends NPCMonster {

    public NPCCaveSpider(int entityId, Location location) {
        super(entityId, EntityType.CAVE_SPIDER, location);
    }

    @Override
    public SpiderMetadata getEntityMetadata() {
        return (SpiderMetadata) super.getEntityMetadata();
    }
}
