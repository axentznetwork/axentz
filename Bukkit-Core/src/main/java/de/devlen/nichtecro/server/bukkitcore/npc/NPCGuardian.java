package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.GuardianMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCGuardian extends NPCMonster {

    public NPCGuardian(int entityId, Location location) {
        super(entityId, EntityType.GUARDIAN, location);
    }

    @Override
    public GuardianMetadata getEntityMetadata() {
        return (GuardianMetadata) super.getEntityMetadata();
    }
}
