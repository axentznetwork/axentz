package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class BatMetadata extends AmbientMetadata {

    public void setHanging(boolean hanging) {
        setMeta(11, (hanging ? 1 : 0));
    }

    public boolean isHanging(){
        return ((byte) getMeta(11) == 1 ? true : false);
    }

}
