package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.ArmorStandMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCArmorStand extends NPCLiving {

    public NPCArmorStand(int entityId, Location location) {
        super(entityId, EntityType.ARMOR_STAND, location);
    }

    @Override
    public ArmorStandMetadata getEntityMetadata() {
        return (ArmorStandMetadata) super.getEntityMetadata();
    }
}
