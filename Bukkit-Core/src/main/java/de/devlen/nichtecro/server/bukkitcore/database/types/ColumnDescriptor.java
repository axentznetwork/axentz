package de.devlen.nichtecro.server.bukkitcore.database.types;

/**
 * Created by Tarik on 21.03.2016.
 */
public class ColumnDescriptor {

    private String columnName;
    private DataType dataTypes;
    private Integer length;
    private SpecialField[] specialFields;

    public ColumnDescriptor(String columnName, DataType dataTypes, Integer length, SpecialField... specialFields) {
        this.columnName = columnName;
        this.dataTypes = dataTypes;
        this.length = length;
        this.specialFields = specialFields;
    }

    public String getColumnName() {
        return columnName;
    }

    public DataType getDataType() {
        return dataTypes;
    }

    public Integer getLength() {
        return length;
    }

    public SpecialField[] getSpecialFields() {
        return specialFields;
    }
}
