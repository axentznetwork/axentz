package de.devlen.nichtecro.server.bukkitcore.language;

import de.devlen.nichtecro.server.bukkitcore.database.MongoDBManager;
import de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager;
import org.bson.Document;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

import static com.mongodb.client.model.Filters.eq;

/**
 * The core-class for getting translation in <i>hopefully</i> several languages.
 *
 * @author Tarik Weiss
 * @see de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager#getLanguage(String) MPlayerSettingsManager.getLanguage(String)
 * @see de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager#getLanguage(Player) MPlayerSettingsManager.getLanguage(Player)
 * @see de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager#getLanguage(UUID) MPlayerSettingsmanager.getLanguage(UUID uuid)
 * @since Bukkit-Core-1.0-SNAPSHOT
 */
public class Language extends MongoDBManager {

    private static String collectionName = getCollectionPrefix() + "translations";

    /**
     * Sample:
     * <blockquote><pre>
     * call: getTranslation("test.node", "en_US", "DevLen_", "You are very nice!");
     * value for translation-node <b>test.node</b> and the language <b>en_US</b>: This is my {0} ign but I want to get another dynamic information like that: {1}
     * returns: This is my DevLen_ ign but I want to get another dynamic information like that: You are very nice!
     * </pre></blockquote>
     *
     * @param translatableNode A node set by the developer e.g. minigame.timv.winner
     * @param commandSender    A player where locale to fetch from.
     * @param variables        Replacings for place-holder like {0} and so on.
     * @return Translated component with replaced fields.
     */
    public static String tl(String translatableNode, CommandSender commandSender, String... variables) {
        if (commandSender instanceof Player) {
            Player player = (Player) commandSender;
            return tl(translatableNode, MPlayerSettingsManager.getLanguage(player), variables);
        }
        return tl(translatableNode, "en_US", variables);
    }

    /**
     * Sample:
     * <blockquote><pre>
     * call: getTranslation("test.node", "en_US", "DevLen_", "You are very nice!");
     * value for translation-node <b>test.node</b> and the language <b>en_US</b>: This is my {0} ign but I want to get another dynamic information like that: {1}
     * returns: This is my DevLen_ ign but I want to get another dynamic information like that: You are very nice!
     * </pre></blockquote>
     *
     * @param translatableNode A node set by the developer e.g. minigame.timv.winner
     * @param locale           A locale got by the player. {@link de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager#getLanguage(UUID)}
     * @param variables        Replacings for place-holder like {0} and so on.
     * @return Translated component with replaced fields.
     */
    public static String tl(String translatableNode, String locale, String... variables) {
        return getTranslation(translatableNode, locale, variables);
    }

    /**
     * Sample:
     * <blockquote><pre>
     * call: getTranslation("test.node", "en_US", "DevLen_", "You are very nice!");
     * value for translation-node <b>test.node</b> and the language <b>en_US</b>: This is my {0} ign but I want to get another dynamic information like that: {1}
     * returns: This is my DevLen_ ign but I want to get another dynamic information like that: You are very nice!
     * </pre></blockquote>
     *
     * @param translatableNode A node set by the developer e.g. minigame.timv.winner
     * @param locale           A locale got by the player. {@link de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager#getLanguage(UUID)}
     * @param variables        Replacings for place-holder like {0} and so on.
     * @return Translated component with replaced fields.
     */
    public static String getTranslation(String translatableNode, String locale, String... variables) {
        Document document = getFirstDocument(collectionName, eq("node", translatableNode));
        if (document != null) {
            Document subDocument = (Document) document.get("languages");
            if (subDocument != null) {
                String translation = subDocument.getString(locale);
                if (translation != null) {
                    for (int i = 0; i < variables.length; i++) {
                        translation = translation.replace("{" + i + "}", variables[i]);
                    }
                } else {
                    if (locale.equalsIgnoreCase("en_US")) {
                        return "%uTranslatable-node: " + translatableNode;
                    } else {
                        translation = subDocument.getString("en_US");
                        if (translation != null) {
                            for (int i = 0; i < variables.length; i++) {
                                translation = translation.replace("{" + i + "}", variables[i]);
                            }
                        } else {
                            return "%uTranslatable-node: " + translatableNode;
                        }
                    }
                }
                return translation;
            }
            return "%uTranslatable-node: " + translatableNode;
        } else {
            return "%uTranslatable-node: " + translatableNode;
        }
    }

}
