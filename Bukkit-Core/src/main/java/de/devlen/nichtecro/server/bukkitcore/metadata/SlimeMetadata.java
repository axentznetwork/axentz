package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class SlimeMetadata extends InsentientMetadata {

    public void setSize(int size){
        setMeta(11, size);
    }

    public int getSize(){
        return (int) getMeta(11);
    }
}
