package de.devlen.nichtecro.server.bukkitcore.manager;

import com.google.common.collect.Maps;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;
import de.devlen.nichtecro.server.bukkitcore.BukkitCore;
import de.devlen.nichtecro.server.bukkitcore.database.MongoDBManager;
import de.devlen.nichtecro.server.bukkitcore.player.UUIDControl;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by Tarik on 21.05.2016.
 */
public class PermissionManager extends MongoDBManager {

    private static String collectionName = getCollectionPrefix() + "permissions";
    private static HashMap<UUID, PermissionAttachment> sessionedPermissions = Maps.newHashMap();

    private static ArrayList<CPermission> cache = new ArrayList<>();

    public static void cache() {
        cache.clear();
        FindIterable<Document> iterable = getCursor(collectionName, null);
        MongoCursor<Document> iterator = iterable.iterator();
        while (iterator.hasNext()) {
            Document document = iterator.next();
            String node = document.getString("node");
            List<String> rankNames = (List<String>) document.get("ranks");
            List<String> serverNames = (List<String>) document.get("servers");
            List<Rank> ranks = new ArrayList<>();
            for (String rankName : rankNames) {
                try {
                    ranks.add(Rank.valueOf(rankName));
                } catch (Exception ex) {
                }
            }
            CPermission cPermission = new CPermission(node, ranks, serverNames);
            cache.add(cPermission);
        }
    }

    public static void calculatePermissions(Player player) {
        invalidatePermissions(player);
        PermissionAttachment attachment = player.addAttachment(BukkitCore.getInstance());
        for (CPermission cPermission : cache) {
            for (Rank rank : MPlayerSettingsManager.getRanks(player)) {
                if (cPermission.getRanks().contains(rank)) {
                    if (cPermission.getServers().size() < 1 || cPermission.getServers().contains(Bukkit.getServerName())) {
                        if (cPermission.getNode().startsWith("!")) {
                            attachment.setPermission(cPermission.getNode().substring(1), false);
                        } else {
                            attachment.setPermission(cPermission.getNode(), true);
                        }
                    }
                }
            }
        }
        sessionedPermissions.put(UUIDControl.getUUID(player), attachment);
    }

    public static void invalidatePermissions(Player player) {
        if (sessionedPermissions.containsKey(UUIDControl.getUUID(player))) {
            player.removeAttachment(sessionedPermissions.get(UUIDControl.getUUID(player)));
            sessionedPermissions.remove(UUIDControl.getUUID(player));
        }
    }


}
