package de.devlen.nichtecro.server.bukkitcore.items.reloaded;

import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Created by Tarik on 05.06.2016.
 */
public interface InteractHandler {

    void handle(PlayerInteractEvent e);

}
