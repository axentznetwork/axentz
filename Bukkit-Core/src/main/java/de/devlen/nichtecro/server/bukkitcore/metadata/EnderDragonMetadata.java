package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class EnderDragonMetadata extends InsentientMetadata {


    public void setPhase(int phase) {
        setMeta(11, phase);
    }

    public int getPhase() {
        return (int) getMeta(11);
    }

}
