package de.devlen.nichtecro.server.bukkitcore.metadata;

import org.bukkit.inventory.ItemStack;

/**
 * Created by Tarik on 31.03.2016.
 */
public class PotionMetadata extends EntityMetadata {

    public void setItemStack(ItemStack itemStack){
        setMeta(5, itemStack);
    }

    public ItemStack getItemStack(){
        return (ItemStack) getMeta(5);
    }

}
