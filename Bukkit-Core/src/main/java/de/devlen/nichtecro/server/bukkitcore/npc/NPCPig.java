package de.devlen.nichtecro.server.bukkitcore.npc;

import de.devlen.nichtecro.server.bukkitcore.metadata.PigMetadata;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

/**
 * Created by Tarik on 31.03.2016.
 */
public class NPCPig extends NPCAnimal {

    public NPCPig(int entityId, Location location) {
        super(entityId, EntityType.PIG, location);
    }

    @Override
    public PigMetadata getEntityMetadata() {
        return (PigMetadata) super.getEntityMetadata();
    }
}
