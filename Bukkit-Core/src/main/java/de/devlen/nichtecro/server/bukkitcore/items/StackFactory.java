package de.devlen.nichtecro.server.bukkitcore.items;

import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.Arrays;

/**
 * Created by Tarik on 02.04.2016.
 */
public class StackFactory {

    private ItemStack itemStack = new ItemStack(Material.STONE);

    public StackFactory() {
    }

    public StackFactory(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public StackFactory material(Material material) {
        itemStack.setType(material);
        return this;
    }

    public StackFactory amount(int amount) {
        itemStack.setAmount(amount);
        return this;
    }

    public StackFactory subid(int subid) {
        itemStack.setDurability((short) subid);
        return this;
    }

    public StackFactory name(String displayName) {
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(Chat.replaceFormatVariables(displayName));
        itemStack.setItemMeta(itemMeta);
        return this;
    }

    public StackFactory lore(String... lore) {
        ItemMeta itemMeta = itemStack.getItemMeta();
        for (int i = 0; i < lore.length; i++) {
            lore[i] = Chat.replaceFormatVariables("%u" + lore[i]);
        }
        itemMeta.setLore(Arrays.asList(lore));
        itemStack.setItemMeta(itemMeta);
        return this;
    }

    public StackFactory flags(ItemFlag... itemFlags) {
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.addItemFlags(itemFlags);
        itemStack.setItemMeta(itemMeta);
        return this;
    }

    public StackFactory leatherColor(Color color) {
        flags(ItemFlag.HIDE_ATTRIBUTES);
        LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) itemStack.getItemMeta();
        leatherArmorMeta.setColor(color);
        itemStack.setItemMeta(leatherArmorMeta);
        return this;
    }

    public StackFactory enchant(Enchantment enchantment, int level) {
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.addEnchant(enchantment, level, true);
        itemStack.setItemMeta(itemMeta);
        return this;
    }

    public ItemStack build() {
        return itemStack;
    }

}
