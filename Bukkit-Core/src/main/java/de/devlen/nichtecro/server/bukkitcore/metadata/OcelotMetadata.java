package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class OcelotMetadata extends TameableAnimalMetadata {

    public void setType(int type){
        setMeta(14, type);
    }

    public int getType(){
        return (int) getMeta(14);
    }

}
