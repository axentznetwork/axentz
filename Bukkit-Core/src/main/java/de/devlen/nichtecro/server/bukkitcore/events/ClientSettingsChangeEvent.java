package de.devlen.nichtecro.server.bukkitcore.events;

import org.bukkit.entity.Player;

/**
 * Created by Tarik on 26.05.2016.
 */
public class ClientSettingsChangeEvent extends EventHelper {

    private Player player = null;
    private String locale = "";

    public ClientSettingsChangeEvent(Player player, String locale) {
        this.player = player;
        this.locale = locale;
    }

    public Player getPlayer() {
        return player;
    }

    public String getLocale() {
        return locale;
    }
}
