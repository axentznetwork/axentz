package de.devlen.nichtecro.server.bukkitcore.metadata;

/**
 * Created by Tarik on 31.03.2016.
 */
public class LivingMetadata extends EntityMetadata {

    public void setHealth(float health) {
        setMeta(6, health);
    }

    public float getHealth() {
        return (float) getMeta(6);
    }

    public void setPotionEffectColor(int color) {
        setMeta(7, color);
    }

    public int getPotionEffectColor() {
        return (int) getMeta(7);
    }

    public void setPotionEffectAmbient(boolean potionEffectAmbient) {
        setMeta(8, potionEffectAmbient);
    }

    public boolean isPotionEffectAmbient() {
        return (boolean) getMeta(8);
    }

    public void setNumberOfArrows(int numberOfArrows) {
        setMeta(9, numberOfArrows);
    }

    public int getNumberOfArrows() {
        return (int) getMeta(9);
    }

}
