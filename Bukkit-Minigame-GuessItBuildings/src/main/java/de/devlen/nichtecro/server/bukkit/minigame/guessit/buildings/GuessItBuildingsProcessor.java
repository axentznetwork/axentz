package de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings;

import de.devlen.nichtecro.server.bukkit.minigame.core.game.Game;
import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Random;

/**
 * Created by Tarik on 02.06.2016.
 */
public class GuessItBuildingsProcessor {

    private static Player currentBuilder = null;
    private static String currentWordNode = "";
    private static List<String> wordNodes = new ExtendedList<>(
            "bukkit.minigame.guessit.buildings.car",
            "bukkit.minigame.guessit.buildings.house",
            "bukkit.minigame.guessit.buildings.minecraft",
            "bukkit.minigame.guessit.buildings.keyboard",
            "bukkit.minigame.guessit.buildings.piano",
            "bukkit.minigame.guessit.buildings.screen",
            "bukkit.minigame.guessit.buildings.mirror",
            "bukkit.minigame.guessit.buildings.phone",
            "bukkit.minigame.guessit.buildings.laptop",
            "bukkit.minigame.guessit.buildings.folder",
            "bukkit.minigame.guessit.buildings.wallet",
            "bukkit.minigame.guessit.buildings.tv",
            "bukkit.minigame.guessit.buildings.carpet",
            "bukkit.minigame.guessit.buildings.towel",
            "bukkit.minigame.guessit.buildings.cable",
            "bukkit.minigame.guessit.buildings.spider",
            "bukkit.minigame.guessit.buildings.ghast",
            "bukkit.minigame.guessit.buildings.zombie",
            "bukkit.minigame.guessit.buildings.money",
            "bukkit.minigame.guessit.buildings.nintendo_ds",
            "bukkit.minigame.guessit.buildings.pen",
            "bukkit.minigame.guessit.buildings.biro",
            "bukkit.minigame.guessit.buildings.cooker",
            "bukkit.minigame.guessit.buildings.fish",
            "bukkit.minigame.guessit.buildings.monkey",
            "bukkit.minigame.guessit.buildings.flag",
            "bukkit.minigame.guessit.buildings.watch",
            "bukkit.minigame.guessit.buildings.arrow",
            "bukkit.minigame.guessit.buildings.coffee",
            "bukkit.minigame.guessit.buildings.mouse",
            "bukkit.minigame.guessit.buildings.music",
            "bukkit.minigame.guessit.buildings.blackboard",
            "bukkit.minigame.guessit.buildings.mickeymouse",
            "bukkit.minigame.guessit.buildings.scissors",
            "bukkit.minigame.guessit.buildings.map",
            "bukkit.minigame.guessit.buildings.calculator",
            "bukkit.minigame.guessit.buildings.gun",
            "bukkit.minigame.guessit.buildings.glasses",
            "bukkit.minigame.guessit.buildings.shield",
            "bukkit.minigame.guessit.buildings.switch",
            "bukkit.minigame.guessit.buildings.button",
            "bukkit.minigame.guessit.buildings.taxi",
            "bukkit.minigame.guessit.buildings.lamp",
            "bukkit.minigame.guessit.buildings.love",
            "bukkit.minigame.guessit.buildings.dice",
            "bukkit.minigame.guessit.buildings.slime");

    public static boolean isCurrentBuilder(Player player) {
        if (getCurrentBuilder().getName().equalsIgnoreCase(player.getName())) {
            return true;
        }
        return false;
    }

    public static String getNextRandomWordNode() {
        int randomPosition = new Random().nextInt(wordNodes.size());
        String wordNode = wordNodes.get(randomPosition);
        wordNodes.remove(randomPosition);
        return wordNode;
    }

    public static Player getCurrentBuilder() {
        return currentBuilder;
    }

    public static void setCurrentBuilder(Player currentBuilder) {
        if (getCurrentBuilder() != null && getCurrentBuilder().isOnline()) {
            getCurrentBuilder().teleport(Game.getInstance().getGameSpawns().get(0));
            getCurrentBuilder().setGameMode(GameMode.ADVENTURE);
            getCurrentBuilder().getInventory().clear();
        }
        GuessItBuildingsProcessor.currentBuilder = currentBuilder;
        if (currentBuilder != null) {
            currentBuilder.teleport(Game.getInstance().getGameSpawns().get(1));
            currentBuilder.setGameMode(GameMode.CREATIVE);
            currentBuilder.getInventory().clear();
        }
    }

    public static String getCurrentWordNode() {
        return currentWordNode;
    }

    public static void setCurrentWordNode(String currentWordNode) {
        GuessItBuildingsProcessor.currentWordNode = currentWordNode;
    }
}
