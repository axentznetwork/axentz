package de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.listeners;

import de.devlen.nichtecro.server.bukkit.minigame.core.game.Game;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.GameState;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.taskmanager.GameRoundTaskManager;
import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.lobby.LobbyStackManager;
import de.devlen.nichtecro.server.bukkitcore.bridge.BungeeCordBridge;
import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import de.devlen.nichtecro.server.bukkitcore.validation.Validate;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Created by Tarik on 03.06.2016.
 */
public class PlayerInteractListenener extends ListenerHelper {

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        if (Game.getInstance().getGameState() == GameState.WAITING) {
            if (e.getItem() != null) {
                if (Validate.isSimilarItem(e.getItem(), LobbyStackManager.getBackToHubStack(player))) {
                    Chat.sendTl(player, Game.getInstance().getGameNameNode(), "bukkit.minigame.core.action.hub");
                    BungeeCordBridge.sendPlayerToServer(player, "lobby01");
                } else if (Validate.isSimilarItem(e.getItem(), LobbyStackManager.getForceStartStack(player))) {
                    if (player.hasPermission("bukkit.minigame.core.forcestart")) {
                        if (Bukkit.getOnlinePlayers().size() > 1) {
                            Game.getInstance().announce("bukkit.minigame.core.forcestart.executed");
                            GameRoundTaskManager gameRoundTaskManager = (GameRoundTaskManager) Game.getInstance().getGameTaskManager();
                            gameRoundTaskManager.startGame(true);
                        } else {
                            Chat.sendTl(player, Game.getInstance().getGameNameNode(), "bukkit.minigame.core.forcestart.failed");
                        }
                    } else {
                        LobbyStackManager.updateLobbyInventory(player);
                    }
                }
            }
            e.setCancelled(true);
        }
    }

}
