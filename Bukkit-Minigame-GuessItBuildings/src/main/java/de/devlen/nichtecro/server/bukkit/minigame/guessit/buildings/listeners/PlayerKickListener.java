package de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.listeners;

import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.manager.GuessItBuildingsPlayers;
import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerKickEvent;

/**
 * Created by Tarik on 02.06.2016.
 */
public class PlayerKickListener extends ListenerHelper {

    @EventHandler
    public void onPlayerKick(PlayerKickEvent e) {
        GuessItBuildingsPlayers.makeCleanPlayerLeave(e.getPlayer());
    }

}
