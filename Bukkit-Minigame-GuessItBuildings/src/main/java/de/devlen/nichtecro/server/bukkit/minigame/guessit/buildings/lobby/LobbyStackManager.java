package de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.lobby;

import de.devlen.nichtecro.server.bukkitcore.items.StackFactory;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import static de.devlen.nichtecro.server.bukkitcore.language.Language.tl;

/**
 * Created by Tarik on 03.06.2016.
 */
public class LobbyStackManager {

    public static void clearLobbyInventory(Player player){
        PlayerInventory playerInventory = player.getInventory();
        playerInventory.clear();
        playerInventory.setHelmet(null);
        playerInventory.setChestplate(null);
        playerInventory.setLeggings(null);
        playerInventory.setBoots(null);
    }

    public static void updateLobbyInventory(Player player) {
        PlayerInventory playerInventory = player.getInventory();
        playerInventory.clear();
        playerInventory.setHelmet(null);
        playerInventory.setChestplate(null);
        playerInventory.setLeggings(null);
        playerInventory.setBoots(null);
        if (player.hasPermission("bukkit.minigame.core.forcestart")) {
            playerInventory.setItem(0, getForceStartStack(player));
        }
        playerInventory.setItem(8, getBackToHubStack(player));
    }

    public static ItemStack getForceStartStack(Player player) {
        return new StackFactory().name(tl("bukkit.minigame.core.itemstack.name.forcestart", player)).material(Material.REDSTONE).build();
    }

    public static ItemStack getBackToHubStack(Player player) {
        return new StackFactory().name(tl("bukkit.minigame.core.itemstack.name.hub", player)).material(Material.MAGMA_CREAM).build();
    }

}
