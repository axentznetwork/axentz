package de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.listeners;

import de.devlen.nichtecro.server.bukkit.minigame.core.game.Game;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.GameState;
import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.lobby.LobbyStackManager;
import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by Tarik on 02.06.2016.
 */
public class PlayerJoinListener extends ListenerHelper {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        player.setGameMode(GameMode.ADVENTURE);
        if (Game.getInstance().getGameState() == GameState.WAITING) {
            LobbyStackManager.updateLobbyInventory(player);
        } else {
            LobbyStackManager.clearLobbyInventory(player);
        }
    }
}
