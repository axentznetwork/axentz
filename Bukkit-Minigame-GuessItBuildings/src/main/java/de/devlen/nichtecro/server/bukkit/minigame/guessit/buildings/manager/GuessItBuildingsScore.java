package de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.manager;

import java.util.UUID;

/**
 * Created by Tarik on 02.06.2016.
 */
public class GuessItBuildingsScore {

    private UUID uuid = null;
    private int knowerOfBuildings = 0;
    private int knownBuildings = 0;
    private int firstKnownBuildings = 0;

    public GuessItBuildingsScore(UUID uuid) {
        this.uuid = uuid;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void addKnowerOfBuildings() {
        knowerOfBuildings += 1;
    }

    public void addKnownBuilding() {
        knownBuildings += 1;
    }

    public void addFirstKnownBuilding() {
        addKnownBuilding();
        firstKnownBuildings += 1;
    }

    public int getKnowerOfBuildings() {
        return knowerOfBuildings;
    }

    public int getKnownBuildings() {
        return knownBuildings;
    }

    public int getFirstKnownBuildings() {
        return firstKnownBuildings;
    }

    public int getCompletePoints() {
        return getKnowerOfBuildings() * 2 + getKnownBuildings() + getFirstKnownBuildings() * 3;
    }
}
