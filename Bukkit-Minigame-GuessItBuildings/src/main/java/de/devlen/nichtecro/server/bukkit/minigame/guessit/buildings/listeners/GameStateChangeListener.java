package de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.listeners;

import de.devlen.nichtecro.server.bukkit.minigame.core.events.GameStateChangeEvent;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.Game;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.GamePlayerManager;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.GameState;
import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.GuessItBuildingsProcessor;
import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.lobby.LobbyStackManager;
import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.manager.GuessItBuildingsBlockStorage;
import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.manager.GuessItBuildingsPlayers;
import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.manager.GuessItBuildingsScore;
import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.manager.GuessItBuildingsWordStorage;
import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import java.util.List;

/**
 * Created by Tarik on 02.06.2016.
 */
public class GameStateChangeListener extends ListenerHelper {

    @EventHandler
    public void onGameStateChange(GameStateChangeEvent e) {
        if (e.getToState() == GameState.RUNNING) {
            GuessItBuildingsPlayers.preparePlayerOrder();
            for (Player target : Bukkit.getOnlinePlayers()) {
                LobbyStackManager.clearLobbyInventory(target);
                target.teleport(Game.getInstance().getGameSpawns().get(0));
            }
        } else if (e.getToState() == GameState.ENDING) {
            for (Player target : Bukkit.getOnlinePlayers()) {
                target.setGameMode(GameMode.ADVENTURE);
                target.setLevel(0);
            }

            GuessItBuildingsPlayers.sortScores();
            List<GuessItBuildingsScore> guessItBuildingsScores = GuessItBuildingsPlayers.getGuessItBuildingsScores();

            if (guessItBuildingsScores.size() > 0) {
                Player first = Bukkit.getPlayer(guessItBuildingsScores.get(0).getUuid());
                Game.getInstance().announce("bukkit.minigame.guessit.buildings.winner.first", first.getName());
                GamePlayerManager.setGameValue(first, "guessit-buildings", "wins", GamePlayerManager.getGameValueInteger(first, "guessit-buildings", "wins") + 1);
                MPlayerSettingsManager.transferDyes(first, 20);

            }

            if (guessItBuildingsScores.size() > 1) {
                Player second = Bukkit.getPlayer(guessItBuildingsScores.get(1).getUuid());
                Game.getInstance().announce("bukkit.minigame.guessit.buildings.winner.second", second.getName());
                MPlayerSettingsManager.transferDyes(second, 10);
            }

            if (guessItBuildingsScores.size() > 2) {
                Player third = Bukkit.getPlayer(guessItBuildingsScores.get(2).getUuid());
                Game.getInstance().announce("bukkit.minigame.guessit.buildings.winner.third", third.getName());
                MPlayerSettingsManager.transferDyes(third, 5);
            }

            for (Player target : Bukkit.getOnlinePlayers()) {
                GamePlayerManager.setGameValue(target, "guessit-buildings", "score", GamePlayerManager.getGameValueInteger(target, "guessit-buildings", "score") + GuessItBuildingsPlayers.getScore(target).getCompletePoints());
                GamePlayerManager.setGameValue(target, "guessit-buildings", "knowerOfBuildings", GamePlayerManager.getGameValueInteger(target, "guessit-buildings", "knowerOfBuildings") + GuessItBuildingsPlayers.getScore(target).getKnowerOfBuildings());
                GamePlayerManager.setGameValue(target, "guessit-buildings", "targetKnownBuildings", GamePlayerManager.getGameValueInteger(target, "guessit-buildings", "targetKnownBuildings") + GuessItBuildingsPlayers.getScore(target).getFirstKnownBuildings());
                GamePlayerManager.setGameValue(target, "guessit-buildings", "allKnownBuildings", GamePlayerManager.getGameValueInteger(target, "guessit-buildings", "allKnownBuildings") + GuessItBuildingsPlayers.getScore(target).getKnownBuildings());
                GamePlayerManager.setGameValue(target, "guessit-buildings", "played", GamePlayerManager.getGameValueInteger(target, "guessit-buildings", "played") + 1);
                MPlayerSettingsManager.transferDyes(target, 5);
            }

            GuessItBuildingsWordStorage.clearSession();
            GuessItBuildingsBlockStorage.clearSession();
            GuessItBuildingsProcessor.setCurrentWordNode("");
            GuessItBuildingsProcessor.setCurrentBuilder(null);
            GuessItBuildingsPlayers.getGuessItBuildingsScores().clear();
        }
    }

}
