package de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.listeners;

import de.devlen.nichtecro.server.bukkit.minigame.core.events.GameEndRoundEvent;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.Game;
import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.GuessItBuildingsProcessor;
import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.manager.GuessItBuildingsBlockStorage;
import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.manager.GuessItBuildingsWordStorage;
import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import static de.devlen.nichtecro.server.bukkitcore.language.Language.tl;

/**
 * Created by Tarik on 02.06.2016.
 */
public class GameEndRoundListener extends ListenerHelper {

    @EventHandler
    public void onGameEndRound(GameEndRoundEvent e) {
        GuessItBuildingsBlockStorage.clearSession();
        for (Player target : Bukkit.getOnlinePlayers()) {
                Chat.send(target, tl(Game.getInstance().getGameNameNode(), target), tl("bukkit.minigame.guessit.buildings.word.resolve", target, tl(GuessItBuildingsProcessor.getCurrentWordNode(), target)));
        }
        GuessItBuildingsWordStorage.clearSession();
    }

}
