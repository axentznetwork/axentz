package de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.listeners;

import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.manager.GuessItBuildingsBlockStorage;
import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockPlaceEvent;

/**
 * Created by Tarik on 02.06.2016.
 */
public class BlockPlaceListener extends ListenerHelper {

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        GuessItBuildingsBlockStorage.addLocation(e.getBlock().getLocation());
    }

}
