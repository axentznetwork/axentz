package de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings;

import de.devlen.nichtecro.server.bukkit.minigame.core.game.Game;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.GameState;
import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.listeners.*;
import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Tarik on 02.06.2016.
 */
public class GuessItBuildings extends JavaPlugin {

    private static Plugin instance = null;

    public void onEnable() {
        instance = this;

        for (World world : Bukkit.getWorlds()) {
            world.setGameRuleValue("doFireTick", "false");
        }

        Game game = Game.getInstance();
        game.setGameState(GameState.WAITING);
        game.setBlockBreakingAllowed(false);
        game.setFullLobbyTime(120);
        game.setRoundBased(true);
        game.setRounds(10);
        game.setFullGameTime(120); //itz roundbased!
        game.setShrinkedGameTime(30);
        game.setHungerAllowed(false);
        game.setGameNameNode("bukkit.minigame.guessit.buildings.tag");
        game.setMinimumPlayerAmount(2);
        game.setMaximumPlayerAmount(8);
        game.setMultipleGameSpawnsEnabled(false);
        game.setLobbySpawnLocation(new Location(Bukkit.getWorld("lobby_2_0"), 251.5, 38, -490.5));
        game.setEvaluationSpawnLocation(new Location(Bukkit.getWorld("lobby_2_0"), 238.5, 41, -488.5));
        game.setGameSpawns(new ExtendedList<>(new Location(Bukkit.getWorld("lobby_2_0"), 237.5, 46, -521.5), new Location(Bukkit.getWorld("lobby_2_0"), 272.5, 47, -522.5)));
        game.setShrinkedLobbyTime(30);
        game.setEntitySpawnAllowed(false);
        game.setRespawnTime(0);
        game.setPlayerDamageAllowed(false);
        game.setPlayerRespawnAllowed(true);

        registerListeners();
    }

    public static void registerListeners() {
        new TranslatedPlayerChatListener().register();
        new BlockBreakListener().register();
        new BlockPlaceListener().register();
        new GameEndRoundListener().register();
        new GameNewRoundListener().register();
        new GameRoundAmountChangedListener().register();
        new GameStateChangeListener().register();
        new PlayerInteractListenener().register();
        new PlayerJoinListener().register();
        new PlayerKickListener().register();
        new PlayerQuitListener().register();
    }

    public static Plugin getInstance() {
        return instance;
    }
}
