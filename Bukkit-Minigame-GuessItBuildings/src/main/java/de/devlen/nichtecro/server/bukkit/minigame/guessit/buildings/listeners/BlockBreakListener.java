package de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.listeners;

import de.devlen.nichtecro.server.bukkit.minigame.core.game.Game;
import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.GuessItBuildingsProcessor;
import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.manager.GuessItBuildingsBlockStorage;
import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;

/**
 * Created by Tarik on 02.06.2016.
 */
public class BlockBreakListener extends ListenerHelper {

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        Player player = e.getPlayer();
        if (GuessItBuildingsProcessor.getCurrentBuilder().getUniqueId().equals(player.getUniqueId())) {
            if (GuessItBuildingsBlockStorage.containsLocation(e.getBlock().getLocation())) {
                e.setCancelled(false);
                e.setExpToDrop(0);
            } else {
                e.setCancelled(true);
                Chat.sendTl(player, Game.getInstance().getGameNameNode(), "bukkit.minigame.guessit.break.block.denied");
            }
        }else{
            e.setCancelled(true);
            Chat.sendTl(player, Game.getInstance().getGameNameNode(), "bukkit.minigame.guessit.break.block.denied");
        }
    }

}
