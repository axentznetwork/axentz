package de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.listeners;

import de.devlen.nichtecro.server.bukkit.minigame.core.game.Game;
import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.GuessItBuildingsProcessor;
import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.manager.GuessItBuildingsWordStorage;
import de.devlen.nichtecro.server.bukkitcore.events.TranslatedPlayerChatEvent;
import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager;
import de.devlen.nichtecro.server.bukkitcore.manager.Rank;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import static de.devlen.nichtecro.server.bukkitcore.language.Language.tl;

/**
 * Created by Tarik on 02.06.2016.
 */
public class TranslatedPlayerChatListener extends ListenerHelper {

    @EventHandler
    public void onTranslatedPlayerChat(TranslatedPlayerChatEvent e) {
        Player player = e.getPlayer();
        if (GuessItBuildingsProcessor.isCurrentBuilder(player)) {
            Chat.sendTl(player, Game.getInstance().getGameNameNode(), "bukkit.minigame.guessit.buildings.chat.noshout");
            e.setCancelled(true);
        } else {
            if (!GuessItBuildingsWordStorage.isKnowingWord(player)) {
                if (tl(GuessItBuildingsProcessor.getCurrentWordNode(), player).equalsIgnoreCase(e.getMessage())) {
                    if (GuessItBuildingsWordStorage.isWordAlreadyKnown()) {
                        Chat.sendTl(player, Game.getInstance().getGameNameNode(), "bukkit.minigame.guessit.buildings.chat.knows");
                        for (Player target : Bukkit.getOnlinePlayers()) {
                            if (player == null || (player != null && !player.getName().equalsIgnoreCase(target.getName()))) {
                                Chat.send(target, tl(Game.getInstance().getGameNameNode(), target), tl("bukkit.minigame.guessit.buildings.announce.knows", target, Rank.getHighestRank(MPlayerSettingsManager.getRanks(player)).getChatPrefixTranslatable(target), player.getName()));
                            }
                        }
                        e.setCancelled(true);
                    } else {
                        Chat.sendTl(player, Game.getInstance().getGameNameNode(), "bukkit.minigame.guessit.buildings.chat.knows.first");
                        for (Player target : Bukkit.getOnlinePlayers()) {
                            if (player == null || (player != null && !player.getName().equalsIgnoreCase(target.getName()))) {
                                Chat.send(target, tl(Game.getInstance().getGameNameNode(), target), tl("bukkit.minigame.guessit.buildings.announce.knows.first", target, Rank.getHighestRank(MPlayerSettingsManager.getRanks(player)).getChatPrefixTranslatable(target), player.getName()));
                            }
                        }
                        e.setCancelled(true);
                    }
                    GuessItBuildingsWordStorage.addWhoKnowsWord(player);
                }
                e.setCancelled(true);
            } else {
                Chat.sendTl(player, Game.getInstance().getGameNameNode(), "bukkit.minigame.guessit.buildings.chat.noshout");
                e.setCancelled(true);
            }
        }
    }
}
