package de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.manager;

import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import org.bukkit.Location;
import org.bukkit.Material;

import java.util.List;

/**
 * Created by Tarik on 02.06.2016.
 */
public class GuessItBuildingsBlockStorage {

    private static List<Location> sessionedLocations = new ExtendedList<>();

    public static void addLocation(Location location) {
        sessionedLocations.add(location);
    }

    public static boolean containsLocation(Location location) {
        return sessionedLocations.contains(location);
    }

    public static void clearSession() {
        for (Location location : sessionedLocations) {
            location.getBlock().setType(Material.AIR);
        }

        sessionedLocations.clear();
    }

}
