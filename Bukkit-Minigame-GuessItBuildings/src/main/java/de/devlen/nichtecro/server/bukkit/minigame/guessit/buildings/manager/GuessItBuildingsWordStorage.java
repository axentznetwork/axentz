package de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.manager;

import com.google.common.collect.Maps;
import de.devlen.nichtecro.server.bukkit.minigame.core.BukkitMinigameCore;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.Game;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.taskmanager.GameRoundTaskManager;
import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.GuessItBuildings;
import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.GuessItBuildingsProcessor;
import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;

import java.util.*;

import static de.devlen.nichtecro.server.bukkitcore.language.Language.tl;

/**
 * Created by Tarik on 02.06.2016.
 */
public class GuessItBuildingsWordStorage {

    private static boolean alreadyKnown = false;
    private static List<UUID> knowCurrentWord = new ExtendedList<>();
    private static HashMap<UUID, Integer> revealingTaskIds = Maps.newHashMap();
    private static Map<UUID, String> hintWords = Maps.newHashMap();
    private static Map<UUID, BossBar> bossBars = Maps.newHashMap();

    public static void addWhoKnowsWord(Player player) {
        if (!alreadyKnown) {
            if (!knowCurrentWord.contains(player.getUniqueId())) {
                knowCurrentWord.add(player.getUniqueId());
                GuessItBuildingsPlayers.getScore(player).addFirstKnownBuilding();
                GuessItBuildingsPlayers.getScore(GuessItBuildingsProcessor.getCurrentBuilder()).addKnowerOfBuildings();
            }
            alreadyKnown = true;
        } else {
            if (!knowCurrentWord.contains(player.getUniqueId())) {
                knowCurrentWord.add(player.getUniqueId());
                GuessItBuildingsPlayers.getScore(player).addKnownBuilding();
                GuessItBuildingsPlayers.getScore(GuessItBuildingsProcessor.getCurrentBuilder()).addKnowerOfBuildings();
            }
        }
        if ((knowCurrentWord.size() >= (Bukkit.getOnlinePlayers().size() - 1))) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(GuessItBuildings.getInstance(), new Runnable() {
                @Override
                public void run() {
                    GameRoundTaskManager gameRoundTaskManager = (GameRoundTaskManager) Game.getInstance().getGameTaskManager();
                    gameRoundTaskManager.forceNextRound();
                }
            },0);
        } else if (knowCurrentWord.size() / (Bukkit.getOnlinePlayers().size() - 1) >= 0.5f) {
            System.out.println("Speed up gametime...");
            Game.getInstance().getGameTaskManager().speedUpGameTime();
        }
    }

    public static boolean isWordAlreadyKnown() {
        return alreadyKnown;
    }

    public static boolean isKnowingWord(Player player) {
        return knowCurrentWord.contains(player.getUniqueId());
    }

    public static void clearSession() {
        alreadyKnown = false;
        knowCurrentWord = new ExtendedList<>();
        for (Integer taskId : revealingTaskIds.values()) {
            Bukkit.getScheduler().cancelTask(taskId);
        }
        revealingTaskIds.clear();
        hintWords.clear();
        for (BossBar bossBar : bossBars.values()) {
            for (Player target : bossBar.getPlayers()) {
                bossBar.removePlayer(target);
            }
        }
    }

    public static void runHintTask(final Player player) {
        int fullGameTime = Game.getInstance().getFullGameTime();
        final String translatedWord = tl(GuessItBuildingsProcessor.getCurrentWordNode(), player);
        int delay = (int) ((int) (((float) fullGameTime / translatedWord.length()) * 20) * 1.3);
        String hint = "";
        String[] words = translatedWord.split(" ");
        for (String word : words) {
            hint += StringUtils.repeat("_", word.length());
            hint += " ";
        }
        hint = hint.substring(0, hint.length() - 1);
        hintWords.put(player.getUniqueId(), hint);
        bossBars.put(player.getUniqueId(), Bukkit.createBossBar(hint, BarColor.BLUE, BarStyle.SOLID));
        bossBars.get(player.getUniqueId()).addPlayer(player);
        revealingTaskIds.put(player.getUniqueId(), Bukkit.getScheduler().scheduleSyncRepeatingTask(BukkitMinigameCore.getInstance(), new Runnable() {
            @Override
            public void run() {
                Random random = new Random();
                List<Integer> positions = new ExtendedList<>();
                String obfuscatedWord = hintWords.get(player.getUniqueId());
                for (int i = 0; i < obfuscatedWord.length(); i++) {
                    if (obfuscatedWord.charAt(i) == '_') {
                        positions.add(i);
                    }
                }
                char[] obfuscatedCharacters = obfuscatedWord.toCharArray();
                int replacePosition = positions.get(random.nextInt(positions.size()));
                obfuscatedCharacters[replacePosition] = translatedWord.charAt(replacePosition);
                String newObfuscatedWord = "";
                for (char oneChar : obfuscatedCharacters) {
                    newObfuscatedWord += oneChar;
                }
                hintWords.put(player.getUniqueId(), newObfuscatedWord);
                bossBars.get(player.getUniqueId()).setTitle(hintWords.get(player.getUniqueId()));
            }
        }, delay, delay));
    }
}
