package de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.listeners;

import de.devlen.nichtecro.server.bukkit.minigame.core.events.GameNewRoundEvent;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.Game;
import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.GuessItBuildingsProcessor;
import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.manager.GuessItBuildingsPlayers;
import de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.manager.GuessItBuildingsWordStorage;
import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import static de.devlen.nichtecro.server.bukkitcore.language.Language.tl;

/**
 * Created by Tarik on 02.06.2016.
 */
public class GameNewRoundListener extends ListenerHelper {

    @EventHandler
    public void onGameNewRound(GameNewRoundEvent e) {
        Player player = Bukkit.getPlayer(GuessItBuildingsPlayers.getNextBuilder());
        GuessItBuildingsProcessor.setCurrentBuilder(player);
        String wordNode = GuessItBuildingsProcessor.getNextRandomWordNode();
        GuessItBuildingsProcessor.setCurrentWordNode(wordNode);
        Chat.sendTl(player, Game.getInstance().getGameNameNode(), "bukkit.minigame.guessit.buildings.new.word", tl(wordNode, player));
        Game.getInstance().announce(player, "bukkit.minigame.guessit.buildings.new.builder", player.getName());
        for (Player target : Bukkit.getOnlinePlayers()) {
            if (GuessItBuildingsProcessor.getCurrentBuilder().getUniqueId() != target.getUniqueId()) {
                GuessItBuildingsWordStorage.runHintTask(target);
            }
        }
    }

}
