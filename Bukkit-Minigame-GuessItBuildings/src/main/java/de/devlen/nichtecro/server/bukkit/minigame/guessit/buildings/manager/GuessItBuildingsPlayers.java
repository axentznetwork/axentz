package de.devlen.nichtecro.server.bukkit.minigame.guessit.buildings.manager;

import de.devlen.nichtecro.server.bukkit.minigame.core.game.Game;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.GameState;
import de.devlen.nichtecro.server.bukkit.minigame.core.game.taskmanager.GameRoundTaskManager;
import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;

/**
 * Created by Tarik on 02.06.2016.
 */
public class GuessItBuildingsPlayers {

    private static List<UUID> haveToBuild = new ExtendedList<>();
    private static List<GuessItBuildingsScore> guessItBuildingsScores = new ExtendedList<>();

    private static void removeScore(Player player) {
        for (GuessItBuildingsScore guessItBuildingsScore : guessItBuildingsScores) {
            if (guessItBuildingsScore.getUuid().equals(player.getUniqueId())) {
                guessItBuildingsScores.remove(guessItBuildingsScore);
            }
        }
    }

    public static GuessItBuildingsScore getScore(Player player) {
        for (GuessItBuildingsScore guessItBuildingsScore : guessItBuildingsScores) {
            if (guessItBuildingsScore.getUuid().equals(player.getUniqueId())) {
                return guessItBuildingsScore;
            }
        }
        GuessItBuildingsScore guessItBuildingsScore = new GuessItBuildingsScore(player.getUniqueId());
        guessItBuildingsScores.add(guessItBuildingsScore);
        return guessItBuildingsScore;
    }

    public static void preparePlayerOrder() {
        haveToBuild.clear();
        for (Player target : Bukkit.getOnlinePlayers()) {
            haveToBuild.add(target.getUniqueId());
        }

        for (Player target : Bukkit.getOnlinePlayers()) {
            haveToBuild.add(target.getUniqueId());
        }
        Game.getInstance().setRounds(haveToBuild.size());
    }

    public static UUID getNextBuilder() {
        if (haveToBuild.size() > 0) {
            UUID uuid = haveToBuild.get(0);
            haveToBuild.remove(0);
            return uuid;
        }
        throw new RuntimeException("The next builder can't be null!");
    }

    public static void makeCleanPlayerLeave(Player player) {
        if (Game.getInstance().getGameState() == GameState.RUNNING) {
            Iterator<UUID> uuidIterator = haveToBuild.iterator();
            while (uuidIterator.hasNext()) {
                UUID uuid = uuidIterator.next();
                if (uuid.equals(player.getUniqueId())) {
                    uuidIterator.remove();
                    Game.getInstance().setRounds(Game.getInstance().getRounds() - 1);
                }
            }
            removeScore(player);
            GameRoundTaskManager gameRoundTaskManager = (GameRoundTaskManager) Game.getInstance().getGameTaskManager();
            gameRoundTaskManager.forceNextRound();
        }
    }

    public static void sortScores() {
        for (int i = 0; i < guessItBuildingsScores.size(); ++i) {
            for (int j = guessItBuildingsScores.size() - 1; j > 0; --j) {
                if (guessItBuildingsScores.get(j).getCompletePoints() > guessItBuildingsScores.get(j - 1).getCompletePoints()) {
                    final GuessItBuildingsScore temp = guessItBuildingsScores.get(j);
                    guessItBuildingsScores.set(j, guessItBuildingsScores.get(j - 1));
                    guessItBuildingsScores.set(j - 1, temp);
                }
            }
        }
    }

    public static List<GuessItBuildingsScore> getGuessItBuildingsScores() {
        return guessItBuildingsScores;
    }
}
