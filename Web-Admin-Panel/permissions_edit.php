<?php
/**
 * Created by IntelliJ IDEA.
 * User: Tarik
 * Date: 12.06.2016
 * Time: 20:30
 */

$title = "Edit Permission";

include 'page_start.php';

if ($logged_in) {
    if (isset($_GET["id"])) {
        $edit_id = $_GET["id"];
        if (UserManager::hasOneNeededRank($_SESSION["uuid"], array("ADMINISTRATOR"))) {
            $possible_ranks = array("GUEST", "ANCIENT", "WICKED", "VETERAN", "FRIEND", "YOUTUBE", "SUPPORTER", "MAPPER", "TRANSLATOR", "DEVELOPER", "MODERATOR", "ADMINISTRATOR");
            ?>
            <div class="modal fade" tabindex="-1" role="dialog" id="rankAddModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Add rank to permission</h4>
                        </div>
                        <div class="modal-body">
                            <p>Choose a rank you want to add to permission.</p>
                            <select id="adding_rank">
                                <?php
                                foreach ($possible_ranks as $possible_rank) {
                                    ?>
                                    <option value="<?php echo $possible_rank; ?>"><?php echo $possible_rank; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Abort</button>
                            <button type="button" class="btn btn-primary" id="saveRankAdd">Save</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-push-3 col-md-6 col-xs-12">
                <?php
                if (isset($_POST["submit"])) {
                    $node = $_POST["node"];
                    $ranks = $_POST["ranks"];
                    $servers = $_POST["servers"];
                    if ($servers == null) {
                        $servers = [];
                    }
                    if ($ranks == null) {
                        $ranks = [];
                    }
                    PermissionsManager::setField($edit_id, array("node" => $node));
                    PermissionsManager::setField($edit_id, array("ranks" => $ranks));
                    PermissionsManager::setField($edit_id, array("servers" => $servers));
                    ?>
                    <div class="alert alert-success" role="alert">Successfully updated permission. (<?php echo date("H:i:s"); ?>, Just to see that update loaded.)</div>
                    <?php
                }

                $permission = PermissionsManager::getPermission($edit_id);
                if (isset($permission["node"])) {
                ?>
                <form action="permissions_edit.php?id=<?php echo $permission["_id"]; ?>" method="post">
                    <div class="form-group">
                        <label for="nodeText">Node</label>
                        <input type="text" class="form-control" name="node" id="nodeText" placeholder="Node" value="<?php echo $permission["node"]; ?>">
                    </div>
                    <div class="form-group">
                        <label>Servers</label>
                        <div class="serversSection">
                            <?php
                            $iteratorServers = 0;
                            foreach ($permission["servers"] as $server) {
                                $iteratorServers++;
                                ?>
                                <div class="form-inline" id="server-<?php echo $iteratorServers; ?>">
                                    <input type="text" class="form-control" name="servers[]" placeholder="Server" value="<?php echo $server; ?>">
                                    <button type="button" class="btn btn-danger" onclick="removeServer(<?php echo $iteratorServers; ?>)">remove</button>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <br>
                        <button type="button" class="btn btn-success" id="addServer">Add Server</button>
                    </div>
                    <div class="form-group">
                        <label>Ranks</label>
                        <div class="ranksSection">
                            <?php
                            $iteratorRanks = 0;
                            foreach ($permission["ranks"] as $rank) {
                                $iteratorRanks++;
                                ?>
                                <div class="form-inline" id="rank-<?php echo $iteratorRanks; ?>">
                                    <input type="text" class="form-control" name="ranks[]" placeholder="Rank" value="<?php echo $rank; ?>" readonly>
                                    <button type="button" class="btn btn-danger" onclick="removeRank(<?php echo $iteratorRanks; ?>)">remove</button>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <br>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#rankAddModal">Add Rank</button>
                    </div>
                    <input type="submit" value="Save" name="submit" class="btn btn-primary">
                    <input type="reset" value="Abort changes" class="btn btn-default">
                </form>
                <a href="permissions_manager.php" class="btn btn-info">Get back to permissions</a>
            </div>
            <script>
                var iteratorRanks = <?php echo $iteratorRanks; ?>;
                var iteratorServers = <?php echo $iteratorServers; ?>;
                $('#saveRankAdd').click(function () {
                    iteratorRanks++;
                    $('.ranksSection').append("<div class='form-inline' id='rank-" + iteratorRanks + "'>" +
                        "<input type='text' class='form-control' name='ranks[]' placeholder='Rank' value='" + $('#adding_rank').val() + "' readonly>" +
                        " <button type='button' class='btn btn-danger' onclick='removeRank(" + iteratorRanks + ")'>remove</button>" +
                        "</div>");
                    $('#rankAddModal').modal('hide');
                });

                $('#addServer').click(function () {
                    iteratorServers++;
                    $('.serversSection').append("<div class='form-inline' id='server-" + iteratorServers + "'>" +
                        "<input type='text' class='form-control' name='servers[]' placeholder='Server' value=''>" +
                        " <button type='button' class='btn btn-danger' onclick='removeServer(" + iteratorServers + ")'>remove</button>" +
                        "</div>");
                });

                function removeRank(id) {
                    $('#rank-' + id).remove();
                }

                function removeServer(id) {
                    $('#server-' + id).remove();
                }
            </script>
            <?php
        } else {
            ?>
            <div class="col-md-push-3 col-md-6 col-xs-12">
                <div class="alert alert-danger" role="alert">Access not available! This id is not in the database.</div>
            </div>
            <?php
        }
        } else {
            ?>
            <div class="col-md-push-3 col-md-6 col-xs-12">
                <div class="alert alert-danger" role="alert">Access not granted! Needed ranks: ADMINISTRATOR</div>
            </div>
            <?php
        }
    } else {
        ?>
        <div class="col-md-push-3 col-md-6 col-xs-12">
            <div class="alert alert-danger" role="alert">Access not granted! Please set an id to edit. (<?php echo $_SERVER["SCRIPT_NAME"]; ?>?id=permissions.node)</div>
        </div>
        <?php
    }
} else {
    ?>
    <div class="col-md-push-3 col-md-6 col-xs-12">
        <div class="alert alert-danger" role="alert">Access not granted! <a href="login.php">Want to login?</a></div>
    </div>
    <?php
}

include 'page_end.php';

?>
