<?php

/**
 * Created by IntelliJ IDEA.
 * User: Tarik
 * Date: 16.06.2016
 * Time: 20:04
 */
class ServersManager
{

    /**
     * @return array
     */
    public static function getAllServers()
    {
        $mongo_cursor = MongoManager::getAllDocuments('bungeecore_servers');
        $returned_documents = array();
        while ($mongo_cursor->hasNext()) {
            $document = $mongo_cursor->next();
            array_push($returned_documents, $document);
        }
        return $returned_documents;
    }

    /**
     * @param $filter
     * @return array
     */
    public static function getFilteredServers($filter)
    {
        $mongo_cursor = MongoManager::getAllDocumentsFiltered('bungeecore_servers', $filter);
        $returned_documents = array();
        while ($mongo_cursor->hasNext()) {
            $document = $mongo_cursor->next();
            array_push($returned_documents, $document);
        }
        return $returned_documents;
    }

    /**
     * @param $name
     * @param $host
     * @param $port
     * @param $restricted
     * @param $maintenance
     * @param $autorestart
     * @param $type
     * @param $max_online
     */
    public static function addServer($name, $host, $port, $restricted, $maintenance, $autorestart, $type, $max_online)
    {
        MongoManager::insert('bungeecore_servers', array(
            'server_name' => $name,
            'host' => $host,
            'port' => intval($port),
            'restricted' => $restricted,
            'maintenance' => $maintenance,
            'autorestart' => $autorestart,
            'server_type' => $type,
            'max_online_count' => intval($max_online),
            'default_motd' => $name,
            'online' => false,
            'current_online_count' => 0));
    }

    /**
     * @param $name
     * @return array|null
     */
    public static function getServer($name)
    {
        return MongoManager::getFirstDocument('bungeecore_servers', array('server_name' => $name));
    }

    /**
     * @param $name
     * @return array|bool
     */
    public static function removeServer($name)
    {
        return MongoManager::remove('bungeecore_servers', array('server_name' => $name));
    }

    /**
     * @param $name
     * @param $obj
     */
    public static function setField($name, $obj)
    {
        MongoManager::update('bungeecore_servers', array('server_name' => $name), array('$set' => $obj));
    }

    /**
     * @param $name
     * @param $obj
     */
    public static function pushField($name, $obj)
    {
        MongoManager::update('bungeecore_servers', array('server_name' => $name), array('$push' => $obj));
    }

    /**
     * @param $name
     * @param $obj
     */
    public static function incrementField($name, $obj)
    {
        MongoManager::update('bungeecore_servers', array('server_name' => $name), array('$inc' => $obj));
    }

}

?>
