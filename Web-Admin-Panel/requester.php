<?php
/**
 * Created by IntelliJ IDEA.
 * User: Tarik
 * Date: 19.06.2016
 * Time: 16:43
 */

include_once 'Util.php';

header("Content-Type: application/json", "true", "200");
if (isset($_POST["mode"])) {
    switch ($_POST["mode"]) {
        case "hash-web":
            if (isset($_POST["cleartext"])) {
                $return_val = array(
                    "cleartext" => $_POST["cleartext"],
                    "digested" => Util::hashPassword($_POST["cleartext"]),
                    "algorithm" => "website-algorithm");
                echo json_encode($return_val);
            }
            break;
    }
} else {
    $return_val = array("error" => "Please choose a mode you want to request");
    echo json_encode($return_val);
}

?>