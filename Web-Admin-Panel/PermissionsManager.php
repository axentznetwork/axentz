<?php

/**
 * Created by IntelliJ IDEA.
 * User: Tarik
 * Date: 12.06.2016
 * Time: 20:20
 */
class PermissionsManager
{

    public static function getAllPermissions()
    {
        $mongo_cursor = MongoManager::getAllDocuments('bukkitcore_permissions');
        $returned_documents = array();
        while ($mongo_cursor->hasNext()) {
            $document = $mongo_cursor->next();
            array_push($returned_documents, $document);
        }
        return $returned_documents;
    }

    public static function getFilteredPermissions($filter)
    {
        $mongo_cursor = MongoManager::getAllDocumentsFiltered('bukkitcore_permissions', $filter);
        $returned_documents = array();
        while ($mongo_cursor->hasNext()) {
            $document = $mongo_cursor->next();
            array_push($returned_documents, $document);
        }
        return $returned_documents;
    }

    public static function addPermission($node)
    {
        MongoManager::insert('bukkitcore_permissions', array('node' => $node, 'ranks' => [], 'servers' => []));
    }

    public static function getPermission($id)
    {
        return MongoManager::getFirstDocument('bukkitcore_permissions', array('_id' => new MongoId($id)));
    }

    public static function removePermission($id)
    {
        return MongoManager::remove('bukkitcore_permissions', array('_id' => new MongoId($id)));
    }

    public static function setField($id, $obj)
    {
        MongoManager::update('bukkitcore_permissions', array('_id' => new MongoId($id)), array('$set' => $obj));
    }

    public static function pushField($id, $obj)
    {
        MongoManager::update('bukkitcore_permissions', array('_id' => new MongoId($id)), array('$push' => $obj));
    }

    public static function incrementField($id, $obj)
    {
        MongoManager::update('bukkitcore_permissions', array('_id' => new MongoId($id)), array('$inc' => $obj));
    }

}