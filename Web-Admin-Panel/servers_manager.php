<?php
/**
 * Created by IntelliJ IDEA.
 * User: Tarik
 * Date: 16.06.2016
 * Time: 19:58
 */

$title = "Servers";

include 'page_start.php';

if ($logged_in) {
    if (UserManager::hasOneNeededRank($_SESSION["uuid"], array("ADMINISTRATOR"))) {
        ?>
        <div class="modal fade" tabindex="-1" role="dialog" id="serverAddModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add server</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Server name in BungeeCord-network</label><br>
                            <input id="adding_server_name" type="text" placeholder="Bungeecord name">
                        </div>
                        <div class="form-group">
                            <label>Host (without port)</label><br>
                            <input id="adding_server_host" type="text" placeholder="Host (not localhost)">
                        </div>
                        <div class="form-group">
                            <label>Port</label><br>
                            <input id="adding_server_port" type="number" placeholder="Port (e.g. 20000)">
                        </div>
                        <div class="form-group">
                            <input id="adding_server_restricted" type="checkbox" value="Restricted mode"><label>Restricted access (permission: bungeecord.server.[server-name]</label>
                        </div>
                        <div class="form-group">
                            <input id="adding_server_maintenance" type="checkbox" placeholder="Maintenance mode"><label>Maintenance</label>
                        </div>
                        <div class="form-group">
                            <input id="adding_server_autorestart" type="checkbox" placeholder="Autorestart after server stops (not affected by WAP-stop)"><label>Autorestart</label>
                        </div>
                        <div class="form-group">
                            <label>Type</label><br>
                            <select id="adding_server_type" type="text" placeholder="Type">
                                <option value="lobby">Lobby</option>
                                <option value="gib">GuessIt-Buildings</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Size of maximal online players</label><br>
                            <input id="adding_server_max_online" type="number" placeholder="Max online count">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Abort</button>
                        <button type="button" class="btn btn-primary" id="saveServerAdd">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-push-3 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="serverSearch">Search for a server</label>
                <input type="search" class="form-control" id="serverSearch" placeholder="Bungeecord name">
                <br>
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#serverAddModal">Add Server</button>
            </div>
            <div class="table-responsive" id="serversTable">
                <table class="table table-striped table-hover">
                    <th>Name</th>
                    <th>Address</th>
                    <th>Online</th>
                    <th>Players online</th>
                    <th>Document editing button</th>
                    <th>Document remove button</th>
                    <?php
                    foreach (ServersManager::getAllServers() as $server) {
                        ?>
                        <tr id="server-<?php echo $server["server_name"]; ?>">
                            <td><?php echo $server["server_name"]; ?></td>
                            <td><?php echo $server["host"] . ":" . $server["port"]; ?></td>
                            <td><?php echo $server["online"] ? "Yes" : "No"; ?></td>
                            <td><?php echo $server["current_online_count"] . "/" . $server["max_online_count"]; ?></td>
                            <td><a href="<?php echo "server/" . $server["server_name"]; ?>" class="btn btn-info">Edit</a></td>
                            <td>
                                <button class="btn btn-danger" onclick="removeServer('<?php echo $server["server_name"]; ?>');">Remove</button>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
        </div>
        <script>
            function removeServer(serverName) {
                if (confirm("Do you really wan't to remove that server?")) {
                    $.ajax({
                        url: 'ajax',
                        dataType: 'text',
                        type: 'POST',
                        data: {
                            specifier: 'remove_server',
                            server_name: serverName
                        },
                        success: function (text) {
                            if (text == "true") {
                                $('#server-' + serverName).remove();
                                alert("Successfully removed server.");
                            } else {
                                alert("Server was not removed.");
                            }
                        }
                    });
                }
            }

            $('#saveServerAdd').click(function () {
                $.ajax({
                    url: 'ajax',
                    dataType: 'text',
                    type: 'POST',
                    data: {
                        specifier: 'add_server',
                        server_name: $('#adding_server_name').val(),
                        autorestart: $('#adding_server_autorestart').is(':checked'),
                        host: $('#adding_server_host').val(),
                        port: $('#adding_server_port').val(),
                        max_online: $('#adding_server_max_online').val(),
                        maintenance: $('#adding_server_maintenance').is(':checked'),
                        type: $('#adding_server_type').val(),
                        restricted: $('#adding_server_restricted').is(':checked')
                    },
                    success: function () {
                        $('#serverAddModal').modal('hide');
                        $('#serverSearch').val($('#adding_server_name').val());
                        $('#adding_server_name').val("");
                        $('#serverSearch').trigger('change');
                    }
                })
            });

            $('#adding_server_name').on('change keyup paste input', function () {
                $.ajax({
                    url: 'ajax',
                    dataType: 'text',
                    type: 'POST',
                    data: {
                        specifier: 'server_exists',
                        server_name: $('#adding_server_name').val()
                    },
                    success: function (text) {
                        if(text == "true"){
                            $('#saveServerAdd').attr('disabled', 'disabled')
                        }else{
                            $('#saveServerAdd').removeAttr('disabled');
                        }
                    }
                })
            });

            $('#serverSearch').on('change keyup paste input', function () {
                $.ajax({
                    url: 'ajax',
                    dataType: 'text',
                    type: 'POST',
                    data: {
                        specifier: 'server_search',
                        search: $('#serverSearch').val()
                    },
                    success: function (text) {
                        $(document).find('#serversTable').html(text);
                    }
                })
            });
        </script>
        <?php
    } else {
        ?>
        <div class="col-md-push-3 col-md-6 col-xs-12">
            <div class="alert alert-danger" role="alert">Access not granted! Needed ranks: ADMINISTRATOR</div>
        </div>
        <?php
    }
} else {
    ?>
    <div class="col-md-push-3 col-md-6 col-xs-12">
        <div class="alert alert-danger" role="alert">Access not granted! <a href="login.php">Want to login?</a></div>
    </div>
    <?php
}
include 'page_end.php';

?>