<?php
/**
 * Created by IntelliJ IDEA.
 * User: Tarik
 * Date: 11.06.2016
 * Time: 21:19
 */

$title = "Edit player";

include 'page_start.php';

if ($logged_in) {
    if (isset($_GET["uuid"])) {
        $edit_uuid = $_GET["uuid"];
        if (UserManager::hasOneNeededRank($_SESSION["uuid"], array("ADMINISTRATOR"))) {
            $possible_ranks = array("GUEST", "ANCIENT", "WICKED", "VETERAN", "FRIEND", "YOUTUBE", "SUPPORTER", "MAPPER", "TRANSLATOR", "DEVELOPER", "MODERATOR", "ADMINISTRATOR");
            ?>
            <div class="modal fade" tabindex="-1" role="dialog" id="rankAddModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Add rank to player</h4>
                        </div>
                        <div class="modal-body">
                            <p>Choose a rank you want to add to player.</p>
                            <select id="adding_rank">
                                <?php
                                foreach ($possible_ranks as $possible_rank) {
                                    ?>
                                    <option value="<?php echo $possible_rank; ?>"><?php echo $possible_rank; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Abort</button>
                            <button type="button" class="btn btn-primary" id="saveRankAdd">Save</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-push-3 col-md-6 col-xs-12">
                <?php
                if (isset($_POST["submit"])) {
                    $givenRanks = $_POST["ranks"];
                    $disguised_rank = $_POST["disguised-rank"];
                    $dyes = $_POST["dyes"];
                    UserManager::setField($edit_uuid, array("rank" => $givenRanks));
                    if (in_array($disguised_rank, UserManager::getRanks($edit_uuid))) {
                        UserManager::setField($edit_uuid, array("disguise_rank" => strtoupper($disguised_rank)));
                    }
                    UserManager::setField($edit_uuid, array("dyes" => intval($dyes)));
                    ?>
                    <div class="alert alert-success" role="alert">Successfully updated player data.</div>
                    <?php
                }
                $player_data = UserManager::getData($edit_uuid);
                if (isset($player_data["uuid"])) {
                ?>
                <form action="player/<?php echo $player_data["uuid"]; ?>" method="post">
                    <img src="https://mcapi.ca/skin/3d/<?php echo Util::getNameByUUID($player_data["uuid"]); ?>">
                    <div class="form-group">
                        <label for="usernameText">Playername (not editable)</label>
                        <input type="text" length="16" class="form-control" id="usernameText" placeholder="Playername" value="<?php echo Util::getNameByUUID($player_data["uuid"]); ?>" disabled>
                    </div>
                    <div class="form-group">
                        <label for="uuidText">Players UUID (not editable)</label>
                        <input type="text" length="36" class="form-control" id="uuidText" placeholder="UUID" value="<?php echo $player_data["uuid"]; ?>" disabled>
                    </div>
                    <div class="form-group">
                        <label>Ranks</label>
                        <div class="ranksSection">
                            <?php
                            $iteratorRanks = 0;
                            foreach ($player_data["rank"] as $rank) {
                                $iteratorRanks++;
                                ?>
                                <div class="form-inline" id="rank-<?php echo $iteratorRanks; ?>">
                                    <input type="text" class="form-control" name="ranks[]" placeholder="Rank" value="<?php echo $rank; ?>" readonly>
                                    <button type="button" class="btn btn-danger" onclick="removeRank(<?php echo $iteratorRanks; ?>)">remove</button>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <br>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#rankAddModal">Add Rank</button>
                    </div>
                    <div class="form-group">
                        <label for="disguisedRankText">Disguised rank</label>
                        <select class="form-control" name="disguised-rank" id="disguisedRankText">
                            <?php
                            $ranks = $player_data["rank"];
                            foreach ($ranks as $rank) {
                                ?>
                                <option value="<?php echo $rank; ?>" <?php if (strcasecmp($rank, $player_data["disguise_rank"]) == 0) echo "selected"; ?>><?php echo $rank; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="dyesText">Dyes (currency)</label>
                        <input type="number" class="form-control" name="dyes" id="dyesText" placeholder="dyes" value="<?php echo $player_data["dyes"] ?>">
                    </div>
                    <input type="submit" value="Save" name="submit" class="btn btn-primary">
                    <input type="reset" value="Abort changes" class="btn btn-default">
                </form>
                <a href="players" class="btn btn-info">Get back to players</a>
            </div>
            <script>
                var iteratorRanks = <?php echo $iteratorRanks; ?>;
                $('#saveRankAdd').click(function () {
                    iteratorRanks++;
                    $('.ranksSection').append("<div class='form-inline' id='rank-" + iteratorRanks + "'>" +
                        "<input type='text' class='form-control' name='ranks[]' placeholder='Rank' value='" + $('#adding_rank').val() + "' readonly>" +
                        " <button type='button' class='btn btn-danger' onclick='removeRank(" + iteratorRanks + ")'>remove</button>" +
                        "</div>");
                    $('#rankAddModal').modal('hide');
                });

                function removeRank(id) {
                    $('#rank-' + id).remove();
                }
            </script>
            <?php
        } else {
            ?>
            <div class="col-md-push-3 col-md-6 col-xs-12">
                <div class="alert alert-danger" role="alert">Access not available! This uuid is not in the database.</div>
            </div>
            <?php
        }
        } else {
            ?>
            <div class="col-md-push-3 col-md-6 col-xs-12">
                <div class="alert alert-danger" role="alert">Access not granted! Needed ranks: ADMINISTRATOR</div>
            </div>
            <?php
        }
    } else {
        ?>
        <div class="col-md-push-3 col-md-6 col-xs-12">
            <div class="alert alert-danger" role="alert">Access not granted! Please set an uuid to edit. (<?php echo $_SERVER["SCRIPT_NAME"]; ?>?uuid=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx)</div>
        </div>
        <?php
    }
} else {
    ?>
    <div class="col-md-push-3 col-md-6 col-xs-12">
        <div class="alert alert-danger" role="alert">Access not granted! <a href="login.php">Want to login?</a></div>
    </div>
    <?php
}

include 'page_end.php';

?>
