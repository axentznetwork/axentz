<?php

/**
 * Created by IntelliJ IDEA.
 * User: Tarik
 * Date: 11.06.2016
 * Time: 16:21
 */

include_once 'MongoManager.php';

class UserManager
{

    /**
     * @param $uuid
     * @return mixed
     */
    public static function get_pass_hash($uuid)
    {
        $document = MongoManager::getFirstDocument('bukkitcore_players', array('uuid' => $uuid));
        $pass_hash = $document["password_hash"];
        return $pass_hash;
    }

    public static function getAllPlayerData()
    {
        $mongo_cursor = MongoManager::getAllDocuments('bukkitcore_players');
        $returned_documents = array();
        while ($mongo_cursor->hasNext()) {
            $document = $mongo_cursor->next();
            array_push($returned_documents, $document);
        }
        return $returned_documents;
    }

    public static function getFilteredPlayerData($filter)
    {
        $mongo_cursor = MongoManager::getAllDocumentsFiltered('bukkitcore_players', $filter);
        $returned_documents = array();
        while ($mongo_cursor->hasNext()) {
            $document = $mongo_cursor->next();
            array_push($returned_documents, $document);
        }
        return $returned_documents;
    }

    /**
     * @param $uuid
     * @return array|null
     */
    public static function getData($uuid)
    {
        return MongoManager::getFirstDocument('bukkitcore_players', array('uuid' => $uuid));
    }

    /**
     * @param $uuid
     * @param $ranks_needed
     * @return bool
     */
    public static function hasOneNeededRank($uuid, $ranks_needed)
    {
        $document = MongoManager::getFirstDocument('bukkitcore_players', array('uuid' => $uuid));
        $ranks = $document["rank"];
        foreach ($ranks_needed as $rank) {
            if (in_array($rank, $ranks)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $uuid
     * @param $rank
     * @return bool
     */
    public static function hasRank($uuid, $rank)
    {
        $document = MongoManager::getFirstDocument('bukkitcore_players', array('uuid' => $uuid));
        $ranks = $document["rank"];
        return in_array($rank, $ranks);
    }

    /**
     * @param $uuid
     * @return mixed
     */
    public static function getRanks($uuid)
    {
        $document = MongoManager::getFirstDocument('bukkitcore_players', array('uuid' => $uuid));
        $ranks = $document["rank"];
        return $ranks;
    }

    public static function getRanksFormatted($uuid)
    {
        $ranks = self::getRanks($uuid);
        $rank_string = "";
        foreach ($ranks as $rank) {
            $rank_string .= $rank;
            $rank_string .= ", ";
        }
        $rank_string = substr($rank_string, 0, strlen($rank_string) - 2);
        return $rank_string;
    }

    public static function setField($uuid, $obj)
    {
        MongoManager::update('bukkitcore_players', array('uuid' => $uuid), array('$set' => $obj));
    }

    public static function pushField($uuid, $obj)
    {
        MongoManager::update('bukkitcore_players', array('uuid' => $uuid), array('$push' => $obj));
    }

    public static function incrementField($uuid, $obj)
    {
        MongoManager::update('bukkitcore_players', array('uuid' => $uuid), array('$inc' => $obj));
    }

}