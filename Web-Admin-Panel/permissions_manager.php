<?php
/**
 * Created by IntelliJ IDEA.
 * User: Tarik
 * Date: 12.06.2016
 * Time: 20:13
 */


$title = "Permissions";

include 'page_start.php';

if ($logged_in) {
    if (UserManager::hasOneNeededRank($_SESSION["uuid"], array("ADMINISTRATOR"))) {
        ?>
        <div class="modal fade" tabindex="-1" role="dialog" id="permissionAddModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add permission</h4>
                    </div>
                    <div class="modal-body">
                        <p>Type in a permissionnode you want to add.</p>
                        <p>After you added the permission-node you can search for it and edit it fully.</p>
                        <input id="adding_permission" type="text" placeholder="Permission Node">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Abort</button>
                        <button type="button" class="btn btn-primary" id="savePermissionAdd">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-push-3 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="permissionSearch">Search for a permission</label>
                <input type="text" class="form-control" id="permissionSearch" placeholder="Permission">
                <br>
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#permissionAddModal">Add Permission</button>
            </div>
            <div class="table-responsive" id="permissionsTable">
                <table class="table table-striped table-hover">
                    <th>Node</th>
                    <th>Ranks</th>
                    <th>Servers</th>
                    <th>Document editing button</th>
                    <th>Document remove button</th>
                    <?php
                    foreach (PermissionsManager::getAllPermissions() as $permission) {
                        ?>
                        <tr id="permission-<?php echo $permission["_id"]; ?>">
                            <td><?php echo $permission["node"]; ?></td>
                            <td><?php echo Util::getSeperatedString($permission["ranks"]); ?></td>
                            <td><?php echo Util::getSeperatedString($permission["servers"]); ?></td>
                            <td><a href="<?php echo "permission/" . $permission["_id"]; ?>" class="btn btn-info">Edit</a></td>
                            <td>
                                <button class="btn btn-danger" s onclick="removePermission('<?php echo $permission["_id"]; ?>');">Remove</button>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
        </div>
        <script>
            function removePermission(removeId) {
                if (confirm("Do you really wan't to remove that permission?")) {
                    $.ajax({
                        url: 'ajax',
                        dataType: 'text',
                        type: 'POST',
                        data: {
                            specifier: 'remove_permission',
                            permission_id: removeId
                        },
                        success: function (text) {
                            if (text == "true") {
                                $('#permission-' + removeId).remove();
                                alert("Successfully remove permission.");
                            } else {
                                alert("Permission was not removed.");
                            }
                        }
                    });
                }
            }

            $('#savePermissionAdd').click(function () {
                $.ajax({
                    url: 'ajax',
                    dataType: 'text',
                    type: 'POST',
                    data: {
                        specifier: 'add_permission',
                        permission_node: $('#adding_permission').val()
                    },
                    success: function () {
                        $('#permissionAddModal').modal('hide');
                        $('#permissionSearch').val($('#adding_permission').val());
                        $('#adding_permission').val("");
                        $('#permissionSearch').trigger('change');
                    }
                })
            });

            $('#permissionSearch').on('change keyup paste input', function () {
                $.ajax({
                    url: 'ajax',
                    dataType: 'text',
                    type: 'POST',
                    data: {
                        specifier: 'permissions_search',
                        search: $('#permissionSearch').val()
                    },
                    success: function (text) {
                        $(document).find('#permissionsTable').html(text);
                    }
                })
            });
        </script>
        <?php
    } else {
        ?>
        <div class="col-md-push-3 col-md-6 col-xs-12">
            <div class="alert alert-danger" role="alert">Access not granted! Needed ranks: ADMINISTRATOR</div>
        </div>
        <?php
    }
} else {
    ?>
    <div class="col-md-push-3 col-md-6 col-xs-12">
        <div class="alert alert-danger" role="alert">Access not granted! <a href="login.php">Want to login?</a></div>
    </div>
    <?php
}
include 'page_end.php';

?>
