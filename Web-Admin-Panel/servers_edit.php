<?php
/**
 * Created by IntelliJ IDEA.
 * User: Tarik
 * Date: 17.06.2016
 * Time: 13:13
 */


$title = "Edit server";

include 'page_start.php';

if ($logged_in) {
    if (isset($_GET["server_name"])) {
        $edit_name = $_GET["server_name"];
        if (UserManager::hasOneNeededRank($_SESSION["uuid"], array("ADMINISTRATOR"))) {
            ?>
            <div class="col-md-push-3 col-md-6 col-xs-12">
                <?php
                if (isset($_POST["submit"])) {
                    if (isset($_POST["server_name"]) &&
                        isset($_POST["host"]) &&
                        isset($_POST["port"]) &&
                        isset($_POST["max_online"]) &&
                        isset($_POST["type"])
                    ) {
                        ServersManager::setField($_POST["server_name"], array(
                            'autorestart' => isset($_POST["autorestart"]) ? true : false,
                            'maintenance' => isset($_POST["maintenance"]) ? true : false,
                            'restricted' => isset($_POST["restricted"]) ? true : false,
                            'host' => $_POST["host"],
                            'port' => intval($_POST["port"]),
                            'max_online_count' => intval($_POST["max_online"]),
                            'server_type' => $_POST["type"]
                        ));
                        ?>
                        <div class="alert alert-success" role="alert">Successfully updated server.</div>
                        <?php
                    }
                }
                $server = ServersManager::getServer($edit_name);
                if (isset($server["server_name"])) {
                ?>
                <form action="server/<?php echo $server["server_name"]; ?>" method="post">
                    <div class="form-group">
                        <label>Server name in BungeeCord-network (not editable)</label><br>
                        <input name="server_name" type="text" placeholder="Bungeecord name" value="<?php echo $server["server_name"]; ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label>Host (without port)</label><br>
                        <input name="host" type="text" placeholder="Host (not localhost)" value="<?php echo $server["host"]; ?>">
                    </div>
                    <div class="form-group">
                        <label>Port</label><br>
                        <input name="port" type="number" placeholder="Port (e.g. 20000)" value="<?php echo $server["port"]; ?>">
                    </div>
                    <div class="form-group">
                        <input name="restricted" type="checkbox" value="Restricted mode" <?php echo $server["restricted"] ? "checked" : ""; ?>><label>Restricted access (permission: bungeecord.server.[server-name]</label>
                    </div>
                    <div class="form-group">
                        <input name="maintenance" type="checkbox" placeholder="Maintenance mode" <?php echo $server["maintenance"] ? "checked" : ""; ?>><label>Maintenance</label>
                    </div>
                    <div class="form-group">
                        <input name="autorestart" type="checkbox" placeholder="Autorestart after server stops (not affected by WAP-stop)" <?php echo $server["autorestart"] ? "checked" : ""; ?>><label>Autorestart</label>
                    </div>
                    <div class="form-group">
                        <label>Type</label><br>
                        <select name="type" type="text" placeholder="Type">
                            <?php
                            $available_types = array("lobby" => "Lobby", "gib" => "GuessIt-Buildings", "build" => "Build");
                            foreach ($available_types as $available_type_key => $available_type_value) {
                                ?>
                                <option value="<?php echo $available_type_key; ?>" <?php echo strcasecmp($server["server_type"], $available_type_key) == 0 ? "selected" : ""; ?>><?php echo $available_type_value; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Size of maximal online players</label><br>
                        <input name="max_online" type="number" placeholder="Max online count" value="<?php echo $server["max_online_count"]; ?>">
                    </div>
                    <input type="submit" value="Save" name="submit" class="btn btn-primary">
                    <input type="reset" value="Abort changes" class="btn btn-default">
                </form>
                <a href="servers" class="btn btn-info">Get back to servers</a>
            </div>
            <script>
            </script>
            <?php
        } else {
            ?>
            <div class="col-md-push-3 col-md-6 col-xs-12">
                <div class="alert alert-danger" role="alert">Access not available! This server is not in the database.</div>
            </div>
            <?php
        }
        } else {
            ?>
            <div class="col-md-push-3 col-md-6 col-xs-12">
                <div class="alert alert-danger" role="alert">Access not granted! Needed ranks: ADMINISTRATOR</div>
            </div>
            <?php
        }
    } else {
        ?>
        <div class="col-md-push-3 col-md-6 col-xs-12">
            <div class="alert alert-danger" role="alert">Access not granted! Please set an server to edit. (<?php echo $_SERVER["SCRIPT_NAME"]; ?>/servername)</div>
        </div>
        <?php
    }
} else {
    ?>
    <div class="col-md-push-3 col-md-6 col-xs-12">
        <div class="alert alert-danger" role="alert">Access not granted! <a href="login.php">Want to login?</a></div>
    </div>
    <?php
}

include 'page_end.php';

?>
