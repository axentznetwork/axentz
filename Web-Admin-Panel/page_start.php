<?php
/**
 * Created by IntelliJ IDEA.
 * User: Tarik
 * Date: 11.06.2016
 * Time: 15:44
 */

session_start();

$logged_in = false;

if (isset($_SESSION["logged_in"])) {
    if ($_SESSION["logged_in"] == true) {
        $logged_in = true;
    }
}

include_once 'Util.php';
include_once 'UserManager.php';
include_once 'PermissionsManager.php';
include_once 'ServersManager.php';

?>
<html>
<head>
    <base href="/">
    <title>Axentz WAP | <?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/paper/bootstrap.min.css" rel="stylesheet" integrity="sha384-2mX2PSpkRSXLQzmNzH3gwK6srb06+OfbDlYjbog8LQuALYJjuQ3+Yzy2JIWNV9rW" crossorigin="anonymous">
    <link href="assets/css/global.css" rel="stylesheet">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="assets/js/jquery.js"></script>
<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="start">Web-Admin-Panel</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <!--            <ul class="nav navbar-nav">-->
            <!--            </ul>-->
            <ul class="nav navbar-nav navbar-right">
                <?php
                if ($logged_in) {
                    ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION["username"]; ?> <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-header">Following are your ranks:</li>
                            <?php
                            foreach (UserManager::getRanks($_SESSION["uuid"]) as $rank) {
                                ?>
                                <li class="dropdown-header"><?php echo $rank; ?></li>
                                <?php
                            }
                            ?>
                            <li class="divider"></li>
                            <?php if (UserManager::hasOneNeededRank($_SESSION["uuid"], array("ADMINISTRATOR"))) {
                                ?>
                                <li><a href="players">Manage Playerdata</a></li>
                                <?php
                            }

                            if (UserManager::hasOneNeededRank($_SESSION["uuid"], array("ADMINISTRATOR"))) {
                                ?>
                                <li><a href="permissions">Manage Permissions</a></li>
                                <?php
                            }

                            if (UserManager::hasOneNeededRank($_SESSION["uuid"], array("ADMINISTRATOR"))) {
                                ?>
                                <li><a href="translations_manager.php">Manage Translations</a></li>
                                <?php
                            }

                            if (UserManager::hasOneNeededRank($_SESSION["uuid"], array("ADMINISTRATOR"))) {
                                ?>
                                <li><a href="servers_manager.php">Manage Servers</a></li>
                                <?php
                            }
                            ?>
                            <li><a href="logout">Logout</a></li>
                        </ul>
                    </li>
                    <?php
                } else {
                    ?>
                    <li><a href="login">Login</a></li>
                    <?php
                }
                ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
