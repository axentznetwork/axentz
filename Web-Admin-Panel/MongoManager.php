<?php

/**
 * Created by IntelliJ IDEA.
 * User: Tarik
 * Date: 11.06.2016
 * Time: 16:29
 */
class MongoManager
{

    static $MONGO_USERNAME = "root";
    static $MONGO_PASSWORD = "763255f9b2bcc952df4a47d8f02a3e82be4349a41549a62858edd4f3c87bf9fa8617a3044afdb4b3b3c6aca0f30d4ff327ad13c206d2d98ecc4de55a392acfd0459fbd2d2cb97ecece047d35f05ab16216fed795cadde576c000fe7a896bed30d4816136a0fb31d31719d9ff5a0f9263ea8cb54153a133ddca241ce004265775673797abee08da6544b9013ab8da120f";
    static $MONGO_ADDRESS = "localhost";
    static $MONGO_DB = "minecraft";

    /**
     * @return MongoClient
     */
    public static function getMongoClient()
    {
        $connection_string = 'mongodb://' . self::$MONGO_USERNAME . ':' . self::$MONGO_PASSWORD . '@' . self::$MONGO_ADDRESS;
        $mongo_client = new MongoClient($connection_string);
        return $mongo_client;
    }

    /**
     * @param $collection_name
     * @param $filter
     * @return array|null
     */
    public static function getFirstDocument($collection_name, $filter)
    {
        $mongo_collection = self::getMongoClient()->selectCollection("minecraft", $collection_name);
        return $mongo_collection->findOne($filter);
    }

    /**
     * @param $collectionName
     * @return MongoCursor
     */
    public static function getAllDocuments($collectionName)
    {
        $mongo_collection = self::getMongoClient()->selectCollection("minecraft", $collectionName);
        return $mongo_collection->find();
    }

    /**
     * @param $collectionName
     * @param $filter
     * @return MongoCursor
     */
    public static function getAllDocumentsFiltered($collectionName, $filter)
    {
        $mongo_collection = self::getMongoClient()->selectCollection("minecraft", $collectionName);
        return $mongo_collection->find($filter);
    }

    /**
     * @param $collectionName
     * @param $filter
     * @return array|bool
     */
    public static function remove($collectionName, $filter)
    {
        return self::getMongoClient()->selectCollection("minecraft", $collectionName)->remove($filter);
    }

    /**
     * @param $collectionName
     * @param $filter
     * @param $obj
     */
    public static function update($collectionName, $filter, $obj)
    {
        self::getMongoClient()->selectCollection("minecraft", $collectionName)->update($filter, $obj, array("upsert" => true));
    }

    public static function insert($collectionName, $obj){
        self::getMongoClient()->selectCollection("minecraft", $collectionName)->insert($obj);
    }

}