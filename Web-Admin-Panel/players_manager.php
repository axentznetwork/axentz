<?php
/**
 * Created by IntelliJ IDEA.
 * User: Tarik
 * Date: 11.06.2016
 * Time: 20:46
 */

$title = "Players";

include 'page_start.php';

if ($logged_in) {
    if (UserManager::hasOneNeededRank($_SESSION["uuid"], array("ADMINISTRATOR"))) {
        ?>
        <div class="col-md-push-3 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="playerSearch">Search for a player</label>
                <input type="text" length="16" class="form-control" id="playerSearch" placeholder="Playername">
            </div>
            <div class="table-responsive" id="playersTable">
                <table class="table table-striped table-hover">
                    <th>Avatar</th>
                    <th>Name (Database-saved names)</th>
                    <th>UUID</th>
                    <th>Document editing link</th>
                    <?php
                    foreach (UserManager::getAllPlayerData() as $player_data) {
                        $actualName = Util::getNameByUUID($player_data["uuid"]);
                        ?>
                        <tr>
                            <td><img src="https://mcapi.ca/avatar/2d/<?php echo $actualName; ?>/32"></td>
                            <td><?php echo $actualName; ?> (<?php echo $player_data["player_name"]; ?>)</td>
                            <td><?php echo $player_data["uuid"]; ?></td>
                            <td><a class="btn btn-info" href="<?php echo "player/" . $player_data["uuid"]; ?>">Edit</a></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
            <script>
                $('#playerSearch').on('change keyup paste input', function () {
                    $.ajax({
                        url: 'ajax',
                        dataType: 'text',
                        type: 'POST',
                        data: {
                            specifier: 'players_search',
                            search: $('#playerSearch').val()
                        },
                        success: function (text) {
                            $(document).find('#playersTable').html(text);
                        }
                    })
                });
            </script>
        </div>
        <?php
    } else {
        ?>
        <div class="col-md-push-3 col-md-6 col-xs-12">
            <div class="alert alert-danger" role="alert">Access not granted! Needed ranks: ADMINISTRATOR</div>
        </div>
        <?php
    }
} else {
    ?>
    <div class="col-md-push-3 col-md-6 col-xs-12">
        <div class="alert alert-danger" role="alert">Access not granted! <a href="login.php">Want to login?</a></div>
    </div>
    <?php
}

include 'page_end.php';

?>
