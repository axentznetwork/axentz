<?php
/**
 * Created by IntelliJ IDEA.
 * User: Tarik
 * Date: 11.06.2016
 * Time: 15:45
 */

session_start();

$logged_in = false;

if (isset($_SESSION["logged_in"])) {
    if ($_SESSION["logged_in"] == true) {
        $logged_in = true;
    }
}

include_once 'Util.php';
include_once 'UserManager.php';


$login_try = false;

if (!$logged_in) {

    if (isset($_POST["login-username"]) && isset($_POST["login-password"])) {
        $login_try = true;
        $username = $_POST["login-username"];
        $password = $_POST["login-password"];
        $memcache = new Memcache;
        $memcache->connect('localhost', 11211);
        if ($memcache->get("uuid-" . $username)) {
            $formatted_uuid = $memcache->get("uuid-" . $username);
        } else {
            $json = file_get_contents("https://api.mojang.com/users/profiles/minecraft/$username");
            $fetched_user_array = json_decode($json, true);
            if (isset($fetched_user_array["id"])) {
                $uuid = $fetched_user_array["id"];
                $formatted_uuid = Util::formatUUID($uuid);
                $memcache->set("uuid-" . $username, $formatted_uuid);
            }
        }

        $db_hash = UserManager::get_pass_hash($formatted_uuid);
        if (strcasecmp(Util::hashPassword($password), $db_hash) == 0) {
            $_SESSION["logged_in"] = true;
            $_SESSION["username"] = $username;
            $_SESSION["uuid"] = $formatted_uuid;
            $login_success = true;
        } else {
            $login_success = false;
        }
    }
}

$title = "Login";

include 'page_start.php';

?>
    <div class="col-md-push-3 col-md-6 col-xs-12">
        <?php
        if ($login_try) {
            if ($login_success) {
                ?>
                <div class="alert alert-success" role="alert">You logged in successfully!</div>
                <?php
            } else {
                ?>
                <div class="alert alert-danger" role="alert">Your username or password is wrong. No account? <a href="register.php">Register</a></div>
                <?php
            }
        } else {
            if ($logged_in) {
                ?>
                <div class="alert alert-danger" role="alert">You are already logged in! <a href="logout.php">Do you want to logout?</a></div>
                <?php
            } else {
                ?>
                <form action="login" method="post">
                    <div class="form-group">
                        <label for="usernameInput">Minecraft-Username</label>
                        <input type="text" class="form-control" name="login-username" id="usernameInput" placeholder="Minecraft-Username">
                    </div>
                    <div class="form-group">
                        <label for="passwordInput">Password</label>
                        <input type="password" class="form-control" name="login-password" id="passwordInput" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
                <?php
            }
        }
        ?>
    </div>

<?php

include 'page_end.php';

?>