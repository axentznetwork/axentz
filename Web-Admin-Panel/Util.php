<?php

/**
 * Created by IntelliJ IDEA.
 * User: Tarik
 * Date: 11.06.2016
 * Time: 16:17
 */
class Util
{
    public static function formatUUID($uuid)
    {
        $uid = "";
        $uid .= substr($uuid, 0, 8) . "-";
        $uid .= substr($uuid, 8, 4) . "-";
        $uid .= substr($uuid, 12, 4) . "-";
        $uid .= substr($uuid, 16, 4) . "-";
        $uid .= substr($uuid, 20);
        return $uid;
    }

    public static function unformatUUID($uuid)
    {
        $uid = str_replace("-", "", $uuid);
        return $uid;
    }

    public static function hashPassword($password)
    {
        $md5Hash = hash("md5", $password);
        $sha512Hash = hash("sha512", $password);
        $whirlpoolHash = hash("whirlpool", $password);
        $completeHash = $md5Hash . $sha512Hash . $whirlpoolHash;
        return $completeHash;
    }

    public static function getSeperatedString($array)
    {
        $array_string = "";
        foreach ($array as $entry) {
            $array_string .= $entry;
            $array_string .= ", ";
        }
        $array_string = substr($array_string, 0, strlen($array_string) - 2);
        return $array_string;
    }

    public static function getNameByUUID($uuid)
    {
        $uid = self::unformatUUID($uuid);
        $memcache = new Memcache;
        $memcache->connect('localhost', 11211);
        if ($memcache->get("name-" . $uid)) {
            $name = $memcache->get("name-" . $uid);
        } else {
            $json = file_get_contents("https://mcapi.ca/name/uuid/" . $uid);
            $decoded = json_decode($json, true);
            $name = $decoded["name"];
            $memcache->add("name-" . $uid, $name, false, 3600);
        }
        return $name;
    }
}