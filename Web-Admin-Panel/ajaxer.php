<?php /**
 * Created by Tarik on 13.06.2016.
 */

session_start();

$logged_in = false;

if (isset($_SESSION["logged_in"])) {
    if ($_SESSION["logged_in"] == true) {
        $logged_in = true;
    }
}

include_once 'Util.php';
include_once 'UserManager.php';
include_once 'PermissionsManager.php';
include_once 'ServersManager.php';

if ($logged_in) {
    if (isset($_POST["specifier"])) {
        switch ($_POST["specifier"]) {
            case "players_search":
                if (isset($_POST["search"])) {
                    if (UserManager::hasOneNeededRank($_SESSION["uuid"], array("ADMINISTRATOR"))) {
                        ?>
                        <table class="table table-striped table-hover">
                            <th>Avatar</th>
                            <th>Name (Database-saved names)</th>
                            <th>UUID</th>
                            <th>Document editing link</th>
                            <?php
                            foreach (UserManager::getFilteredPlayerData(array('player_name' => array('$regex' => ".*" . $_POST["search"] . ".*"))) as $player_data) {
                                $actualName = Util::getNameByUUID($player_data["uuid"]);
                                ?>
                                <tr>
                                    <td><img src="https://mcapi.ca/avatar/2d/<?php echo $actualName; ?>/32"></td>
                                    <td><?php echo $actualName; ?> (<?php echo $player_data["player_name"]; ?>)</td>
                                    <td><?php echo $player_data["uuid"]; ?></td>
                                    <td><a class="btn btn-info" href="<?php echo "player/" . $player_data["uuid"]; ?>">Edit</a></td>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                        <?php
                    }
                }
                break;
            case "permissions_search":
                if (isset($_POST["search"])) {
                    if (UserManager::hasOneNeededRank($_SESSION["uuid"], array("ADMINISTRATOR"))) {
                        ?>
                        <table class="table table-striped table-hover">
                            <th>Node</th>
                            <th>Ranks</th>
                            <th>Servers</th>
                            <th>Document editing button</th>
                            <th>Document remove button</th>
                            <?php
                            foreach (PermissionsManager::getFilteredPermissions(array('node' => array('$regex' => ".*" . $_POST["search"] . ".*"))) as $permission) {
                                ?>
                                <tr id="permission-<?php echo $permission["_id"]; ?>">
                                    <td><?php echo $permission["node"]; ?></td>
                                    <td><?php echo Util::getSeperatedString($permission["ranks"]); ?></td>
                                    <td><?php echo Util::getSeperatedString($permission["servers"]); ?></td>
                                    <td><a href="<?php echo "permission/" . $permission["_id"]; ?>" class="btn btn-info">Edit</a></td>
                                    <td>
                                        <button class="btn btn-danger" onclick="removePermission('<?php echo $permission["_id"]; ?>');">Remove</button>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                        <?php
                    }
                }
                break;
            case "remove_permission":
                if (isset($_POST["permission_id"])) {
                    if (UserManager::hasOneNeededRank($_SESSION["uuid"], array("ADMINISTRATOR"))) {
                        if (PermissionsManager::removePermission($_POST["permission_id"])) {
                            echo "true";
                        } else {
                            echo "false";
                        }
                    }
                }
                break;
            case "add_permission":
                if (isset($_POST["permission_node"])) {
                    if (UserManager::hasOneNeededRank($_SESSION["uuid"], array("ADMINISTRATOR"))) {
                        PermissionsManager::addPermission($_POST["permission_node"]);
                    }
                }
                break;
            case "server_exists":
                if (isset($_POST["server_name"])) {
                    if (UserManager::hasOneNeededRank($_SESSION["uuid"], array("ADMINISTRATOR"))) {
                        echo(ServersManager::getServer($_POST["server_name"]) != null ? "true" : "false");
                    }
                }
                break;
            case "server_search":
                if (isset($_POST["search"])) {
                    if (UserManager::hasOneNeededRank($_SESSION["uuid"], array("ADMINISTRATOR"))) {
                        ?>
                        <table class="table table-striped table-hover">
                            <th>Name</th>
                            <th>Address</th>
                            <th>Online</th>
                            <th>Players online</th>
                            <th>Document editing button</th>
                            <th>Document remove button</th>
                            <?php
                            foreach (ServersManager::getFilteredServers(array('server_name' => array('$regex' => ".*" . $_POST["search"] . ".*"))) as $server) {
                                ?>
                                <tr id="server-<?php echo $server["server_name"]; ?>">
                                    <td><?php echo $server["server_name"]; ?></td>
                                    <td><?php echo $server["host"] . ":" . $server["port"]; ?></td>
                                    <td><?php echo $server["online"] ? "Yes" : "No"; ?></td>
                                    <td><?php echo $server["current_online_count"] . "/" . $server["max_online_count"]; ?></td>
                                    <td><a href="<?php echo "server/" . $server["server_name"]; ?>" class="btn btn-info">Edit</a></td>
                                    <td>
                                        <button class="btn btn-danger" onclick="removeServer('<?php echo $server["server_name"]; ?>');">Remove</button>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                        <?php
                    }
                }
                break;
            case "remove_server":
                if (isset($_POST["server_name"])) {
                    if (UserManager::hasOneNeededRank($_SESSION["uuid"], array("ADMINISTRATOR"))) {
                        if (ServersManager::removeServer($_POST["server_name"])) {
                            $root_path = "/home/servers/";
                            $parent_path = $root_path . "minecraft/";
                            $server_name = $_POST["server_name"];
                            $server_root_path = $parent_path . $server_name . "/";
                            if (file_exists($server_root_path)) {
                                unlink($server_root_path);
                            }
                            echo "true";
                        } else {
                            echo "false";
                        }
                    }
                }
                break;
            case "add_server":
                if (isset($_POST["server_name"]) &&
                    isset($_POST["autorestart"]) &&
                    isset($_POST["host"]) &&
                    isset($_POST["port"]) &&
                    isset($_POST["max_online"]) &&
                    isset($_POST["maintenance"]) &&
                    isset($_POST["type"]) &&
                    isset($_POST["restricted"])
                ) {
                    if (UserManager::hasOneNeededRank($_SESSION["uuid"], array("ADMINISTRATOR"))) {
                        if (ServersManager::getServer($_POST["server_name"]) == null) {
                            ServersManager::addServer(
                                $_POST["server_name"],
                                $_POST["host"],
                                $_POST["port"],
                                $_POST["restricted"] == "true" ? true : false,
                                $_POST["maintenance"] == "true" ? true : false,
                                $_POST["autorestart"] == "true" ? true : false,
                                $_POST["type"],
                                $_POST["max_online"]);
                            $root_path = "/home/servers/";
                            $shared_jars_path = $root_path . "sharedJars/";
                            $shared_files_path = $root_path . "sharedFiles/";
                            $parent_path = $root_path . "minecraft/";
                            $server_name = $_POST["server_name"];
                            $server_port = $_POST["port"];
                            $server_type = $_POST["type"];
                            $server_max_online = $_POST["max_online"];
                            $server_root_path = $parent_path . $server_name . "/";
                            if (file_exists($server_root_path)) {
                                unlink($server_root_path);
                            }
                            mkdir($server_root_path, 0777, true);
                            mkdir($server_root_path . "plugins", 0777, true);
                            symlink($shared_jars_path . "Bukkit-Core-1.0-SNAPSHOT.jar", $server_root_path . "plugins/Bukkit-Core-1.0-SNAPSHOT.jar");
                            symlink($shared_jars_path . "Shared-Mongo-Driver-1.0-SNAPSHOT.jar", $server_root_path . "plugins/Shared-Mongo-Driver-1.0-SNAPSHOT.jar");
                            symlink($shared_jars_path . "ProtocolLib-4.0.0-SNAPSHOT.jar", $server_root_path . "plugins/ProtocolLib-4.0.0-SNAPSHOT.jar");
                            symlink($shared_jars_path . "ViaVersion.jar", $server_root_path . "plugins/ViaVersion.jar");
                            symlink($shared_jars_path . "spigot.jar", $server_root_path . "plugins/spigot_linked.jar");
                            symlink($shared_files_path . "bukkit.yml", $server_root_path . "bukkit.yml");
                            symlink($shared_files_path . "eula.txt", $server_root_path . "eula.txt");
                            file_put_contents($server_root_path . "server.properties", "spawn-protection=0" . PHP_EOL);
                            file_put_contents($server_root_path . "server.properties", "announce-player-achievements=false" . PHP_EOL, FILE_APPEND);
                            file_put_contents($server_root_path . "server.properties", "server-port=" . $server_port . PHP_EOL, FILE_APPEND);
                            file_put_contents($server_root_path . "server.properties", "online-mode=false" . PHP_EOL, FILE_APPEND);
                            file_put_contents($server_root_path . "server.properties", "level-name=world" . PHP_EOL, FILE_APPEND);
                            file_put_contents($server_root_path . "server.properties", "max-players=" . $server_max_online . PHP_EOL, FILE_APPEND);
                            file_put_contents($server_root_path . "start.sh", "screen -S " . $server_name . " java -jar -Xmx512M spigot_linked.jar");
                            chmod($server_root_path . "start.sh", 0777);

                            switch ($server_type) {
                                case "lobby":
                                    copy($shared_files_path . "/worlds/lobby", $server_root_path . "lobby");
                                    rename($server_root_path . "lobby", $server_root_path . "world");
                                    symlink($shared_jars_path . "Bukkit-Lobby-1.0-SNAPSHOT.jar", $server_root_path . "plugins/Bukkit-Lobby-1.0-SNAPSHOT.jar");
                                    break;
                                case "gib":
                                    copy($shared_files_path . "/worlds/lobby", $server_root_path . "world");

                            }

                        }
                    }
                }
                break;
        }
    }
}
?>
