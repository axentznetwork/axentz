<?php
/**
 * Created by IntelliJ IDEA.
 * User: Tarik
 * Date: 11.06.2016
 * Time: 19:32
 */

$title = "Logout";

session_start();

$logged_in = false;

if (isset($_SESSION["logged_in"])) {
    if ($_SESSION["logged_in"] == true) {
        $logged_in = true;
    }
}

if ($logged_in) {
    $_SESSION = array();
    $logged_in = false;
    include 'page_start.php';
    ?>
    <div class="col-md-push-3 col-md-6 col-xs-12">
        <div class="alert alert-success" role="alert">You successfully logged out! <a href="index.php">Want to restart?</a></div>
    </div>
    <?php
}else{
    include 'page_start.php';
    ?>
    <div class="col-md-push-3 col-md-6 col-xs-12">
        <div class="alert alert-danger" role="alert">You are not logged in! <a href="login.php">Want to login?</a></div>
    </div>
    <?php
}

include 'page_end.php';
?>
