package de.devlen.nichtecro.server.bungeecore.comfy.exception;

public class ArgumentParseException extends Exception {
    public ArgumentParseException(String msg) {
        super(msg);
    }
}
