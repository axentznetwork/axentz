package de.devlen.nichtecro.server.bungeecore.database.types;

/**
 * Created by Tarik on 21.03.2016.
 */
public enum SpecialField {

    UNIQUE("UNIQUE"),
    PRIMARY_KEY("PRIMARY KEY"),
    AUTO_INC("AUTO_INCREMENT");

    private String fieldName;

    SpecialField(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldName() {
        return fieldName;
    }
}
