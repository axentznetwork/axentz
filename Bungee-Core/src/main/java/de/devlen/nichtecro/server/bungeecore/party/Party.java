package de.devlen.nichtecro.server.bungeecore.party;

import de.devlen.nichtecro.server.bungeecore.BungeeCore;
import de.devlen.nichtecro.server.bungeecore.data.ExtendedList;
import de.devlen.nichtecro.server.bungeecore.message.Chat;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.List;
import java.util.UUID;

/**
 * Created by Tarik on 09.06.2016.
 */
public class Party {

    private UUID host = null;
    private List<UUID> members = new ExtendedList<>();
    private int maxMembers = 5;

    public UUID getHost() {
        return host;
    }

    public void setHost(UUID host) {
        this.host = host;
    }

    public List<UUID> getMembers() {
        return members;
    }

    public void setMembers(List<UUID> members) {
        this.members = members;
    }

    public int getMaxMembers() {
        return maxMembers;
    }

    public void setMaxMembers(int maxMembers) {
        this.maxMembers = maxMembers;
    }

    public void invite(ProxiedPlayer proxiedPlayer) {
        ProxiedPlayer hostProxiedPlayer = BungeeCore.getInstance().getProxy().getPlayer(host);
        assert hostProxiedPlayer.isConnected() : "Illegal state of the host. Host need to be online.";
        Chat.sendTl(proxiedPlayer, "bungee.core.friends.tag", "bungee.core.friend.invite.invited", hostProxiedPlayer.getName());
    }

    public boolean join(ProxiedPlayer player) {
        if (members.size() < maxMembers) {
            members.add(player.getUniqueId());
            return true;
        }
        return false;
    }
}
