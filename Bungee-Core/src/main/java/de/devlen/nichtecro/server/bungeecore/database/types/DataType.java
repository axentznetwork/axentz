package de.devlen.nichtecro.server.bungeecore.database.types;

import java.util.Date;

/**
 * Created by Tarik on 21.03.2016.
 */
public enum DataType {

    VARCHAR("VARCHAR", String.class),
    TEXT("TEXT", String.class),
    FLOAT("FLOAT", Float.class),
    DOUBLE("DOUBLE", Double.class),
    BOOLEAN("BOOLEAN", Boolean.class),
    DATETIME("DATETIME", Date.class),
    LONG("LONG", Long.class),
    INT("INT", Integer.class);

    private String databaseType;
    private Class<?> javaClass;

    DataType(String databaseType, Class<?> javaClass) {
        this.databaseType = databaseType;
        this.javaClass = javaClass;
    }

    public String getDatabaseType() {
        return databaseType;
    }

    public Class<?> getJavaClass() {
        return javaClass;
    }
}
