package de.devlen.nichtecro.server.bungeecore.listeners;

import de.devlen.nichtecro.server.bungeecore.BungeeCore;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.PluginManager;

/**
 * Created by Tarik on 14.05.2016.
 */
public class ListenerHelper implements Listener {

    public void register() {
        PluginManager pm = BungeeCore.getInstance().getProxy().getPluginManager();
        pm.registerListener(BungeeCore.getInstance(), this);
    }

}
