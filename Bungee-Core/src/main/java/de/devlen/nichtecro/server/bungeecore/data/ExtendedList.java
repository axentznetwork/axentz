package de.devlen.nichtecro.server.bungeecore.data;

import java.util.ArrayList;

/**
 * Created by Tarik on 06.03.2016.
 */
public class ExtendedList<E> extends ArrayList<E> {

    public ExtendedList(E... autoAdds) {
        for (E autoAdd : autoAdds) {
            add(autoAdd);
        }
    }

}
