package de.devlen.nichtecro.server.bungeecore.comfy.exception;

public class CommandTreeException extends RuntimeException {
    public CommandTreeException(String msg) {
        super(msg);
    }
}
