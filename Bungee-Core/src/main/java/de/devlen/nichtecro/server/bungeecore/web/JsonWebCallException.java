package de.devlen.nichtecro.server.bungeecore.web;

/**
 * Created by Tarik on 19.06.2016.
 */
public class JsonWebCallException extends RuntimeException {
    public JsonWebCallException() {
        super();
    }

    public JsonWebCallException(String message) {
        super(message);
    }

    public JsonWebCallException(String message, Throwable cause) {
        super(message, cause);
    }

    public JsonWebCallException(Throwable cause) {
        super(cause);
    }

    protected JsonWebCallException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
