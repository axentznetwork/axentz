package de.devlen.nichtecro.server.bungeecore.manager;

import com.mongodb.BasicDBObject;
import de.devlen.nichtecro.server.bungeecore.BungeeCore;
import de.devlen.nichtecro.server.bungeecore.data.ExtendedList;
import de.devlen.nichtecro.server.bungeecore.database.MongoDBManager;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.bson.Document;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static com.mongodb.client.model.Filters.eq;

/**
 * Created by Tarik on 22.05.2016.
 */
public class PlayerSettingsManager extends MongoDBManager {

    private static String collectionName = "bukkitcore_players";

    /**
     * Checks if a player is existing in the database. If not you should deny any action which is document related.
     *
     * @param player The player you want to check.
     * @return isExisting
     * @deprecated never needed but created for formal reason.
     */
    @Deprecated
    public static boolean isPlayerExisting(ProxiedPlayer player) {
        UUID uuid = player.getUniqueId();
        return isPlayerExisting(uuid);
    }

    /**
     * Checks if a player is existing in the database. If not you should deny any action which is document related.
     *
     * @param clientName The client you want to check.
     * @return isExisting
     */
    public static boolean isPlayerExisting(String clientName) {
        return isPlayerExisting(BungeeCore.getInstance().getProxy().getPlayer(clientName).getUniqueId());
    }

    /**
     * Checks if a player is existing in the database. If not you should deny any action which is document related.
     *
     * @param uuid The fetched Mojang-UUID.
     * @return isExisting
     */
    public static boolean isPlayerExisting(UUID uuid) {
        if (getCursor(collectionName, new BasicDBObject("uuid", uuid.toString())).iterator().hasNext()) {
            return true;
        }
        return false;
    }

    /**
     * Please call this method only in Bukkit-Core.
     *
     * @param uuid The fetched Mojang-UUID
     * @param name The clientName you want to associate.
     */
    public static void insertNewPlayer(UUID uuid, String name) {
        if (!isPlayerExisting(uuid)) {
            Document document = new Document("uuid", uuid.toString())
                    .append("player_name", name)
                    .append("rank", Arrays.asList(Rank.GUEST.name()))
                    .append("language", "de_DE")
                    .append("dyes", 0);
            insert(collectionName, document);
        }
    }

    public static void insertNewPlayer(ProxiedPlayer player) {
        UUID uuid = player.getUniqueId();
        String name = player.getName();
        insertNewPlayer(uuid, name);
    }

    public static List<Rank> getRanks(ProxiedPlayer player) {
        return getRanks(player.getUniqueId());

    }

    public static List<Rank> getRanks(String name) {
        return getRanks(BungeeCore.getInstance().getProxy().getPlayer(name).getUniqueId());
    }

    public static List<Rank> getRanks(UUID uuid) {
        if (isPlayerExisting(uuid)) {
            Document document = getFirstDocument(collectionName, eq("uuid", uuid.toString()));
            List<String> rankNames = (List<String>) document.get("rank");
            List<Rank> ranks = new ExtendedList<>();
            if (rankNames != null && rankNames.size() > 0) {
                for (String rankName : rankNames) {
                    try {
                        Rank rank = Rank.valueOf(rankName.toUpperCase());
                        ranks.add(rank);
                    } catch (Exception ex) {
                    }
                }
                Collections.sort(ranks);
                Collections.reverse(ranks);
                return ranks;
            } else {
                return new ExtendedList<>(Rank.GUEST);
            }
        } else {
            return new ExtendedList<>(Rank.GUEST);
        }
    }

    public static Rank getRank(ProxiedPlayer player) {
        return getRank(player.getUniqueId());
    }

    public static Rank getRank(String name) {
        return getRank(BungeeCore.getInstance().getProxy().getPlayer(name).getUniqueId());
    }

    public static Rank getRank(UUID uuid) {
        if (isPlayerExisting(uuid)) {
            Document document = getFirstDocument(collectionName, eq("uuid", uuid.toString()));
            List<String> rankName = (List<String>) document.get("rank");
            if (rankName != null && rankName.get(0) != null) {
                return Rank.valueOf(rankName.get(0).toUpperCase());
            }
            return Rank.GUEST;
        } else {
            return Rank.GUEST;
        }
    }

    public static void addRank(ProxiedPlayer player, Rank rank) {
        addRank(player.getUniqueId(), rank);
    }

    public static void addRank(String name, Rank rank) {
        addRank(BungeeCore.getInstance().getProxy().getPlayer(name).getUniqueId(), rank);
    }

    public static void addRank(UUID uuid, Rank rank) {
        if (isPlayerExisting(uuid)) {
            if (!getRanks(uuid).contains(rank)) {
                update(collectionName, eq("uuid", uuid.toString()), new BasicDBObject("$push", new BasicDBObject("rank", rank.name())));
            }
        }
    }

    public static void removeRank(ProxiedPlayer player, Rank rank) {
        removeRank(player.getUniqueId(), rank);
    }

    public static void removeRank(String name, Rank rank) {
        removeRank(BungeeCore.getInstance().getProxy().getPlayer(name).getUniqueId(), rank);
    }

    public static void removeRank(UUID uuid, Rank rank) {
        if (isPlayerExisting(uuid)) {
            List<Rank> ranks = getRanks(uuid);
            if (ranks.contains(rank) && ranks.size() > 1) {
                update(collectionName, eq("uuid", uuid.toString()), new BasicDBObject("$pull", new BasicDBObject("rank", rank.name())));
            }
        }
    }

    public static void setRank(ProxiedPlayer player, Rank rank) {
        setRank(player.getUniqueId(), rank);
    }

    public static void setRank(String name, Rank rank) {
        setRank(BungeeCore.getInstance().getProxy().getPlayer(name).getUniqueId(), rank);
    }

    public static void setRank(UUID uuid, Rank rank) {
        if (isPlayerExisting(uuid)) {
            update(collectionName, eq("uuid", uuid.toString()), new BasicDBObject("$set", new BasicDBObject("rank", Arrays.asList(rank.name()))));
        }
    }

    public static String getLanguage(String name) {
        return getLanguage(BungeeCore.getInstance().getProxy().getPlayer(name).getUniqueId());
    }

    public static String getLanguage(ProxiedPlayer player) {
        return getLanguage(player.getUniqueId());
    }

    /**
     * Language-locales are e.g. en_US, de_DE, fr_CA.
     *
     * @param uuid - The Mojang-fetched UUID. See {@link #getLanguage(String)} or {@link #getLanguage(ProxiedPlayer)}  for further usage.}
     * @return A language locale.
     */
    public static String getLanguage(UUID uuid) {
        if (isPlayerExisting(uuid)) {
            Document document = getFirstDocument(collectionName, eq("uuid", uuid.toString()));
            String language = document.getString("language");
            return language;
        }
        return "en_US";
    }

    /**
     * Used by the incoming packet of minecraft to set the language known for the server.
     * Language-locales are e.g. en_US, de_DE, fr_CA.
     *
     * @param player   - A Bukkit-player.
     * @param language - A language locale.
     */
    public static void setLanguage(ProxiedPlayer player, String language) {
        setLanguage(player.getUniqueId(), language);
    }

    /**
     * Used by the incoming packet of minecraft to set the language known for the server.
     * Language-locales are e.g. en_US, de_DE, fr_CA.
     *
     * @param name     - A valid Minecraft-username.
     * @param language - A language locale.
     */
    public static void setLanguage(String name, String language) {
        setLanguage(BungeeCore.getInstance().getProxy().getPlayer(name).getUniqueId(), language);
    }

    /**
     * Used by the incoming packet of minecraft to set the language known for the server.
     * Language-locales are e.g. en_US, de_DE, fr_CA.
     *
     * @param uuid     - A Mojang-fetched UUID. See {@link #setLanguage(ProxiedPlayer, String)}  or {@link #setLanguage} for further usage.}
     * @param language - A language locale.
     */
    public static void setLanguage(UUID uuid, String language) {
        if (isPlayerExisting(uuid)) {
            update(collectionName, eq("uuid", uuid.toString()), new BasicDBObject("$set", new BasicDBObject("language", language)));
        }
    }

    public static void transferDyes(ProxiedPlayer player, int dyes) {
        transferDyes(player.getUniqueId(), dyes);
    }

    public static void transferDyes(String name, int dyes) {
        transferDyes(BungeeCore.getInstance().getProxy().getPlayer(name).getUniqueId(), dyes);
    }

    public static void transferDyes(UUID uuid, int dyes) {
        setDyes(uuid, getDyes(uuid) + dyes);
    }

    public static void debitDyes(ProxiedPlayer player, int dyes) {
        setDyes(player.getUniqueId(), dyes);
    }

    /**
     * Dyes are the currency. Be careful what you do!
     *
     * @param name Name of the client
     * @param dyes Amount of dyes.
     */
    public static void debitDyes(String name, int dyes) {
        setDyes(BungeeCore.getInstance().getProxy().getPlayer(name).getUniqueId(), dyes);
    }

    /**
     * Dyes are the currency. Be careful what you do!
     *
     * @param uuid - The fetched Mojang-UUID.
     * @param dyes - How much dyes should be debited.
     * @return Transaction was sucessfully.
     */
    public static boolean debitDyes(UUID uuid, int dyes) {
        if (hasEnoughDyes(uuid, dyes)) {
            setDyes(uuid, getDyes(uuid) - dyes);
            return true;
        }
        return false;
    }

    public static boolean hasEnoughDyes(UUID uuid, int dyes) {
        if (isPlayerExisting(uuid)) {
            return getDyes(uuid) > dyes;
        }
        return false;
    }

    private static void setDyes(UUID uuid, int dyes) {
        if (isPlayerExisting(uuid)) {
            update(collectionName, eq("uuid", uuid.toString()), new BasicDBObject("$set", new BasicDBObject("dyes", dyes)));
        }
    }

    public static int getDyes(UUID uuid) {
        if (isPlayerExisting(uuid)) {
            Document document = getFirstDocument(collectionName, eq("uuid", uuid.toString()));
            Integer dyes = document.getInteger("dyes");
            if (dyes != null) {
                return dyes;
            }
            return 0;
        }
        return 0;
    }

    public static boolean isRegistered(ProxiedPlayer player) {
        return isRegistered(player.getUniqueId());
    }

    public static boolean isRegistered(String name) {
        return isRegistered(BungeeCore.getInstance().getProxy().getPlayer(name));
    }

    public static boolean isRegistered(UUID uuid) {
        if (isPlayerExisting(uuid)) {
            Document document = getFirstDocument(collectionName, eq("uuid", uuid.toString()));
            return document.getString("password_hash") != null;
        }
        return false;
    }

    public static void setPasswordHash(ProxiedPlayer proxiedPlayer, String hash) {
        setPasswordHash(proxiedPlayer.getUniqueId(), hash);
    }

    public static void setPasswordHash(String name, String hash){
        setPasswordHash(BungeeCore.getInstance().getProxy().getPlayer(name), hash);
    }

    public static void setPasswordHash(UUID uuid, String hash) {
        if (isPlayerExisting(uuid)) {
            update(collectionName, eq("uuid", uuid.toString()), new BasicDBObject("$set", new BasicDBObject("password_hash", hash)));
        }
    }
}
