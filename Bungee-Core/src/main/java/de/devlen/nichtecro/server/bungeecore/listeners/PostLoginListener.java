package de.devlen.nichtecro.server.bungeecore.listeners;

import de.devlen.nichtecro.server.bungeecore.permission.PermissionManager;
import de.devlen.nichtecro.server.bungeecore.manager.PlayerSettingsManager;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by Tarik on 03.06.2016.
 */
public class PostLoginListener extends ListenerHelper {

    @EventHandler
    public void onPostLogin(PostLoginEvent e) {
        ProxiedPlayer proxiedPlayer = e.getPlayer();
        PlayerSettingsManager.insertNewPlayer(proxiedPlayer.getUniqueId(), proxiedPlayer.getName());

        PermissionManager.calculatePermissions(proxiedPlayer);
    }

}
