package de.devlen.nichtecro.server.bungeecore.comfy.argument;

import de.devlen.nichtecro.server.bungeecore.comfy.ArgumentType;
import de.devlen.nichtecro.server.bungeecore.comfy.exception.ArgumentParseException;

public class StringType extends ArgumentType<String> {
    @Override
    public String parse(String segment) throws ArgumentParseException {
        return segment;
    }

    @Override
    public boolean matches(String segment) {
        return segment.matches(".+");
    }
}
