package de.devlen.nichtecro.server.bungeecore;

import de.devlen.nichtecro.server.bungeecore.comfy.argument.StringType;
import de.devlen.nichtecro.server.bungeecore.comfy.bungeecord.BungeeCommandManager;
import de.devlen.nichtecro.server.bungeecore.comfy.tree.Argument;
import de.devlen.nichtecro.server.bungeecore.comfy.tree.Literal;
import de.devlen.nichtecro.server.bungeecore.commands.CommandGtest;
import de.devlen.nichtecro.server.bungeecore.commands.CommandMotd;
import de.devlen.nichtecro.server.bungeecore.database.MongoDBManager;
import de.devlen.nichtecro.server.bungeecore.listeners.PostLoginListener;
import de.devlen.nichtecro.server.bungeecore.listeners.ProxyPingListener;
import de.devlen.nichtecro.server.bungeecore.listeners.ProxyReloadListener;
import de.devlen.nichtecro.server.bungeecore.permission.PermissionManager;
import de.devlen.nichtecro.server.bungeecore.manager.ProxyServersManager;
import net.md_5.bungee.api.plugin.Plugin;

/**
 * Created by Tarik on 14.05.2016.
 */
public class BungeeCore extends Plugin {

    private static Plugin instance;

    @Override
    public void onEnable() {
        instance = this;
        registerListeners();
        registerCommands();
        prepareDatabase();


    }

    public static Plugin getInstance() {
        return instance;
    }

    public void prepareDatabase() {
        MongoDBManager.initClient();
        ProxyServersManager.cache();
        PermissionManager.cache();
    }

    public void registerListeners() {
        new PostLoginListener().register();
        new ProxyPingListener().register();
        new ProxyReloadListener().register();
    }

    public void registerCommands() {
        BungeeCommandManager bungeeCommandManager = new BungeeCommandManager(this);
        bungeeCommandManager.addCommand(new Literal("motd")
                .then(new Argument("message", new StringType())
                        .executes("command.motd.message")
                        .permission("bungee.core.command.motd")
                ));
        bungeeCommandManager.addCommand(new Literal("gtest")
                .executes("command.gtest"));

        bungeeCommandManager.addListener(new CommandGtest());
        bungeeCommandManager.addListener(new CommandMotd());
        bungeeCommandManager.registerCommands();
    }

    public static void runTeamUpdateTask() {
//        for (ProxiedPlayer proxiedPlayer : getInstance().getProxy().getPlayers()) {
//            for (final Rank rank : Rank.values()) {
//                String tl = tl(rank.getTagPrefixNode(), entry.getKey());
//                if (tl.length() > 0) {
//                    final Scroller scroller = new Scroller(tl, 7, 3);
//                    Bukkit.getScheduler().scheduleSyncRepeatingTask(getInstance(), new Runnable() {
//                        @Override
//                        public void run() {
//                            Team team = scoreboard.getTeam(rank.getTeamName());
//                            if (team == null) {
//                                team = scoreboard.registerNewTeam(rank.getTeamName());
//                            }
//                            team.setPrefix(rank.getFormatParamters() + scroller.next() + " �7");
//                        }
//                    }, 0, 4);
//                } else {
//                    Team team = scoreboard.getTeam(rank.getTeamName());
//                    if (team == null) {
//                        team = scoreboard.registerNewTeam(rank.getTeamName());
//                    }
//                    team.setPrefix("�7");
//                }
//            }
//        }
    }
}
