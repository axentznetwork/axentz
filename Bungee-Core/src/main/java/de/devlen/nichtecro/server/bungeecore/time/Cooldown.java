package de.devlen.nichtecro.server.bungeecore.time;

import com.google.common.collect.Maps;
import de.devlen.nichtecro.server.bungeecore.data.ExtendedList;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.HashMap;
import java.util.List;

public class Cooldown {

    private static HashMap<String, List<CooldownProperties>> playersCooldowns = Maps.newHashMap();

    public static void charge(final ProxiedPlayer player, String key, int timeInSeconds) {
        if (hasCharge(player, key))
            return;

        final CooldownProperties cooldownProperties = new CooldownProperties(key, System.currentTimeMillis(), (long) (timeInSeconds * 1000));
        if (playersCooldowns.get(player.getName()) != null) {
            List<CooldownProperties> cooldownPropertiesList = playersCooldowns.get(player.getName());
            cooldownPropertiesList.add(cooldownProperties);
            playersCooldowns.put(player.getName(), cooldownPropertiesList);
        } else {
            playersCooldowns.put(player.getName(), new ExtendedList<>(cooldownProperties));
        }

        Delay.make((long) timeInSeconds, new Runnable() {
            @Override
            public void run() {
                playersCooldowns.get(player.getName()).remove(cooldownProperties);
            }
        });
    }

    public static boolean hasCharge(ProxiedPlayer player, String key) {
        if (playersCooldowns.containsKey(player.getName())) {
            for (CooldownProperties cooldownProperties : playersCooldowns.get(player.getName())) {
                if (cooldownProperties.getTag().equalsIgnoreCase(key))
                    return true;
            }
            return false;
        } else {
            return false;
        }
    }

    public static float getRemainingTime(ProxiedPlayer player, String key) {
        if (hasCharge(player, key)) {
            if (playersCooldowns.containsKey(player.getName())) {
                for (CooldownProperties cooldownProperties : playersCooldowns.get(player.getName())) {
                    if (cooldownProperties.getTag().equalsIgnoreCase(key))
                        return (cooldownProperties.getLengthMillis() - (System.currentTimeMillis() - cooldownProperties.getStartedMillis())) / 1000.0f;
                }
                return 0f;
            } else {
                return 0f;
            }
        }
        return 0f;
    }
}