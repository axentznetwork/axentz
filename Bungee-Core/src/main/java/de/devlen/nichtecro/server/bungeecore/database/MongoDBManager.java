package de.devlen.nichtecro.server.bungeecore.database;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import de.devlen.nichtecro.server.bungeecore.Constants;
import de.devlen.nichtecro.server.bungeecore.data.ExtendedList;
import org.bson.Document;
import org.bson.conversions.Bson;

/**
 * Created by Tarik on 21.05.2016.
 */
public class MongoDBManager {

    private static MongoClient instance = null;
    private static String tablePrefix = "bungeecore_";

    public static void initClient() {
        MongoCredential credential = MongoCredential.createCredential(Constants.getMongoDbUser(), "admin", Constants.getMongoDbPassword().toCharArray());
        instance = new MongoClient(new ServerAddress(Constants.getMongoDbAddress()), new ExtendedList<>(credential));
    }

    public static MongoClient getInstance() {
        return instance;
    }

    public static MongoDatabase getMongoDatabase(String name) {
        return instance.getDatabase(name);
    }

    public static MongoCollection<Document> getCollection(String collectionName) {
        MongoCollection<Document> documentMongoCollection = getMongoDatabase(Constants.getMongoDbDatabase()).getCollection(collectionName);
        return documentMongoCollection;
    }

    public static Document getFirstDocument(String collectionName, Bson filterCriteria) {
        MongoCollection<Document> docs = getCollection(collectionName);
        Document document = docs.find(filterCriteria).first();
        return document;
    }

    public static FindIterable<Document> getCursor(String collectionName, Bson filterCriteria) {
        if (filterCriteria == null) {
            return getCollection(collectionName).find();
        }
        return getCollection(collectionName).find(filterCriteria);
    }

    public static void update(String collectionName, Bson filter, Bson update) {
        getCollection(collectionName).updateOne(filter, update, new UpdateOptions().upsert(true));
    }

    public static void insert(String collectionName, Document document) {
        getCollection(collectionName).insertOne(document);
    }

    public static String getCollectionPrefix() {
        return tablePrefix;
    }
}
