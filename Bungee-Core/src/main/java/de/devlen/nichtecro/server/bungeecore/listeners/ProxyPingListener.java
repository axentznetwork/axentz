package de.devlen.nichtecro.server.bungeecore.listeners;

import de.devlen.nichtecro.server.bungeecore.BungeeCore;
import de.devlen.nichtecro.server.bungeecore.manager.ProxySettingsManager;
import de.devlen.nichtecro.server.bungeecore.message.Chat;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by Tarik on 14.05.2016.
 */
public class ProxyPingListener extends ListenerHelper {

    @EventHandler
    public void onProxyPing(ProxyPingEvent e) {
        ServerPing.Protocol protocol = e.getResponse().getVersion();
        int online = e.getResponse().getPlayers().getOnline();
        ServerPing.PlayerInfo[] playerInfos = e.getResponse().getPlayers().getSample();
        ServerPing.Players players = new ServerPing.Players((online + 1), online, playerInfos);
        ServerPing serverPing = new ServerPing(protocol, players, new TextComponent(Chat.replaceFormatVariables(ProxySettingsManager.getPropertyString("motd"))), BungeeCore.getInstance().getProxy().getConfig().getFaviconObject());
        e.setResponse(serverPing);
    }

}
