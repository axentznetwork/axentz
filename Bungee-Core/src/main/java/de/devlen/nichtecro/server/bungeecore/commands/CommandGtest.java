package de.devlen.nichtecro.server.bungeecore.commands;

import de.devlen.nichtecro.server.bungeecore.comfy.CommandListener;
import de.devlen.nichtecro.server.bungeecore.comfy.annotation.CommandHandler;
import de.devlen.nichtecro.server.bungeecore.comfy.bungeecord.BungeeCommandContext;
import de.devlen.nichtecro.server.bungeecore.data.ExtendedList;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.protocol.packet.Team;

/**
 * Created by Tarik on 09.06.2016.
 */
public class CommandGtest implements CommandListener {

    @CommandHandler("command.gtest")
    public void onCommandGtest(BungeeCommandContext c) {
        if (c.getSender().isPlayer()) {
            ProxiedPlayer proxiedPlayer = (ProxiedPlayer) c.getSender().getSender();
            Team team = new Team("012", (byte) 0, "Administraotr", "�6", " �3LULU", "always", "always", (byte) 2, (byte) 0x01, new ExtendedList<>(proxiedPlayer.getName()).toArray(new String[0]));
            proxiedPlayer.unsafe().sendPacket(team);
        }
    }

}
