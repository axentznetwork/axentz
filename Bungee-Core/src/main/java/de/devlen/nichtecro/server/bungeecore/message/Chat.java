package de.devlen.nichtecro.server.bungeecore.message;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.Arrays;
import java.util.Iterator;

import static de.devlen.nichtecro.server.bungeecore.language.Language.tl;

/**
 * Created by Tarik on 14.05.2016.
 */
public class Chat {

    public static void sendTl(ProxiedPlayer player, String prefixTranslatableNode, String messageTranslatableNode, String... messageVariables) {
        send(player, tl(prefixTranslatableNode, player), tl(messageTranslatableNode, player, messageVariables));
    }

    public static void send(CommandSender player, String prefix, String message) {
        player.sendMessage(new TextComponent(replaceFormatVariables("%d" + prefix + " %s� %u" + message)));
    }

    public static String replaceFormatVariables(String message) {
        message = "%u" + message;
        message = message.replace("%s", ChatColor.DARK_GRAY.toString());
        message = message.replace("%u", ChatColor.GRAY.toString());
        message = message.replace("%l", ChatColor.AQUA.toString());
        message = message.replace("%d", ChatColor.DARK_AQUA.toString());
        return message;
    }

    public static String normalizeEnumValueName(Enum enumValue) {
        String name = enumValue.name();
        String[] words = name.split("_");
        String normalized = "";
        Iterator<String> iterator = Arrays.asList(words).iterator();
        while (iterator.hasNext()) {
            String word = iterator.next();
            normalized += word.toLowerCase();
            if (iterator.hasNext()) {
                normalized += " ";
            }
        }
        normalized = normalized.substring(0, 1).toUpperCase() + normalized.substring(1);
        return normalized;
    }

}
