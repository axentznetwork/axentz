package de.devlen.nichtecro.server.bungeecore.proxy;

import com.google.common.collect.Maps;
import de.devlen.nichtecro.server.bungeecore.data.ExtendedList;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.protocol.packet.Team;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Tarik on 10.06.2016.
 */
public class ProxiedTeam {

    private String name = "";
    private String displayName = "";
    private String prefix = "";
    private String suffix = "";
    private boolean allowFriendlyFire = true;
    private boolean seeInvisiblePlayersSameTeam = true;
    private List<String> players = new ExtendedList<>();
    private NameTagVisibility nameTagVisibility = NameTagVisibility.ALWAYS;
    private CollisionRule collisionRule = CollisionRule.ALWAYS;

    private static List<ProxiedTeam> storedTeams = new ExtendedList<>();
    private static HashMap<ProxiedPlayer, List<ProxiedTeam>> sentTeams = Maps.newHashMap();

    public ProxiedTeam(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public boolean isAllowFriendlyFire() {
        return allowFriendlyFire;
    }

    public void setAllowFriendlyFire(boolean allowFriendlyFire) {
        this.allowFriendlyFire = allowFriendlyFire;
    }

    public boolean isSeeInvisiblePlayersSameTeam() {
        return seeInvisiblePlayersSameTeam;
    }

    public void setSeeInvisiblePlayersSameTeam(boolean seeInvisiblePlayersSameTeam) {
        this.seeInvisiblePlayersSameTeam = seeInvisiblePlayersSameTeam;
    }

    public List<String> getPlayers() {
        return players;
    }

    public void setPlayers(List<String> players) {
        this.players = players;
    }

    public NameTagVisibility getNameTagVisibility() {
        return nameTagVisibility;
    }

    public void setNameTagVisibility(NameTagVisibility nameTagVisibility) {
        this.nameTagVisibility = nameTagVisibility;
    }

    public CollisionRule getCollisionRule() {
        return collisionRule;
    }

    public void setCollisionRule(CollisionRule collisionRule) {
        this.collisionRule = collisionRule;
    }

    public void create(ProxiedPlayer proxiedPlayer) {
        byte friendlyOptions = 0x00;
        if (isAllowFriendlyFire()) {
            friendlyOptions |= 0x01;
        }
        if (isSeeInvisiblePlayersSameTeam()) {
            friendlyOptions |= 0x02;
        }
        Team team = new Team(getName());
        team.setName(getName());
        team.setCollisionRule(getCollisionRule().getCollisionRuleName());
        team.setColor((byte) 0);
        team.setDisplayName(getDisplayName());
        team.setFriendlyFire(friendlyOptions);
        team.setMode((byte) 0);
        team.setNameTagVisibility(getNameTagVisibility().getNameTagVisibilityName());
        team.setPlayers(getPlayers().toArray(new String[0]));
        team.setPrefix(getPrefix());
        team.setSuffix(getSuffix());
        proxiedPlayer.unsafe().sendPacket(team);
    }

    public void remove(ProxiedPlayer proxiedPlayer) {
        Team team = new Team(getName());
        team.setName(getName());
        team.setMode((byte) 1);
        proxiedPlayer.unsafe().sendPacket(team);
    }

    public void update(ProxiedPlayer proxiedPlayer) {
        if (isTeamSent(proxiedPlayer, this)) {
            byte friendlyOptions = 0x00;
            if (isAllowFriendlyFire()) {
                friendlyOptions |= 0x01;
            }

            if (isSeeInvisiblePlayersSameTeam()) {
                friendlyOptions |= 0x02;
            }

            Team team = new Team(getName());
            team.setName(getName());
            team.setCollisionRule(getCollisionRule().getCollisionRuleName());
            team.setColor((byte) 0);
            team.setDisplayName(getDisplayName());
            team.setFriendlyFire(friendlyOptions);
            team.setMode((byte) 2);
            team.setNameTagVisibility(getNameTagVisibility().getNameTagVisibilityName());
            team.setPrefix(getPrefix());
            team.setSuffix(getSuffix());
            proxiedPlayer.unsafe().sendPacket(team);
        } else {
            sendTeam(proxiedPlayer, this);
        }
    }

    public void join(ProxiedPlayer proxiedPlayer) {
        if (isTeamSent(proxiedPlayer, this)) {
            Team team = new Team(getName());
            team.setName(getName());
            team.setMode((byte) 3);
            team.setPlayers(new ExtendedList<>(proxiedPlayer.getName()).toArray(new String[0]));
            getPlayers().add(proxiedPlayer.getName());
            proxiedPlayer.unsafe().sendPacket(team);
        }
    }

    public void leave(ProxiedPlayer proxiedPlayer) {
        if (isTeamSent(proxiedPlayer, this)) {
            Team team = new Team(getName());
            team.setName(getName());
            team.setMode((byte) 4);
            team.setPlayers(new ExtendedList<>(proxiedPlayer.getName()).toArray(new String[0]));
            getPlayers().remove(proxiedPlayer.getName());
            proxiedPlayer.unsafe().sendPacket(team);
        }
    }

    public static void sendTeam(ProxiedPlayer proxiedPlayer, ProxiedTeam proxiedTeam) {
        if (!isTeamSent(proxiedPlayer, proxiedTeam)) {
            if (sentTeams.containsKey(proxiedPlayer)) {
                List<ProxiedTeam> proxiedTeams = sentTeams.get(proxiedPlayer);
                proxiedTeams.add(proxiedTeam);
                proxiedTeam.create(proxiedPlayer);
            } else {
                sentTeams.put(proxiedPlayer, new ExtendedList<>(proxiedTeam));
                proxiedTeam.create(proxiedPlayer);
            }
        }
    }

    public static boolean isTeamSent(ProxiedPlayer proxiedPlayer, ProxiedTeam proxiedTeam) {
        if (sentTeams.containsKey(proxiedPlayer)) {
            if (sentTeams.get(proxiedPlayer).contains(proxiedTeam)) {
                return true;
            }
            return false;
        }
        return false;
    }

    public static boolean containsTeam(ProxiedPlayer proxiedPlayer, String name){
        List<ProxiedTeam> proxiedTeams = sentTeams.get(proxiedPlayer);
        for(ProxiedTeam proxiedTeam : proxiedTeams){
            if(proxiedTeam.getName().equalsIgnoreCase(name)){
                return true;
            }
        }
        return false;
    }

    public static void removePlayer(ProxiedPlayer proxiedPlayer) {
        if (sentTeams.containsKey(proxiedPlayer)) {
            sentTeams.remove(proxiedPlayer);
        }
    }

    enum NameTagVisibility {
        ALWAYS("always"),
        HIDE_FOR_OTHER_TEAMS("hideForOtherTeams"),
        HIDE_FOR_OWN_TEAM("hideForOwnTeam"),
        NEVER("never");

        private String nameTagVisibilityName = "";

        NameTagVisibility(String nameTagVisibilityName) {
            this.nameTagVisibilityName = nameTagVisibilityName;
        }

        public String getNameTagVisibilityName() {
            return nameTagVisibilityName;
        }
    }

    enum CollisionRule {
        ALWAYS("always"),
        PUSH_OTHER_TEAMS("pushOtherTeams"),
        PUSH_OWN_TEAM("pushOwnTeam"),
        NEVER("never");

        private String collisionRuleName = "";

        CollisionRule(String collisionRuleName) {
            this.collisionRuleName = collisionRuleName;
        }

        public String getCollisionRuleName() {
            return collisionRuleName;
        }
    }


}
