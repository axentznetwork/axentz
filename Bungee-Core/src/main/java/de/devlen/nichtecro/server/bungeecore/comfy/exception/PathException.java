package de.devlen.nichtecro.server.bungeecore.comfy.exception;

public class PathException extends Exception {
    public PathException(String msg) {
        super(msg);
    }
}
