package de.devlen.nichtecro.server.bungeecore.manager;

import de.devlen.nichtecro.server.bungeecore.BungeeCore;
import net.md_5.bungee.api.config.ServerInfo;

import java.net.InetSocketAddress;

/**
 * Created by Tarik on 09.06.2016.
 */
public class CServerInfo {

    private String serverName = "";
    private String host = "";
    private String defaultMotd = "";
    private String serverType = "";
    private int port = 0;
    private int currentOnlineCount = 0;
    private int maxOnlineCount = 0;
    private boolean restricted = false;
    private boolean online = false;
    private boolean maintenance = false;
    private boolean autoRestart = false;

    public CServerInfo(String serverName, String host, String defaultMotd, int port, boolean restricted) {
        this.serverName = serverName;
        this.host = host;
        this.defaultMotd = defaultMotd;
        this.port = port;
        this.restricted = restricted;
    }

    public String getServerName() {
        return serverName;
    }

    public String getHost() {
        return host;
    }

    public String getDefaultMotd() {
        return defaultMotd;
    }

    public String getServerType() {
        return serverType;
    }

    public void setServerType(String serverType) {
        this.serverType = serverType;
    }

    public int getPort() {
        return port;
    }

    public int getCurrentOnlineCount() {
        return currentOnlineCount;
    }

    public void setCurrentOnlineCount(int currentOnlineCount) {
        this.currentOnlineCount = currentOnlineCount;
    }

    public int getMaxOnlineCount() {
        return maxOnlineCount;
    }

    public void setMaxOnlineCount(int maxOnlineCount) {
        this.maxOnlineCount = maxOnlineCount;
    }

    public boolean isRestricted() {
        return restricted;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public boolean isMaintenance() {
        return maintenance;
    }

    public void setMaintenance(boolean maintenance) {
        this.maintenance = maintenance;
    }

    public boolean isAutoRestart() {
        return autoRestart;
    }

    public void setAutoRestart(boolean autoRestart) {
        this.autoRestart = autoRestart;
    }

    public ServerInfo getServerInfo() {
        ServerInfo serverInfo = BungeeCore.getInstance().getProxy().constructServerInfo(getServerName(), new InetSocketAddress(getHost(), getPort()), getDefaultMotd(), isRestricted());
        return serverInfo;
    }
}
