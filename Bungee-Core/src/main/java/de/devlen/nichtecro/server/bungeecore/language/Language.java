package de.devlen.nichtecro.server.bungeecore.language;


import de.devlen.nichtecro.server.bungeecore.database.MongoDBManager;
import de.devlen.nichtecro.server.bungeecore.manager.PlayerSettingsManager;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.bson.Document;

import static com.mongodb.client.model.Filters.eq;

/**
 * The core-class for getting translation in <i>hopefully</i> several languages.
 *
 * @author Tarik Weiss
 * @since Bukkit-Core-1.0-SNAPSHOT
 */
public class Language extends MongoDBManager {

    private static String collectionName = getCollectionPrefix() + "translations";

    /**
     * Sample:
     * <blockquote><pre>
     * call: getTranslation("test.node", "en_US", "DevLen_", "You are very nice!");
     * value for translation-node <b>test.node</b> and the language <b>en_US</b>: This is my {0} ign but I want to get another dynamic information like that: {1}
     * returns: This is my DevLen_ ign but I want to get another dynamic information like that: You are very nice!
     * </pre></blockquote>
     *
     * @param translatableNode A node set by the developer e.g. minigame.timv.winner
     * @param player           A player where locale to fetch from.
     * @param variables        Replacings for place-holder like {0} and so on.
     * @return Translated component with replaced fields.
     */
    public static String tl(String translatableNode, ProxiedPlayer player, String... variables) {
        return tl(translatableNode, PlayerSettingsManager.getLanguage(player), variables);
    }

    /**
     * Sample:
     * <blockquote><pre>
     * call: getTranslation("test.node", "en_US", "DevLen_", "You are very nice!");
     * value for translation-node <b>test.node</b> and the language <b>en_US</b>: This is my {0} ign but I want to get another dynamic information like that: {1}
     * returns: This is my DevLen_ ign but I want to get another dynamic information like that: You are very nice!
     * </pre></blockquote>
     *
     * @param translatableNode A node set by the developer e.g. minigame.timv.winner
     * @param locale           A locale got by the player.
     * @param variables        Replacings for place-holder like {0} and so on.
     * @return Translated component with replaced fields.
     */
    public static String tl(String translatableNode, String locale, String... variables) {
        return getTranslation(translatableNode, locale, variables);
    }

    /**
     * Sample:
     * <blockquote><pre>
     * call: getTranslation("test.node", "en_US", "DevLen_", "You are very nice!");
     * value for translation-node <b>test.node</b> and the language <b>en_US</b>: This is my {0} ign but I want to get another dynamic information like that: {1}
     * returns: This is my DevLen_ ign but I want to get another dynamic information like that: You are very nice!
     * </pre></blockquote>
     *
     * @param translatableNode A node set by the developer e.g. minigame.timv.winner
     * @param locale           A locale got by the player.
     * @param variables        Replacings for place-holder like {0} and so on.
     * @return Translated component with replaced fields.
     */
    public static String getTranslation(String translatableNode, String locale, String... variables) {
        Document document = getFirstDocument(collectionName, eq("node", translatableNode));
        if (document != null) {
            Document subDocument = (Document) document.get("languages");
            if (subDocument != null) {
                String translation = subDocument.getString(locale);
                if (translation != null) {
                    for (int i = 0; i < variables.length; i++) {
                        translation = translation.replace("{" + i + "}", variables[i]);
                    }
                } else {
                    if (locale.equalsIgnoreCase("en_US")) {
                        return "The default translation en_US is not available. Sorry for that! (Following only needed for �3�lDEV %uTranslatable-node: " + translatableNode + ")";
                    } else {
                        translation = subDocument.getString("en_US");
                        if (translation != null) {
                            for (int i = 0; i < variables.length; i++) {
                                translation = translation.replace("{" + i + "}", variables[i]);
                            }
                        } else {
                            return "The default translation en_US is not available. Sorry for that! (Following only needed for �3�lDEV %uTranslatable-node: " + translatableNode + ")";
                        }
                    }
                }
                return translation;
            }
            return "This translation isn't currently available, sorry for that! (Following only needed for �3�lDEV %uTranslatable-node: " + translatableNode + ")";
        } else {
            return "This translation isn't currently available, sorry for that! (Following only needed for �3�lDEV %uTranslatable-node: " + translatableNode + ")";
        }
    }

}
