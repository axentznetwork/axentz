package de.devlen.nichtecro.server.bungeecore.listeners;

import de.devlen.nichtecro.server.bungeecore.BungeeCore;
import de.devlen.nichtecro.server.bungeecore.permission.PermissionManager;
import de.devlen.nichtecro.server.bungeecore.manager.ProxyServersManager;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ProxyReloadEvent;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by Tarik on 03.06.2016.
 */
public class ProxyReloadListener extends ListenerHelper {

    @EventHandler
    public void onProxyReload(ProxyReloadEvent e) {
        ProxyServersManager.cache();
        PermissionManager.cache();
        for (ProxiedPlayer proxiedPlayer : BungeeCore.getInstance().getProxy().getPlayers()) {
            PermissionManager.calculatePermissions(proxiedPlayer);
        }
    }

}
