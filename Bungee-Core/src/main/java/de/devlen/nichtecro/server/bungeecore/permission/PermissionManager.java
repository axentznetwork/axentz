package de.devlen.nichtecro.server.bungeecore.permission;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;
import de.devlen.nichtecro.server.bungeecore.data.ExtendedList;
import de.devlen.nichtecro.server.bungeecore.database.MongoDBManager;
import de.devlen.nichtecro.server.bungeecore.manager.PlayerSettingsManager;
import de.devlen.nichtecro.server.bungeecore.manager.Rank;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tarik on 21.05.2016.
 */
public class PermissionManager extends MongoDBManager {

    private static String collectionName = "bukkitcore_permissions";

    private static ArrayList<Permission> cache = new ArrayList<>();

    public static void cache() {
        cache.clear();
        FindIterable<Document> iterable = getCursor(collectionName, null);
        MongoCursor<Document> iterator = iterable.iterator();
        while (iterator.hasNext()) {
            Document document = iterator.next();
            String node = document.getString("node");
            List<String> rankNames = (List<String>) document.get("ranks");
            List<String> serverNames = (List<String>) document.get("servers");
            List<Rank> ranks = new ArrayList<>();
            for (String rankName : rankNames) {
                try {
                    ranks.add(Rank.valueOf(rankName));
                } catch (Exception ex) {
                }
            }
            Permission permission = new Permission(node, ranks, serverNames);
            cache.add(permission);
        }
    }

    public static void calculatePermissions(ProxiedPlayer proxiedPlayer) {
        invalidatePermissions(proxiedPlayer);
        for (Permission permission : cache) {
            for (Rank rank : PlayerSettingsManager.getRanks(proxiedPlayer)) {
                System.out.println(proxiedPlayer.getName() + " has the rank " + rank.name());
                if (permission.getRanks().contains(rank)) {
                    if (permission.getServers().size() < 1 || permission.getServers().contains("bungeecord")) {
                        if (permission.getNode().startsWith("!")) {
                            proxiedPlayer.setPermission(permission.getNode().substring(1), false);
                        } else {
                            proxiedPlayer.setPermission(permission.getNode(), true);
                        }
                    }
                }
            }
        }
    }

    public static void invalidatePermissions(ProxiedPlayer player) {
        ExtendedList<String> tempPermissions = new ExtendedList<>(player.getPermissions().toArray(new String[0]));
        for (String perm : tempPermissions) {
            player.setPermission(perm, false);
        }
    }


}
