package de.devlen.nichtecro.server.bungeecore.commands;

import de.devlen.nichtecro.server.bungeecore.comfy.CommandListener;
import de.devlen.nichtecro.server.bungeecore.comfy.annotation.Arg;
import de.devlen.nichtecro.server.bungeecore.comfy.annotation.CommandHandler;
import de.devlen.nichtecro.server.bungeecore.comfy.bungeecord.BungeeCommandContext;
import de.devlen.nichtecro.server.bungeecore.manager.ProxySettingsManager;
import de.devlen.nichtecro.server.bungeecore.message.Chat;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;

/**
 * Created by Tarik on 15.05.2016.
 */
public class CommandMotd implements CommandListener {

    @CommandHandler("command.motd.message")
    public void onCommandMotdMessage(BungeeCommandContext c, @Arg("message") String motd) {
        CommandSender commandSender = c.getSender().getSender();
        motd = motd.replace("%br", "\n");
        motd = ChatColor.translateAlternateColorCodes('&', motd);
        motd = Chat.replaceFormatVariables(motd);
        ProxySettingsManager.setProperty("motd", motd);
        Chat.send(commandSender, "MOTD", "Du hast die MOTD ge�ndert! Die �nderung wird in %usp�testens 30 Sekunden aktiv.");
    }

}
