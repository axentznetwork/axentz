package de.devlen.nichtecro.server.bungeecore.manager;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;
import de.devlen.nichtecro.server.bungeecore.BungeeCore;
import de.devlen.nichtecro.server.bungeecore.database.MongoDBManager;
import net.md_5.bungee.api.config.ServerInfo;
import org.bson.Document;

import java.net.InetSocketAddress;
import java.util.ArrayList;

import static com.mongodb.client.model.Filters.eq;

/**
 * Created by Tarik on 06.06.2016.
 */
public class ProxyServersManager extends MongoDBManager {

    private static String collectionName = "bungeecore_servers";

    private static ArrayList<String> cache = new ArrayList<>();

    public static void cache() {
        cache.clear();
        FindIterable<Document> iterable = getCursor(collectionName, null);
        MongoCursor<Document> iterator = iterable.iterator();
        while (iterator.hasNext()) {
            Document document = iterator.next();
            String serverName = document.getString("server_name");
            String host = document.getString("host");
            boolean restricted = document.getBoolean("restricted");
            String defaultMotd = document.getString("default_motd");
            int port = document.getInteger("port");

            cache.add(serverName);
            if (!BungeeCore.getInstance().getProxy().getServers().containsKey(serverName)) {
                ServerInfo serverInfo = BungeeCore.getInstance().getProxy().constructServerInfo(serverName, new InetSocketAddress(host, port), defaultMotd, restricted);
                BungeeCore.getInstance().getProxy().getServers().put(serverName, serverInfo);
            }
        }
    }

    public static int getMaxOnlineCount(String serverName) {
        Document document = getFirstDocument(collectionName, eq("server_name", serverName));
        return document.getInteger("max_online_count");
    }

    public static boolean isOnline(String serverName) {
        Document document = getFirstDocument(collectionName, eq("server_name", serverName));
        return document.getBoolean("online");
    }

    public static String getServerType(String serverName) {
        Document document = getFirstDocument(collectionName, eq("server_name", serverName));
        return document.getString("server_type");
    }

    public static Boolean isAutoRestart(String serverName) {
        Document document = getFirstDocument(collectionName, eq("server_name", serverName));
        return document.getBoolean("autorestart");
    }

    public static Boolean isMaintenance(String serverName) {
        Document document = getFirstDocument(collectionName, eq("server_name", serverName));
        return document.getBoolean("maintenance");
    }

    public static int getCurrentOnlineCount(String serverName) {
        Document document = getFirstDocument(collectionName, eq("server_name", serverName));
        return document.getInteger("current_online_count");
    }
}
