package de.devlen.nichtecro.server.bungeecore.manager;

import de.devlen.nichtecro.server.bungeecore.data.ExtendedList;
import de.devlen.nichtecro.server.bungeecore.database.DatabaseManager;
import de.devlen.nichtecro.server.bungeecore.database.types.ColumnDescriptor;
import de.devlen.nichtecro.server.bungeecore.database.types.SpecialField;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Tarik on 21.03.2016.
 */
public class Manager extends DatabaseManager {

    private static String tablePrefix = "bungeecore_";

    protected static void createTable(String tableName, ColumnDescriptor... columnDescriptors) {
        database().openConnection();
        String query = "CREATE TABLE IF NOT EXISTS " + tableName + " (";
        ExtendedList<ColumnDescriptor> columnDescriptorExtendedList = new ExtendedList<>(columnDescriptors);
        Iterator<ColumnDescriptor> columnDescriptorIterator = columnDescriptorExtendedList.iterator();
        while (columnDescriptorIterator.hasNext()) {
            ColumnDescriptor columnDescriptor = columnDescriptorIterator.next();
            query += columnDescriptor.getColumnName();
            query += " " + columnDescriptor.getDataType().getDatabaseType();
            if (columnDescriptor.getLength() != null) {
                query += "(" + columnDescriptor.getLength() + ")";
            }
            for (SpecialField specialField : columnDescriptor.getSpecialFields()) {
                query += " " + specialField.getFieldName();
            }
            if (columnDescriptorIterator.hasNext()) {
                query += ", ";
            }
        }
        query += ")";
        database().updateSQL(query);
    }

    public static ResultSet insertData(String tableName, Map<String, Object> data) {
        database().openConnection();
        String query = "INSERT INTO " + tableName + " (";
        Iterator<String> columnNameIterator = data.keySet().iterator();
        while (columnNameIterator.hasNext()) {
            String columnName = columnNameIterator.next();
            query += columnName;
            if (columnNameIterator.hasNext()) {
                query += ", ";
            }
        }
        query += ") VALUES (";
        Iterator<Object> valuesIterator = data.values().iterator();
        while (valuesIterator.hasNext()) {
            Object value = valuesIterator.next();
            if (value instanceof String && !((String) value).endsWith("()") || value instanceof UUID) {
                query += "'";
            }
            query += value;
            if (value instanceof String && !((String) value).endsWith("()") || value instanceof UUID) {
                query += "'";
            }
            if (valuesIterator.hasNext()) {
                query += ", ";
            }
        }
        query += ")";
        return database().updateSQL(query);
    }

    public static void updateData(String tableName, String setQueryPart, String whereQueryPart) {
        database().openConnection();
        String query = "UPDATE " + tableName + " SET " + setQueryPart + " WHERE " + whereQueryPart;
        database().updateSQL(query);
    }

    public static void deleteData(String tableName, String whereQueryPart) {
        database().openConnection();
        String query = "DELETE FROM " + tableName + " WHERE " + whereQueryPart;
        database().updateSQL(query);
    }

    public static ResultSet selectData(String tableName, String fieldsQueryPart, String whereQueryPart) {
        database().openConnection();
        String query = "SELECT " + fieldsQueryPart + " FROM " + tableName + (whereQueryPart.length() > 0 ? " WHERE " + whereQueryPart : "");
        ResultSet resultSet = database().querySQL(query);
        return resultSet;
    }

    public static String getTablePrefix() {
        return tablePrefix;
    }

}
