package de.devlen.nichtecro.server.bungeecore.manager;

import com.mongodb.BasicDBObject;
import de.devlen.nichtecro.server.bungeecore.database.MongoDBManager;
import org.bson.Document;

import static com.mongodb.client.model.Filters.eq;

/**
 * Created by Tarik on 15.05.2016.
 */
public class ProxySettingsManager extends MongoDBManager {

    private static String collectionName = getCollectionPrefix() + "settings";


    public static boolean isPropertyExisting(String propertyName) {
        if (getCursor(collectionName, new BasicDBObject("property_name", propertyName)).iterator().hasNext()) {
            return true;
        }
        return false;
    }

    public static int getPropertyInt(String propertyName) {
        if (isPropertyExisting(propertyName)) {
            Document document = getFirstDocument(collectionName, eq("property_name", propertyName));
            Integer propertyValue = document.getInteger("property_value");
            return propertyValue;
        }
        return 0;
    }

    public static String getPropertyString(String propertyName) {
        if (isPropertyExisting(propertyName)) {
            Document document = getFirstDocument(collectionName, eq("property_name", propertyName));
            String propertyValue = document.getString("property_value");
            return propertyValue;
        }
        return "";
    }

    public static void setProperty(String propertyName, String propertyValue) {
        update(collectionName, eq("property_name", propertyName), new BasicDBObject("$set", new BasicDBObject("property_value", propertyValue)));
    }

}
