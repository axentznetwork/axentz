package de.devlen.nichtecro.server.bungeecore.commands;

import de.devlen.nichtecro.server.bungeecore.comfy.CommandListener;
import de.devlen.nichtecro.server.bungeecore.comfy.annotation.Arg;
import de.devlen.nichtecro.server.bungeecore.comfy.annotation.CommandHandler;
import de.devlen.nichtecro.server.bungeecore.comfy.bungeecord.BungeeCommandContext;
import de.devlen.nichtecro.server.bungeecore.manager.PlayerSettingsManager;
import de.devlen.nichtecro.server.bungeecore.message.Chat;
import de.devlen.nichtecro.server.bungeecore.web.JsonWebCall;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * Created by Tarik on 19.06.2016.
 */
public class CommandWap implements CommandListener {

    @CommandHandler("command.wap.register.password")
    public void onCommandWapRegisterPassword(BungeeCommandContext c, @Arg("password") String password, @Arg("repeatedPassword") String repeatedPassword) {
        if (c.getSender().isPlayer()) {
            ProxiedPlayer proxiedPlayer = (ProxiedPlayer) c.getSender().getSender();
            if (!PlayerSettingsManager.isRegistered(proxiedPlayer)) {
                if (password.equals(repeatedPassword)) {
                    if (password.matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{8,}")) {
                        String digested = JsonWebCall.callWebMessageDigest(password);
                        PlayerSettingsManager.setPasswordHash(proxiedPlayer, digested);
                        Chat.sendTl(proxiedPlayer, "bungee.wap.tag", "bungee.wap.register.password.registered");
                    } else {
                        Chat.sendTl(proxiedPlayer, "bungee.wap.tag", "bungee.wap.register.password.specifications"); //8 Characters, One uppercase, One lowercase, One special
                    }
                }
                else Chat.sendTl(proxiedPlayer, "bungee.wap.tag", "bungee.wap.register.password.repeating");
            } else Chat.sendTl(proxiedPlayer, "bungee.wap.tag", "bungee.wap.register.already");
        } else {
            c.getSender().warning("This command is only available to players");
            c.getSender().warning("For further instructions please get online as player.");
        }
    }

}
