package de.devlen.nichtecro.server.bungeecore.web;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 * Created by Tarik on 19.06.2016.
 */
public class JsonWebCall {

    public static String callWebMessageDigest(String password) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL("http://192.227.230.67/hash/" + password).openConnection();
            writePost(httpURLConnection, "mode=hash-web&cleartext=" + password);

            JsonParser jsonParser = new JsonParser();
            JsonElement jsonElement = jsonParser.parse(new InputStreamReader(httpURLConnection.getInputStream()));
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            if (jsonObject.get("error") != null) {
                new JsonWebCallException("" + jsonObject.get("error").getAsString());
            } else {
                String digested = jsonObject.get("digested").getAsString();
                return digested;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void writePost(HttpURLConnection httpURLConnection, String parameters) {
        try {
            byte[] postData = parameters.getBytes(StandardCharsets.UTF_8);
            int postDataLength = postData.length;

            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setInstanceFollowRedirects(false);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpURLConnection.setRequestProperty("charset", "utf-8");
            httpURLConnection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            httpURLConnection.setUseCaches(false);
            DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
            dataOutputStream.write(postData);
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
