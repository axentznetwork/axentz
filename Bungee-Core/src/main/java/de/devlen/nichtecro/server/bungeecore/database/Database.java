package de.devlen.nichtecro.server.bungeecore.database;

import java.sql.Connection;
import java.sql.ResultSet;

public abstract class Database {
    public abstract Connection openConnection();

    public abstract boolean checkConnection();

    public abstract Connection getConnection();

    public abstract boolean closeConnection();

    public abstract ResultSet querySQL(String paramString);

    public abstract ResultSet updateSQL(String paramString);
}
