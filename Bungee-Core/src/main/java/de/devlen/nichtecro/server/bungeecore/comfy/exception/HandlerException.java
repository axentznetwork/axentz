package de.devlen.nichtecro.server.bungeecore.comfy.exception;

public class HandlerException extends RuntimeException {
    public HandlerException(String msg) {
        super(msg);
    }
}
