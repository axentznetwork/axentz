package de.devlen.nichtecro.server.bungeecore.time;

import java.util.Calendar;

/**
 * Created by Tarik on 01.06.2016.
 */
public class TimeConversion {

    public static int ticksFromRealtime() {
        Calendar calendar = Calendar.getInstance();
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);
        float commaHours = hours;
        commaHours += (minutes / 60);
        int ticks = (int) (commaHours * 1000 - 6000);
        if (ticks < 0) {
            ticks += 24000;
        }
        return ticks;
    }

}
