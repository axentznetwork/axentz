package de.devlen.nichtecro.server.bungeecore.comfy.bungeecord;

import de.devlen.nichtecro.server.bungeecore.comfy.Arguments;
import de.devlen.nichtecro.server.bungeecore.comfy.CommandContext;
import de.devlen.nichtecro.server.bungeecore.comfy.tree.CommandPath;

/**
 * Created by Tarik on 08.05.2016.
 */
public class BungeeCommandContext extends CommandContext<BungeeCommandSender> {
    protected BungeeCommandContext(BungeeCommandSender sender, CommandPath path, Arguments arguments) {
        super(sender, path, arguments);
    }
}
