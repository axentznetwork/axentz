package de.devlen.nichtecro.server.bungeecore.manager;

import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.List;

import static de.devlen.nichtecro.server.bungeecore.language.Language.tl;


/**
 * Created by Tarik on 19.05.2016.
 */
public enum Rank {

    ADMINISTRATOR("�4�l", "bukkit.core.ranks.tag.administrator", "001"),
    MODERATOR("�6�l", "bukkit.core.ranks.tag.moderator", "002"),
    DEVELOPER("�3�l", "bukkit.core.ranks.tag.developer", "003"),
    TRANSLATOR("�2�l", "bukkit.core.ranks.tag.translator", "004"),
    MAPPER("�a�l", "bukkit.core.ranks.tag.builder", "005"),
    SUPPORTER("�e�l", "bukkit.core.ranks.tag.support", "006"),
    YOUTUBER("�c�l", "bukkit.core.ranks.tag.youtube", "007"),
    FRIEND("�b�l", "bukkit.core.ranks.tag.friend", "008"),
    VETERAN("�1�l", "bukkit.core.ranks.tag.veteran", "009"),
    WICKED("�9�l", "bukkit.core.ranks.tag.wicked", "010"),
    ANCIENT("�5�l", "bukkit.core.ranks.tag.ancient", "011"),
    GUEST("�7", "bukkit.core.ranks.tag.guest", "012");

    private String formatParamters = "";
    private String tagPrefixNode = "";
    private String teamName = "";

    Rank(String formatParamters, String tagPrefixNode, String teamName) {
        this.formatParamters = formatParamters;
        this.tagPrefixNode = tagPrefixNode;
        this.teamName = teamName;
    }

    public String getFormatParamters() {
        return formatParamters;
    }

    public String getChatPrefixTranslatable(ProxiedPlayer player) {
        String translated = tl(tagPrefixNode, player);
        return getFormatParamters() + translated + (translated.length() > 0 ? " " : "");
    }

    public String getTagPrefixNode() {
        return tagPrefixNode;
    }

    public String getTeamName() {
        return teamName;
    }

    public static Rank getHighestRank(List<Rank> ranks) {
        Rank rank = Rank.GUEST;
        for (Rank checkingRank : ranks) {
            if (checkingRank.ordinal() < rank.ordinal()) {
                rank = checkingRank;
            }
        }
        return rank;
    }


    @Override
    public String toString() {
        return "Rank{" +
                "formatParamters='" + formatParamters + '\'' +
                ", tagPrefixNode='" + tagPrefixNode + '\'' +
                ", teamName='" + teamName + '\'' +
                '}';
    }
}
