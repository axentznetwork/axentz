package de.devlen.nichtecro.server.bungeecore.time;

import de.devlen.nichtecro.server.bungeecore.BungeeCore;

import java.util.concurrent.TimeUnit;

/**
 * Created by Tarik on 01.06.2016.
 */
public class Delay {

    public static void make(long seconds, Runnable runnable) {
        BungeeCore.getInstance().getProxy().getScheduler().schedule(BungeeCore.getInstance(), runnable, seconds, TimeUnit.SECONDS);
    }

}
