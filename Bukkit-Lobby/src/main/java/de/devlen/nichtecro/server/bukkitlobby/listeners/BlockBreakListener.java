package de.devlen.nichtecro.server.bukkitlobby.listeners;

import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;

/**
 * Created by Tarik on 16.05.2016.
 */
public class BlockBreakListener extends ListenerHelper {

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        Player player = e.getPlayer();
        if (!player.hasPermission("bukkit.lobby.listeners.blockbreak")) {
            e.setCancelled(true);
        }
    }

}
