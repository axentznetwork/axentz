package de.devlen.nichtecro.server.bukkitlobby;

import de.devlen.nichtecro.server.bukkitcore.comfy.bukkit.BukkitCommandManager;
import de.devlen.nichtecro.server.bukkitcore.comfy.tree.Literal;
import de.devlen.nichtecro.server.bukkitlobby.commands.CommandDebugmode;
import de.devlen.nichtecro.server.bukkitlobby.gadgets.BootsGadgets;
import de.devlen.nichtecro.server.bukkitlobby.gadgets.InteractiveGadgets;
import de.devlen.nichtecro.server.bukkitlobby.inventory.InventoryRegistry;
import de.devlen.nichtecro.server.bukkitlobby.listeners.*;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Tarik on 15.05.2016.
 */
public class BukkitLobby extends JavaPlugin {

    private static Plugin instance;

    @Override
    public void onEnable() {
        instance = this;
        registerListeners();
        registerCommands();
        BootsGadgets.runUpdateTask();
        InteractiveGadgets.runUpdateTask();
    }

    public static Plugin getInstance() {
        return instance;
    }

    public static void registerCommands() {
        BukkitCommandManager bukkitCommandManager = new BukkitCommandManager(getInstance());
        bukkitCommandManager.addCommand(new Literal("debugmode", "dm")
                .executes("command.debugmode")
                .permission("bukkit.lobby.debugmode")
                .description("Only available for admins..."));
        bukkitCommandManager.addListener(new CommandDebugmode());
        bukkitCommandManager.registerCommands();
    }

    public static void registerListeners() {
        new BootsGadgets().register();
        new BlockBreakListener().register();
        new BlockBurnListener().register();
        new BlockDamageListener().register();
        new BlockPhysicsListener().register();
        new BlockPlaceListener().register();
        new CauldronLevelChangeListener().register();
        new ClientSettingsChangeListener().register();
        new DisguiseRankChangedListener().register();
        new EntityDamageListener().register();
        new EntityExplodeListener().register();
        new EntityExplodeListener().register();
        new FoodLevelChangeListener().register();
        new PlayerAchievementAwardedListener().register();
        new PlayerArmorStandManipulateListener().register();
        new PlayerDeathListener().register();
        new PlayerDropItemListener().register();
//        new PlayerInteractListener().register();
        new PlayerItemDamageListener().register();
        new PlayerJoinListener().register();
        new PlayerKickListener().register();
        new PlayerPickupItemListener().register();
        new PlayerSwapHandItemsListener().register();
        new TranslatedPlayerChatListener().register();
        for (InventoryRegistry inventoryRegistry : InventoryRegistry.values()) {
            inventoryRegistry.getInventoryFactory().register();
        }
    }
}
