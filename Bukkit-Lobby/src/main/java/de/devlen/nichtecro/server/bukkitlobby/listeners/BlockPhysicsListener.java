package de.devlen.nichtecro.server.bukkitlobby.listeners;

import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockPhysicsEvent;

/**
 * Created by Tarik on 18.05.2016.
 */
public class BlockPhysicsListener extends ListenerHelper {

    @EventHandler
    public void onBlockPhysics(BlockPhysicsEvent e){
        e.setCancelled(false);
    }

}
