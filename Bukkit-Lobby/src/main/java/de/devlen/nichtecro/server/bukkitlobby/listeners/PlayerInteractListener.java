package de.devlen.nichtecro.server.bukkitlobby.listeners;

import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import de.devlen.nichtecro.server.bukkitcore.packetwrapper.WrapperPlayServerSetCooldown;
import de.devlen.nichtecro.server.bukkitcore.time.Cooldown;
import de.devlen.nichtecro.server.bukkitcore.time.Delay;
import de.devlen.nichtecro.server.bukkitcore.validation.Validate;
import de.devlen.nichtecro.server.bukkitlobby.gadgets.InteractiveGadgets;
import de.devlen.nichtecro.server.bukkitlobby.inventory.InventoryRegistry;
import de.devlen.nichtecro.server.bukkitlobby.inventory.StackManager;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.meta.FireworkMeta;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Tarik on 29.04.2016.
 */
public class PlayerInteractListener extends ListenerHelper {

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        switch (e.getAction()) {
            case LEFT_CLICK_AIR:
            case LEFT_CLICK_BLOCK:
            case RIGHT_CLICK_AIR:
            case RIGHT_CLICK_BLOCK:
                if (e.getItem() != null) {
                    final Player player = e.getPlayer();
                    if (e.getItem().equals(StackManager.Hotbar.getGadgetsStack())) {
                        if (player.hasPermission("bukkit.lobby.inventories.gadgets.open")) {
                            InventoryRegistry.GADGETS.getInventoryFactory().open(e.getPlayer());
                        } else {
                            Chat.send(player, "Gadgets", "Du kannst Gadgets nicht benutzen.");
                        }
                        e.setCancelled(true);
                    } else if (e.getItem().getType().equals(StackManager.Gadgets.Armed.getC4DynamiteInteractiveStack().getType())) {
                        e.setCancelled(false);
                    } else if (e.getItem().getType().equals(StackManager.Gadgets.Armed.getVulcanoBlockInteractiveStack().getType())) {
                        e.setCancelled(false);
                    } else if (e.getItem().equals(StackManager.Gadgets.Armed.getC4IgniterInteractiveStack())) {
                        if (BlockPlaceListener.placedTntBlocks.containsKey(player.getName())) {
                            ArrayList<Location> locationArrayList = BlockPlaceListener.placedTntBlocks.get(player.getName());
                            for (Location location : locationArrayList) {
                                Block block = location.getBlock();
                                if (block.getType() == Material.TNT) {
                                    block.setType(Material.AIR);
                                    TNTPrimed tntPrimed = (TNTPrimed) player.getWorld().spawnEntity(block.getLocation(), EntityType.PRIMED_TNT);
                                    tntPrimed.setFuseTicks(1);
                                    tntPrimed.setYield(12.0F);
                                }
                            }
                            InteractiveGadgets.cleanGadget(player);
                            e.setCancelled(true);
                        } else {
                            e.setCancelled(true);
                        }
                    } else if (e.getItem().equals(StackManager.Gadgets.Armed.getVulcanoIgniterInteractiveStack())) {
                        if (BlockPlaceListener.vulcanoFirework.containsKey(player.getName())) {
                            if (!Cooldown.hasCharge(player, "vulcano")) {
                                Cooldown.charge(player, "vulcano", 30);
                                final Location absoluteLocation = BlockPlaceListener.vulcanoFirework.get(player.getName());
                                Location hole = absoluteLocation;
                                Location hole1 = hole.add(0.1875, 0, 0.1875);
                                Location hole2 = hole.add(0.1875, 0, 0);
                                Location hole3 = hole.add(0.1875, 0, 0);
                                Location hole4 = hole.add(0, 0, 0.1875);
                                Location hole5 = hole.add(-0.1875, 0, 0);
                                Location hole6 = hole.add(-0.1875, 0, 0);
                                Location hole7 = hole.add(0, 0, 0.1875);
                                Location hole8 = hole.add(0.1875, 0, 0);
                                Location hole9 = hole.add(0.1875, 0, 0);

                                final List<Location> locationList = new ExtendedList<>(hole1, hole2, hole3, hole4, hole5, hole6, hole7, hole8, hole9);

                                final FireworkEffect.Builder builder = FireworkEffect.builder();
                                builder.withColor(Color.AQUA, Color.BLUE, Color.NAVY);
                                builder.withFade(Color.AQUA, Color.BLUE, Color.NAVY);
                                builder.withFlicker();
                                builder.withTrail();
                                builder.with(FireworkEffect.Type.BALL_LARGE);
                                final FireworkEffect effect = builder.build();

                                final FireworkEffect.Builder builder1 = FireworkEffect.builder();
                                builder1.withColor(Color.ORANGE, Color.RED, Color.YELLOW);
                                builder1.withFade(Color.ORANGE, Color.RED, Color.YELLOW);
                                builder1.withFlicker();
                                builder1.withTrail();
                                builder1.with(FireworkEffect.Type.STAR);
                                final FireworkEffect effect1 = builder1.build();

                                final FireworkEffect.Builder builder2 = FireworkEffect.builder();
                                builder2.withColor(Color.GREEN, Color.OLIVE, Color.LIME);
                                builder2.withFade(Color.GREEN, Color.OLIVE, Color.LIME);
                                builder2.withFlicker();
                                builder2.withTrail();
                                builder2.with(FireworkEffect.Type.BURST);
                                final FireworkEffect effect2 = builder2.build();

                                final FireworkEffect.Builder builder3 = FireworkEffect.builder();
                                builder3.withColor(Color.WHITE, Color.SILVER, Color.BLACK);
                                builder3.withFade(Color.WHITE, Color.SILVER, Color.BLACK);
                                builder3.withFlicker();
                                builder3.withTrail();
                                builder3.with(FireworkEffect.Type.BALL);
                                final FireworkEffect effect3 = builder3.build();

                                final int delayBetween = 4;

                                for (int i = 0; i < locationList.size(); i++) {
                                    final Location location = locationList.get(i);
                                    final int finalI = i;
                                    Delay.make(delayBetween * i, new Runnable() {
                                        @Override
                                        public void run() {
                                            Firework firework = (Firework) location.getWorld().spawnEntity(locationList.get(finalI), EntityType.FIREWORK);
                                            FireworkMeta fireworkMeta = firework.getFireworkMeta();
                                            fireworkMeta.addEffect(effect);
                                            fireworkMeta.setPower(3);
                                            firework.setFireworkMeta(fireworkMeta);
                                        }
                                    });
                                }

                                Delay.make(delayBetween * 9 * 1 + 60 * 1, new Runnable() {
                                    @Override
                                    public void run() {
                                        for (int i = 0; i < locationList.size(); i++) {
                                            final Location location = locationList.get(i);
                                            final int finalI = i;
                                            Delay.make(delayBetween * i, new Runnable() {
                                                @Override
                                                public void run() {
                                                    Firework firework = (Firework) location.getWorld().spawnEntity(locationList.get(finalI), EntityType.FIREWORK);
                                                    FireworkMeta fireworkMeta = firework.getFireworkMeta();
                                                    fireworkMeta.addEffect(effect1);
                                                    fireworkMeta.setPower(3);
                                                    firework.setFireworkMeta(fireworkMeta);
                                                }
                                            });
                                        }
                                    }
                                });


                                Delay.make(delayBetween * 9 * 2 + 60 * 2, new Runnable() {
                                    @Override
                                    public void run() {
                                        for (int i = 0; i < locationList.size(); i++) {
                                            final Location location = locationList.get(i);
                                            final int finalI = i;
                                            Delay.make(delayBetween * i, new Runnable() {
                                                @Override
                                                public void run() {
                                                    Firework firework = (Firework) location.getWorld().spawnEntity(locationList.get(finalI), EntityType.FIREWORK);
                                                    FireworkMeta fireworkMeta = firework.getFireworkMeta();
                                                    fireworkMeta.addEffect(effect2);
                                                    fireworkMeta.setPower(3);
                                                    firework.setFireworkMeta(fireworkMeta);
                                                }
                                            });
                                        }
                                    }
                                });


                                Delay.make(delayBetween * 9 * 3 + 60 * 3, new Runnable() {
                                    @Override
                                    public void run() {
                                        for (int i = 0; i < locationList.size(); i++) {
                                            final Location location = locationList.get(i);
                                            final int finalI = i;
                                            Delay.make(delayBetween * i, new Runnable() {
                                                @Override
                                                public void run() {
                                                    Firework firework = (Firework) location.getWorld().spawnEntity(locationList.get(finalI), EntityType.FIREWORK);
                                                    FireworkMeta fireworkMeta = firework.getFireworkMeta();
                                                    fireworkMeta.addEffect(effect3);
                                                    fireworkMeta.setPower(3);
                                                    firework.setFireworkMeta(fireworkMeta);
                                                }
                                            });
                                        }
                                    }
                                });

                                Delay.make(delayBetween * 9 * 4 + 60 * 3, new Runnable() {
                                    @Override
                                    public void run() {
                                        absoluteLocation.getBlock().setType(Material.AIR);
                                    }
                                });
                                InteractiveGadgets.cleanGadget(player);
                            } else {
                                Chat.sendTl(player, "gadgets.vulcano.tag", "gadgets.vulcano.cooldown");
                            }
                            e.setCancelled(true);
                        }
                    } else if (Validate.isSimilarItem(e.getItem(), StackManager.Gadgets.Armed.getTeleportStickInteractiveStack())) {
                        Block block = player.getTargetBlock((Set) null, 100);
                        Location blockLocation = block.getLocation();
                        blockLocation.setY(block.getY() + 1);
                        if (blockLocation.getBlock().getType() == Material.AIR) {
                            blockLocation.setY(block.getY() + 2);
                            if (blockLocation.getBlock().getType() == Material.AIR) {
                                blockLocation.setY(blockLocation.getY() - 1);
                                blockLocation.setPitch(player.getLocation().getPitch());
                                blockLocation.setYaw(player.getLocation().getYaw());
                                blockLocation.add(0.5, 0, 0.5);
                                if (!Cooldown.hasCharge(player, "tpstick")) {
                                    player.teleport(blockLocation, PlayerTeleportEvent.TeleportCause.PLUGIN);
                                    player.playSound(blockLocation, Sound.ENTITY_ENDERMEN_TELEPORT, 1, 1);
                                    Chat.sendTl(player, "gadgets.teleportstick.prefix", "gadgets.teleportstick.teleported");
                                    WrapperPlayServerSetCooldown wrapperPlayServerSetCooldown = new WrapperPlayServerSetCooldown();
                                    wrapperPlayServerSetCooldown.setItem(Material.STICK);
                                    wrapperPlayServerSetCooldown.setTicks(10 * 20);
                                    wrapperPlayServerSetCooldown.sendPacket(player);
                                    Cooldown.charge(player, "tpstick", 10);
                                } else {
                                    Chat.sendTl(player, "gadgets.teleportstick.prefix", "gadgets.teleportstick.cooldown.remaining", new DecimalFormat("#.##").format(Cooldown.getRemainingTime(player, "tpstick")));
                                }
                            }
                        }
                    } else {
                        e.setCancelled(true);
                    }
                }
                break;
        }
    }

}
