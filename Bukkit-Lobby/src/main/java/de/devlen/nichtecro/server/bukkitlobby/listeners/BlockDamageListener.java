package de.devlen.nichtecro.server.bukkitlobby.listeners;

import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockDamageEvent;

/**
 * Created by Tarik on 16.05.2016.
 */
public class BlockDamageListener extends ListenerHelper {

    @EventHandler
    public void onBlockDamage(BlockDamageEvent e) {
        e.setCancelled(true);
    }

}
