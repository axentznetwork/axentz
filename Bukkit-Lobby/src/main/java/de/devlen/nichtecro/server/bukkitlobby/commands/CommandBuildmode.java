package de.devlen.nichtecro.server.bukkitlobby.commands;

import de.devlen.nichtecro.server.bukkitcore.comfy.CommandListener;
import de.devlen.nichtecro.server.bukkitcore.comfy.annotation.CommandHandler;
import de.devlen.nichtecro.server.bukkitcore.comfy.bukkit.BukkitCommandContext;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Tarik on 19.05.2016.
 */
public class CommandBuildmode implements CommandListener {

    private ArrayList<UUID> buildModed = new ArrayList<>();

    @CommandHandler("command.buildmode")
    public static void onCommandBuildmode(BukkitCommandContext c) {
        if (c.getSender().isPlayer()) {
            Player player = (Player) c.getSender().getSender();
            Chat.send(player, "Buildmode", "");
        }
    }

}
