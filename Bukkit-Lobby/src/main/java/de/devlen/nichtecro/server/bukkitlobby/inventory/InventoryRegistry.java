package de.devlen.nichtecro.server.bukkitlobby.inventory;

import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import de.devlen.nichtecro.server.bukkitlobby.gadgets.BootsGadgets;
import de.devlen.nichtecro.server.bukkitlobby.gadgets.HelmetGadgets;
import de.devlen.nichtecro.server.bukkitlobby.gadgets.InteractiveGadgets;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.ClickHandler;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * Created by Tarik on 04.04.2016.
 */
public enum InventoryRegistry {

    GADGETS(new InventoryFactory("Gadgets", 1,
            new InventoryButton().item(StackManager.Gadgets.getHelmetsStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    GADGETS_HELMETS.getInventoryFactory().open(player);
                }
            }).position(7, 1),
            new InventoryButton().item(StackManager.Gadgets.getBootsStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    GADGETS_BOOTS.getInventoryFactory().open(player);
                }
            }).position(3, 1),
            new InventoryButton().item(StackManager.Gadgets.getInteractiveStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    GADGETS_INTERACTIVE.getInventoryFactory().open(player);
                }
            }))),
    GADGETS_BOOTS(new InventoryFactory("Gadgets » Boots", 2,
            new InventoryButton().item(StackManager.Gadgets.Boots.getEnderBootsStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    BootsGadgets.armGadget(player, BootsGadgets.ShoeType.ENDER);
                    Chat.send(player, "Gadgets", "Du hast dir %lEnder-Stiefel %uangelegt.");
                    player.closeInventory();
                }
            }).position(1, 1),
            new InventoryButton().item(StackManager.Gadgets.Boots.getGhostBootsStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    BootsGadgets.armGadget(player, BootsGadgets.ShoeType.GHOST);
                    Chat.send(player, "Gadgets", "Du hast dir %lGeister-Schuhe %uangelegt.");
                    player.closeInventory();
                }
            }).position(2, 1),
            new InventoryButton().item(StackManager.Gadgets.Boots.getJetpackBootsStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    BootsGadgets.armGadget(player, BootsGadgets.ShoeType.JETPACK);
                    Chat.send(player, "Gadgets", "Du hast dir %lJetpack-Schuhe %uangelegt.");
                    player.closeInventory();
                }
            }).position(3, 1),
            new InventoryButton().item(StackManager.Gadgets.Boots.getLoveBootsStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    BootsGadgets.armGadget(player, BootsGadgets.ShoeType.LOVE);
                    Chat.send(player, "Gadgets", "Du hast dir %lLiebes-Schuhe %uangelegt.");
                    player.closeInventory();
                }
            }).position(4, 1),
            new InventoryButton().item(StackManager.Gadgets.Boots.getHappynessBootsStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    BootsGadgets.armGadget(player, BootsGadgets.ShoeType.HAPPYNESS);
                    Chat.send(player, "Gadgets", "Du hast dir %lFröhlichkeits-Schuhe %uangelegt.");
                    player.closeInventory();
                }
            }).position(5, 1),
            new InventoryButton().item(StackManager.Gadgets.Boots.getKnowledgeBootsStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    BootsGadgets.armGadget(player, BootsGadgets.ShoeType.KNOWLEDGE);
                    Chat.send(player, "Gadgets", "Du hast dir %lWissens-Schuhe %uangelegt.");
                    player.closeInventory();
                }
            }).position(6, 1),
            new InventoryButton().item(StackManager.Gadgets.Boots.getSevenMilesBootsStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    BootsGadgets.armGadget(player, BootsGadgets.ShoeType.SEVEN_MILES_BOOTS);
                    Chat.send(player, "Gadgets", "Du hast dir %lSieben-Meilen-Stiefel %uangelegt.");
                    player.closeInventory();
                }
            }).position(7, 1),
            new InventoryButton().item(StackManager.Gadgets.Boots.getFireBootsStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    BootsGadgets.armGadget(player, BootsGadgets.ShoeType.FIRE);
                    Chat.send(player, "Gadgets", "Du hast dir %lFeuer-Stiefel %uangelegt.");
                    player.closeInventory();
                }
            }).position(8, 1),
            new InventoryButton().item(StackManager.Gadgets.Boots.getRemoveBootsStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    BootsGadgets.cleanGadget(player);
                    Chat.send(player, "Gadgets", "Du hast deine Schuhe %lentfernt%u.");
                    player.closeInventory();
                }
            }).position(8, 2),
            new InventoryButton().item(StackManager.General.getBackToGadgetsStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    GADGETS.getInventoryFactory().open(player);
                }
            }).position(9, 2))),
    GADGETS_HELMETS(new InventoryFactory("Gadgets » Helme & Hutdeko", 2,
            new InventoryButton().item(StackManager.Gadgets.Helmets.getFeatherHelmetStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    HelmetGadgets.armGadget(player, HelmetGadgets.HelmetType.FEATHER);
                    Chat.send(player, "Gadgets", "Du hast dir die %lHut-Feder %uangelegt.");
                    player.closeInventory();
                }
            }).position(1, 1),
            new InventoryButton().item(StackManager.Gadgets.Helmets.getSpaceHelmetStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    HelmetGadgets.armGadget(player, HelmetGadgets.HelmetType.SPACE);
                    Chat.send(player, "Gadgets", "Du hast dir den %lAstronauten-Helm %uangelegt.");
                    player.closeInventory();
                }
            }).position(2, 1),
            new InventoryButton().item(StackManager.Gadgets.Helmets.getUnicornHelmetStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    HelmetGadgets.armGadget(player, HelmetGadgets.HelmetType.UNICORN);
                    Chat.send(player, "Gadgets", "Du hast dir den %lEinhorn-Aufsatz %uangelegt.");
                    player.closeInventory();
                }
            }).position(3, 1),
            new InventoryButton().item(StackManager.Gadgets.Helmets.getRemoveHelmetStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    HelmetGadgets.cleanGadget(player);
                    Chat.send(player, "Gadgets", "Du hast deinen Helm/Hutdeko %lentfernt%u.");
                    player.closeInventory();
                }
            }).position(8, 2),
            new InventoryButton().item(StackManager.General.getBackToGadgetsStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    GADGETS.getInventoryFactory().open(player);
                }
            }).position(9, 2))),
    GADGETS_INTERACTIVE(new InventoryFactory("Gadgets » Interaktives", 2,
            new InventoryButton().item(StackManager.Gadgets.Interactive.getC4InteractiveStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    InteractiveGadgets.armGadget(player, InteractiveGadgets.InteractiveType.C4);
                    Chat.send(player, "Gadgets", "Du hast dich mit %lC4-Sprengstoff %ubewaffnet.");
                    player.closeInventory();
                }
            }).position(1, 1),
            new InventoryButton().item(StackManager.Gadgets.Interactive.getTeleportStickInteractiveStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    InteractiveGadgets.armGadget(player, InteractiveGadgets.InteractiveType.TELEPORT_STICK);
                    Chat.send(player, "Gadgets", "Du hast dich mit dem %lTeleport-Stab %ubewaffnet.");
                    player.closeInventory();
                }
            }).position(2, 1),
            new InventoryButton().item(StackManager.Gadgets.Interactive.getVulcanoInteractiveStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    InteractiveGadgets.armGadget(player, InteractiveGadgets.InteractiveType.VULCANO);
                    Chat.send(player, "Gadgets", "Du hast dich mit dem %lVulcano-Feuerwerk %ubewaffnet.");
                    player.closeInventory();
                }
            }).position(3, 1),
            new InventoryButton().item(StackManager.Gadgets.Interactive.getRemoveInteraktiveStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    InteractiveGadgets.cleanGadget(player);
                    Chat.send(player, "Gadgets", "Du hast deine %lBewaffnung %uentfernt.");
                    player.closeInventory();
                }
            }).position(8, 2),
            new InventoryButton().item(StackManager.General.getBackToGadgetsStack()).click(new ClickHandler() {
                @Override
                public void handle(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    GADGETS.getInventoryFactory().open(player);
                }
            }).position(9, 2)));

    private InventoryFactory inventoryFactory = null;

    InventoryRegistry(InventoryFactory inventoryFactory) {
        this.inventoryFactory = inventoryFactory;
    }

    public InventoryFactory getInventoryFactory() {
        return inventoryFactory;
    }
}
