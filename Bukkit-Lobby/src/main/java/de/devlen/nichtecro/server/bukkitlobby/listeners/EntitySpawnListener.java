package de.devlen.nichtecro.server.bukkitlobby.listeners;

import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntitySpawnEvent;

/**
 * Created by Tarik on 01.06.2016.
 */
public class EntitySpawnListener extends ListenerHelper {

    @EventHandler
    public void onEntitySpawn(EntitySpawnEvent e) {
        e.setCancelled(true);
    }

}
