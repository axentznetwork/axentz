package de.devlen.nichtecro.server.bukkitlobby.listeners;

import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import de.devlen.nichtecro.server.bukkitcore.npc.NPCIronGolem;
import de.devlen.nichtecro.server.bukkitlobby.gadgets.GadgetManager;
import de.devlen.nichtecro.server.bukkitlobby.inventory.PlayerInventoryTemplates;
import de.devlen.nichtecro.server.bukkitlobby.scoreboard.ScoreboardTemplates;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by Tarik on 15.05.2016.
 */
public class PlayerJoinListener extends ListenerHelper {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        GadgetManager.cleanGadgets(player);
        PlayerInventoryTemplates.armHotbar(player);

        NPCIronGolem npcIronGolem = new NPCIronGolem(635, new Location(Bukkit.getWorld("world"), 322.5, 34, -519.5, 90, 0)); //head rotation, body rotation
        npcIronGolem.getEntityMetadata().setCustomName("�3Test �blol");
        npcIronGolem.getEntityMetadata().setCustomNameVisible(true);
        npcIronGolem.spawn(player);

        ScoreboardTemplates.setDefaultTemplate(player);

//        NPCArmorStand npcArmorStand = new NPCArmorStand(636, new Location(Bukkit.getWorld("world"), 317.5, 32.5, -519.5, 0, 0));
//        npcArmorStand.getEntityMetadata().setCustomNameVisible(true);
//        npcArmorStand.getEntityMetadata().setCustomName("Test");
//        npcArmorStand.getEntityMetadata().setFlag(0, (byte) 0x20);
//        npcArmorStand.spawn(player);
//
//        NPCItem npcItem = new NPCItem(696, player.getLocation());
//        npcItem.getEntityMetadata().setCustomNameVisible(true);
//        npcItem.getEntityMetadata().setCustomName(Chat.replaceFormatVariables(tl("bukkit.lobby.spawnpoint", player)));
//        npcItem.getEntityMetadata().setItem(new ItemStack(Material.ENDER_PEARL));
//        npcItem.spawn(player);
    }
}
