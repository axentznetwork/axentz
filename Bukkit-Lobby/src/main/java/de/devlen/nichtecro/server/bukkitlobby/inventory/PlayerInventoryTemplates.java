package de.devlen.nichtecro.server.bukkitlobby.inventory;

import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import de.devlen.nichtecro.server.bukkitcore.items.NBTItem;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.Button;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.ButtonFactory;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.ClickHandler;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.InteractHandler;
import de.devlen.nichtecro.server.bukkitlobby.menus.GadgetsMenu;
import de.devlen.nichtecro.server.bukkitlobby.menus.SettingsMenu;
import de.devlen.nichtecro.server.bukkitlobby.menus.TagMenu;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.List;

/**
 * Created by Tarik on 19.05.2016.
 */
public class PlayerInventoryTemplates {

    public static void clear(Player player) {
        PlayerInventory playerInventory = player.getInventory();
        playerInventory.clear();
        playerInventory.setHelmet(null);
        playerInventory.setChestplate(null);
        playerInventory.setLeggings(null);
        playerInventory.setBoots(null);
    }

    public static void armHotbar(Player player) {
        clear(player);
        PlayerInventory playerInventory = player.getInventory();
        List<Button> buttonList = new ExtendedList<>(
                new Button()
                        .id("bukkit.lobby.items.tagchanger")
                        .click(new ClickHandler() {
                            @Override
                            public void handle(InventoryClickEvent e) {
                                Player target = (Player) e.getWhoClicked();
                                TagMenu.openTag(target);
                            }
                        })
                        .interact(new InteractHandler() {
                            @Override
                            public void handle(PlayerInteractEvent e) {
                                Player target = e.getPlayer();
                                TagMenu.openTag(target);
                            }
                        })
                        .slot(0)
                        .factory(new ButtonFactory()
                                .nameNode("bukkit.lobby.items.tagchanger.name")
                                .type(Material.NAME_TAG))
                        .registerHandlers(),
                new Button()
                        .id("bukkit.lobby.items.gadgets")
                        .click(new ClickHandler() {
                            @Override
                            public void handle(InventoryClickEvent e) {
                                Player target = (Player) e.getWhoClicked();
                                GadgetsMenu.openGadget(target);
                            }
                        })
                        .interact(new InteractHandler() {
                            @Override
                            public void handle(PlayerInteractEvent e) {
                                Player target = e.getPlayer();
                                GadgetsMenu.openGadget(target);
                            }
                        })
                        .slot(1)
                        .factory(new ButtonFactory()
                                .nameNode("bukkit.lobby.items.gadgets.name")
                                .type(Material.CHEST))
                        .registerHandlers(),
                new Button()
                        .id("bukkit.lobby.items.settings")
                        .click(new ClickHandler() {
                            @Override
                            public void handle(InventoryClickEvent e) {
                                Player target = (Player) e.getWhoClicked();
                                SettingsMenu.openSettings(target);
                            }
                        })
                        .interact(new InteractHandler() {
                            @Override
                            public void handle(PlayerInteractEvent e) {
                                Player target = e.getPlayer();
                                SettingsMenu.openSettings(target);
                            }
                        })
                        .slot(2)
                        .factory(new ButtonFactory()
                                .nameNode("bukkit.lobby.items.settings.name")
                                .type(Material.DIODE))
                        .registerHandlers()
        );
        for (Button button : buttonList) {
            ItemStack itemStack = button.getButtonFactory().build(player);
            NBTItem nbtItem = new NBTItem(itemStack);
            nbtItem.setString("server_name_id", button.getNameId());
            playerInventory.setItem(button.getSlot(), nbtItem.getItem());
        }
    }

}
