package de.devlen.nichtecro.server.bukkitlobby.listeners;

import com.google.common.collect.Maps;
import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import de.devlen.nichtecro.server.bukkitlobby.gadgets.InteractiveGadgets;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Tarik on 16.05.2016.
 */
public class BlockPlaceListener extends ListenerHelper {

    public static HashMap<String, ArrayList<Location>> placedTntBlocks = Maps.newHashMap();
    public static HashMap<String, Location> vulcanoFirework = Maps.newHashMap();

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        Player player = e.getPlayer();
        if (e.getBlockPlaced().getType().equals(Material.TNT) && InteractiveGadgets.hasGadgetArmed(player)) {
            if (!e.getBlockReplacedState().getType().equals(Material.AIR)) {
                e.setBuild(false);
                e.setCancelled(true);
            } else {
                ArrayList<Location> currentLocation = placedTntBlocks.get(player.getName());
                currentLocation.add(e.getBlockPlaced().getLocation());
                placedTntBlocks.put(player.getName(), currentLocation);
                e.setBuild(true);
                e.setCancelled(false);
            }
        } else if (e.getBlockPlaced().getType().equals(Material.DAYLIGHT_DETECTOR) && InteractiveGadgets.hasGadgetArmed(player)) {
            if (!e.getBlockReplacedState().getType().equals(Material.AIR)) {
                e.setBuild(false);
                e.setCancelled(true);
            } else {
                vulcanoFirework.put(player.getName(), e.getBlockPlaced().getLocation());
            }
        } else if (!player.hasPermission("bukkit.lobby.listeners.blockplace")) {
            e.setCancelled(true);
            e.setBuild(false);
            System.out.println("Gripped Not Permission");
        }
    }

}
