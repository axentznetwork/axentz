package de.devlen.nichtecro.server.bukkitlobby.menus;

import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.Button;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.ButtonFactory;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.ClickHandler;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.Menu;
import de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import de.devlen.nichtecro.server.bukkitlobby.gadgets.InteractiveGadgets;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.util.List;

/**
 * Created by Tarik on 05.06.2016.
 */
public class GadgetsMenu {

    public static void openGadget(Player target) {
        List<Button> gadgetButtons = new ExtendedList<>();
        Button interactiveButton = new Button()
                .id("bukkit.lobby.items.interactive")
                .click(new ClickHandler() {
                    @Override
                    public void handle(InventoryClickEvent e) {
                        Player subTarget = (Player) e.getWhoClicked();
                        subTarget.playSound(subTarget.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                        openGadgetInteractive(subTarget);
                    }
                })
                .slot(0)
                .factory(new ButtonFactory()
                        .type(Material.REDSTONE)
                        .nameNode("bukkit.lobby.items.interactive.name"));
        gadgetButtons.add(interactiveButton);
        Menu gadgetsMenu = new Menu()
                .lines(1)
                .titleNode("bukkit.lobby.gadgets.inventory.title")
                .buttons(gadgetButtons.toArray(new Button[0]));
        gadgetsMenu.openMenu(target);
    }

    public static void openGadgetInteractive(Player target) {

        List<Button> gadgetInteractiveButtons = new ExtendedList<>();

        int c4Amount = MPlayerSettingsManager.getAmount(target, "c4");
        int teleportStickAmount = MPlayerSettingsManager.getAmount(target, "teleportstick");
        int vulcanoAmount = MPlayerSettingsManager.getAmount(target, "vulcano");

        ButtonFactory c4ButtonFactory = new ButtonFactory()
                .type(Material.TNT)
                .nameNode("bukkit.lobby.gadgets.interactive.items.c4.name")
                .loreNode("bukkit.lobby.gadgets.interactive.items.c4.description");
        if (c4Amount <= 0) {
            c4ButtonFactory
                    .loreNode("bukkit.core.space")
                    .loreNode("bukkit.lobby.gadgets.not.usable");
        }
        c4ButtonFactory
                .loreNode("bukkit.core.space")
                .loreNode("bukkit.lobby.gadgets.amount", String.valueOf(c4Amount))
                .loreNode("bukkit.core.space")
                .loreNode("bukkit.lobby.gadgets.use")
                .loreNode("bukkit.lobby.gadgets.buy");

        ButtonFactory teleportStickButtonFactory = new ButtonFactory()
                .type(Material.STICK)
                .nameNode("bukkit.lobby.gadgets.interactive.items.teleportstick.name")
                .loreNode("bukkit.lobby.gadgets.interactive.items.teleportstick.description");
        if (teleportStickAmount <= 0) {
            teleportStickButtonFactory
                    .loreNode("bukkit.core.space")
                    .loreNode("bukkit.lobby.gadgets.not.usable");
        }
        teleportStickButtonFactory
                .loreNode("bukkit.core.space")
                .loreNode("bukkit.lobby.gadgets.amount", String.valueOf(teleportStickAmount))
                .loreNode("bukkit.core.space")
                .loreNode("bukkit.lobby.gadgets.use")
                .loreNode("bukkit.lobby.gadgets.buy");

        ButtonFactory vulcanoButtonFactory = new ButtonFactory()
                .type(Material.DAYLIGHT_DETECTOR)
                .nameNode("bukkit.lobby.gadgets.interactive.items.vulcano.name")
                .loreNode("bukkit.lobby.gadgets.interactive.items.vulcano.description");
        if (vulcanoAmount <= 0) {
            vulcanoButtonFactory
                    .loreNode("bukkit.core.space")
                    .loreNode("bukkit.lobby.gadgets.not.usable");
        }
        vulcanoButtonFactory
                .loreNode("bukkit.core.space")
                .loreNode("bukkit.lobby.gadgets.amount", String.valueOf(vulcanoAmount))
                .loreNode("bukkit.core.space")
                .loreNode("bukkit.lobby.gadgets.use")
                .loreNode("bukkit.lobby.gadgets.buy");

        Button c4Button = new Button()
                .id("bukkit.lobby.items.interactive.c4")
                .click(new ClickHandler() {
                    @Override
                    public void handle(InventoryClickEvent e) {
                        Player subTarget = (Player) e.getWhoClicked();
                        if (e.isLeftClick()) {
                            int c4Amount = MPlayerSettingsManager.getAmount(subTarget, "c4");
                            if (c4Amount > 0) {
                                InteractiveGadgets.armGadget(subTarget, InteractiveGadgets.InteractiveType.C4);
                                Chat.sendTl(subTarget, "bukkit.lobby.gadgets.tag", "bukkit.lobby.gadgets.c4.armed");
                                subTarget.playSound(subTarget.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                                subTarget.closeInventory();
                            } else {
                                subTarget.playSound(subTarget.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 0);
                            }
                        } else if (e.isRightClick()) {
                            subTarget.playSound(subTarget.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                            ShopDialogMenu.openShopDialog(subTarget, new ButtonFactory().type(Material.TNT).nameNode("bukkit.lobby.gadgets.interactive.items.c4.name"), 1000, "bukkit.lobby.shop.purchase.c4.reward", new ClickHandler() {
                                @Override
                                public void handle(InventoryClickEvent e) {
                                    Player shopUser = (Player) e.getWhoClicked();
                                    MPlayerSettingsManager.increaseAmount(shopUser, "c4", 50);
                                }
                            });
                        }
                    }
                })
                .slot(0)
                .factory(c4ButtonFactory);

        Button teleportStickButton = new Button()
                .id("bukkit.lobby.items.interactive.tpstick")
                .click(new ClickHandler() {
                    @Override
                    public void handle(InventoryClickEvent e) {
                        Player subTarget = (Player) e.getWhoClicked();
                        if (e.isLeftClick()) {
                            int teleportStickAmount = MPlayerSettingsManager.getAmount(subTarget, "teleportstick");
                            if (teleportStickAmount > 0) {
                                InteractiveGadgets.armGadget(subTarget, InteractiveGadgets.InteractiveType.TELEPORT_STICK);
                                Chat.sendTl(subTarget, "bukkit.lobby.gadgets.tag", "bukkit.lobby.gadgets.teleportstick.armed");
                                subTarget.playSound(subTarget.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                                subTarget.closeInventory();
                            } else {
                                subTarget.playSound(subTarget.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 0);
                            }
                        } else if (e.isRightClick()) {
                            subTarget.playSound(subTarget.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                            ShopDialogMenu.openShopDialog(subTarget, new ButtonFactory().type(Material.STICK).nameNode("bukkit.lobby.gadgets.interactive.items.teleportstick.name"), 100, "bukkit.lobby.shop.purchase.teleportstick.reward", new ClickHandler() {
                                @Override
                                public void handle(InventoryClickEvent e) {
                                    Player shopUser = (Player) e.getWhoClicked();
                                    MPlayerSettingsManager.increaseAmount(shopUser, "teleportstick", 100);
                                }
                            });
                        }
                    }
                })
                .slot(1)
                .factory(teleportStickButtonFactory);

        Button vulcanoButton = new Button()
                .id("bukkit.lobby.items.interactive.vulcano")
                .click(new ClickHandler() {
                    @Override
                    public void handle(InventoryClickEvent e) {
                        Player subTarget = (Player) e.getWhoClicked();
                        if (e.isLeftClick()) {
                            int vulcanoAmount = MPlayerSettingsManager.getAmount(subTarget, "vulcano");
                            if (vulcanoAmount > 0) {
                                InteractiveGadgets.armGadget(subTarget, InteractiveGadgets.InteractiveType.VULCANO);
                                Chat.sendTl(subTarget, "bukkit.lobby.gadgets.tag", "bukkit.lobby.gadgets.vulcano.armed");
                                subTarget.playSound(subTarget.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                                subTarget.closeInventory();
                            } else {
                                subTarget.playSound(subTarget.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 0);
                            }
                        } else if (e.isRightClick()) {
                            subTarget.playSound(subTarget.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                            ShopDialogMenu.openShopDialog(subTarget, new ButtonFactory().type(Material.STICK).nameNode("bukkit.lobby.gadgets.interactive.items.vulcano.name"), 1300, "bukkit.lobby.shop.purchase.vulcano.reward", new ClickHandler() {
                                @Override
                                public void handle(InventoryClickEvent e) {
                                    Player shopUser = (Player) e.getWhoClicked();
                                    MPlayerSettingsManager.increaseAmount(shopUser, "vulcano", 10);
                                }
                            });
                        }
                    }
                })
                .slot(2)
                .factory(vulcanoButtonFactory);

        Button removeButton = new Button()
                .id("bukkit.lobby.items.interactive.remove")
                .click(new ClickHandler() {
                    @Override
                    public void handle(InventoryClickEvent e) {
                        Player subTarget = (Player) e.getWhoClicked();
                        InteractiveGadgets.cleanGadget(subTarget);
                        Chat.sendTl(subTarget, "bukkit.lobby.gadgets.tag", "bukkit.lobby.gadgets.removed");
                        subTarget.playSound(subTarget.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                        subTarget.closeInventory();
                    }
                })
                .slot(16)
                .factory(new ButtonFactory()
                        .nameNode("bukkit.lobby.gadgets.interactive.items.remove.name")
                        .type(Material.BARRIER));

        Button backButton = new Button()
                .id("bukkit.lobby.items.interactive.back")
                .click(new ClickHandler() {
                    @Override
                    public void handle(InventoryClickEvent e) {
                        Player subTarget = (Player) e.getWhoClicked();
                        subTarget.playSound(subTarget.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                        openGadget(subTarget);
                    }
                })
                .slot(17)
                .factory(new ButtonFactory()
                        .nameNode("bukkit.lobby.gadgets.items.back.name")
                        .type(Material.IRON_DOOR));

        gadgetInteractiveButtons.add(c4Button);
        gadgetInteractiveButtons.add(teleportStickButton);
        gadgetInteractiveButtons.add(vulcanoButton);
        gadgetInteractiveButtons.add(removeButton);
        gadgetInteractiveButtons.add(backButton);

        Menu gadgetInteractiveMenu = new Menu()
                .lines(2)
                .titleNode("bukkit.lobby.gadgets.interactive.inventory.title")
                .buttons(gadgetInteractiveButtons.toArray(new Button[0]));
        gadgetInteractiveMenu.openMenu(target);
    }

}
