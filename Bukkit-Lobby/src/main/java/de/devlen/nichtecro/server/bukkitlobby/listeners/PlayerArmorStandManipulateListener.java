package de.devlen.nichtecro.server.bukkitlobby.listeners;

import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;

/**
 * Created by Tarik on 16.05.2016.
 */
public class PlayerArmorStandManipulateListener extends ListenerHelper {

    @EventHandler
    public void onPlayerArmorStandManipulate(PlayerArmorStandManipulateEvent e){
        e.setCancelled(true);
    }

}
