package de.devlen.nichtecro.server.bukkitlobby.gadgets;

import org.bukkit.entity.Player;

/**
 * Created by Tarik on 01.05.2016.
 */
public class GadgetManager {

    public static void cleanGadgets(Player player) {
        BootsGadgets.cleanGadget(player);
        HelmetGadgets.cleanGadget(player);
        InteractiveGadgets.cleanGadget(player);
    }

}
