package de.devlen.nichtecro.server.bukkitlobby.listeners;

import de.devlen.nichtecro.server.bukkitcore.events.DisguiseRankChangedEvent;
import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import de.devlen.nichtecro.server.bukkitlobby.scoreboard.ScoreboardTemplates;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

/**
 * Created by Tarik on 15.06.2016.
 */
public class DisguiseRankChangedListener extends ListenerHelper {

    @EventHandler
    public void onDisguiseRankChanged(DisguiseRankChangedEvent e){
        Player player = e.getPlayer();
        ScoreboardTemplates.setDefaultTemplate(player);
    }

}
