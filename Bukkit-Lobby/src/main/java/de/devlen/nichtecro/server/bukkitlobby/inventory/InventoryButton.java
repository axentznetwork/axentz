package de.devlen.nichtecro.server.bukkitlobby.inventory;

import de.devlen.nichtecro.server.bukkitcore.items.StackFactory;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.ClickHandler;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Tarik on 03.04.2016.
 */
public class InventoryButton {

    private ItemStack itemStack = new StackFactory().name("Default").material(Material.STONE).build();
    private ClickHandler clickHandler = new ClickHandler() {
        public void handle(InventoryClickEvent e) {
        }
    };
    private InventoryPosition inventoryPosition = new InventoryPosition(1, 1);

    @Deprecated
    public InventoryButton(ItemStack itemStack, ClickHandler clickHandler, InventoryPosition inventoryPosition) {
        this.itemStack = itemStack;
        this.clickHandler = clickHandler;
        this.inventoryPosition = inventoryPosition;
    }

    public InventoryButton() {
    }

    public InventoryButton item(ItemStack itemStack) {
        this.itemStack = itemStack;
        return this;
    }

    public InventoryButton click(ClickHandler clickHandler) {
        this.clickHandler = clickHandler;
        return this;
    }

    public InventoryButton position(int x, int y) {
        inventoryPosition = new InventoryPosition(x, y);
        return this;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public ClickHandler getClickHandler() {
        return clickHandler;
    }

    public InventoryPosition getInventoryPosition() {
        return inventoryPosition;
    }
}
