package de.devlen.nichtecro.server.bukkitlobby.inventory;

import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * Created by Tarik on 04.04.2016.
 */
public class InventoryFilling {

    private List<InventoryPosition> inventoryPositions = new ExtendedList<>();
    private InventoryButton inventoryButton = new InventoryButton().item(null);

    public InventoryFilling() {
    }

    public InventoryFilling(ItemStack itemStack) {
        this.inventoryButton = new InventoryButton().item(itemStack);
    }

    public InventoryFilling pos(int x, int y) {
        inventoryPositions.add(new InventoryPosition(x, y));
        return this;
    }

    public List<InventoryPosition> getInventoryPositions() {
        return inventoryPositions;
    }

    public InventoryButton getInventoryButton() {
        return inventoryButton;
    }
}
