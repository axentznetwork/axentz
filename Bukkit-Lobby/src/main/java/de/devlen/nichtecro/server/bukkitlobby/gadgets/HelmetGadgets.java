package de.devlen.nichtecro.server.bukkitlobby.gadgets;

import com.google.common.collect.Maps;
import de.devlen.nichtecro.server.bukkitlobby.inventory.StackManager;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;

import java.util.HashMap;

/**
 * Created by Tarik on 16.05.2016.
 */
public class HelmetGadgets {
    private static HashMap<String, HelmetType> stringHeadTypeHashMap = Maps.newHashMap();

    public static void armGadget(Player player, HelmetType shoeType) {
        cleanGadget(player);
        PlayerInventory playerInventory = player.getInventory();
        switch (shoeType) {
            case SPACE:
                stringHeadTypeHashMap.put(player.getName(), HelmetType.SPACE);
                playerInventory.setHelmet(StackManager.Gadgets.Armed.getSpaceHelmetStack());
                break;
            case FEATHER:
                stringHeadTypeHashMap.put(player.getName(), HelmetType.FEATHER);
                playerInventory.setHelmet(StackManager.Gadgets.Armed.getFeatherHelmetStack());
                break;
            case UNICORN:
                stringHeadTypeHashMap.put(player.getName(), HelmetType.UNICORN);
                playerInventory.setHelmet(StackManager.Gadgets.Armed.getUnicornHelmetStack());
                break;
            default:
                break;
        }
    }

    public static void cleanGadget(Player player) {
        if (stringHeadTypeHashMap.containsKey(player.getName())) {
            PlayerInventory playerInventory = player.getInventory();
            playerInventory.setHelmet(null);
            stringHeadTypeHashMap.remove(player.getName());
        }
    }

    public enum HelmetType {
        SPACE,
        FEATHER,
        UNICORN,
    }

}
