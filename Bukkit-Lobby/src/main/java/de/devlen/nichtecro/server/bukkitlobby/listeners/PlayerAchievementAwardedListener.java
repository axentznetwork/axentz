package de.devlen.nichtecro.server.bukkitlobby.listeners;

import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerAchievementAwardedEvent;

/**
 * Created by Tarik on 16.05.2016.
 */
public class PlayerAchievementAwardedListener extends ListenerHelper {

    @EventHandler
    public void onPlayerAchievementAwarded(PlayerAchievementAwardedEvent e) {
        e.setCancelled(true);
    }

}
