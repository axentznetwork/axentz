package de.devlen.nichtecro.server.bukkitlobby.menus;

import de.devlen.nichtecro.server.bukkitcore.items.reloaded.Button;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.ButtonFactory;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.ClickHandler;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.Menu;
import de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager;
import de.devlen.nichtecro.server.bukkitcore.manager.Rank;
import de.devlen.nichtecro.server.bukkitcore.message.CScoreboard;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

import static de.devlen.nichtecro.server.bukkitcore.language.Language.tl;

/**
 * Created by Tarik on 15.06.2016.
 */
public class DebugMenu {

    public static void openDebug(final Player player) {
        Menu debugMenu = new Menu()
                .buttons(new Button()
                        .slot(0)
                        .id("bukkit.lobby.debugmode")
                        .click(new ClickHandler() {
                            @Override
                            public void handle(InventoryClickEvent e) {
                                Rank disguisedRank = MPlayerSettingsManager.getDisguisedRank(player);

                                CScoreboard cScoreboard = new CScoreboard(player, tl("bukkit.lobby.scoreboard.welcome", player), "sbObj");
                                cScoreboard.resetScores();
                                cScoreboard.add("%l    ");
                                cScoreboard.add(tl("bukkit.lobby.scoreboard.dyes", player));
                                cScoreboard.add("%u" + MPlayerSettingsManager.getDyes(player));
                                cScoreboard.add("%d   ");
                                cScoreboard.add(tl("bukkit.lobby.scoreboard.disguised.rank", player));
                                cScoreboard.add(disguisedRank.getChatPrefixTranslatable(player));
                                cScoreboard.build();
                                cScoreboard.send(player);
                            }
                        })
                        .factory(new ButtonFactory()
                                .nameNode("bukkit.lobby.debugmode.name")
                                .type(Material.STONE)))
                .lines(6)
                .titleNode("bukkit.lobby.debugmode.inventory.title");
        debugMenu.openMenu(player);
    }

}
