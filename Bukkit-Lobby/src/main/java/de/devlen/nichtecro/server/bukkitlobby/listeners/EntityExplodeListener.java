package de.devlen.nichtecro.server.bukkitlobby.listeners;

import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityExplodeEvent;

/**
 * Created by Tarik on 16.05.2016.
 */
public class EntityExplodeListener extends ListenerHelper {

    @EventHandler
    public void onEntityExplode(EntityExplodeEvent e) {
        e.blockList().clear();
//        int times = 1;
//        for (Block block : e.blockList()) {
//            final BlockState blockState = block.getState();
//
//            block.setType(Material.AIR);
//
//            Bukkit.getScheduler().scheduleSyncDelayedTask(BukkitLobby.getInstance(), new Runnable() {
//                @Override
//                public void run() {
//                    blockState.update(true, false);
//                }
//            }, 1 * times);
//            times++;
//        }
    }

}