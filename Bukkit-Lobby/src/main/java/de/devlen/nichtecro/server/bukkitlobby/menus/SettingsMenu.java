package de.devlen.nichtecro.server.bukkitlobby.menus;

import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.Button;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.ButtonFactory;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.ClickHandler;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.Menu;
import de.devlen.nichtecro.server.bukkitcore.manager.ChatVisibilityType;
import de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemFlag;

import java.util.List;

/**
 * Created by Tarik on 06.06.2016.
 */
public class SettingsMenu {

    public static void openSettings(Player player) {
        List<Button> settingsButtons = new ExtendedList<>();

        ButtonFactory chatVisibilityTypeButtonFactory = new ButtonFactory()
                .amount(1)
                .type(Material.BOOK_AND_QUILL)
                .nameNode("bukkit.lobby.settings.items.chatvisibility.name");
        ChatVisibilityType chatVisibilityType = MPlayerSettingsManager.getChatVisibilityType(player);
        switch (chatVisibilityType) {
            case ALL:
                chatVisibilityTypeButtonFactory
                        .loreNode("bukkit.lobby.settings.items.chatvisibility.all.lore");
                break;
            case PREMIUM:
                chatVisibilityTypeButtonFactory
                        .loreNode("bukkit.lobby.settings.items.chatvisibility.premium.lore");
                break;
            case NONE:
                chatVisibilityTypeButtonFactory
                        .loreNode("bukkit.lobby.settings.items.chatvisibility.none.lore");
                break;
        }

        ButtonFactory chatVisibilityTypeAllButtonFactory = new ButtonFactory()
                .amount(1)
                .type(Material.WOOL)
                .damage(8)
                .nameNode("bukkit.lobby.settings.items.chatvisibility.all.name")
                .loreNode("bukkit.lobby.settings.items.chatvisibility.all.description");

        ButtonFactory chatVisibilityTypePremiumButtonFactory = new ButtonFactory()
                .amount(1)
                .type(Material.WOOL)
                .damage(1)
                .nameNode("bukkit.lobby.settings.items.chatvisibility.premium.name")
                .loreNode("bukkit.lobby.settings.items.chatvisibility.premium.description");

        if (!player.hasPermission("bukkit.lobby.settings.chatvisibility.premium.use")) {
            chatVisibilityTypePremiumButtonFactory
                    .loreNode("bukkit.core.space")
                    .loreNode("bukkit.lobby.settings.items.chatvisibility.premium.no.premium");
        }

        ButtonFactory chatVisibilityTypeNoneButtonFactory = new ButtonFactory()
                .amount(1)
                .type(Material.WOOL)
                .damage(14)
                .nameNode("bukkit.lobby.settings.items.chatvisibility.none.name")
                .loreNode("bukkit.lobby.settings.items.chatvisibility.none.description");

        switch (chatVisibilityType) {
            case ALL:
                chatVisibilityTypeAllButtonFactory
                        .enchant(Enchantment.WATER_WORKER, 1)
                        .flag(ItemFlag.HIDE_ENCHANTS);
                break;
            case PREMIUM:
                chatVisibilityTypePremiumButtonFactory
                        .enchant(Enchantment.WATER_WORKER, 1)
                        .flag(ItemFlag.HIDE_ENCHANTS);
                break;
            case NONE:
                chatVisibilityTypeNoneButtonFactory
                        .enchant(Enchantment.WATER_WORKER, 1)
                        .flag(ItemFlag.HIDE_ENCHANTS);
                break;
        }

        Button chatVisibilityTypeButton = new Button()
                .id("bukkit.lobby.settings.items.chatvisibility")
                .click(new ClickHandler() {
                    @Override
                    public void handle(InventoryClickEvent e) {
                        Player target = (Player) e.getWhoClicked();
                        target.playSound(target.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 0);
                    }
                })
                .slot(0)
                .factory(chatVisibilityTypeButtonFactory);

        Button chatVisibilityTypeAllButton = new Button()
                .id("bukkit.lobby.settings.items.chatvisibility.all")
                .click(new ClickHandler() {
                    @Override
                    public void handle(InventoryClickEvent e) {
                        Player target = (Player) e.getWhoClicked();
                        target.playSound(target.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                        MPlayerSettingsManager.setChatVisibilityType(target, ChatVisibilityType.ALL);
                        openSettings(target);
                    }
                })
                .slot(1)
                .factory(chatVisibilityTypeAllButtonFactory);

        Button chatVisibilityTypePremiumButton = new Button()
                .id("bukkit.lobby.settings.items.chatvisibility.premium")
                .click(new ClickHandler() {
                    @Override
                    public void handle(InventoryClickEvent e) {
                        Player target = (Player) e.getWhoClicked();
                        if (target.hasPermission("bukkit.lobby.settings.chatvisibility.premium.use")) {
                            target.playSound(target.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                            MPlayerSettingsManager.setChatVisibilityType(target, ChatVisibilityType.PREMIUM);
                            openSettings(target);
                        } else {
                            target.playSound(target.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 0);
                        }
                    }
                })
                .slot(2)
                .factory(chatVisibilityTypePremiumButtonFactory);

        Button chatVisibilityTypeNoneButton = new Button()
                .id("bukkit.lobby.settings.items.chatvisibility.none")
                .click(new ClickHandler() {
                    @Override
                    public void handle(InventoryClickEvent e) {
                        Player target = (Player) e.getWhoClicked();
                        target.playSound(target.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                        MPlayerSettingsManager.setChatVisibilityType(target, ChatVisibilityType.NONE);
                        openSettings(target);
                    }
                })
                .slot(3)
                .factory(chatVisibilityTypeNoneButtonFactory);

        settingsButtons.add(chatVisibilityTypeButton);
        settingsButtons.add(chatVisibilityTypeAllButton);
        settingsButtons.add(chatVisibilityTypePremiumButton);
        settingsButtons.add(chatVisibilityTypeNoneButton);

        Menu settingsMenu = new Menu()
                .buttons(settingsButtons.toArray(new Button[0]))
                .lines(6)
                .titleNode("bukkit.lobby.settings.inventory.title");

        settingsMenu.openMenu(player);
    }

}
