package de.devlen.nichtecro.server.bukkitlobby.listeners;

import de.devlen.nichtecro.server.bukkitcore.events.ClientSettingsChangeEvent;
import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import de.devlen.nichtecro.server.bukkitlobby.inventory.PlayerInventoryTemplates;
import de.devlen.nichtecro.server.bukkitlobby.scoreboard.ScoreboardTemplates;
import org.bukkit.event.EventHandler;

/**
 * Created by Tarik on 15.06.2016.
 */
public class ClientSettingsChangeListener extends ListenerHelper {

    @EventHandler
    public void onClientSettingsChange(ClientSettingsChangeEvent e){
        ScoreboardTemplates.setDefaultTemplate(e.getPlayer());
        PlayerInventoryTemplates.armHotbar(e.getPlayer());
    }

}
