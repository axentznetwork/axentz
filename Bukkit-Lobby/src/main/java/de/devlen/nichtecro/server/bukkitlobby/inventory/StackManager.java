package de.devlen.nichtecro.server.bukkitlobby.inventory;

import de.devlen.nichtecro.server.bukkitcore.items.StackFactory;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import static de.devlen.nichtecro.server.bukkitcore.language.Language.tl;

/**
 * Created by Tarik on 25.03.2016.
 */
public class StackManager {

    public static class Gadgets {

        //radioactive mode

        public static ItemStack getBootsStack() {
            return new StackFactory().name("%dSchuhe").material(Material.LEATHER_BOOTS).leatherColor(Color.fromRGB(255, 255, 255)).build();
        }

        public static ItemStack getHelmetsStack() {
            return new StackFactory().name("%dHelme & Hutdeko").material(Material.LEATHER_HELMET).leatherColor(Color.fromRGB(255, 255, 255)).build();
        }

        public static ItemStack getInteractiveStack() {
            return new StackFactory().name("%dInteraktives").material(Material.LEVER).build();
        }

        public static class Interactive {

            public static ItemStack getRemoveInteraktiveStack() {
                return new StackFactory().name("%dInteraktives enfternen").material(Material.BARRIER).build();
            }

            public static ItemStack getC4InteractiveStack() {
                return new StackFactory().name("%dC4-Sprengstoff").material(Material.TNT).lore("� 3, 2, 1, %lBOOM%u!", "� %l64 %uC4 zum %lhochjagen").build();
            }

            public static ItemStack getTeleportStickInteractiveStack() {
                return new StackFactory().name("%dTeleport-Stab").material(Material.STICK).lore("� WUSCH, und weg bist du!").enchant(Enchantment.WATER_WORKER, 1).flags(ItemFlag.HIDE_ENCHANTS).amount(1).build();
            }

            public static ItemStack getVulcanoInteractiveStack() {
                return new StackFactory().name("%dVulcano-Feuerwerk").material(Material.DAYLIGHT_DETECTOR).lore("� Lass es in der %lLobby %ukrachen", "� %l36 %uSchuss").build();
            }
        }

        public static class Helmets {

            public static ItemStack getRemoveHelmetStack() {
                return new StackFactory().name("%dHelm enfternen").material(Material.LEATHER_HELMET).leatherColor(Color.fromRGB(255, 0, 0)).build();
            }

            public static ItemStack getSpaceHelmetStack() {
                return new StackFactory().name("%dAstronauten-Helm").material(Material.STAINED_GLASS).lore("� 3... 2... 1... 0... %lSTART%u!").subid(1).build();
            }

            public static ItemStack getUnicornHelmetStack() {
                return new StackFactory().name("%dEinhorn-Hut").material(Material.END_ROD).lore("� Zeig wer hier das %lEinhorn %uist").build();
            }

            public static ItemStack getFeatherHelmetStack() {
                return new StackFactory().name("%dHut-Feder").material(Material.FEATHER).lore("� Da kommt ein edler %lMonsieur %udaher").build();
            }
        }

        public static class Boots {

            public static ItemStack getRemoveBootsStack() {
                return new StackFactory().name("�4Schuhe entfernen").material(Material.LEATHER_BOOTS).leatherColor(Color.fromRGB(255, 0, 0)).lore("� Laufe �cunbeschwert �7durch die Lobby").build();
            }

            public static ItemStack getJetpackBootsStack() {
                return new StackFactory().name("�6Jetpack-Schuhe").material(Material.LEATHER_BOOTS).leatherColor(Color.fromRGB(255, 157, 0)).lore("� Schnuppere �efrische Luft �7in �eh�heren Lagen").build();
            }

            public static ItemStack getLoveBootsStack() {
                return new StackFactory().name("�4Liebes-Schuhe").material(Material.LEATHER_BOOTS).leatherColor(Color.fromRGB(255, 0, 0)).lore("� Bringen dich mit ganz viel �cHerz �7zu deiner �cLiebe").build();
            }

            public static ItemStack getKnowledgeBootsStack() {
                return new StackFactory().name("�5Wissens-Schuhe").material(Material.LEATHER_BOOTS).leatherColor(Color.fromRGB(212, 0, 255)).lore("� Lassen dich auch ohne Brille ganz �dschlau �7wirken").build();
            }

            public static ItemStack getGhostBootsStack() {
                return new StackFactory().name("�fGeister-Schuhe").material(Material.LEATHER_BOOTS).leatherColor(Color.fromRGB(255, 255, 255)).lore("� Jage anderen einen �8H�llenschreck �7ein").build();
            }

            public static ItemStack getSevenMilesBootsStack() {
                return new StackFactory().name("�17-Meilen-Stiefel").material(Material.LEATHER_BOOTS).leatherColor(Color.fromRGB(0, 0, 255)).lore("� �9Renne �7schneller als �9Speedy Gonzales").build();
            }

            public static ItemStack getHappynessBootsStack() {
                return new StackFactory().name("�2Fr�hlichkeits-Schuhe").material(Material.LEATHER_BOOTS).leatherColor(Color.fromRGB(20, 150, 0)).lore("� Verteile �berall �agute Laune").build();
            }

            public static ItemStack getEnderBootsStack() {
                return new StackFactory().name("�5Ender-Stiefel").material(Material.LEATHER_BOOTS).leatherColor(Color.fromRGB(212, 0, 255)).lore("� Nutze den �dMythos �7des �dEnds").build();
            }

            public static ItemStack getFireBootsStack() {
                return new StackFactory().name("�6Feuer-Stiefel").material(Material.LEATHER_BOOTS).leatherColor(Color.fromRGB(255, 157, 0)).lore("� Sei der �ehei�este �7Feger auf dem Server").build();
            }
        }

        public static class Armed {
            public static ItemStack getJetpackChestplateStack() {
                return new StackFactory().name("�6Jetpack-Schirm").material(Material.ELYTRA).build();
            }

            public static ItemStack getJetpackBootsStack() {
                return new StackFactory().name("�6Jetpack-Schuhe").material(Material.LEATHER_BOOTS).leatherColor(Color.fromRGB(255, 157, 0)).build();
            }

            public static ItemStack getLoveBootsStack() {
                return new StackFactory().name("�4Liebes-Schuhe").material(Material.LEATHER_BOOTS).leatherColor(Color.fromRGB(255, 0, 0)).build();
            }

            public static ItemStack getKnowledgeBootsStack() {
                return new StackFactory().name("�5Wissens-Schuhe").material(Material.LEATHER_BOOTS).leatherColor(Color.fromRGB(212, 0, 255)).build();
            }

            public static ItemStack getGhostBootsStack() {
                return new StackFactory().name("�fGeister-Schuhe").material(Material.LEATHER_BOOTS).leatherColor(Color.fromRGB(255, 255, 255)).build();
            }

            public static ItemStack getSevenMilesBootsStack() {
                return new StackFactory().name("�17-Meilen-Stiefel").material(Material.LEATHER_BOOTS).leatherColor(Color.fromRGB(0, 0, 255)).build();
            }

            public static ItemStack getHappynessBootsStack() {
                return new StackFactory().name("�2Fr�hlichkeits-Schuhe").material(Material.LEATHER_BOOTS).leatherColor(Color.fromRGB(20, 150, 0)).build();
            }

            public static ItemStack getEnderBootsStack() {
                return new StackFactory().name("�5Ender-Stiefel").material(Material.LEATHER_BOOTS).leatherColor(Color.fromRGB(212, 0, 255)).build();
            }

            public static ItemStack getFireBootsStack() {
                return new StackFactory().name("�6Feuer-Stiefel").material(Material.LEATHER_BOOTS).leatherColor(Color.fromRGB(255, 157, 0)).build();
            }

            public static ItemStack getSpaceHelmetStack() {
                return new StackFactory().name("�6Astronauten-Helm").material(Material.STAINED_GLASS).subid(1).build();
            }

            public static ItemStack getUnicornHelmetStack() {
                return new StackFactory().name("�6Einhorn-Hut").material(Material.END_ROD).build();
            }

            public static ItemStack getFeatherHelmetStack() {
                return new StackFactory().name("�6Hut-Feder").material(Material.FEATHER).build();
            }

            public static ItemStack getC4DynamiteInteractiveStack() {
                return new StackFactory().name("%s� %dC4-Sprengstoff").material(Material.TNT).amount(64).build();
            }

            public static ItemStack getC4IgniterInteractiveStack() {
                return new StackFactory().name("%s� %dC4-Z�nder %u(Rechtsklick)").material(Material.WOOD_BUTTON).enchant(Enchantment.WATER_WORKER, 1).flags(ItemFlag.HIDE_ENCHANTS).build();
            }

            public static ItemStack getTeleportStickInteractiveStack() {
                return new StackFactory().name("%s� %dTeleport-Stab").material(Material.STICK).enchant(Enchantment.WATER_WORKER, 1).flags(ItemFlag.HIDE_ENCHANTS).amount(1).build();
            }

            public static ItemStack getVulcanoBlockInteractiveStack() {
                return new StackFactory().name("%s� %dVulcano-Feuerwerk").material(Material.DAYLIGHT_DETECTOR).amount(1).build();
            }

            public static ItemStack getVulcanoIgniterInteractiveStack() {
                return new StackFactory().name("%s� %dVulcano-Feuerwerk-Z�nder %u(Rechtsklick)").material(Material.LEVER).enchant(Enchantment.WATER_WORKER, 1).flags(ItemFlag.HIDE_ENCHANTS).build();
            }
        }
    }

    public static class RankTagChanger {

        public static ItemStack getGuestStack(Player player) {

            return null;
        }
    }

    public static class General {

        public static ItemStack getBackToGadgetsStack() {
            return new StackFactory().name("%dZur�ck zu den Gadgets").material(Material.IRON_DOOR).build();
        }
    }

    public static class Hotbar {

        public static ItemStack getRankTagChangerStack(Player player) {
            return new StackFactory().name(tl("bukkit.lobby.items.tagchanger.name", player)).material(Material.NAME_TAG).build();
        }

        public static ItemStack getGadgetsStack() {
            return new StackFactory().name(Chat.replaceFormatVariables("%s� %dGadgets %u(Rechtsklick)")).material(Material.CHEST).build();
        }
    }
}
