package de.devlen.nichtecro.server.bukkitlobby.gadgets;

import com.comphenix.protocol.wrappers.EnumWrappers;
import com.google.common.collect.Maps;
import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import de.devlen.nichtecro.server.bukkitcore.items.NBTItem;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.Button;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.ButtonFactory;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.ClickHandler;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.InteractHandler;
import de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import de.devlen.nichtecro.server.bukkitcore.message.Hotbar;
import de.devlen.nichtecro.server.bukkitcore.packetwrapper.WrapperPlayServerSetCooldown;
import de.devlen.nichtecro.server.bukkitcore.particle.ParticleSender;
import de.devlen.nichtecro.server.bukkitcore.time.Cooldown;
import de.devlen.nichtecro.server.bukkitcore.time.Delay;
import de.devlen.nichtecro.server.bukkitlobby.BukkitLobby;
import de.devlen.nichtecro.server.bukkitlobby.listeners.BlockPlaceListener;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.util.Vector;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import static de.devlen.nichtecro.server.bukkitcore.language.Language.tl;

/**
 * Created by Tarik on 17.05.2016.
 */
public class InteractiveGadgets {

    private static HashMap<String, InteractiveType> stringInteractiveTypeHashMap = Maps.newHashMap();
    private static boolean updateTaskRunning = false;

    public static void armGadget(Player player, InteractiveType shoeType) {
        cleanGadget(player);
        PlayerInventory playerInventory = player.getInventory();
        switch (shoeType) {
            case C4:
                stringInteractiveTypeHashMap.put(player.getName(), InteractiveType.C4);

                BlockPlaceListener.placedTntBlocks.put(player.getName(), new ExtendedList<Location>());

                List<Button> c4ButtonList = new ExtendedList<>();

                ButtonFactory c4DynamiteButtonFactory = new ButtonFactory()
                        .amount(10)
                        .nameNode("bukkit.lobby.gadgets.c4.items.dynamite.name")
                        .loreNode("bukkit.lobby.gadgets.c4.items.dynamite.usage")
                        .type(Material.TNT);

                ButtonFactory c4IgniterButtonFactory = new ButtonFactory()
                        .amount(1)
                        .nameNode("bukkit.lobby.gadgets.c4.items.igniter.name")
                        .loreNode("bukkit.lobby.gadgets.c4.items.igniter.usage")
                        .type(Material.WOOD_BUTTON)
                        .enchant(Enchantment.WATER_WORKER, 1)
                        .flag(ItemFlag.HIDE_ENCHANTS);

                Button c4DynamiteButton = new Button()
                        .id("bukkit.lobby.gadgets.c4.dynamite")
                        .click(new ClickHandler() {
                            @Override
                            public void handle(InventoryClickEvent e) {
                                Player target = (Player) e.getWhoClicked();
                                target.playSound(target.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 0);
                                e.setCancelled(true);
                            }
                        })
                        .interact(new InteractHandler() {
                            @Override
                            public void handle(PlayerInteractEvent e) {
                                System.out.println("Gripped TNT Interact");
                                e.setCancelled(false);
                            }
                        })
                        .slot(4)
                        .factory(c4DynamiteButtonFactory)
                        .registerHandlers();

                Button c4IgniterButton = new Button()
                        .id("bukkit.lobby.gadgets.c4.igniter")
                        .click(new ClickHandler() {
                            @Override
                            public void handle(InventoryClickEvent e) {
                                Player target = (Player) e.getWhoClicked();
                                target.playSound(target.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 0);
                                e.setCancelled(true);
                            }
                        })
                        .interact(new InteractHandler() {
                            @Override
                            public void handle(PlayerInteractEvent e) {
                                Player target = e.getPlayer();
                                if (BlockPlaceListener.placedTntBlocks.containsKey(target.getName())) {
                                    MPlayerSettingsManager.setAmount(target, "c4", MPlayerSettingsManager.getAmount(target, "c4") - 1);
                                    ArrayList<Location> locationArrayList = BlockPlaceListener.placedTntBlocks.get(target.getName());
                                    for (Location location : locationArrayList) {
                                        Block block = location.getBlock();
                                        if (block.getType() == Material.TNT) {
                                            block.setType(Material.AIR);
                                            TNTPrimed tntPrimed = (TNTPrimed) target.getWorld().spawnEntity(block.getLocation(), EntityType.PRIMED_TNT);
                                            tntPrimed.setFuseTicks(1);
                                            tntPrimed.setYield(12.0F);
                                        }
                                    }
                                    InteractiveGadgets.cleanGadget(target);
                                }
                            }
                        })
                        .slot(5)
                        .factory(c4IgniterButtonFactory)
                        .registerHandlers();

                c4ButtonList.add(c4DynamiteButton);
                c4ButtonList.add(c4IgniterButton);

                for (Button button : c4ButtonList) {
                    ItemStack itemStack = button.getButtonFactory().build(player);
                    NBTItem nbtItem = new NBTItem(itemStack);
                    nbtItem.setString("server_name_id", button.getNameId());
                    playerInventory.setItem(button.getSlot(), nbtItem.getItem());
                }

                break;
            case TELEPORT_STICK:
                stringInteractiveTypeHashMap.put(player.getName(), InteractiveType.TELEPORT_STICK);

                List<Button> teleportStickButtonList = new ExtendedList<>();

                ButtonFactory teleportStickButtonFactory = new ButtonFactory()
                        .amount(1)
                        .nameNode("bukkit.lobby.gadgets.teleportstick.items.name")
                        .loreNode("bukkit.lobby.gadgets.teleportstick.items.usage")
                        .type(Material.STICK);

                Button teleportStickButton = new Button()
                        .id("bukkit.lobby.gadgets.teleportstick")
                        .click(new ClickHandler() {
                            @Override
                            public void handle(InventoryClickEvent e) {
                                Player target = (Player) e.getWhoClicked();
                                target.playSound(target.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 0);
                                e.setCancelled(true);
                            }
                        })
                        .interact(new InteractHandler() {
                            @Override
                            public void handle(PlayerInteractEvent e) {
                                Player target = e.getPlayer();
                                if (MPlayerSettingsManager.getAmount(target, "teleportstick") > 0) {
                                    Block block = target.getTargetBlock((Set) null, 100);
                                    Location blockLocation = block.getLocation();
                                    blockLocation.setY(block.getY() + 1);
                                    if (blockLocation.getBlock().getType() == Material.AIR) {
                                        blockLocation.setY(block.getY() + 2);
                                        if (blockLocation.getBlock().getType() == Material.AIR) {
                                            blockLocation.setY(blockLocation.getY() - 1);
                                            blockLocation.setPitch(target.getLocation().getPitch());
                                            blockLocation.setYaw(target.getLocation().getYaw());
                                            blockLocation.add(0.5, 0, 0.5);
                                            if (!Cooldown.hasCharge(target, "tpstick")) {
                                                target.teleport(blockLocation, PlayerTeleportEvent.TeleportCause.PLUGIN);
                                                target.playSound(blockLocation, Sound.ENTITY_ENDERMEN_TELEPORT, 1, 1);
                                                Chat.sendTl(target, "gadgets.teleportstick.prefix", "gadgets.teleportstick.teleported");
                                                WrapperPlayServerSetCooldown wrapperPlayServerSetCooldown = new WrapperPlayServerSetCooldown();
                                                wrapperPlayServerSetCooldown.setItem(Material.STICK);
                                                wrapperPlayServerSetCooldown.setTicks(3 * 20);
                                                wrapperPlayServerSetCooldown.sendPacket(target);
                                                MPlayerSettingsManager.setAmount(target, "teleportstick", MPlayerSettingsManager.getAmount(target, "teleportstick") - 1);
                                                Cooldown.charge(target, "tpstick", 3);
                                            } else {
                                                Chat.sendTl(target, "gadgets.teleportstick.prefix", "gadgets.teleportstick.cooldown.remaining", new DecimalFormat("#.##").format(Cooldown.getRemainingTime(target, "tpstick")));
                                            }
                                        }
                                    }
                                } else {
                                    Chat.sendTl(target, "bukkit.lobby.gadgets.tag", "bukkit.lobby.gadgets.shop.buy.teleportstick");
                                }
                            }
                        })
                        .slot(4)
                        .factory(teleportStickButtonFactory)
                        .registerHandlers();

                teleportStickButtonList.add(teleportStickButton);

                for (Button button : teleportStickButtonList) {
                    ItemStack itemStack = button.getButtonFactory().build(player);
                    NBTItem nbtItem = new NBTItem(itemStack);
                    nbtItem.setString("server_name_id", button.getNameId());
                    playerInventory.setItem(button.getSlot(), nbtItem.getItem());
                }

                break;
            case VULCANO:
                stringInteractiveTypeHashMap.put(player.getName(), InteractiveType.VULCANO);

                List<Button> vulcanoButtonList = new ExtendedList<>();

                ButtonFactory vulcanoBlockButtonFactory = new ButtonFactory()
                        .amount(1)
                        .nameNode("bukkit.lobby.gadgets.vulcano.items.block.name")
                        .loreNode("bukkit.lobby.gadgets.vulcano.items.block.usage")
                        .type(Material.DAYLIGHT_DETECTOR);

                ButtonFactory vulcanoIgniterButtonFactory = new ButtonFactory()
                        .amount(1)
                        .nameNode("bukkit.lobby.gadgets.vulcano.items.igniter.name")
                        .loreNode("bukkit.lobby.gadgets.vulcano.items.igniter.usage")
                        .type(Material.WOOD_BUTTON)
                        .enchant(Enchantment.WATER_WORKER, 1)
                        .flag(ItemFlag.HIDE_ENCHANTS);

                Button vulcanoBlockButton = new Button()
                        .id("bukkit.lobby.gadgets.vulcano.block")
                        .click(new ClickHandler() {
                            @Override
                            public void handle(InventoryClickEvent e) {
                                Player target = (Player) e.getWhoClicked();
                                target.playSound(target.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 0);
                                e.setCancelled(true);
                            }
                        })
                        .interact(new InteractHandler() {
                            @Override
                            public void handle(PlayerInteractEvent e) {
                                e.setCancelled(false);
                            }
                        })
                        .slot(4)
                        .factory(vulcanoBlockButtonFactory)
                        .registerHandlers();

                Button vulcanoIgniterButton = new Button()
                        .id("bukkit.lobby.gadgets.vulcano.igniter")
                        .click(new ClickHandler() {
                            @Override
                            public void handle(InventoryClickEvent e) {
                                Player target = (Player) e.getWhoClicked();
                                target.playSound(target.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 0);
                                e.setCancelled(true);
                            }
                        })
                        .interact(new InteractHandler() {
                            @Override
                            public void handle(PlayerInteractEvent e) {
                                Player target = e.getPlayer();
                                if (BlockPlaceListener.vulcanoFirework.containsKey(target.getName())) {
                                    if (!Cooldown.hasCharge(target, "vulcano")) {
                                        MPlayerSettingsManager.setAmount(target, "vulcano", MPlayerSettingsManager.getAmount(target, "vulcano") - 1);
                                        Cooldown.charge(target, "vulcano", 30);
                                        final Location absoluteLocation = BlockPlaceListener.vulcanoFirework.get(target.getName());
                                        Location hole = absoluteLocation;
                                        Location hole1 = hole.add(0.1875, 0, 0.1875);
                                        Location hole2 = hole.add(0.1875, 0, 0);
                                        Location hole3 = hole.add(0.1875, 0, 0);
                                        Location hole4 = hole.add(0, 0, 0.1875);
                                        Location hole5 = hole.add(-0.1875, 0, 0);
                                        Location hole6 = hole.add(-0.1875, 0, 0);
                                        Location hole7 = hole.add(0, 0, 0.1875);
                                        Location hole8 = hole.add(0.1875, 0, 0);
                                        Location hole9 = hole.add(0.1875, 0, 0);

                                        final List<Location> locationList = new ExtendedList<>(hole1, hole2, hole3, hole4, hole5, hole6, hole7, hole8, hole9);

                                        final FireworkEffect.Builder builder = FireworkEffect.builder();
                                        builder.withColor(Color.AQUA, Color.BLUE, Color.NAVY);
                                        builder.withFade(Color.AQUA, Color.BLUE, Color.NAVY);
                                        builder.withFlicker();
                                        builder.withTrail();
                                        builder.with(FireworkEffect.Type.BURST);
                                        final FireworkEffect effect = builder.build();

                                        final FireworkEffect.Builder builder1 = FireworkEffect.builder();
                                        builder1.withColor(Color.ORANGE, Color.RED, Color.YELLOW);
                                        builder1.withFade(Color.ORANGE, Color.RED, Color.YELLOW);
                                        builder1.withFlicker();
                                        builder1.withTrail();
                                        builder1.with(FireworkEffect.Type.BURST);
                                        final FireworkEffect effect1 = builder1.build();

                                        final FireworkEffect.Builder builder2 = FireworkEffect.builder();
                                        builder2.withColor(Color.GREEN, Color.OLIVE, Color.LIME);
                                        builder2.withFade(Color.GREEN, Color.OLIVE, Color.LIME);
                                        builder2.withFlicker();
                                        builder2.withTrail();
                                        builder2.with(FireworkEffect.Type.BURST);
                                        final FireworkEffect effect2 = builder2.build();

                                        final FireworkEffect.Builder builder3 = FireworkEffect.builder();
                                        builder3.withColor(Color.WHITE, Color.SILVER, Color.BLACK);
                                        builder3.withFade(Color.WHITE, Color.SILVER, Color.BLACK);
                                        builder3.withFlicker();
                                        builder3.withTrail();
                                        builder3.with(FireworkEffect.Type.BURST);
                                        final FireworkEffect effect3 = builder3.build();

                                        final int delayBetween = 4;

                                        for (int i = 0; i < locationList.size(); i++) {
                                            final Location location = locationList.get(i);
                                            final int finalI = i;
                                            Delay.make(delayBetween * i, new Runnable() {
                                                @Override
                                                public void run() {
                                                    Firework firework = (Firework) location.getWorld().spawnEntity(locationList.get(finalI), EntityType.FIREWORK);
                                                    FireworkMeta fireworkMeta = firework.getFireworkMeta();
                                                    fireworkMeta.addEffect(effect);
                                                    fireworkMeta.setPower(3);
                                                    firework.setFireworkMeta(fireworkMeta);
                                                }
                                            });
                                        }

                                        Delay.make(delayBetween * 9 * 1 + 60 * 1, new Runnable() {
                                            @Override
                                            public void run() {
                                                for (int i = 0; i < locationList.size(); i++) {
                                                    final Location location = locationList.get(i);
                                                    final int finalI = i;
                                                    Delay.make(delayBetween * i, new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            Firework firework = (Firework) location.getWorld().spawnEntity(locationList.get(finalI), EntityType.FIREWORK);
                                                            FireworkMeta fireworkMeta = firework.getFireworkMeta();
                                                            fireworkMeta.addEffect(effect1);
                                                            fireworkMeta.setPower(3);
                                                            firework.setFireworkMeta(fireworkMeta);
                                                        }
                                                    });
                                                }
                                            }
                                        });


                                        Delay.make(delayBetween * 9 * 2 + 60 * 2, new Runnable() {
                                            @Override
                                            public void run() {
                                                for (int i = 0; i < locationList.size(); i++) {
                                                    final Location location = locationList.get(i);
                                                    final int finalI = i;
                                                    Delay.make(delayBetween * i, new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            Firework firework = (Firework) location.getWorld().spawnEntity(locationList.get(finalI), EntityType.FIREWORK);
                                                            FireworkMeta fireworkMeta = firework.getFireworkMeta();
                                                            fireworkMeta.addEffect(effect2);
                                                            fireworkMeta.setPower(3);
                                                            firework.setFireworkMeta(fireworkMeta);
                                                        }
                                                    });
                                                }
                                            }
                                        });


                                        Delay.make(delayBetween * 9 * 3 + 60 * 3, new Runnable() {
                                            @Override
                                            public void run() {
                                                for (int i = 0; i < locationList.size(); i++) {
                                                    final Location location = locationList.get(i);
                                                    final int finalI = i;
                                                    Delay.make(delayBetween * i, new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            Firework firework = (Firework) location.getWorld().spawnEntity(locationList.get(finalI), EntityType.FIREWORK);
                                                            FireworkMeta fireworkMeta = firework.getFireworkMeta();
                                                            fireworkMeta.addEffect(effect3);
                                                            fireworkMeta.setPower(3);
                                                            firework.setFireworkMeta(fireworkMeta);
                                                        }
                                                    });
                                                }
                                            }
                                        });

                                        Delay.make(delayBetween * 9 * 4 + 60 * 3, new Runnable() {
                                            @Override
                                            public void run() {
                                                absoluteLocation.getBlock().setType(Material.AIR);
                                            }
                                        });
                                        InteractiveGadgets.cleanGadget(target);
                                    } else {
                                        Chat.sendTl(target, "gadgets.vulcano.tag", "gadgets.vulcano.cooldown");
                                    }
                                    e.setCancelled(true);
                                }
                            }
                        })
                        .slot(5)
                        .factory(vulcanoIgniterButtonFactory)
                        .registerHandlers();

                vulcanoButtonList.add(vulcanoBlockButton);
                vulcanoButtonList.add(vulcanoIgniterButton);

                for (Button button : vulcanoButtonList) {
                    ItemStack itemStack = button.getButtonFactory().build(player);
                    NBTItem nbtItem = new NBTItem(itemStack);
                    nbtItem.setString("server_name_id", button.getNameId());
                    playerInventory.setItem(button.getSlot(), nbtItem.getItem());
                }

                break;
            default:
                break;
        }
    }

    public static void cleanGadget(Player player) {
        if (stringInteractiveTypeHashMap.containsKey(player.getName())) {
            PlayerInventory playerInventory = player.getInventory();
            if (BlockPlaceListener.placedTntBlocks.containsKey(player.getName())) {
                for (Location location : BlockPlaceListener.placedTntBlocks.get(player.getName())) {
                    location.getBlock().setType(Material.AIR);
                }
                BlockPlaceListener.placedTntBlocks.remove(player.getName());
            }

            if (BlockPlaceListener.vulcanoFirework.containsKey(player.getName())) {
                BlockPlaceListener.vulcanoFirework.get(player.getName()).getBlock().setType(Material.AIR);
                BlockPlaceListener.vulcanoFirework.remove(player.getName());

            }
            playerInventory.setItem(4, null);
            playerInventory.setItem(5, null);
            player.updateInventory();
            stringInteractiveTypeHashMap.remove(player.getName());
        }
    }

    public static boolean hasGadgetArmed(Player player) {
        return stringInteractiveTypeHashMap.containsKey(player.getName());
    }


    public static boolean isUpdateTaskRunning() {
        return updateTaskRunning;
    }

    public static void runUpdateTask() {
        if (!isUpdateTaskRunning()) {
            Bukkit.getScheduler().runTaskTimerAsynchronously(BukkitLobby.getInstance(), new Runnable() {
                @Override
                public void run() {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        if (stringInteractiveTypeHashMap.containsKey(player.getName())) {
                            InteractiveType interactiveType = stringInteractiveTypeHashMap.get(player.getName());
                            switch (interactiveType) {
                                case TELEPORT_STICK:
                                    if (player.getInventory().getItemInMainHand() != null || player.getInventory().getItemInMainHand().getType() != Material.AIR) {
                                        try {
                                            NBTItem nbtItem = new NBTItem(player.getInventory().getItemInMainHand());
                                            if (nbtItem.hasKey("server_name_id") && nbtItem.getString("server_name_id").equalsIgnoreCase("bukkit.lobby.gadgets.teleportstick")) {
                                                Block block = player.getTargetBlock((Set) null, 100);
                                                Location blockLocation = block.getLocation();
                                                blockLocation.setY(block.getY() + 1);
                                                if (blockLocation.getBlock().getType() == Material.AIR) {
                                                    blockLocation.setY(block.getY() + 2);
                                                    if (blockLocation.getBlock().getType() == Material.AIR) {
                                                        blockLocation.setY(blockLocation.getY() - 0.8);
                                                        Hotbar.send(player, " ");
                                                        blockLocation.add(0.5, 0, 0.5);
                                                        ParticleSender.send(player, EnumWrappers.Particle.SPELL_WITCH, blockLocation, new Vector(0.1, 0, 0.1), 0, 20);
                                                    } else {
                                                        Hotbar.send(player, tl("bukkit.lobby.tpstick.invalid.target", player));
                                                    }
                                                } else {
                                                    Hotbar.send(player, tl("bukkit.lobby.tpstick.invalid.target", player));
                                                }
                                            }
                                        } catch (Exception ex) {
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                }
            }, 0, 1);
        }
    }

    public enum InteractiveType {
        C4,
        TELEPORT_STICK,
        VULCANO
    }
}
