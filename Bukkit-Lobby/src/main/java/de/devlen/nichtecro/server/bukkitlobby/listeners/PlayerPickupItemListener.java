package de.devlen.nichtecro.server.bukkitlobby.listeners;

import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerPickupItemEvent;

/**
 * Created by Tarik on 16.05.2016.
 */
public class PlayerPickupItemListener extends ListenerHelper {

    @EventHandler
    public void onPlayerPickupItem(PlayerPickupItemEvent e){
        e.setCancelled(true);
    }

}
