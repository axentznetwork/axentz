package de.devlen.nichtecro.server.bukkitlobby.inventory;

import org.bukkit.event.inventory.InventoryOpenEvent;

/**
 * Created by Tarik on 03.04.2016.
 */
public interface OpenHandler {

    void handle(InventoryOpenEvent e);

}
