package de.devlen.nichtecro.server.bukkitlobby.scoreboard;

import de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager;
import de.devlen.nichtecro.server.bukkitcore.manager.Rank;
import de.devlen.nichtecro.server.bukkitcore.message.CScoreboard;
import org.bukkit.entity.Player;

import static de.devlen.nichtecro.server.bukkitcore.language.Language.tl;

/**
 * Created by Tarik on 15.06.2016.
 */
public class ScoreboardTemplates {

    public static void setDefaultTemplate(Player player){
        Rank disguisedRank = MPlayerSettingsManager.getDisguisedRank(player);

        CScoreboard cScoreboard = new CScoreboard(player, tl("bukkit.lobby.scoreboard.welcome", player), "sbObj");
        cScoreboard.resetScores();
        cScoreboard.add("%l    ");
        cScoreboard.add(tl("bukkit.lobby.scoreboard.dyes", player));
        cScoreboard.add("%u" + MPlayerSettingsManager.getDyes(player));
        cScoreboard.add("%d   ");
        cScoreboard.add(tl("bukkit.lobby.scoreboard.disguised.rank", player));
        cScoreboard.add(disguisedRank.getChatPrefixTranslatable(player));
        cScoreboard.build();
        cScoreboard.send(player);
    }

}
