package de.devlen.nichtecro.server.bukkitlobby.listeners;

import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.CauldronLevelChangeEvent;

/**
 * Created by Tarik on 16.05.2016.
 */
public class CauldronLevelChangeListener extends ListenerHelper {

    @EventHandler
    public void onCauldronLevelChange(CauldronLevelChangeEvent e){
        e.setCancelled(true);
    }

}
