package de.devlen.nichtecro.server.bukkitlobby.menus;

import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.Button;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.ButtonFactory;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.ClickHandler;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.Menu;
import de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager;
import de.devlen.nichtecro.server.bukkitcore.manager.Rank;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemFlag;

import java.util.List;

/**
 * Created by Tarik on 05.06.2016.
 */
public class TagMenu {

    public static void openTag(Player target){
        List<Button> tagButtons = new ExtendedList<>();
        List<Rank> ranks = MPlayerSettingsManager.getRanks(target);
        Rank currentDisguisedRank = MPlayerSettingsManager.getDisguisedRank(target);
        for (int i = 0; i < ranks.size(); i++) {
            final Rank rank = ranks.get(i);
            ButtonFactory buttonFactory = new ButtonFactory()
                    .amount(1)
                    .type(Material.WOOL)
                    .nameNode("bukkit.lobby.tagchanger.items." + rank.name().toLowerCase())
                    .damage(rank.getDamage());
            if (rank.equals(currentDisguisedRank)) {
                buttonFactory.enchant(Enchantment.WATER_WORKER, 1)
                        .flag(ItemFlag.HIDE_ENCHANTS)
                        .loreNode("bukkit.lobby.tagchanger.items.chosen.disguise");
            }

            Button button = new Button()
                    .click(new ClickHandler() {
                        @Override
                        public void handle(InventoryClickEvent e) {
                            Player target = (Player) e.getWhoClicked();
                            target.performCommand("tag " + Chat.normalizeEnumValueName(rank));
                            target.closeInventory();
                        }
                    })
                    .factory(buttonFactory)
                    .id("bukkit.lobby.tagchanger." + rank.name().toLowerCase())
                    .slot(i);
            tagButtons.add(button);
        }

        Menu menu = new Menu()
                .buttons(tagButtons.toArray(new Button[0]))
                .lines(2)
                .titleNode("bukkit.lobby.tagchanger.inventory.title")
                .type(InventoryType.CHEST);
        menu.openMenu(target);
    }

}
