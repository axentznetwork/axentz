package de.devlen.nichtecro.server.bukkitlobby.listeners;

import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import de.devlen.nichtecro.server.bukkitlobby.gadgets.GadgetManager;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Tarik on 16.05.2016.
 */
public class PlayerQuitListener extends ListenerHelper {

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        GadgetManager.cleanGadgets(player);
        if(BlockPlaceListener.placedTntBlocks.containsKey(player.getName())){
            for(Location location : BlockPlaceListener.placedTntBlocks.get(player.getName())){
                location.getBlock().setType(Material.AIR);
            }
            BlockPlaceListener.placedTntBlocks.remove(player.getName());
        }
    }

}
