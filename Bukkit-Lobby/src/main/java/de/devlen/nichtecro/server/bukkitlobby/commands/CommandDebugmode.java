package de.devlen.nichtecro.server.bukkitlobby.commands;

import de.devlen.nichtecro.server.bukkitcore.comfy.CommandListener;
import de.devlen.nichtecro.server.bukkitcore.comfy.annotation.CommandHandler;
import de.devlen.nichtecro.server.bukkitcore.comfy.bukkit.BukkitCommandContext;
import de.devlen.nichtecro.server.bukkitlobby.menus.DebugMenu;
import org.bukkit.entity.Player;

/**
 * Created by Tarik on 15.06.2016.
 */
public class CommandDebugmode implements CommandListener {

    @CommandHandler("command.debugmode")
    public void onCommandDebugmode(BukkitCommandContext c) {
        if(c.getSender().isPlayer()){
            Player player = (Player) c.getSender().getSender();
            DebugMenu.openDebug(player);
        }
    }

}
