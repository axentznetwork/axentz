package de.devlen.nichtecro.server.bukkitlobby.gadgets;

import com.comphenix.protocol.wrappers.EnumWrappers;
import com.google.common.collect.Maps;
import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import de.devlen.nichtecro.server.bukkitcore.particle.ParticleSender;
import de.devlen.nichtecro.server.bukkitlobby.BukkitLobby;
import de.devlen.nichtecro.server.bukkitlobby.inventory.StackManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.HashMap;

/**
 * Created by Tarik on 01.05.2016.
 */
public class BootsGadgets extends ListenerHelper {

    private static HashMap<String, ShoeType> stringShoeTypeHashMap = Maps.newHashMap();
    private static boolean updateTaskRunning = false;

    public static void armGadget(Player player, ShoeType shoeType) {
        cleanGadget(player);
        PlayerInventory playerInventory = player.getInventory();
        switch (shoeType) {
            case JETPACK:
                stringShoeTypeHashMap.put(player.getName(), ShoeType.JETPACK);
                playerInventory.setChestplate(StackManager.Gadgets.Armed.getJetpackChestplateStack());
                playerInventory.setBoots(StackManager.Gadgets.Armed.getJetpackBootsStack());
                break;
            case LOVE:
                stringShoeTypeHashMap.put(player.getName(), ShoeType.LOVE);
                playerInventory.setBoots(StackManager.Gadgets.Armed.getLoveBootsStack());
                break;
            case KNOWLEDGE:
                stringShoeTypeHashMap.put(player.getName(), ShoeType.KNOWLEDGE);
                playerInventory.setBoots(StackManager.Gadgets.Armed.getKnowledgeBootsStack());
                break;
            case GHOST:
                stringShoeTypeHashMap.put(player.getName(), ShoeType.GHOST);
                playerInventory.setBoots(StackManager.Gadgets.Armed.getGhostBootsStack());
                player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 100000000, 1, false, false), true);
                break;
            case SEVEN_MILES_BOOTS:
                stringShoeTypeHashMap.put(player.getName(), ShoeType.SEVEN_MILES_BOOTS);
                playerInventory.setBoots(StackManager.Gadgets.Armed.getSevenMilesBootsStack());
                player.setWalkSpeed(0.6F);
                break;
            case HAPPYNESS:
                stringShoeTypeHashMap.put(player.getName(), ShoeType.HAPPYNESS);
                playerInventory.setBoots(StackManager.Gadgets.Armed.getHappynessBootsStack());
                break;
            case ENDER:
                stringShoeTypeHashMap.put(player.getName(), ShoeType.ENDER);
                playerInventory.setBoots(StackManager.Gadgets.Armed.getEnderBootsStack());
                break;
            case FIRE:
                stringShoeTypeHashMap.put(player.getName(), ShoeType.FIRE);
                playerInventory.setBoots(StackManager.Gadgets.Armed.getFireBootsStack());
                break;
            default:
                break;
        }
    }

    public static void cleanGadget(Player player) {
        if (stringShoeTypeHashMap.containsKey(player.getName())) {
            PlayerInventory playerInventory = player.getInventory();
            switch (stringShoeTypeHashMap.get(player.getName())) {
                case SEVEN_MILES_BOOTS:
                    player.setWalkSpeed(0.2F);
                    break;
                case GHOST:
                    player.removePotionEffect(PotionEffectType.INVISIBILITY);
                    break;
            }
            playerInventory.setChestplate(null);
            playerInventory.setBoots(null);
            stringShoeTypeHashMap.remove(player.getName());
        }
    }

    public enum ShoeType {
        JETPACK,
        LOVE,
        KNOWLEDGE,
        GHOST,
        SEVEN_MILES_BOOTS,
        HAPPYNESS,
        ENDER,
        FIRE
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        Player player = e.getPlayer();
        if (stringShoeTypeHashMap.containsKey(player.getName())) {
            ShoeType shoeType = stringShoeTypeHashMap.get(player.getName());
            switch (shoeType) {
                case JETPACK:
                    if (player.isSneaking()) {
                        for (Player target : Bukkit.getOnlinePlayers()) {
                            ParticleSender.send(target, EnumWrappers.Particle.FLAME, player.getLocation(), new Vector(0.2, 0.3, 0.7), 0, 15);
                            ParticleSender.send(target, EnumWrappers.Particle.CLOUD, player.getLocation(), new Vector(0.7, 0.3, 0.7), 0, 30);
                        }
                    }
                    break;
                case LOVE:
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        ParticleSender.send(target, EnumWrappers.Particle.HEART, player.getLocation(), new Vector(0.7, 0, 0.7), 0, 5);
                    }
                    break;
                case KNOWLEDGE:
                    Location location = player.getLocation();
                    location.setY(location.getY() + 2.0D);
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        ParticleSender.send(target, EnumWrappers.Particle.ENCHANTMENT_TABLE, location, new Vector(1, 0, 1), 1, 30);
                    }
                    break;
                case HAPPYNESS:
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        ParticleSender.send(target, EnumWrappers.Particle.VILLAGER_HAPPY, player.getLocation(), new Vector(0.7, 0, 0.7), 0, 15);
                    }
                    break;
                case ENDER:
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        ParticleSender.send(target, EnumWrappers.Particle.SPELL_WITCH, player.getLocation(), new Vector(0.7, 0, 0.7), 0, 15);
                    }
                    break;
                case FIRE:
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        ParticleSender.send(target, EnumWrappers.Particle.FLAME, player.getLocation(), new Vector(0.7, 0, 0.7), 0, 15);
                    }
                    break;
            }
        }
    }

    public static boolean isUpdateTaskRunning() {
        return updateTaskRunning;
    }

    public static void runUpdateTask() {
        if (!isUpdateTaskRunning()) {
            Bukkit.getScheduler().runTaskTimerAsynchronously(BukkitLobby.getInstance(), new Runnable() {
                @Override
                public void run() {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        if (stringShoeTypeHashMap.containsKey(player.getName())) {
                            ShoeType shoeType = stringShoeTypeHashMap.get(player.getName());
                            switch (shoeType) {
                                case JETPACK:
                                    if (player.isSneaking()) {
                                        Vector vector = player.getLocation().getDirection();
                                        player.setVelocity(vector.multiply(2.5));
                                        if (!player.isGliding()) {
                                            player.setGliding(true);
                                        }
                                        player.playSound(player.getLocation(), Sound.ENTITY_CREEPER_HURT, 1, 2);
                                    }
                                    break;
                            }
                        }
                    }
                }
            }, 0, 1);
        }
    }
}
