package de.devlen.nichtecro.server.bukkitlobby.inventory;

/**
 * Created by Tarik on 04.04.2016.
 */
public class InventoryPosition {

    private int x;
    private int y;

    public InventoryPosition(final int x, final int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return this.x;
    }

    public void setX(final int x) {
        this.x = x;
    }

    public int getY() {
        return this.y;
    }

    public void setY(final int y) {
        this.y = y;
    }

    public int toSlot() {
        int line = this.y - 1;
        line *= 9;
        return line + this.x - 1;
    }

    public static InventoryPosition fromSlot(int slot) {
        int toSubstract;
        for (toSubstract = 0; slot % 9 != 0; --slot, ++toSubstract) {
        }
        final int y = slot / 9 + 1;
        final int x = toSubstract + 1;
        return new InventoryPosition(x, y);
    }

}
