package de.devlen.nichtecro.server.bukkitlobby.menus;

import de.devlen.nichtecro.server.bukkitcore.items.reloaded.Button;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.ButtonFactory;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.ClickHandler;
import de.devlen.nichtecro.server.bukkitcore.items.reloaded.Menu;
import de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;

/**
 * Created by Tarik on 07.06.2016.
 */
public class ShopDialogMenu {

    public static void openShopDialog(final Player player, ButtonFactory shopItemButtonFactory, final int dyeCost, String rewardNode, final ClickHandler confirmHandler) {
        ButtonFactory sellingItemButtonFactory = new ButtonFactory()
                .nameNode(shopItemButtonFactory.getTranslatableName(), shopItemButtonFactory.getNameVariables())
                .damage(shopItemButtonFactory.getDamage())
                .skin(shopItemButtonFactory.getSkin())
                .leatherColor(shopItemButtonFactory.getColor())
                .type(shopItemButtonFactory.getMaterial())
                .loreNode("bukkit.lobby.shop.purchase.cost", String.valueOf(dyeCost))
                .loreNode(rewardNode);

        ButtonFactory confirmPurchaseButtonFactory = new ButtonFactory()
                .nameNode("bukkit.lobby.shop.purchase.confirm.name")
                .type(Material.EMERALD_BLOCK);

        ButtonFactory abortPurchaseButtonFactory = new ButtonFactory()
                .nameNode("bukkit.lobby.shop.purchase.abort.name")
                .type(Material.REDSTONE_BLOCK);

        Button sellingItemButton = new Button()
                .id("bukkit.lobby.shop.purchase.selling.item")
                .click(new ClickHandler() {
                    @Override
                    public void handle(InventoryClickEvent e) {
                        Player target = (Player) e.getWhoClicked();
                        target.playSound(target.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 0);
                    }
                })
                .factory(sellingItemButtonFactory)
                .slot(2);

        Button confirmPurchaseButton = new Button()
                .id("bukkit.lobby.shop.purchase.confirm")
                .click(new ClickHandler() {
                    @Override
                    public void handle(InventoryClickEvent e) {
                        Player target = (Player) e.getWhoClicked();
                        if (MPlayerSettingsManager.debitDyes(target, dyeCost)) {
                            target.playSound(target.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                            Chat.sendTl(target, "bukit.lobby.shop.tag", "bukkit.lobby.shop.purchase.successful");
                            confirmHandler.handle(e);
                            target.closeInventory();
                        } else {
                            Chat.sendTl(target, "bukit.lobby.shop.tag", "bukkit.lobby.shop.purchase.not.enough.money");
                            target.playSound(target.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 0);
                            target.closeInventory();
                        }
                    }
                })
                .factory(confirmPurchaseButtonFactory)
                .slot(0);

        Button abortPurchaseButton = new Button()
                .id("bukkit.lobby.shop.purchase.abort")
                .click(new ClickHandler() {
                    @Override
                    public void handle(InventoryClickEvent e) {
                        Player target = (Player) e.getWhoClicked();
                        target.playSound(target.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                        Chat.sendTl(target, "bukit.lobby.shop.tag", "bukkit.lobby.shop.purchase.aborted");
                        target.closeInventory();
                    }
                })
                .factory(abortPurchaseButtonFactory)
                .slot(4);

        Menu shopDialogMenu = new Menu()
                .type(InventoryType.HOPPER)
                .titleNode("bukkit.lobby.shop.purchase.inventory.name")
                .buttons(confirmPurchaseButton, sellingItemButton, abortPurchaseButton);

        shopDialogMenu.openMenu(player);
    }

}
