package de.devlen.nichtecro.server.bukkitlobby.listeners;

import de.devlen.nichtecro.server.bukkitcore.events.TranslatedPlayerChatEvent;
import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import de.devlen.nichtecro.server.bukkitcore.manager.ChatVisibilityType;
import de.devlen.nichtecro.server.bukkitcore.manager.MPlayerSettingsManager;
import de.devlen.nichtecro.server.bukkitcore.message.Chat;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by Tarik on 04.06.2016.
 */
public class TranslatedPlayerChatListener extends ListenerHelper {

    @EventHandler
    public void onTranslatedPlayerChat(TranslatedPlayerChatEvent e) {
        Set<Player> playerSet = e.getParticipants();
        Player player = e.getPlayer();
        ChatVisibilityType chatVisibilityType = MPlayerSettingsManager.getChatVisibilityType(player);

        switch (chatVisibilityType) {
            case ALL:
            case PREMIUM:
                Iterator<Player> iterator = playerSet.iterator();
                while (iterator.hasNext()) {
                    Player target = iterator.next();
                    switch (MPlayerSettingsManager.getChatVisibilityType(target)) {
                        case PREMIUM:
                            if (!player.hasPermission("bukkit.core.rank.premium") && !player.hasPermission("bukkit.lobby.chat.bypass.mode")) {
                                iterator.remove();
                            }
                            break;
                        case NONE:
                            if (!player.hasPermission("bukkit.lobby.chat.bypass.mode")) {
                                iterator.remove();
                            }
                    }
                }
                break;
            case NONE:
                Chat.sendTl(player, "bukkit.lobby.chat.tag", "bukkit.lobby.settings.chatvisiblity.need.change");
                break;
        }
    }
}
