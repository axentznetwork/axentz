package de.devlen.nichtecro.server.bukkitlobby.inventory;

import org.bukkit.event.inventory.InventoryCloseEvent;

/**
 * Created by Tarik on 03.04.2016.
 */
public interface CloseHandler {

    void handle(InventoryCloseEvent e);

}