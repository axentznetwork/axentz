package de.devlen.nichtecro.server.bukkitlobby.listeners;

import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;

/**
 * Created by Tarik on 16.05.2016.
 */
public class PlayerDeathListener extends ListenerHelper {

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        e.setKeepInventory(true);
        e.setKeepLevel(true);
        e.setDeathMessage(null);
    }

}
