package de.devlen.nichtecro.server.bukkitlobby.inventory;

import de.devlen.nichtecro.server.bukkitcore.data.ExtendedList;
import de.devlen.nichtecro.server.bukkitcore.listeners.ListenerHelper;
import de.devlen.nichtecro.server.bukkitcore.validation.Validate;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;

import java.util.List;

/**
 * Created by Tarik on 03.04.2016.
 */
public class InventoryFactory extends ListenerHelper {

    private String inventoryName = ""; //max length 30
    private int lines = 0;
    private CloseHandler closeHandler = null;
    private OpenHandler openHandler = null;
    private List<InventoryButton> inventoryButtons = new ExtendedList<>();

    public InventoryFactory(String inventoryName, int lines) {
        this.inventoryName = inventoryName;
        this.lines = lines;
    }

    public InventoryFactory(String inventoryName, int lines, CloseHandler closeHandler) {
        this.inventoryName = inventoryName;
        this.lines = lines;
        this.closeHandler = closeHandler;
    }

    public InventoryFactory(String inventoryName, int lines, OpenHandler openHandler) {
        this.inventoryName = inventoryName;
        this.lines = lines;
        this.openHandler = openHandler;
    }

    public InventoryFactory(String inventoryName, int lines, InventoryButton... inventoryButtons) {
        this.inventoryName = inventoryName;
        this.lines = lines;
        this.inventoryButtons = new ExtendedList<>(inventoryButtons);
    }

    public InventoryFactory(String inventoryName, int lines, CloseHandler closeHandler, OpenHandler openHandler) {
        this.inventoryName = inventoryName;
        this.lines = lines;
        this.closeHandler = closeHandler;
        this.openHandler = openHandler;
    }

    public InventoryFactory(String inventoryName, int lines, CloseHandler closeHandler, InventoryButton... inventoryButtons) {
        this.inventoryName = inventoryName;
        this.lines = lines;
        this.closeHandler = closeHandler;
        this.inventoryButtons = new ExtendedList<>(inventoryButtons);
    }

    public InventoryFactory(String inventoryName, int lines, OpenHandler openHandler, InventoryButton... inventoryButtons) {
        this.inventoryName = inventoryName;
        this.inventoryButtons = new ExtendedList<>(inventoryButtons);
        this.openHandler = openHandler;
        this.lines = lines;
    }

    public InventoryFactory(String inventoryName, int lines, CloseHandler closeHandler, OpenHandler openHandler, InventoryButton... inventoryButtons) {
        this.inventoryName = inventoryName;
        this.lines = lines;
        this.closeHandler = closeHandler;
        this.openHandler = openHandler;
        this.inventoryButtons = new ExtendedList<>(inventoryButtons);
    }

    public InventoryFactory fill(InventoryFilling inventoryFilling) {
        for (InventoryPosition inventoryPosition : inventoryFilling.getInventoryPositions()) {
            InventoryButton inventoryButton = inventoryFilling.getInventoryButton();
            inventoryButtons.add(inventoryButton.position(inventoryPosition.getX(), inventoryPosition.getY()));
        }
        return this;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    public InventoryFactory inventoryName(String inventoryName) {
        setInventoryName(inventoryName);
        return this;
    }

    public int getLines() {
        return lines;
    }

    public void setLines(int lines) {
        this.lines = lines;
    }

    public InventoryFactory lines(int lines){
        setLines(lines);
        return this;
    }

    public CloseHandler getCloseHandler() {
        return closeHandler;
    }

    public void setCloseHandler(CloseHandler closeHandler) {
        this.closeHandler = closeHandler;
    }

    public InventoryFactory closeHandler(CloseHandler closeHandler){
        setCloseHandler(closeHandler);
        return this;
    }

    public OpenHandler getOpenHandler() {
        return openHandler;
    }

    public void setOpenHandler(OpenHandler openHandler) {
        this.openHandler = openHandler;
    }

    public InventoryFactory openHandler(OpenHandler openHandler){
        setOpenHandler(openHandler);
        return this;
    }

    public List<InventoryButton> getInventoryButtons() {
        return inventoryButtons;
    }

    public void setInventoryButtons(List<InventoryButton> inventoryButtons) {
        this.inventoryButtons = inventoryButtons;
    }

    public void addInventoryButtons(InventoryButton... inventoryButtons) {
        this.inventoryButtons.addAll(new ExtendedList<>(inventoryButtons));
    }

    public void open(Player extendedPlayer) {
        Inventory inventory = Bukkit.createInventory(null, lines * 9, (inventoryName.length() > 30 ? inventoryName.substring(0, 30) : inventoryName));
        for (InventoryButton inventoryButton : inventoryButtons) {
            inventory.setItem(inventoryButton.getInventoryPosition().toSlot(), inventoryButton.getItemStack());
        }
        extendedPlayer.openInventory(inventory);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        if (e.getInventory().getTitle().equalsIgnoreCase(inventoryName)) {
            for (InventoryButton inventoryButton : inventoryButtons) {
                if (e.getSlot() == inventoryButton.getInventoryPosition().toSlot()) {
                    if (Validate.isSimilarItem(e.getCurrentItem(), inventoryButton.getItemStack())) {
                        e.setCancelled(true);
                        inventoryButton.getClickHandler().handle(e);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onInventoryOpen(InventoryOpenEvent e) {
        if (getOpenHandler() != null) {
            getOpenHandler().handle(e);
        }
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent e) {
        if (getCloseHandler() != null) {
            getCloseHandler().handle(e);
        }
    }
}
